import ModalWindows from "../guiComponent/modalWindow";
import { ansiToHtmlConverter, fetchW } from "../utils/misc";
import Parameters from "./ParametersManager";
import loggingManager from "./LoggingManager";
import { quickToast, toastr } from "../guiComponent/notification";
import cppErrorManager from "./CppErrorManager";

//const ansiToHtmlConverter = new ansiToHtml({ colors: { 0: '#222222', 1: '#fc618d', 2: '#7bd88f', 11: '#fd9353', 5: '#5ad4e6', 4: '#948ae3', 6: '#847ac3', 7: '#AAA', 8: '#888', 9: '#fd698f', 10: '#7edf9f', 3: '#fce566', 12: '#9f8fef', 13: '#9f6fef', 14: '#5fdfef', 15: '#F7F1FF' } });

//const ansiToHtmlConverter = new ansiToHtml();
function term2html(msg,debug,finalJson){
    if(debug){
        const htmlCode = ansiToHtmlConverter.toHtml(msg.replaceAll("<", "&lt;").replaceAll(">", "&gt;"))
        builderManager._errModel.contentDivInfo.innerHTML =  '<code style="white-space: pre;">' + htmlCode + "</code>"
        return
    }
    cppErrorManager.update(msg,builderManager._errModel,finalJson)
}
/**
 * Singleton class listing and managing all the types.
 * It also contains the type manager modal window
 */
class BuilderManager {

    // Singleton initialisation
    constructor() { 
        let d = document.createElement('div')
        this._errModel = new ModalWindows('Compilation Error',d);
        this._errModel.contentDivInfo = d
    }

    get errModel(){
        return this._errModel
    }

    build(type,token,compiledBy,finalJson,callback,params){
        console.log(loggingManager.username,'==',compiledBy)
        token = loggingManager.username===compiledBy?token:'noToken'
        /*
        const loader = getLoader()
        loader.setText('Compiling. This can take several minutes.');
        loader.show();*/
        fetchW('/api/compile', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ data: finalJson, token, type,params })
        }).then(res => res.json()
        ).then((data) => {
            //loader.hide();
            if(type === 'debug') {
                term2html(data.error+"\n\n"+data.gdb,true,finalJson);
                if (callback) callback(true, data.token)
                const elem = toastr.error('<b>click here for show error messages</b>','Compilation Error',{timeOut:0,onclick:false,tapToDismiss:false})
                elem.addEventListener('click',(e)=>{
                    e.preventDefault();
                    e.stopPropagation();
                    this.errModel.show()
                })
            } else if (data.error !== undefined) {
                term2html(data.error,false,finalJson);
                this.errModel.show();
                if (callback) callback(true, data.token)
                const elem = toastr.error('<b>click here for show error messages</b>','Compilation Error',{timeOut:0,onclick:false,tapToDismiss:false})
                elem.addEventListener('click',(e)=>{
                    e.preventDefault();
                    e.stopPropagation();
                    this.errModel.show()
                })
            }
            else {
                if (callback) callback(false, data.token)
                quickToast.fire({icon:'success',title: 'Compilation Success'})

            }
            Parameters.dateLastChange = Date.now();
        }).catch((err) => { /*loader.hide();*/ console.error(err); if (callback) callback(true) });
    }

    isFailToBuild(){
        return this.failToBuild;
    }
}

const builderManager = new BuilderManager();
export default builderManager;