import { callBackCheckIntegrity } from "../utils/utilsVaraibleName";
import Parameters from "./ParametersManager";
import FunctionManager from "./functionManager";


function capitalizeFirst(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}
/**
 * class listing and managing all the types.
 * It also contains the type manager modal window
 */
class CppTypeManager {

    constructor(){ }

    /**
     * This function initialises the singleton
     * using data
     * @param {*} data The json config listing everything
     */
    init(mainContainer,defaultData){
        this.defaultTypes = defaultData.Default.defaultTypes
        this.updater = []

        this.container = document.createElement('div')
        this.container.id = 'CppTypeManager'
        this.container.style= "width:100%;height:100%"

        mainContainer.appendChild(this.container)
        this.container.style.display = 'none'

        this.customList = document.createElement('custom-list')
        this.container.appendChild(this.customList)


        this.itemTypeUserBuilder = (type) => {
            var itemType = document.createElement('custom-item')
            itemType.editable = (itemEdited, newType) => {
                if (!itemEdited.valueText){
                    document.getElementById('Cell Attributes').items.forEach((item) => { if (item.type === itemEdited.valueText) item.type = newType })
                    document.getElementById('Scenario Attributes').items.forEach((item) => { if (item.type === itemEdited.valueText) item.type = newType })
                }
            }
            itemType.deletable = true
            if (type) itemType.valueText = type
            return itemType
        }

        this.typeSection = this.customList.addSection("C++ types", {
            addable: () => this.itemTypeUserBuilder()
        })
        this.typeSection.classList.add('title')
        this.typeSection.style.color = '#c44'

        this.defaultTypes.forEach((type) => {
            var itemType = document.createElement('custom-item')
            this.typeSection.addItem(itemType)
            itemType.taggable = true
            itemType.addTag('CORE')
            itemType.valueText = type
         })


        this.secEnumBuilder = (enumName) => {
            var itemEnum = document.createElement('custom-section')
            itemEnum.editable = (itemEdited, newType) => {
                if (!itemEdited.titleText) {
                    document.getElementById('Cell Attributes').items.forEach((item) => { if (item.type === itemEdited.titleText) item.type = newType })
                    document.getElementById('Scenario Attributes').items.forEach((item) => { if (item.type === itemEdited.titleText) item.type = newType })
                    Parameters.changeDetected()
                }
            }
            itemEnum.intergrityCheck = (item, prevVal) => { const newVal = capitalizeFirst(callBackCheckIntegrity(item, prevVal)); FunctionManager.refactorCode(item.valueText, newVal, 'cellFunction'); return newVal }
            itemEnum.toggleable = true
            itemEnum.deletable = true
            itemEnum._header.style.color = '#c44'
            itemEnum.addable = (name) => {
                let i = document.createElement('custom-item')
                if(name)i.valueText = name
                i.editable = () => { Parameters.changeDetected() }
                i.intergrityCheck = (item, prevVal) => { const newVal = capitalizeFirst(callBackCheckIntegrity(item, prevVal)); FunctionManager.refactorCode(item.valueText, newVal, 'cellFunction'); return newVal }
                i.deletable = true
                return i
            }
            if (enumName) itemEnum.titleText = enumName
            return itemEnum
        }

        this.enumSection = this.customList.addSection("C++ Enum", {
            addable: () => this.secEnumBuilder()
        })
        this.enumSection.classList.add('title')
    }

    /**
     * Returns the list of available types
     */
    getUserTypes() {
        return this.typeSection.items.filter((item) => !item.hasTag('CORE')).map((item) => item.valueText)
    }

    getCoreTypes() {
        return this.typeSection.items.filter((item) => item.hasTag('CORE')).map((item) => item.valueText)
    }

    getAllTypes() {
        return [...this.typeSection.items.map((item) => item.valueText), ...this.enumSection.items.map((item) => item.titleText)]
    }

    getEnums() {
        return Object.fromEntries(this.enumSection.items.map((item) => [item.titleText,item.items.map((item) => item.valueText)]))
    }
    /**
     * Returns the list of default types
     */
    getDefaultTypes(){
        return this.defaultTypes;
    }
    
    /**
     * Replaces all available types in the manager
     * @param {*} types List of type names
     */
    importTypes(types) {
        //this.typeSection.items.filter((item) => item.hasTag('CORE')).forEach((item)=>item.delete())
        types.forEach((type) => this.typeSection.addItem(this.itemTypeUserBuilder(type)))
    }

    importEnums(enums) {
        Object.keys(enums).forEach((enumName) => {
            let enumItem = this.secEnumBuilder(enumName)
            this.enumSection.addItem(enumItem)
            enums[enumName].forEach((value) => enumItem.addItem(enumItem.itemBuilder(value)))
        })
    }

    resetTypes() {
        this.typeSection.items.forEach(i=>{if(!i.hasTag('CORE'))i.delete()})
        this.enumSection.clear()
    }

    /**
     * Add a function call the the updater
     * @param {*} fun The function to add to the updater
     */
    addUpdater(fun){
        this.updater.push(fun)
    }
    /** Calls all functions in the updater */
    update(){
        let listeType = []
        for(var fun of this.updater){
            fun(listeType)
        }
    }

    /**
     * Calls the updater
     */
    updateTypes(){
        this.update()
    }
}

const cppTypeManager = new CppTypeManager();
export default cppTypeManager;