import CodeEditorProvider from '../guiComponent/codeEditorProvider'
import getFinalJson from '../inputs_outputs/getFinalJson';
import { toCamelCase, toSnakeCase, getNewID } from '../utils/utilsVaraibleName'
import Parameters from './ParametersManager';
import FunctionManager from './functionManager';



function createHeader(){
    var template = document.createElement('template');
    let html = `<div style="background-color:var(--bg-4);height:35px;display: flex;padding: 5px 5px;gap: 10px;">
                <span style="width:100%"><select style="height:20px;padding: 3px;padding-left: 10px;border: 1px;width: 100%;background-color: var(--bg-input);color: var(--text);-webkit-appearance: none;"><option value="-1">choose version</option></select></span>
                <span style="background-color: var(--theme-color);color: #fff;border-radius: 5px;height:20px"><a style="font-family: Helvetica, sans-serif;cursor:pointer;display:block;width:28px;height:15px;margin: 2px 0 0 5px;height: 23px;border-radius: 5px;margin-top: -1px;">[ | ]</a></span>
            </div>`.replace('\n', '').trim();
    template.innerHTML = html;
    let dom = template.content.firstChild;
    let select = dom.querySelector('select')
    dom.refreshListBuild = ()=>{
        select.innerHTML = ''
        const versions = Parameters.getCurrentItemBuild().parent.items
        select.add(new Option("choose version", -1))
        for (let i=0; i<versions.length;i++) {
            //if(versions[i]!==currentVersion) 
            select.add(new Option(versions[i].titleText, i))
        }
    }
    const onChange = (i) => {
        if(i==-1){
            diffManager.clear()
        } else {
            let currentVersion = Parameters.getCurrentItemBuild()
            let versions = currentVersion.parent.items
            diffManager.diffOnFinalJson(getFinalJson(),versions[i].finalJson)
        }
    }
    dom.changeDiff = (i)=>{
        if(i===undefined) i=select.value
        dom.refreshListBuild()
        select.value=i
        onChange(i)
    }
    select.addEventListener('focus',dom.refreshListBuild)
    select.addEventListener('change', (e)=>onChange(e.target.value))
    let renderSideBySide = false
    let btnSideBySide = dom.querySelector('a')
    btnSideBySide.addEventListener('click', (e) => {
        renderSideBySide = !renderSideBySide;
        btnSideBySide.innerHTML = renderSideBySide?'[&nbsp;&nbsp;&nbsp;]':'[ | ]'
        CodeEditorProvider.getEditor('diffEditor').updateOptions({renderSideBySide})
    })
    return dom;
}

class DiffManager {

    constructor(){  }

    init(mainContainer){

        this.codeEditor = document.createElement('div')
        this.codeEditor.id = 'diffEditor'
        this.codeEditor.style = "height:100%; width:100%"
        this.codeEditor.style.display = 'none'
        document.getElementById('contentView').appendChild(this.codeEditor)
        CodeEditorProvider.createEditor('diffEditor', {
            lineNumbers: 'on', minimap:true, diff:true,readOnly: true,renderSideBySide: false
        });

        this.currentFile = ''

        this.container = document.createElement('div')
        this.container.id = 'diffActivity'
        this.container.style = "height:100%; width:100%"

        mainContainer.appendChild(this.container)
        this.domHeader = createHeader()
        this.container.appendChild(this.domHeader)

        this.customList = document.createElement('custom-list')
        this.container.appendChild(this.customList)
        /*
        this.customList.updateType(['','Body','Plugin'])
        this.customList.regexCharacterUnaccepted = /[]/g
*/

    }

    diff(i){
        this.domHeader.changeDiff(i)
    }

    diffOnFinalJson(current, old){
        this.clear()
        let compareDiff = (key1,value1,key2,value2)=>{
            let ret = {'added':{},'removed':{},'modified':{}}
            ret.added = Object.fromEntries(key1.reduce((l,k,i)=>!key2.includes(k)?[...l,[k,{new:value1[i],old:''}]]:l,[]))
            ret.removed = Object.fromEntries(key2.reduce((l,k,i)=>!key1.includes(k)?[...l,[k,{new:'',old:value2[i]}]]:l,[]))
            ret.modified = Object.fromEntries(key1.reduce((l,k,i)=>{
                if(key2.includes(k)){
                    const i2 = key2.indexOf(k)
                    return value1[i]!==value2[i2]?[...l,[k,{new:value1[i],old:value2[i2]}]]:l
                } else return l
            },[]))
            return ret
        }
        let diffJson = {'Cell':{},'Scenario':{}}

        let toDelete = (o)=>Object.keys(o.added).length+Object.keys(o.modified).length+Object.keys(o.removed).length===0
        //STATES
        //ajouter initializationCode
        diffJson['Cell']['States'] = compareDiff(['cell Instantiation',...current.cell.States.map(o=>o.name)],
                                                [current.cell.initializationCode,...current.cell.States.map(o=>o.code)],
                                                ['cell Instantiation',...old.cell.States.map(o=>o.name)],
                                                [current.cell.initializationCode,...old.cell.States.map(o=>o.code)])
        if(toDelete(diffJson['Cell']['States'])) delete diffJson['Cell']['States']
        //ajouter initializationTransition
        diffJson['Cell']['Transitions'] = compareDiff([].concat(...current.cell.initializationTransitions.map(o=>o.destinations.map(o2=>o.from + " -> "+o2.to)),...current.cell.Transitions.map(o=>o.destinations.map(o2=>o.from + " -> "+o2.to))),
                                              [].concat(...current.cell.initializationTransitions.map(o=>o.destinations.map(o2=>o2.condition)),...current.cell.Transitions.map(o=>o.destinations.map(o2=>o2.condition))),
                                              [].concat(...old.cell.initializationTransitions.map(o=>o.destinations.map(o2=>o.from + " -> "+o2.to)),...old.cell.Transitions.map(o=>o.destinations.map(o2=>o.from + " -> "+o2.to))),
                                              [].concat(...old.cell.initializationTransitions.map(o=>o.destinations.map(o2=>o2.condition)),...old.cell.Transitions.map(o=>o.destinations.map(o2=>o2.condition))))
        if(toDelete(diffJson['Cell']['Transitions'])) delete diffJson['Cell']['Transitions']

        let getCodeAttr=(attr)=>[...attr].sort((a,b)=>(a.value>b.value)?1:-1).map(o=>o.type+' '+o.value+';').join('\n')

        diffJson['Cell']['attributes'] = compareDiff(['cell attributes','cell parameters','visibility'],
                                             [getCodeAttr(current.cell.attributes),getCodeAttr(current.paramInputs.Cell),getCodeAttr(current.CellAttributesRecorded)],
                                             ['cell attributes','cell parameters','visibility'],
                                             [getCodeAttr(old.cell.attributes),getCodeAttr(old.paramInputs.Cell),getCodeAttr(old.CellAttributesRecorded)])
        if(toDelete(diffJson['Cell']['attributes'])) delete diffJson['Cell']['attributes']


        const oldCellFunc = Array.isArray(old.cell.functions)?old.cell.functions:Object.values(old.cell.functions)
        const currentCellFunc = Array.isArray(current.cell.functions)?current.cell.functions:Object.values(current.cell.functions)
        diffJson['Cell']['functions'] = compareDiff(currentCellFunc.map(c=>FunctionManager.extractHeaderCppFunction(c).functionName),
                                             currentCellFunc,
                                             oldCellFunc.map(c=>FunctionManager.extractHeaderCppFunction(c).functionName),
                                             oldCellFunc)
        if(toDelete(diffJson['Cell']['functions'])) delete diffJson['Cell']['functions']

        diffJson['Scenario']['attributes'] = compareDiff(['scenario attributes','scenario parameters'],
                                             [getCodeAttr(current.scenario.attributes),getCodeAttr(current.paramInputs.Scenario)],
                                             ['scenario attributes','scenario parameters'],
                                             [getCodeAttr(old.scenario.attributes),getCodeAttr(old.paramInputs.Scenario)])
        if(toDelete(diffJson['Scenario']['attributes'])) delete diffJson['Scenario']['attributes']

        diffJson['Scenario']['codes'] = compareDiff(['initialization','loop'],
                                             [current.scenario.codeInit,current.scenario.codeLoop],
                                             ['initialization','loop'],
                                             [old.scenario.codeInit,old.scenario.codeLoop])
        if(toDelete(diffJson['Scenario']['codes'])) delete diffJson['Scenario']['codes']

        const oldScenarFunc = Array.isArray(old.scenario.functions)?old.scenario.functions:Object.values(old.scenario.functions)
        const currentScenarFunc = Array.isArray(current.scenario.functions)?current.scenario.functions:Object.values(current.scenario.functions)
        diffJson['Scenario']['functions'] = compareDiff(currentScenarFunc.map(c=>FunctionManager.extractHeaderCppFunction(c).functionName),
                                            currentScenarFunc,
                                             oldScenarFunc.map(c=>FunctionManager.extractHeaderCppFunction(c).functionName),
                                             oldScenarFunc)
        if(toDelete(diffJson['Scenario']['functions'])) delete diffJson['Scenario']['functions']
        
        let getCMkey = (finalJson)=>[].concat(...Object.keys(finalJson.customModule).map(k=>Object.keys(finalJson.customModule[k]).map(f=>k+"#_#"+f)))
        let getCMfile = (finalJson)=>[].concat(...Object.keys(finalJson.customModule).map(k=>Object.keys(finalJson.customModule[k]).map(f=>finalJson.customModule[k][f].code)))
        let tmpCMdiff = compareDiff(getCMkey(current),getCMfile(current),getCMkey(old),getCMfile(old))
        let tmpCMKeysDiff = ([].concat(Object.keys(tmpCMdiff.added),Object.keys(tmpCMdiff.removed),Object.keys(tmpCMdiff.modified)))
        diffJson['Custom Module'] = Object.fromEntries([...new Set(tmpCMKeysDiff.map(k=>[k.split('#_#')[0],{'added':{},'removed':{},'modified':{}}]))])

        Object.keys(tmpCMdiff.added).map(k=>[...k.split('#_#'),k]).forEach(k=>{
            diffJson['Custom Module'][k[0]].added[k[1]] = tmpCMdiff.added[k[2]]
        })
        Object.keys(tmpCMdiff.removed).map(k=>[...k.split('#_#'),k]).forEach(k=>{
            diffJson['Custom Module'][k[0]].removed[k[1]] = tmpCMdiff.removed[k[2]]
        })
        Object.keys(tmpCMdiff.modified).map(k=>[...k.split('#_#'),k]).forEach(k=>{
            diffJson['Custom Module'][k[0]].modified[k[1]] = tmpCMdiff.modified[k[2]]
        })
        Object.keys(diffJson['Custom Module']).forEach(k=>{
            if(toDelete(diffJson['Custom Module'][k])) delete diffJson['Custom Module'][k]
        })

        let createItem = (name,values,typeChange)=>{
            let item = document.createElement('custom-item')
            item.valueText = name
            item.classList.add(typeChange)
            item.addEventListener('click', (e) => {
                this.displayCode(item,values.new,values.old)
            })
            return item
        }

        let createTreeExplore = (diffJson,dom)=>{
            if(diffJson.added&&diffJson.removed&&diffJson.modified){
                Object.keys(diffJson.added).forEach(el=>{
                    dom.addItem(createItem(el,diffJson.added[el],'added-diff'))
                })
                Object.keys(diffJson.removed).forEach(el=>{
                    dom.addItem(createItem(el,diffJson.removed[el],'removed-diff'))
                })
                Object.keys(diffJson.modified).forEach(el=>{
                    dom.addItem(createItem(el,diffJson.modified[el],'modified-diff'))
                })
            } else {
                Object.keys(diffJson).forEach(k=>{
                    let sec;
                    if(dom===this.customList){
                        sec = dom.addSection(k,{
                            addable:false,
                            toggleable:true,
                            deletable: false,
                        })
                        sec.classList.add('title')
                    } else {
                        sec = document.createElement('custom-section')
                        sec.addable = false
                        sec.toggleable = true
                        sec.deletable = false
                        sec.titleText = k
                        dom.addItem(sec)
                    }
                    createTreeExplore(diffJson[k],sec)
                })
            }
        }
        createTreeExplore(diffJson,this.customList)
    }


    /**
     * Returns the list of default types
     */
    displayCode(item, newV, oldV) {
        this.codeEditor.style.display = null
        if (this.currentFile!==item){
            if(this.currentFile)this.currentFile.style.backgroundColor = null
            this.currentFile = item
            this.currentFile.style.backgroundColor = "var(--bg-4-hover)"
            CodeEditorProvider.setDiffValue('diffEditor', newV,oldV,'cpp')
        }
    }


    clear(){
        this.currentFile = ''
        this.customList.clear()
        CodeEditorProvider.setDiffValue('diffEditor', '','','cpp')
    }

    onClickActivity(){
        this.diff()
    }

    addUpdater(fun){
        this.updater.push(fun)
    }
    /** Calls all functions in the updater */
    update(){
        
    }

}

const diffManager = new DiffManager();
export default diffManager;