import dat from "../guiComponent/dat.gui";
import { setColorTheme } from "../utils/MonacoUtils";
function getAllCSSVariableNames(){
    const styleSheets = document.styleSheets
    const style = getComputedStyle(document.body)
    const cssVars = {dark:{},light:{}};
    // loop each stylesheet
    for(let i = 0; i < styleSheets.length; i++){
       // loop stylesheet's cssRules
       try{ // try/catch used because 'hasOwnProperty' doesn't work
          for(let j = 0; j < styleSheets[i].cssRules.length; j++){
             try{
                // loop stylesheet's cssRules' style (property names)
                for(let k = 0; k < styleSheets[i].cssRules[j].style.length; k++){
                   const name = styleSheets[i].cssRules[j].style[k];
                   // test name for css variable signiture and uniqueness
                   for(let theme in cssVars)
                   if(name.startsWith(`--${theme}-`)){
                        let nameVariable = name.split(`--${theme}-`)[1]
                        let cat = nameVariable.split('-')[0]
                        nameVariable = nameVariable.split(`${cat}-`)[1]
                        if(!cssVars[theme].hasOwnProperty(cat)) cssVars[theme][cat]={}
                        if(!cssVars[theme][cat].hasOwnProperty(nameVariable)) cssVars[theme][cat][nameVariable] = style.getPropertyValue(name);
                   }
                }
             } catch (error) {}
          }
       } catch (error) {}
    }
    console.log(cssVars)
    return cssVars;
 }

 const unflattenObject = (obj, delimiter = '.') => Object.keys(obj).reduce((res, k) => {
      k.split(delimiter).reduce((acc, e, i, keys) =>acc[e] ||(acc[e] = isNaN(Number(keys[i + 1]))? keys.length - 1 === i? obj[k]: {}: []),res);
      return res;
    }, {});
const flattenObject = (obj, delimiter = '.', prefix = '') => Object.keys(obj).reduce((acc, k) => {
    const pre = prefix.length ? `${prefix}${delimiter}` : '';
    if (typeof obj[k] === 'object' && obj[k] !== null && Object.keys(obj[k]).length > 0)
    Object.assign(acc, flattenObject(obj[k], delimiter, pre + k));
    else acc[pre + k] = obj[k];
    return acc;
}, {});

const leftMerge = (a, b) => Object.keys(a).reduce((r, key) => (r[key] = (key in b ? b : a)[key], r), {});
        
class ThemeManager {

    /**
     * This function initialises the singleton
     * using data
     * @param {*} data The json config listing everything
     */
    init(defaultData) {
        document.querySelector('.header .logo-theme-switch').oncontextmenu = (e)=>{
            e.preventDefault();
            if(this.gui){ 
                this.gui.destroy()
                this.gui = undefined
            } else this.createEditor()
        }
        this.defaultColor = getAllCSSVariableNames()
        this.variable = unflattenObject(leftMerge(flattenObject(this.defaultColor),JSON.parse(localStorage.getItem('themeColor')||'{}')))
        this.applyTheme()
    }

    save(){
        localStorage.setItem('themeColor',JSON.stringify(flattenObject(this.variable)))
    }

    reset(){
        this.variable = JSON.parse(JSON.stringify(this.defaultColor))
        if(this.gui){
            this.gui.destroy()
            this.createEditor()
        }
        this.applyTheme()
    }

    applyTheme(){
        for(let theme in this.variable)
            for(let cat in this.variable[theme])
                for(let vari in this.variable[theme][cat])
                    document.documentElement.style.setProperty(`--${theme}-${cat}-${vari}`, this.variable[theme][cat][vari]);
        setColorTheme('dark',this.variable['dark']['editor'])
        setColorTheme('light',this.variable['light']['editor'])
    }

    createEditor(){
        this.gui = new dat.GUI({width:'300px'});
        this.gui.add(this,'save')
        this.gui.add(this,'reset')
        this.gui.domElement.parentElement.classList.add('themeCustomization')
        for(let theme in this.variable){
            const folder = this.gui.addFolder(theme)
            for(let cat in this.variable[theme]){
                const catFolder = folder.addFolder(cat)
            for(let vari in this.variable[theme][cat])
                try{
                    catFolder.addColor(this.variable[theme][cat],vari).onChange((e) => {
                        document.documentElement.style.setProperty(`--${theme}-${cat}-${vari}`, e);
                        if(cat==='editor') setColorTheme(theme,this.variable[theme][cat])
                    });
                } catch(e){
                    catFolder.add(this.variable[theme][cat],vari).onChange((e) => {
                        document.documentElement.style.setProperty(`--${theme}-${cat}-${vari}`, e);
                        if(cat==='editor') setColorTheme(theme,this.variable[theme][cat])
                    });
                }
            }
        }
    }
}
const themeManager = new ThemeManager();
export default themeManager;