import behaviorDiagram from "../diagrams/behaviorDiagram";
import stateMachine from "../diagrams/stateMachine";
import CodeEditorProvider from "../guiComponent/codeEditorProvider";
import ActivityManager from "./ActivityManager";


function createHeader() {
    var template = document.createElement('template');
    let html = `<div style="background-color:var(--bg-4);height:35px;display: flex;padding: 5px 5px;gap: 10px;">
                <span style="width:100%"><input style="height:20px;padding: 3px;padding-left: 15px;border: 0px;width: 100%;background-color: var(--bg-input);color: var(--text);-webkit-appearance: none;" type="text" placeholder="Search"></span>
                <span style="background-color: var(--theme-color);color: #fff;border-radius: 5px;height:20px"><a style="font-family: Helvetica, sans-serif;cursor:pointer;display:block;width:15px;height:15px;margin: 2px 0 0 5px;height: 20px;border-radius: 5px;margin-top: 1px;-webkit-transform: rotate(45deg); -moz-transform: rotate(45deg); -o-transform: rotate(45deg);transform: rotate(45deg);">&#9906;</a></span>
            </div>`.replace('\n', '').trim();
    template.innerHTML = html;
    let dom = template.content.firstChild;
    let input = dom.querySelector('input')
    dom.search = (value)=>{
        input.value = value
        searchManager.filterItems(value)
    }
    input.addEventListener('keyup',()=>{
        searchManager.filterItems(input.value)
    })/*
    dom.querySelector('a').addEventListener('click', (e) => {
        let moduleName = toCamelCase(input.value)
        input.value = ''
        if (moduleName !== '') {
            moduleName = moduleName.charAt(0).toUpperCase() + moduleName.slice(1)
            const moduleSection = CustomModulesManager.newModule(moduleName)
            moduleSection.addItem(CustomModulesManager.newFile(moduleName, 'Body', 'Body' + moduleName + '.hpp', bodyTemplate(moduleName)));
            moduleSection.addItem(CustomModulesManager.newFile(moduleName, 'Plugin', 'Plugin' + moduleName + '.hpp', templatePlugin(moduleName)));
        }
    })*/
    return dom;
}
/**
 * Singleton class listing and managing all the types.
 * It also contains the type manager modal window
 */
class SearchManager {

    // Singleton initialisation
    constructor() {  }

    /**
     * This function initialises the singleton
     * using data
     * @param {*} data The json config listing everything
     */
    init(mainContainer) {
        this.updater = []

        this.container = document.createElement('div')
        this.container.id = 'SearchManager'
        this.container.style.width = '100%'
        this.container.style.height = '100%'

        mainContainer.appendChild(this.container)
        this.container.style.display = 'none'

        this.domHeader = createHeader()
        this.container.appendChild(this.domHeader)

        this.customList = document.createElement('custom-list')
        this.container.appendChild(this.customList)
        this.customList.style.setProperty('height', 'calc(100% - 35px)');


        this.section = {}
        this.section['cell call'] = this.customList.addSection("call's cell", { toggleable: true })
        this.section['cell call'].classList.add('title')
        this.section['cell transition'] = this.customList.addSection("transition cell", { toggleable: true, })
        this.section['cell transition'].classList.add('title')

        this.section['protocol call'] = this.customList.addSection("call's protocol", { toggleable: true })
        this.section['protocol call'].classList.add('title')
        this.section['protocol transition'] = this.customList.addSection("transition protocol", { toggleable: true, })
        this.section['protocol transition'].classList.add('title')

        this.section['states transition'] = this.customList.addSection("states transition", { toggleable: true, })
        this.section['states transition'].classList.add('title')
        
        this.section['functions cell'] = this.customList.addSection("cell functions", { toggleable: true, })
        this.section['functions cell'].classList.add('title')

        this.section['functions protocol'] = this.customList.addSection("protocol functions", { toggleable: true, })
        this.section['functions protocol'].classList.add('title')
        
        this.index = {}
        this.statesInfo = {}

        this.itemBuilderBehavior = (options) => {
            let key = options.category + options.stateId + options.id
            this.index[key] = document.createElement('custom-item')
            this.index[key].classList.add('editable')
            this.index[key].applyOptions(options)
            this.index[key].valueText = options.txt
            this.index[key].type = this.statesInfo[options.stateId] ? this.statesInfo[options.stateId] : options.stateId
            this.index[key].reveal = async(displayCode)=>{await this.showElementBehaviorDiagram(this.index[key],displayCode)}
            this.index[key].addEventListener('click', ()=>this.index[key].reveal(false))
            return this.index[key]
        }

        this.itemBuilderState = (options) => {
            let key = options.id
            this.index[key] = document.createElement('custom-item')
            this.index[key].classList.add('editable')
            this.index[key].applyOptions(options)
            this.index[key].valueText = options.txt
            this.index[key].reveal = (displayCode)=>{ this.showElementStateMachine(this.index[key],displayCode)}
            this.index[key].addEventListener('click', ()=>this.index[key].reveal(false))
            return this.index[key]

        }

        this.itemBuilderFunction = (options) => {
            let key = options.idFunc
            this.index[key] = document.createElement('custom-item')
            this.index[key].classList.add('editable')
            this.index[key].applyOptions(options)
            this.index[key].valueText = options.call + '{...}'
            this.index[key].reveal = async ()=>{await this.showFunction(this.index[key])}
            this.index[key].addEventListener('click', this.index[key].reveal)
            return this.index[key]
        }
    }

    search(value){
        ActivityManager.toggleActivity('search')
        this.domHeader.search(value)
    }


    updateIndexBeaviorCall(category,stateId,id,txt){
        if(!txt)return
        let key = category+ stateId+id
        if(this.index.hasOwnProperty(key)){
            this.index[key].txt = txt
            this.index[key].valueText = txt
        } else {
            let sec = category === 'cellFunction' ? 'cell call' : 'protocol call'
            this.section[sec].addItem(this.itemBuilderBehavior({ txt, stateId, id, category }))
        }
        this.filterItems()
    }
    updateIndexBeaviorTransition(category, stateId, id, txt) {
        if (!txt) return
        let key = category + stateId + id
        if (this.index.hasOwnProperty(key)) {
            this.index[key].txt = txt
            this.index[key].valueText = txt
        } else {
            let sec = category === 'cellFunction' ? 'cell transition' : 'protocol transition'
            this.section[sec].addItem(this.itemBuilderBehavior({ txt, stateId, id, category }))
        }
        this.filterItems()
    }

    updateIndexStateTransition(id, txt) {
        if (!txt) return
        let key = id
        if (this.index.hasOwnProperty(key)) {
            this.index[key].txt = txt
            this.index[key].valueText = txt
        } else {
            this.section['states transition'].addItem(this.itemBuilderState({ txt, id }))
        }
        this.filterItems()
    }

    updateIndexFunction(idFunc, category, call, code) {
        if (!code) return
        let key = idFunc
        if (this.index.hasOwnProperty(key)) {
            this.index[key].txt = code
            this.index[key].valueText = call+'{...}'
        } else {
            let sec = category === 'cellFunction' ? 'functions cell' : 'functions protocol'
            this.section[sec].addItem(this.itemBuilderFunction({ txt: code, call, idFunc, category }))
        }
        this.filterItems()
    }

    updateInfoState(id,name){
        this.statesInfo[id] = name;
        Object.entries(this.index).forEach(([key, dom]) => {
            if (dom.stateId === id) this.index[key].type = name
        })
    }
    removeInfoState(id) {
        Object.entries(this.index).forEach(([key,dom])=>{
            if(dom.stateId===id){
                dom.remove()
                delete this.index[key]
            }
        })
        delete this.statesInfo[id];
    }

    removeIndexFunction(idFunc) {
        if (!this.index[idFunc])return
        this.index[idFunc].remove()
        delete this.index[idFunc]
    }

    clearIndexFunction(){
        Object.entries(this.index).forEach(([key, dom]) => {
            if(dom.idFunc) this.removeIndexFunction(key)
        })
    }

    removeIndexStateTransition(id) {
        if (!this.index[id]) return
        this.index[id].remove()
        delete this.index[id]
    }

    removeIndexBehavior(category, stateId, id) {
        let key = category + stateId + id
        if (!this.index[key]) return
        this.index[key].remove()
        delete this.index[key]
    }

    filterItems(value){
        if(value!==undefined)this.valueSearch = value
        if(this.valueSearch!==undefined)
        Object.values(this.index).forEach(item=>{
            item.style.display=item.txt.includes(this.valueSearch)?null:'none'
        })
        //return Object.values(this.index)
    }


    async showFunction(dom) {
        if (dom.category !== behaviorDiagram.category) {
            let element;
            let options;
            let modeler = stateMachine.modeler;
            if (dom.category === 'cellFunction') {
                element = modeler.get('elementRegistry').get('Event_1ulp3l4');
                stateMachine.showCellsDiagram()
            } else {
                element = modeler.get('elementRegistry').get('Process_1');
                stateMachine.showScenarioDiagram()
                options = { complementTitle: 'initialisation' }
            }
            await stateMachine.viewElement(element, dom.category, options);
        }
        stateMachine.parentPanelManager.unminimize([behaviorDiagram.mainContainer.id, 'panelLocalList'], -1);
        behaviorDiagram.viewFunc(dom.idFunc)
        if (this.valueSearch !== undefined && this.valueSearch !== ''){
            const editor = CodeEditorProvider.getEditor('editorFunction')
            let range = editor.getModel().findMatches(this.valueSearch)[0].range;
            editor.setSelection(range);
            setTimeout(()=>editor.getAction('actions.findWithSelection').run(),200)
        }
    }

    showElementStateMachine(dom,displayCode) {
        stateMachine.parentPanelManager.minimize(['panelLocalList', behaviorDiagram.mainContainer.id], -1);
        var modeler = stateMachine.modeler
        modeler.get('zoomScroll').reset();
        var element = modeler.get('elementRegistry').get(dom.id);
        modeler.get('selection').select(element);
        modeler.get('canvas').zoom(1.0, element);

        if(displayCode){
            modeler.get('eventBus').fire('element.dblclick',{element})
        }
    }

    async showElementBehaviorDiagram(dom,displayCode) {
        let stateId = dom.stateId
        let options;
        if(!this.statesInfo[stateId]){
            options = { complementTitle: stateId.split(' ')[1] }
            stateId = 'Process_1'
            stateMachine.showScenarioDiagram()
        } else {
            stateMachine.showCellsDiagram()
        }
        let element = stateMachine.modeler.get('elementRegistry').get(stateId)
        await stateMachine.viewElement(element, dom.category, options);

        const modeler = behaviorDiagram.modeler
        element = modeler.get('elementRegistry').get(dom.id);
        modeler.get('selection').select(element);
        const isCall = this.section['cell call'].items.indexOf(dom) !== -1 || this.section['protocol call'].items.indexOf(dom) !== -1
        if (isCall)
            behaviorDiagram.viewElement(element);

        if(displayCode && !isCall){
            modeler.get('eventBus').fire('element.dblclick',{element})
        }
    }

    clearIndex(){
        this.index = {}
        this.statesInfo = {}
        Object.values(this.section).forEach(s=>s.clear());
    }
}

const searchManager = new SearchManager();
export default searchManager;