import { splitLines } from "monaco-editor/esm/vs/base/common/strings"
import codeEditorProvider from "../guiComponent/codeEditorProvider"
import { ansiToHtmlConverter } from "../utils/misc"
import customModulesManager from "./customModulesManager"
import searchManager from "./searchManager"
import activityManager from "./ActivityManager"
import md5 from 'js-md5'
import getFinalJson from "../inputs_outputs/getFinalJson"

function convertBrutText(htmlCode){
    let domTmp = document.createElement("code")
    domTmp.innerHTML = htmlCode
    return domTmp.innerText
}

function extractMetaData(msg){
    try{
        cppErrorManager.metaInfo=JSON.parse(msg.match(/metaInfo:(.*)\n/)[1])
        msg=msg.replace(/metaInfo:.*\n/,'')
    } catch (error) {
        console.error(error);
        cppErrorManager.metaInfo=undefined
    }
    return msg
}

function extractErrorFromTxt(txtBrut,isCurrentVersion){
    cppErrorManager.listError = {}
    let lineStartContext = 0
    const allData = [...txtBrut.matchAll(/(?<file>PrimoCell\.hpp|Scenario\.hpp|modules\/cellModules\/(?<module>[\w_\d]+?)\/(?<fileModule>[\w_\d\.]*?)): (In member function|In instantiation of).*?::(?<funcName>[\w_\d]*?)(?<funcParam>\(.*?\))[ \S]*\n.*?error:(?<error1>.*?)\n\S|(?<file2>PrimoCell\.hpp|Scenario\.hpp|modules\/cellModules\/(?<module2>[\w_\d]+?)\/(?<fileModule2>[\w_\d\.]*?)):(?<line2>\d+):\d+: error:(?<error2>.*?)\n\S/gs)].map(i => {
        const f=i.groups
        const data = {}
        const errorLine = (f.error1||f.error2).match(/^(?<errorComment>.*)\s*(?<lineNumber>[\d\?]+)\s*\|(?<spacePrefix>\s*)(?<line>.*?)\s*\|(?<spacePrefixError>\s*)(?<error>\~*\^\~*)$/m).groups

        if(f.module) data.file = f.module+'/'+f.fileModule
        else if(f.module2) data.file = f.module2+'/'+f.fileModule2
        else if(f.file) data.file = f.file
        else if(f.file2) data.file = f.file2

        let metaInfoFunc = cppErrorManager.metaInfo?.[f.file]?.[f.funcName]

        if(f.funcName)data.funcName = f.funcName
        if(f.funcParam)data.funcParam = f.funcParam
        if(f.module||f.module2) lineStartContext = 0
        else if(metaInfoFunc) lineStartContext = metaInfoFunc.line-1
        //else lineStartContext = 0
        
        
        if(f.module||f.module2) data.category = 'Module'
        else if((f.file||f.file2)==='PrimoCell.hpp') data.category = 'Cell'
        else if((f.file||f.file2)==='Scenario.hpp') data.category = 'Protocol'

        
        errorLine.underlineCode = errorLine.line.substr(errorLine.spacePrefixError.length-errorLine.spacePrefix.length,errorLine.error.length)
        
        let item,id,l,sc,ec;
        data.line = Number(errorLine.lineNumber)-lineStartContext
        if(metaInfoFunc?.idFunc){
            l = data.line;
            sc = errorLine.spacePrefixError.length-metaInfoFunc.column;
            ec = sc+errorLine.error.length;
            item = isCurrentVersion?searchManager.index[metaInfoFunc.idFunc]:undefined
            id = metaInfoFunc.idFunc
            data.type = 'function'
        } else if(f.module||f.module2){
            data.type = 'Module'
            id=data.file
            l=data.line
            sc = errorLine.spacePrefixError.length;
            ec = sc+errorLine.error.length;
            if(isCurrentVersion){
                const file = customModulesManager.getFile(data.file)
                item = {}
                item.reveal = ()=>{customModulesManager.displayCode(file)}
            }
        } else {
            if(data.file==='PrimoCell.hpp' && f.funcName==='behave'){
                item = [...searchManager.section['cell transition'].items,...searchManager.section['cell call'].items].find(it=>errorLine.line.includes(it.txt)&&it.txt.includes(errorLine.underlineCode))
                data.type = searchManager.section['cell transition'].items.includes(item)?'condition':'call'
                data.stateName = Object.values(searchManager.index).find(it=>it===item)?.type
            } else if(data.file==='PrimoCell.hpp' && f.funcName==='nextState'){
                item = searchManager.section["states transition"].items.find(it=>errorLine.line.includes(it.txt))
                data.type = 'condition'
            } else if(data.file==='Scenario.hpp' && (f.funcName==='loop' ||f.funcName==='init')){
                item = [...searchManager.section['protocol transition'].items,...searchManager.section['protocol call'].items].find(it=>errorLine.line.includes(it.txt)&&it.txt.includes(errorLine.underlineCode))
                data.type = searchManager.section['protocol transition'].items.includes(item)?'condition':'call'
            }
            if(data.type && item){
                l = 1;
                sc = item.txt.indexOf(errorLine.underlineCode)+1;
                ec = sc+errorLine.underlineCode.length
                id = item.id
                //data.codeOrigin = item.txt
                data.line = ''
            }
        }
        if(data.type && item){
            if(!cppErrorManager.listError[data.type])cppErrorManager.listError[data.type]={}
            if(!cppErrorManager.listError[data.type][id])cppErrorManager.listError[data.type][id]={/*'previousValue':item.txt,*/'errors':[]}
            //https://stackoverflow.com/questions/76018193/cleanup-marker-in-monaco-editor-once-a-quickfixcodeaction-is-clicked
            cppErrorManager.listError[data.type][id].errors.push({
                message: errorLine.errorComment.replace(/]8;;(.*)\x07(.*)\e]8;;\x07/g,(_,link,text)=>`${text}`),
                severity: 8,//monaco.MarkerSeverity.Error,
                startLineNumber: l,
                startColumn: sc,
                endLineNumber: l,
                endColumn: ec,
            })
            data.reveal = item.reveal
        }
        data.errorLine =(f.error1||f.error2).replace(/^(\s*)(\d+)/gm,(_,s,v)=>{let c=''+data.line;while(c.length<v.length){c=' '+c};return s+c})
        return data;
    });
    return allData.reduce((mergedData,data)=>{
        const needMerge = !data.reveal
        if(mergedData.length-1<0 || !needMerge){
            data.errorLine = [data.errorLine]
            mergedData.push(data)
            return mergedData
        }
        const previousData = mergedData[mergedData.length-1]
        if(previousData.file === data.file && needMerge && !!previousData.reveal){
            previousData.errorLine.push(data.errorLine)
        } else {
            data.errorLine = [data.errorLine]
            mergedData.push(data)
        }
        return mergedData
    },[])
}

async function replaceAsync(str, regex, asyncFn) {
    const promises = [];
    str.replace(regex, (full, ...args) => {
        promises.push(asyncFn(full, ...args));
        return full;
    });
    const data = await Promise.all(promises);
    return str.replace(regex, () => data.shift());
}
async function colorizeErrorCode(errors){
    return (await Promise.all(errors.map(async (error)=>{
        error = error.replace(/]8;;(.*)\x07(.*)\e]8;;\x07/g,(_,link,text)=> `<a target="_blank" style="color:var(--theme)" href="${link}">${text}</a>`)
        return await replaceAsync(error,/^(.*)(\s*[\d\?]*\s*\|\s*)(.*)([\s\|]*)(.*)([\s\|]*)(.*)/g,async (_,info,line,code,line2,underline,line3,suggest)=>{
        let html = `${line.match(/\n*(\s*[\d\?]*\s*\|)\s*/)[1].replace(/[\d\?]/g,' ')}${info}${line}` +
        `${(await codeEditorProvider.colorizeCodeToHtml(code,'cpp')).replace('<br/>','')}${line2}` +
                        `<span style="color:var(--terminal-red-bright)">${underline}</span>`
            if(line3&&suggest) html += line3+`<span style="color:var(--terminal-red-bright)">${suggest}</span>`
            return html
    })
    }))).join('\n')
}

async function generateHtml(data,i){
    let html = `<span ${data.reveal?'data-id-error="'+i+'"':'style="filter: opacity(0.7)"'}><span style="color:var(--terminal-blue-bright)">${data.category}${data.category==='Module'?' '+data.file:''}</span><span style="color:var(--editor-foreground)">`
    if(data.funcName){
        if(data.funcName==='init' && data.category==='Cell'){
            html += ` In a ${data.type} of cell instantiation`
        } else if(data.funcName==='nextState' && data.category==='Cell'){
            html += ` In a ${data.type} of state diagram`
        } else if(data.funcName==='behave' && data.category==='Cell'){
            html += ` In a ${data.type} of <b>${data.stateName}</b> state`
        } else if(data.funcName==='init' && data.category==='Protocol'){
            html += ` In a ${data.type} of protocol initialization`
        } else if(data.funcName==='loop' && data.category==='Protocol'){
            html += ` In a ${data.type} of protocol interaction`
        } else {
            html += ` In function <span style="color:var(--terminal-green-bright);">${data.funcName}${data.funcParam}</span>`
        }
    } else {
        html += ` In line <span style="color:var(--terminal-yellow);">${data.line}</span>`
    }
    html += '</span>\n'+(await colorizeErrorCode(data.errorLine))+'</span>'
    return html
}

class CppErrorManager {

    constructor() {
        this.metaInfo={}
        this.listError={}
    }

    async update(msg,modal,finalJson){
        const div = modal.contentDivInfo
        msg = extractMetaData(msg)
        let htmlCode = ansiToHtmlConverter.toHtml(msg.replaceAll("<", "&lt;").replaceAll(">", "&gt;"))
        const isCurrentVersion = JSON.stringify(getFinalJson())===JSON.stringify(finalJson)
        try{
            const txtBrut = convertBrutText(htmlCode)
            htmlCode=htmlCode.replace(/]8;;(.*)\x07(.*)\e]8;;\x07/g,(_,link,text)=>{
                return `<a target="_blank" style="color:var(--theme)" href="${link}">${text}</a>`
            })
            const data = extractErrorFromTxt(txtBrut,finalJson,isCurrentVersion)
            let htmlCodeSimple = (await Promise.all(data.map(generateHtml))).join('\n\n')
            if(htmlCodeSimple!==''){
                htmlCodeSimple= '<code style="white-space: pre;">'+htmlCodeSimple+"</code>"//.replaceAll('\n', '<br>').replaceAll('  ', '&nbsp;&nbsp;').replaceAll('&nbsp; ', '&nbsp;&nbsp;')
                let btn = '<p><a id="showHideErrorCompile" style="cursor: pointer;color: var(--theme);text-decoration: underline;">Show/hide true compile error</a></p>'
                div.innerHTML =  htmlCodeSimple+btn+'<code style="white-space: pre;display:none">' + htmlCode + "</code>"
                let btnShowHide = document.getElementById('showHideErrorCompile')
                if(btnShowHide) btnShowHide.onclick = function(){
                    let c=this.parentElement.nextElementSibling.style;
                    c.display=(c.display)?null:'none';
                }
            if(isCurrentVersion)
                div.querySelectorAll('[data-id-error]').forEach(element => {
                    element.classList.add('goto-error')
                    element.addEventListener('click',()=>{
                        const dataE = data[element.dataset.idError]
                        if(dataE.category==='Cell')activityManager.toggleActivity('cells_behavior')
                        else if(dataE.category==='Protocol') activityManager.toggleActivity('scenario_behavior')
                        else if(dataE.category==='Module') activityManager.toggleActivity('module')
                        dataE.reveal(true)
                        modal.hide()
                    });
                });

            } else {
                div.innerHTML =  '<code style="white-space: pre;">' + htmlCode + "</code>"
            }
        } catch(error){
            console.error(error)
            div.innerHTML =  '<code style="white-space: pre;">' + htmlCode + "</code>"
        }
    }

    setContainer(div){
        this.container = div
    }

    getMarkers(cat,id){
        if(!this.listError[cat]) return []
        if(!this.listError[cat][id]) return []
        else return this.listError[cat][id].errors
    }
}

const cppErrorManager = new CppErrorManager()

export default cppErrorManager