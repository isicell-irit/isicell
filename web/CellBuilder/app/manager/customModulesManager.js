import CodeEditorProvider from '../guiComponent/codeEditorProvider'
import { fetchW } from '../utils/misc';
import { toCamelCase, toSnakeCase, getNewID } from '../utils/utilsVaraibleName'
import cppErrorManager from './CppErrorManager';


function bodyTemplate(moduleName){
    return `#ifndef ${'BODY' + moduleName.toUpperCase() + '_HPP'}
#define ${'BODY' + moduleName.toUpperCase() + '_HPP'}

#include "../../../../src/core/BaseBody.hpp"

${helpHeader(moduleName,'Body')}
using namespace std;

namespace ${moduleName} {

    template<typename cell_t, class plugin_t>
    class ${'Body' + moduleName} : public virtual BaseBody<plugin_t> {

        private:
            /* Body attribute here */

        
        public:
            ${'Body' + moduleName}() = default;

            /**
             * @brief hook called when the body has access to the plugin, this 
             * happens when the cell is added to the world ( w.addCell(c) ).
             * This hook can be useful for setting up Body properties based
             * on elements from the Plugin part.
            */
            void onCellPluginLinking(){

            }

            /* Body methods here */

    };
}
#endif`
}

function helpHeader(moduleName,type){
    if(type==='Plugin')
    return `/*****************************************************************************************
* NOTE : to access this module in the scenario, use: w.cellPlugin.plugin${moduleName}.
******************************************************************************************/\n\n`
    else if(type==='Body')
    return `/*****************************************************************************************
* NOTE : to access the plugin part of this module from the body, use: this->cellPlugin->plugin${moduleName}.
******************************************************************************************/\n\n`
    else return ''
}

function templatePlugin(moduleName){
    return `#ifndef ${'PLUGIN' + moduleName.toUpperCase() + '_HPP'}
#define ${'PLUGIN' + moduleName.toUpperCase() + '_HPP'}

${helpHeader(moduleName,'Plugin')}
#include "${'Body' + moduleName + '.hpp'}"

namespace ${moduleName} {

    template<typename cell_t>
    class ${'Plugin' + moduleName} {

        private:
            /* Plugin attribute here */
        
        
        
        public:
            ${'Plugin' + moduleName}() = default;

            /* Plugin methods here */



            /**
             * Mecacell hook in the order of call in the world update
             * @tparam world_t 
             * @param w 
             */

            template<typename world_t>
            void beginUpdate(world_t *w) {

            }

            template<typename world_t>
            void preBehaviorUpdate(world_t *w){

            }

            template<typename world_t>
            void onAddCell(world_t *w) {

            }

            template<typename world_t>
            void preDeleteDeadCellsUpdate(world_t *w) {

            }

            template<typename world_t>
            void postBehaviorUpdate(world_t *w){

            }

            template<typename world_t>
            void endUpdate(world_t *w) {

            }
    };
}
#endif`
}


function createHeader(){
    var template = document.createElement('template');
    let html = `<div style="background-color:var(--bg-4);height:35px;display: flex;padding: 5px 5px;gap: 10px;">
                <span style="width:100%"><input style="height:20px;padding: 3px;padding-left: 15px;border: 0px;width: 100%;background-color: var(--bg-input);color: var(--text);-webkit-appearance: none;" type="text" placeholder="name module"></span>
                <span style="background-color: var(--theme-color);color: #fff;border-radius: 5px;height:20px"><a style="font-family: Helvetica, sans-serif;cursor:pointer;display:block;width:100px;height:15px;margin: 2px 0 0 5px;height: 20px;border-radius: 5px;margin-top: 1px;">new module</a></span>
            </div>`.replace('\n', '').trim();
    template.innerHTML = html;
    let dom = template.content.firstChild;
    let input = dom.querySelector('input')
    dom.querySelector('a').addEventListener('click', (e) => {
        let moduleName = toCamelCase(input.value)
        input.value = ''
        if(moduleName!==''){
            moduleName = moduleName.charAt(0).toUpperCase() + moduleName.slice(1)
            const moduleSection = customModulesManager.newModule(moduleName)
            moduleSection.addItem(customModulesManager.newFile(moduleName, 'Body', 'Body' + moduleName + '.hpp', bodyTemplate(moduleName) ));
            moduleSection.addItem(customModulesManager.newFile(moduleName, 'Plugin', 'Plugin' + moduleName + '.hpp', templatePlugin(moduleName)));
        }
    })
    return dom;
}

/**
 * Singleton class listing and managing all the types.
 * It also contains the type manager modal window
 */
class CustomModulesManager {

    // Singleton initialisation
    constructor(){  }

    /**
     * This function initialises the singleton
     * using data
     * @param {*} data The json config listing everything
     */
    init(mainContainer,defaultData){
        this.updater=[]

        this.codeEditor = document.createElement('div')
        this.codeEditor.id = 'pluginEditorView'
        this.codeEditor.style = "height:100%; width:100%"
        this.codeEditor.style.display = 'none'
        document.getElementById('contentView').appendChild(this.codeEditor)
        CodeEditorProvider.createEditor('pluginEditorView', {
            lineNumbers: 'on', minimap:true
        });

        this.currentFile = ''

        this.container = document.createElement('div')
        this.container.id = 'CustomModulesManager'
        this.container.style.width='100%'
        mainContainer.style.overflowY = 'auto'
        mainContainer.appendChild(this.container)
        this.container.appendChild(createHeader())

        this.fileItemBuilder = (moduleName, type, name) => {
            let file = document.createElement('custom-item')
            file.type = type?type:' '
            file.editable = true
            file.valueText = name
            file.typeProvider = ()=>[' ','Body','Plugin']
            file.deletable = () => {
                document.getElementById('pluginEditorView').style.display = 'none';
                document.getElementById('visualProgrammingView').style.display = null;
            }
            file.codedata = ""
            file.addEventListener('click', (e) => {
                this.displayCode(file)
            })
            return file
        }
        this.customList = document.createElement('custom-list')
        this.container.appendChild(this.customList)
        /*
        this.customList.updateType(['','Body','Plugin'])
        this.customList.regexCharacterUnaccepted = /[]/g
*/


        this.codeEditor.addEventListener('focusout', () =>{
            if ((this.currentFile)){
                this.currentFile.codedata = CodeEditorProvider.getValue('pluginEditorView')
            }
        })
    }

    /**
     * Returns the list of available types
     */
    newModule(name){
        if(name!==''){
            var sec = this.customList.addSection(name,{
                addable:()=>this.fileItemBuilder(name),
                toggleable:true,
                deletable: true,
            })
            sec.classList.add('title')
            return sec
        }
    }


    /**
     * Returns the list of default types
     */
    displayCode(file) {
        if (this.currentFile!==file){
            if(this.currentFile)this.currentFile.style.color = null
            this.currentFile = file
            this.currentFile.style.color = "var(--text-hover)"
            document.getElementById('pluginEditorView').style.display = null;
            document.getElementById('visualProgrammingView').style.display = 'none';
            const isJson = (file.valueText.split('.').pop()==='json')
            CodeEditorProvider.setEditorCompletionProvider('pluginEditorView','module')
            CodeEditorProvider.setLanguage('pluginEditorView', isJson?'json':'cpp');
            CodeEditorProvider.setValue('pluginEditorView', file.codedata)
            CodeEditorProvider.setError('pluginEditorView',cppErrorManager.getMarkers('Module',file.parent.titleText+'/'+file.valueText))
        }
    }

    getValues() {
        if (this.currentFile) {
            this.currentFile.codedata = CodeEditorProvider.getValue('pluginEditorView')
        }
        let data = {};
        [...this.customList.children].forEach(moduleSection=>{
            data[moduleSection.titleText] = {}
            moduleSection.items.forEach(item=>{
                data[moduleSection.titleText][item.valueText] = { type: item.type, code: item.codedata }
            })
        })
        return data
    }

    getFile(path){
        const [moduleName,file] = path.split('/');
        return [...this.customList.children].find(moduleSection=>moduleSection.titleText===moduleName)?.items?.find(f=>f.valueText===file)
    }

    importCustomModules(data) {
        this.currentFile = ''
        this.customList.clear()
        for (const [moduleName, files] of Object.entries(data)) {
            var moduleSection = this.newModule(moduleName)
            for (const [fileName, content] of Object.entries(files)) {
                moduleSection.addItem(this.newFile(moduleName,content.type?content.type:' ',fileName,content.code))
            }
        }
        this.update()
    }

    importExistingModule(moduleInfo) {
        console.log(moduleInfo)
        fetchW('/api/module/' + moduleInfo.DirName).then((res) => res.json()).then((data) => {
            let moduleName = moduleInfo.DirName.replaceAll(' ', '_')
            var moduleSection = this.newModule(moduleName)
            for (const [fileName, content] of Object.entries(data)) {
                let type = fileName.startsWith('Body') ? 'Body' : undefined
                if(!type) type = fileName.startsWith('Plugin') ? 'Plugin' : ' '
                moduleSection.addItem(this.newFile(moduleName, type, fileName, helpHeader(moduleName,type)+content))
            }
            this.update()
        });

        /*
        for (const [moduleName, files] of Object.entries(data)) {
            var moduleSection = this.newModule(moduleName)
            for (const [fileName, content] of Object.entries(files)) {
                moduleSection.addItem(this.newFile(moduleName, content.type ? content.type : '', fileName, content.code))
            }
        }*/
    }

    clear(){
        this.currentFile = ''
        this.customList.clear()
    }

    newFile(moduleName, type, fileName, code=''){
        var file = this.fileItemBuilder(moduleName,type,fileName)
        file.codedata = code
        return file
    }

    refreshCollapsedList() {
        [...this.customList.children].forEach(moduleSection => moduleSection.updateSize())
        if (this.currentFile) this.currentFile.style.color = null
    }

    unselectFile() {
        if(this.currentFile)this.currentFile.style.color = null
        this.currentFile = undefined
    }

    addUpdater(fun){
        this.updater.push(fun)
    }
    /** Calls all functions in the updater */
    update(){
        const modules = this.getValues()
        for(var fun of this.updater){
            fun(modules)
        }
    }

}

const customModulesManager = new CustomModulesManager();
export default customModulesManager;