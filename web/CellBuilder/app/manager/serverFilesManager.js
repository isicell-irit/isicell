import { file } from 'jszip'
import CodeEditorProvider from '../guiComponent/codeEditorProvider'
import { quickToast } from '../guiComponent/notification'
import Parameters from './ParametersManager'
import { Notebook } from '../guiComponent/notebook'
import ModalWindows from '../guiComponent/modalWindow'
import ContextMenu from '../guiComponent/contextMenu'
import { fetchW, utf8_to_b64 } from '../utils/misc'
import { io } from "socket.io-client";
import { Terminal } from "@xterm/xterm";
import { FitAddon } from '@xterm/addon-fit';
import { WebglAddon } from '@xterm/addon-webgl';
import '../css/welcome.css'
import "@xterm/xterm/css/xterm.css"; // DO NOT forget importing xterm.css
import PanelManager from '../guiComponent/panelManager'
import themeManager from './ThemeManager'
import welcomePageContent from '../guiComponent/welcomePageContent.html'
import codeEditorProvider from '../guiComponent/codeEditorProvider'
import notebookManager from './notebookManager'
let termnialCount = 0;
function userFolder(action, data, cb) {
    
    fetchW('/api/userFolder/' + action, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }, signal: AbortSignal.timeout(5000),
        body: JSON.stringify(data)
    }).then(res => res.json()).then(cb).catch(err=>console.error(err))
}
 
function createModalDeleteFile() {
    const div = document.createElement('div')
    const msgNode = document.createTextNode("Are you sure you want to delete the folder: \"\"?")
    div.appendChild(msgNode)
    const footer = document.createElement('footer')
    div.appendChild(footer)
    const yes = document.createElement('a')
    const no = document.createElement('a')
    yes.innerText = "Yes"
    no.innerText = 'No'
    yes.style.backgroundColor = '#e74c3c'
    no.style.backgroundColor = '#2ecc71'

    footer.appendChild(no); footer.appendChild(yes);
    const modal = new ModalWindows('Confirmation', div)
    modal.callbackFocusOut = modal.callbackClose = ()=>{modal?.resolve(false);modal.resolve = undefined}
    modal.contentAndHeader.style.width = '450px'

    modal.showConfirmation = (isFolder,name) =>{
        msgNode.nodeValue = `Are you sure you want to delete the ${isFolder?'folder':'file'}: "${name}"?`
        modal.show()
        yes.focus()
        return new Promise((resolve, reject) => {
            modal.resolve = resolve
        })
    }

    yes.addEventListener('click', () => {
        modal?.resolve(true)
        modal.resolve = undefined
        modal.hide()
    })
    no.addEventListener('click', () => {
        modal?.resolve(false)
        modal.resolve = undefined
        modal.hide()
    })
    return modal
}


function createModalCloseWithoutSave() {
    const div = document.createElement('div')
    const msgNode = document.createTextNode("Your changes will be lost if you don'tsave them.")
    div.appendChild(msgNode)
    const footer = document.createElement('footer')
    div.appendChild(footer)
    const dontsave = document.createElement('a')
    const cancel = document.createElement('a')
    const save = document.createElement('a')
    dontsave.innerText = "Don't Save"
    cancel.innerText = "Cancel"
    save.innerText = 'Save'
    dontsave.style.backgroundColor = '#e74c3c'
    cancel.style.backgroundColor = '#10499e'
    save.style.backgroundColor = '#2ecc71'

    footer.appendChild(dontsave); footer.appendChild(cancel); footer.appendChild(save);
    var modal = new ModalWindows('Do you want to save the changes you made ?', div)
    modal.contentAndHeader.style.width = '450px'
    dontsave.addEventListener('click', () => {
        modal.hide()
        modal.cbClose()
    })
    cancel.addEventListener('click', () => {
        modal.hide()
    })
    save.addEventListener('click', () => {
        modal.hide()
        modal.cbSave()
    })
    return modal
}

const modalDelete = createModalDeleteFile();

export {modalDelete};

const modalClose = createModalCloseWithoutSave();

function splitPath(path){
    let i = path.lastIndexOf('/') + 1
    return [path.slice(0, i), path.slice(i)]
}

let headerPythonFile = "from isicell import *\n"


function colorizeFile(name,dom){
    if (name === "Welcome") dom.className = 'icon-logo-min'
    else if (name === "Terminal") dom.className = 'icon-terminal'
    else if (name.endsWith('.py')) dom.className = 'icon-file-code color4'
    else if (name.endsWith('.json')) dom.className = 'icon-file-json color3'
    else if (name.endsWith('.ipynb')) dom.className = 'icon-file-notebook color5'
    else if (name.match(/\.(gif|jpe?g|tiff?|png|webp|bmp)$/i)) dom.className = 'icon-file-image color6'
    else if (name.endsWith('.csv')) dom.className = 'icon-file-table color2'
    else dom.className = 'icon-file'
}
const comparatorSorting = function(a,b){
    if(a.tagName==='CUSTOM-SECTION' && b.tagName==='CUSTOM-ITEM') return -1
    else if(a.tagName==='CUSTOM-ITEM' && b.tagName==='CUSTOM-SECTION') return 1
    else return (a.valueText || a.titleText) < (b.valueText || b.titleText) ? -1 : 1
}
export class FileExplorer {

    constructor(filesViewer) {
        this.root = document.createElement('custom-section')
        /* TODO: add keyboard navigation (copy, paste, delete, rename)*/
        this.root.tabIndex = 0
        this.root.addEventListener('blur', (e) => {
            if(this.root.contains(e.relatedTarget)) return
            this.selectedFiles.forEach((f)=>f.classList.remove('item-selected'))
            this.selectedFiles = []
            if(this.cutItem) this.cutItem.forEach((i) => i.classList.remove('item-selected-2'))
            if(this.copyItem) this.copyItem.forEach((i) => i.classList.remove('item-selected-2'))
            this.cutItem = undefined
            this.copyItem = undefined
        },true)
        this.root.addEventListener('keydown', (e) => {
            if(this.selectedFiles.length === 0) return
            let current = this.selectedFiles[this.selectedFiles.length-1]
            let next;
            if(e.key === 'Delete'){
                this.selectedFiles.forEach((f)=>f._btnDel?.click())
            } else if(e.key === 'F2'){
                current.displayEditMode()
            } else if(e.key === 'Enter'){
                this.selectedFiles.forEach((f)=>(f._content||f._header).click())
            } else if(e.key === 'ArrowUp'){
                let idx = current.parent.items.indexOf(current)
                if(idx > 0){
                    next = current.parent.items[idx-1]
                    while(next.tagName==='CUSTOM-SECTION' && next._toggled && next.items.length > 0){
                        next = next.items[next.items.length-1]
                    }
                } else {
                    next = current.parent
                }
                if(next===this.root) return
                if(e.shiftKey){
                    if(!this.selectedFiles.includes(next)){
                        this.selectedFiles.push(next)
                        next.classList.add('item-selected')
                    } else {
                        this.selectedFiles.splice(this.selectedFiles.indexOf(current),1)
                        current.classList.remove('item-selected')
                    }
                } else {
                    this.clickOnOneItem(next)
                }
            } else if(e.key === 'ArrowDown'){
                if(current.tagName==='CUSTOM-SECTION' && current._toggled && current.items.length > 0){
                    next = current.items[0]
                } else {
                    let idx = current.parent.items.indexOf(current)
                    if(idx < current.parent.items.length-1){
                        next = current.parent.items[idx+1]
                    } else {
                        next = current
                        while(next.parent !== this.root && next.parent.items.indexOf(next) === next.parent.items.length-1){
                            next = next.parent
                        }
                        if(next !== current){
                            next = next.parent.items[next.parent.items.indexOf(next)+1]
                        } else {
                            next = undefined
                        }
                    }
                }
                if(!next) return
                if(e.shiftKey){
                    if(!this.selectedFiles.includes(next)){
                        this.selectedFiles.push(next)
                        next.classList.add('item-selected')
                    } else {
                        this.selectedFiles.splice(this.selectedFiles.indexOf(current),1)
                        current.classList.remove('item-selected')
                    }
                } else {
                    this.clickOnOneItem(next)
                }
            } else if(e.key === 'ArrowRight'){
                if(current.tagName==='CUSTOM-SECTION' && !current._toggled){
                    current.toggle()
                }
            } else if(e.key === 'ArrowLeft'){
                if(current.tagName==='CUSTOM-SECTION' && current._toggled){
                    current.toggle()
                }
            } else if((window.isMac ? e.metaKey : e.ctrlKey) && e.key === 'c'){
                if (this.cutItem) this.cutItem.forEach((i) => i.classList.remove('item-selected-2'))
                if (this.copyItem) this.copyItem.forEach((i) => i.classList.remove('item-selected-2'))
                this.copyItem = this.selectedFiles
                this.cutItem = undefined;
                this.selectedFiles.forEach((i) => i.classList.remove('item-selected'))
                this.selectedFiles = [this.selectedFiles[this.selectedFiles.length-1]]
                this.selectedFiles[0].classList.add('item-selected')
                this.copyItem.forEach((i) => i.classList.add('item-selected-2'))
            } else if((window.isMac ? e.metaKey : e.ctrlKey) && e.key === 'x'){
                if (this.cutItem) this.cutItem.forEach((i) => i.classList.remove('item-selected-2'))
                if (this.copyItem) this.copyItem.forEach((i) => i.classList.remove('item-selected-2'))
                this.cutItem = this.selectedFiles
                this.copyItem = undefined;
                this.cutItem.forEach((i) => i.classList.add('item-selected-2'))
                this.selectedFiles.forEach((i) => i.classList.remove('item-selected'))
                this.selectedFiles = [this.selectedFiles[this.selectedFiles.length-1]]
                this.selectedFiles[0].classList.add('item-selected')
            } else if((window.isMac ? e.metaKey : e.ctrlKey) && e.key === 'v'){
                if(this.selectedFiles.length === 1) {
                    if(this.cutItem) moveFiles(this.selectedFiles[0])
                    else if(this.copyItem) pasteFiles(this.selectedFiles[0])
                }
            }
        })

        let destDrag = undefined
        let lastTimeoutOver = undefined
        const dragImage = document.createElement('div')
        dragImage.style.cssText = `
        position: absolute;
        top: -9999px;
        left: -9999px;
        pointer-events: none;
        background: var(--bg-2);
        color: var(--text);
        border: 1px solid var(--border);
    `;
        document.body.appendChild(dragImage)
        this.root.addEventListener('dragstart', (e) => {
            const item = e.target?.closest('custom-section, custom-item')
            if(item && !this.selectedFiles.includes(item)){
                unselectFiles()
                selectFile(item)
                if(item.tagName==='CUSTOM-SECTION'){
                    dragImage.className = 'icon-folder'
                    dragImage.innerHTML = item.titleText
                } else {
                    colorizeFile(item.valueText,dragImage)
                    dragImage.innerHTML = item.valueText
                }
            } else {
                dragImage.className = 'icon-copy'
                dragImage.innerHTML = 'move '+this.selectedFiles.length+' files'
            }
            e.dataTransfer.setDragImage(dragImage, -10, -10);
        },false)
        this.root.addEventListener('dragover', (e) => {
            e.preventDefault()
            const prevItem = destDrag?.getFullPath()
            destDrag = e.target.closest('custom-section')/*
            if(!this.root.contains(destDrag)){
                e.dataTransfer.setDragImage(null, 0, 0);
                return
            }*/
           destDrag.classList.add('hoverDrag')
            if(!destDrag._toggled && prevItem !== destDrag.getFullPath()){
                if(lastTimeoutOver) clearTimeout(lastTimeoutOver)
                lastTimeoutOver = setTimeout(()=>{
                    if(destDrag && !destDrag._toggled && prevItem !== destDrag.getFullPath()){
                        lastTimeoutOver = undefined
                        destDrag.toggle()
                    }
                },1000)
            }
        },false)
        this.root.addEventListener('dragleave', (e) => { destDrag?.classList?.remove('hoverDrag');destDrag=undefined },false);
        this.root.addEventListener('drop', (e) => {
            e.preventDefault()
            if(!destDrag) return
            destDrag.classList.remove('hoverDrag')
            if(e.dataTransfer.files.length > 0){
                e.dataTransfer.dropEffect = 'copy';
                let formData = new FormData()
                formData.append("path", destDrag.getFullPath());
                for (let f of e.dataTransfer.files) {
                    formData.append('files', f)
                }
                fetchW('/api/uploadFiles', {method: 'POST',body: formData})
                .then(()=>{if(destDrag._toggled)this.refreshFiles(destDrag)})
                .catch(()=>quickToast.fire({ icon: 'error', title: 'upload files' }))
            } else {
                this.cutItem = this.selectedFiles
                this.copyItem = undefined
                moveFiles(destDrag)
            }
        },true)
        this.copyItem = undefined
        this.cutItem = undefined
        this.selectedFiles = []
        const unselectFiles = () => {
            this.selectedFiles.forEach((f)=>f.classList.remove('item-selected'))
            this.selectedFiles = []
        }
        const selectFile = (file) => {
            this.selectedFiles.push(file)
            file.classList.add('item-selected')
        }

        const moveFiles = (destItem) => {
            destItem = destItem.tagName === 'CUSTOM-SECTION' ? destItem : destItem.parent
            let dest = destItem.getFullPath()
            let cutPath = this.cutItem?.map((i) => i.getFullPath())
            //check if cuted file is not opened (key of this.filesViewer.openFiles)
            if(!cutPath.includes(dest))
            userFolder('move', { path: cutPath, newPath:dest }, (res) => {
                if (res.err) {
                    quickToast.fire({ icon: 'error', title: 'move error' })
                    this.refreshFiles()
                } else {
                    this.refreshFiles(destItem,(newDir)=>{
                        let openedFileImpacted = {}
                        for(const pathOpen of Object.keys(this.filesViewer.openFiles)){
                            for(const [oldPath,newPath] of Object.entries(res.newName)){
                                if(pathOpen===oldPath) openedFileImpacted[newPath] = oldPath
                                else if(pathOpen.startsWith(oldPath)) openedFileImpacted[newPath +'/'+ pathOpen.slice(oldPath.length)] = oldPath
                            }
                        }
                        const renameOpenedFileInDir = (folder)=>{
                            folder.items.forEach((item)=>{
                                const p = item.getFullPath()
                                if(item.tagName!=='CUSTOM-SECTION' && openedFileImpacted.hasOwnProperty(p)) this.filesViewer.rename(item,openedFileImpacted[p])
                                else if(item.tagName==='CUSTOM-SECTION' && Object.keys(openedFileImpacted).reduce((acc,cut)=>acc || p.startsWith(cut),false))
                                    renameOpenedFileInDir(item)
                            })
                        }
                        renameOpenedFileInDir(newDir)
                        this.cutItem.forEach((i) => i.delete())
                        this.cutItem = undefined
                    })
                }
            })
            else {
                if (this.cutItem) this.cutItem.forEach((i) => i.classList.remove('item-selected-2'))
                 this.cutItem = undefined
            }
            this.contextMenu.pastBtn.style.display = 'none'
        }

        const pasteFiles = (destItem) => {
            destItem = destItem.tagName === 'CUSTOM-SECTION' ? destItem : destItem.parent
            let dest = destItem.getFullPath()
            let copyPath = this.copyItem.map((i) => i.getFullPath())
            userFolder('copy', { path: copyPath, newPath:dest }, (err) => {
                if (err.err) {
                    quickToast.fire({ icon: 'error', title: 'copy error' })
                    this.refreshFiles()
                } else if (this.copyItem !== dest) {
                    const [path,nameF] = splitPath(dest)
                    if (this.copyItem.tagName === 'CUSTOM-SECTION'){
                        this.refreshFiles(destItem)
                    } else {
                        let newFile = this._buildFileItem(path,nameF)
                        destItem.addItem(newFile)
                    }
                }
                if (this.copyItem) this.copyItem.forEach((i) => i.classList.remove('item-selected-2'))
                this.copyItem = undefined
                this.contextMenu.pastBtn.style.display = 'none'
            })
        }

        this.clickOnOneItem = (item)=>{
            this.selectedFiles.forEach((f)=>f.classList.remove('item-selected'))
            /*if(this.cutItem) this.cutItem.forEach((i) => i.classList.remove('item-selected-2'))
            if(this.copyItem) this.copyItem.forEach((i) => i.classList.remove('item-selected-2'))
            this.cutItem = undefined
            this.copyItem = undefined*/
            if(item){
                this.selectedFiles = [item]
                item.classList.add('item-selected')
            }
        }

        this.contextMenu = new ContextMenu({
            "rename": ["icon-rename",(item) => {
                this.contextMenu.cb_cancel();
                item[0]?.displayEditMode()
            }],
            "copy":["icon-copy", (items) => {
                this.contextMenu.cb_cancel()
                if (this.cutItem) this.cutItem.forEach((i) => i.classList.remove('item-selected-2'))
                if (this.copyItem) this.copyItem.forEach((i) => i.classList.remove('item-selected-2'))
                this.copyItem = items
                this.cutItem = undefined;
                this.copyItem.forEach((i) => i.classList.add('item-selected-2'))
                this.contextMenu.pastBtn.style.display = null
            }],
            "cut":["icon-cut", (items) => {
                this.contextMenu.cb_cancel()
                if (this.cutItem) this.cutItem.forEach((i) => i.classList.remove('item-selected-2'))
                if (this.copyItem) this.copyItem.forEach((i) => i.classList.remove('item-selected-2'))
                this.cutItem = items
                this.copyItem = undefined;
                this.cutItem.forEach((i) => i.classList.add('item-selected-2'))
                this.contextMenu.pastBtn.style.display = null

            }],
            "paste": ["icon-paste", (items) => {
                this.contextMenu.cb_cancel();
                let item = items[0]
                if (item.tagName !== 'CUSTOM-SECTION') item = item.parent
                if(this.copyItem){
                    pasteFiles(item)
                } else if (this.cutItem) {
                    moveFiles(item)
                } else {
                    if (this.cutItem) this.cutItem.forEach((i) => i.classList.remove('item-selected-2'))
                    if (this.copyItem) this.copyItem.forEach((i) => i.classList.remove('item-selected-2'))
                    this.cutItem = undefined
                    this.copyItem = undefined
                    this.contextMenu.pastBtn.style.display = 'none'
                }
            }],
            "download": ["icon-download", (items) => {
                this.contextMenu.cb_cancel()
                for (let item of items) {
                    let link = document.createElement("a");
                    link.download = (item.valueText || item.titleText);
                    link.href = "/api/userFolder/download/" + encodeURIComponent(utf8_to_b64(item.getPath())) + "/" + encodeURIComponent(link.download);
                    document.body.appendChild(link);
                    link.click();
                    document.body.removeChild(link);
                }
            }],
            "copy to local notebook": ["icon-load-version", (item) => {
                this.contextMenu.cb_cancel()
                userFolder('read', { path: item.getFullPath() }, (data) => {
                    if (!data.err) {
                        notebookManager.addNotebook(item.valueText, data.data)
                    } else {
                        quickToast.fire({ icon: 'error', title: 'read file error : '+data.err })
                    }
                })
            }],
            "delete": ["icon-trash",(items)=>{
                for(let item of items){
                    item._btnDel?.click()
                }
            }]
        });
        [...this.contextMenu.div.querySelectorAll("li")].filter(i => i.textContent === 'delete')[0].style.color = 'var(--ui-warning)'
        this.contextMenu.copyToLocalNBBtn = [...this.contextMenu.div.querySelectorAll("li")].filter(i => i.textContent === 'copy to local notebook')[0]
        this.contextMenu.pastBtn = [...this.contextMenu.div.querySelectorAll("li")].filter(i => i.textContent === 'paste')[0]
        if(this.contextMenu.pastBtn)this.contextMenu.pastBtn.style.display = 'none'
        this.contextMenu.cb_cancel = () => { this.contextMenu.target.forEach(f=>f.classList.remove('item-selected')); }
        this.root.addEventListener("contextmenu", (e) => {
            e.preventDefault();
            let div = e.target.closest('custom-section, custom-item')
            if (div && div !== this.root) {
                if(!this.selectedFiles.includes(div)) {
                    unselectFiles()
                    selectFile(div)
                    if(div?.valueText?.endsWith('.ipynb')) this.contextMenu.copyToLocalNBBtn.style.display = null
                    else this.contextMenu.copyToLocalNBBtn.style.display = 'none'
                    this.contextMenu.target = [div]
                    div.classList.add('item-selected');
                } else {
                    this.contextMenu.target = this.selectedFiles
                    this.contextMenu.copyToLocalNBBtn.style.display = 'none'
                }
                this.contextMenu.show(e.pageX, e.pageY)
            }
        })
        this.filesViewer = filesViewer
        this._buildFolderItem('.', [],[])
    }

    refreshFiles(path,cb){
        if(!path) path = this.root
        path._list.style.filter = "opacity(0.3)";
        path._list.style.pointerEvents = 'none';
        path.classList.add('loading')
        const scanOpenedFolder = (folder)=>{
            return folder.items.reduce((listOpen,item)=>{
                if(item._toggled && item.tagName==='CUSTOM-SECTION'){
                    listOpen = listOpen.concat(scanOpenedFolder(item))
                }
                return listOpen
            },[folder.getFullPath()])
        }
        const openFolder = scanOpenedFolder(path)
        userFolder('refresh', {path:path.getFullPath(),listOpen:openFolder}, (data) => {
            //TODO: add save of opened folder
            path.clear();
            const newDir = this._buildFolderItem(path.getFullPath(), data.files.files,openFolder)
            /*if(path.parent)
            path.parent.addItem(newDir)*/
            newDir.items.forEach((item) => path.addItem(item))
            //path.replaceWith(newDir)
            path._list.style.filter = null;
            path._list.style.pointerEvents = null;
            path.classList.remove('loading')
            if(cb)cb(path)
        })
    }

    _buildFileItem(path, name){
        let file = document.createElement('custom-item')
        file.draggable = true
        if (name) {
            file.valueText = name
            file.prevValueText = name
        }
        file.getPath = () => (file.parent ? file.parent.getFullPath() : path)+'/'
        file.getFullPath = () => file.getPath() + file.valueText
        file.editable = () => {
            if (file.prevValueText) {
                if (file.prevValueText !== file.valueText) {
                    let newPath = file.getFullPath()
                    let oldPath = file.getPath() + file.prevValueText
                    userFolder('rename', { path:oldPath, newPath }, (err) => {
                        if (err.err) {
                            file.valueText = file.prevValueText
                            quickToast.fire({ icon: 'error', title: 'rename error' })
                        } else {
                            file.prevValueText = file.valueText
                            this.filesViewer.rename(file,oldPath)
                        }
                    })
                }
            } else {
                userFolder('write', { path: file.getFullPath(), data: file.valueText.endsWith('.py')?headerPythonFile:'' }, (err) => {
                    if (err.err){
                         file.remove()
                        quickToast.fire({ icon: 'error', title: 'new file error' })
                    } else {
                        file.prevValueText = file.valueText
                    }
                })
            }
            colorizeFile(file.valueText,file._content);
        }
        file.checkBeforeDelete = ()=>{return modalDelete.showConfirmation(false,file.valueText)}
        file.deletable = ()=>{
            userFolder('delete', { path: file.getFullPath() }, (err) => {
                if (err.err) {
                    quickToast.fire({ icon: 'error', title: 'delete file error' })
                    this.refreshFiles(path.parent)
                } else {
                    if (this.filesViewer.isOpen(file))this.filesViewer.close(file)
                    file.remove()
                }
            })
        } 
        file._content.addEventListener('click', (e) => {
            if((window.isMac ? e.metaKey : e.ctrlKey)){
                if (this.selectedFiles.includes(file)){
                    this.selectedFiles.splice(this.selectedFiles.indexOf(file),1)
                    file.classList.remove('item-selected');
                } else {
                    this.selectedFiles.push(file)
                    file.classList.add('item-selected');
                }
            } else if(e.shiftKey){
                if(this.selectedFiles.length === 0){
                    this.selectedFiles.push(file)
                    file.classList.add('item-selected');
                } else if(!this.selectedFiles.includes(file) && this.selectedFiles.reduce((acc,item)=>acc && item.parent === dir.parent,true)){
                    let idx = file.parent.items.indexOf(file)
                    let idxLast = file.parent.items.indexOf(this.selectedFiles[this.selectedFiles.length-1])
                    const direction = idx > idxLast ? 1 : -1
                    for(let i = idxLast+direction; i !== idx+direction; i+=direction){
                        this.selectedFiles.push(file.parent.items[i])
                        file.parent.items[i].classList.add('item-selected')
                    }
                }
            } else {

                if (file.clickTimeout) {clearTimeout(file.clickTimeout);file.clickTimeout = null;}
                else {
                    file.clickTimeout = setTimeout(() => {
                        if (!file.editMode && e.target.localName !== 'a') {
                            const pathFile = file.getFullPath()
                            this.clickOnOneItem(file)
                            if (!this.filesViewer.isOpen(file))
                                userFolder('read', { path: pathFile }, (data) => {
                                    if (!data.err) {
                                        const isIMG = file.valueText.match(/\.(gif|jpe?g|tiff?|png|webp|bmp)$/i)
                                        this.filesViewer.open(file,data.data,{save:isIMG?false:true,type:isIMG?'img':'txt',runInBackground:pathFile.endsWith('.py'),addParameters:pathFile.endsWith('.py'),reload:!pathFile.endsWith('.py')})
                                    } else {
                                        quickToast.fire({ icon: 'error', title: 'read file error : '+data.err })
                                    }
                                })
                            else {
                                this.filesViewer.select(file)
                            }
                        }
                        file.clickTimeout = null;
                    },300)
                }
            }
        })
        this.filesViewer.updateDomItem(file)
        colorizeFile(file.valueText, file._content);
        return file
    }

    _buildFolderItem(pathWithName, content,openedFolder) {
        let dir = (pathWithName === '.') ? this.root : document.createElement('custom-section')
        dir.comparator = comparatorSorting
        dir.tabIndex = 0
        dir.toggleable = true
        const [path, name] = splitPath(pathWithName)
        dir.titleText = (pathWithName === '.')?'server files':name
        if (pathWithName !== '.'){
            dir.getPath = () => (dir.parent ? dir.parent.getFullPath()+'/' : path)
            dir.getFullPath = () => dir.getPath() + dir.titleText
        } else {
            dir.getPath = () => '.'
            dir.getFullPath = () => '.'
        }

        if (pathWithName === '.' && !this.root.alreadyInit) {
            dir.deletable = true
            dir.classList.add('title')
            let btnRefresh = document.createElement('a')
            btnRefresh.classList.add('icon-reload')
            btnRefresh.addEventListener('click', () => {
                this.refreshFiles()
            })
            dir._btnSlot.insertBefore(btnRefresh, dir._btnSlot.firstChild);
            dir.deletable = false
            dir._toggleOrigin = dir.toggle
        }

        if (pathWithName !== '.'){
            dir.draggable = true
            dir._toggleOrigin = dir.toggle
            dir.toggle = (cb) => {
                dir._toggleOrigin()
                if(dir._toggled) this.refreshFiles(dir,cb)
            }
            dir.checkBeforeDelete = ()=>{return modalDelete.showConfirmation(true,dir.titleText)}
            dir.deletable = () => {
                userFolder('delete', { path: dir.getFullPath() }, (err) => {
                    if (err.err) {
                        quickToast.fire({ icon: 'error', title: 'delete folder error' })
                        this.refreshFiles(path.parent)
                    } else {
                        this.filesViewer.closeAllPathStartWith(dir.getFullPath())
                    }
                })
            }
            dir._header.addEventListener('click', (e) => {
                if((window.isMac ? e.metaKey : e.ctrlKey)){
                    if (this.selectedFiles.includes(dir)){
                        this.selectedFiles.splice(this.selectedFiles.indexOf(dir),1)
                        dir.classList.remove('item-selected');
                    } else {
                        this.selectedFiles.push(dir)
                        dir.classList.add('item-selected');
                    }
                } else if(e.shiftKey){
                    if(this.selectedFiles.length === 0) {
                        this.selectedFiles.push(dir)
                        dir.classList.add('item-selected');
                    } else if(!this.selectedFiles.includes(dir) && this.selectedFiles.reduce((acc,item)=>acc && item.parent === dir.parent,true)){
                        let idx = dir.parent.items.indexOf(dir)
                        let idxLast = dir.parent.items.indexOf(this.selectedFiles[this.selectedFiles.length-1])
                        const direction = idx > idxLast ? 1 : -1
                        for(let i = idxLast+direction; i !== idx+direction; i+=direction){
                            this.selectedFiles.push(dir.parent.items[i])
                            dir.parent.items[i].classList.add('item-selected')
                        }
                    }
                } else {
                    this.clickOnOneItem(dir)
                }
            })
            if(!openedFolder.includes(dir.getFullPath())) dir._toggleOrigin();
            dir.editable = (prevVal, newVal) => {
                if (prevVal !== newVal)
                if (prevVal) {
                    let oldPath = dir.getFullPath()
                    let newPath = dir.getPath() + newVal 
                    userFolder('rename', { path: oldPath, newPath }, (err) => {
                        if (err.err) {
                            dir.titleText = prevVal
                            quickToast.fire({ icon: 'error', title: 'rename folder error' })
                        } else {
                            renameAllPathStartWith(oldPath, newPath)
                        }
                    })
                } else {
                    userFolder('newDir', { path: dir.getPath() + newVal }, (err) => {
                        if (err.err) {
                            quickToast.fire({ icon: 'error', title: 'new dir error' })
                            dir.remove()
                        }
                    })
                }
            }
        }
        if (pathWithName !== '.' || !this.root.alreadyInit) {
            let btnAddDir = document.createElement('a')
            btnAddDir.classList.add('icon-folder')
            btnAddDir.addEventListener('click', () => {
                if(!dir._toggled) dir.toggle((dir)=>{
                    let newDir = this._buildFolderItem(dir.getFullPath()+'/' , [], openedFolder)
                    dir.addItem(newDir)
                    newDir.displayEditMode()
                })
                else {
                    let newDir = this._buildFolderItem(dir.getFullPath()+'/' , [], openedFolder)
                    dir.addItem(newDir)
                    newDir.displayEditMode()
                }
            })
            dir._btnSlot.insertBefore(btnAddDir, dir._btnSlot.firstChild);

            let btnAddFile = document.createElement('a')
            btnAddFile.classList.add('icon-file')
            btnAddFile.addEventListener('click', () => {
                if(!dir._toggled) dir.toggle((dir)=>{
                    let newFile = this._buildFileItem(dir.getFullPath())
                    dir.addItem(newFile)
                    newFile.displayEditMode()
                })
                else {
                    let newFile = this._buildFileItem(dir.getFullPath())
                    dir.addItem(newFile)
                    newFile.displayEditMode()
                }
            })
            dir._btnSlot.insertBefore(btnAddFile, dir._btnSlot.firstChild);
            
        }


        if (pathWithName === '.') dir.alreadyInit = true
        for (let f of content) {
            if (typeof f === 'object') {
                dir.addItem(this._buildFolderItem('./'+f.path,f.files,openedFolder))
            } else {
                dir.addItem(this._buildFileItem(dir.getFullPath(),f,openedFolder))
            }
        }
        return dir
    }
}

export class FilesViewer {
    constructor(container) {
        this.openFiles = {}
        PanelManager.createSplitterContainer('notebookGUI', 'osef111', true)
        this.panelManager = new PanelManager('osef111',true);
        this.splitCount = 0
        this.panelList = {}
        this.container = container
        this.newPanel(true)
        window.fv = this
        if(localStorage.getItem('showWelcomePageOnStartup')!=='false')
            this.open({valueText:'Welcome',getFullPath:()=>">>>Welcome"})
    }

    newPanel(isMainPanel=false){
        const id = isMainPanel?'FVmainPanel':"FVpanel"+(this.splitCount++)
        this.panelList[id] = []
        const panel = this.panelManager.createPanel(id);

        const headerWrapper = document.createElement('div')
        headerWrapper.className = "header-files-viewer"

        headerWrapper.addEventListener("wheel", (evt) => {
            evt.preventDefault();
            if(Math.abs(evt.deltaY) > Math.abs(evt.deltaX)) headerWrapper.scrollLeft += evt.deltaY;
            else headerWrapper.scrollLeft += evt.deltaX;
        });
        panel.contextMenu = new ContextMenu({
            "split": ["icon-columns",(item) => {
                const itemPath = Object.keys(this.openFiles).filter(k=>this.openFiles[k].domTab===item)[0]
                const info = this.openFiles[itemPath]
                const listPanel = Object.keys(this.panelList)
                const idxPanel = listPanel.indexOf(info.panel.id)
                const panelId = listPanel[idxPanel]
                
                const currentListPanelItem = this.panelList[panelId]
                if (info.domTab.classList.contains('selected')){
                    let i =currentListPanelItem.indexOf(itemPath)
                    if(i > 0)i--;else i++;
                    if (currentListPanelItem.length > 1) this.select(this.openFiles[currentListPanelItem[i]].domListItem)
                }
                this.panelList[panelId].splice(this.panelList[panelId].indexOf(itemPath),1)

                if(idxPanel === listPanel.length-1){
                    this.newPanel()
                } else {
                    this.currentPanel = document.getElementById(listPanel[idxPanel+1])
                }
                this.currentPanel.header.appendChild(info.domTab)
                this.currentPanel.contentTab.appendChild(info.domView)
                this.panelList[this.currentPanel.id].push(itemPath)
                info.panel = this.currentPanel

                this.select(info.domListItem)
                this.cleanEmptyPanel()
            }],
        })
        panel.contextMenuNew = new ContextMenu({
            "new Terminal": ["icon-terminal",(item) => {
                const fakePath = ">>>"+(termnialCount++)+".term"
                this.currentPanel = item
                this.open({valueText:'Terminal',getFullPath:()=>fakePath})
            }],
            "Welcome": ["icon-logo-min",(item) => {
                const fakePath = ">>>Welcome"
                this.currentPanel = item
                this.open({valueText:'Welcome',getFullPath:()=>fakePath})
            }]
        })
        headerWrapper.addEventListener("contextmenu", (e) => {
            e.preventDefault();
            const tab = e.target.closest('.tab')
            if(e.target===headerWrapper){
                panel.contextMenuNew.target = panel
                panel.contextMenuNew.show(e.pageX,e.pageY)
            }else if (tab) {
                panel.contextMenu.target = tab
                const itemPath = Object.keys(this.openFiles).filter(k=>this.openFiles[k].domTab===tab)[0]
                panel.contextMenu.show(e.pageX, e.pageY)
            }
        })
        
        panel.header = document.createElement('div')
        panel.header.style ="width: max-content; height:40px;isplay: inline-block;"
        headerWrapper.appendChild(panel.header)



		panel.header.addEventListener('dragstart', (e) => {
            if(isMainPanel && this.panelList[id].length<=1){
                e.preventDefault();
                return;
            }
			let tabsContainer0 = panel.header;
			let dragStartIndex = Array.from(tabsContainer0.children).indexOf(e.target);
            e.dataTransfer.effectAllowed = 'move';
            e.dataTransfer.setData('text/plain', null);
			e.target.classList.add('dragging');
			const draggingTab = e.target
		    const allTab = document.getElementById('notebookGUI').querySelectorAll('.header-files-viewer')
			const ondragover = (e) => {
			    e.preventDefault();
                const overwraper =e.target.classList.contains('header-files-viewer')
                if(overwraper){
                    e.target.firstChild.appendChild(draggingTab)
                    dragStartIndex = Array.from(e.target.firstChild.children).indexOf(draggingTab);
                    tabsContainer0 = e.target.firstChild
                    return;
                }
			    const tabsContainer = e.target.closest('.header-files-viewer > div');
			    const dragOverTab = e.target.closest('.tab');
			    if(tabsContainer0 !== tabsContainer) dragStartIndex = 0
			    if (!draggingTab || !dragOverTab || draggingTab === dragOverTab) return;

			    const dropIndex = Array.from(tabsContainer.children).indexOf(dragOverTab);
			    
			    if (dragStartIndex < dropIndex) tabsContainer.insertBefore(draggingTab, dragOverTab.nextSibling);
			    else tabsContainer.insertBefore(draggingTab, dragOverTab);
			    
			    dragStartIndex = Array.from(tabsContainer.children).indexOf(draggingTab);
			    tabsContainer0 = tabsContainer
			}
			const ondragend = () => {
			    if (draggingTab) draggingTab.classList.remove('dragging');
				allTab.forEach(t=>{
					t.ondragover = null
					t.ondragend = null
				})
                const itemPath = Object.keys(this.openFiles).filter(k=>this.openFiles[k].domTab===draggingTab)[0]
                if(panel.header !== tabsContainer0){
                    const targetPanel = tabsContainer0.closest('.panel-split-h')
                    const info = this.openFiles[itemPath]
                    const isSelected =info.domTab.classList.contains('selected')
                    if (isSelected){
                        let i =this.panelList[id].indexOf(itemPath)
                        if(i > 0)i--;else i++;
                        if (this.panelList[id].length > 1) this.select(this.openFiles[this.panelList[id][i]].domListItem)
                    }
                    this.panelList[id].splice(this.panelList[id].indexOf(itemPath),1)
                    this.panelList[targetPanel.id].splice(dragStartIndex,0,itemPath)
                    info.panel = targetPanel
                    targetPanel.contentTab.appendChild(info.domView)
                    this.currentPanel = targetPanel
                    if(isSelected)this.select(info.domListItem)
                    this.cleanEmptyPanel()
                } else {
                    this.panelList[id].splice(this.panelList[id].indexOf(itemPath),1)
                    this.panelList[id].splice(dragStartIndex,0,itemPath)
                }
			}
			allTab.forEach(t=>{
				t.ondragover = ondragover
				t.ondragend = ondragend
			})
		})


        panel.contentTab = document.createElement('div')
        panel.contentTab.classList.add('container-file-viewer')
        panel.appendChild(headerWrapper)
        panel.appendChild(panel.contentTab)
        this.currentPanel = panel
        this.updateSizePanel()
    }

    updateSizePanel(){
        const listPanel = Object.keys(this.panelList)
        const pc = 100/listPanel.length
        this.panelManager.setWidthPanel(listPanel.map(_=>pc),false)
    }

    
    open(itemFile,dataContent,options={}){
        const itemPath = itemFile.getFullPath()
        if (!this.openFiles.hasOwnProperty(itemPath)){
            const domTab = document.createElement('div')
            domTab.className = 'tab'
            domTab.draggable = true
            const domFileName = document.createElement('span')
            domFileName.innerText = itemFile.valueText
            colorizeFile(itemFile.valueText, domFileName)
            domTab.appendChild(domFileName)
            const closeBtn = document.createElement('a')
            closeBtn.innerText = '×'
            domTab.appendChild(closeBtn)
            let domView = document.createElement('div')
            this.currentPanel.contentTab.appendChild(domView)
            const info = { domTab, domView, domListItem: itemFile }

            let remove;
            let save;
            let reload;
            if(itemFile.valueText.endsWith('.ipynb')){
                let callBackSave;
                let setEdited;
                if(options.save){
                    if (typeof options.save === 'function') callBackSave = (json,cb) => { options.save(json); info.domTab.classList.remove('edited');if(cb)cb(); }
                    else callBackSave = (data,cb)=>{
                            userFolder('write', { path:info.domListItem.getFullPath(), data: JSON.stringify(data) }, (err) => {
                                if (err.err) quickToast.fire({ icon: 'error', title: 'save error' })
                                else info.domTab.classList.remove('edited')
                                if(cb)cb(err.err)
                            })
                        }
                    setEdited = (edited) => { if (edited) info.domTab.classList.add('edited'); else info.domTab.classList.remove('edited') }
                }
                const nb = new Notebook(domView,()=>info.domListItem.valueText,dataContent,callBackSave,setEdited);
                if(options.save) save = (cb)=>callBackSave(nb.getJSON(),cb)
                remove = ()=>nb.remove()
            } else if (itemFile.valueText.endsWith('.csv')) {
                const setEdited = (edited) => { if (edited) info.domTab.classList.add('edited'); else info.domTab.classList.remove('edited') }
                const ce = new CsvEditor(domView, () => info.domListItem.getFullPath(), dataContent, setEdited, options)
                remove = () => ce.remove()
                save = ce.save
                reload = ()=>ce.reload()
            } else if (itemPath.endsWith('.term')) {
                const ce = new TerminalUI(domView)
                remove = () => ce.remove()
            } else if (itemPath.endsWith('Welcome')) {
                const ce = new WelcomeUI(domView)
                remove = () => ce.remove()
            } else {
                const setEdited = (edited) => { if (edited) info.domTab.classList.add('edited'); else info.domTab.classList.remove('edited') }
                const fe = new FileEditor(domView, () => info.domListItem.getFullPath(), dataContent, setEdited,this.runInBackground,options)
                remove = ()=>fe.remove()
                save = fe.save
                reload = ()=>fe.reload()
            }
            info.remove = remove
            info.save = save
            info.reload = reload
            //if (itemFile.valueText.match(/\.(gif|jpe?g|tiff?|png|webp|bmp)$/i)) domView = document.createElement('div')
            //else 
            closeBtn.addEventListener('click', (e) => {
                if(info.domTab.classList.contains('edited')){
                    modalClose.cbClose = ()=>{
                        this.close(info.domListItem)
                    }
                    modalClose.cbSave = ()=>{
                        info.save((err)=>{
                            console.log(err)
                            if (!err) this.close(info.domListItem)
                        })
                    }
                    modalClose.show()
                } else this.close(info.domListItem)
                e.preventDefault(); e.stopPropagation()},false)
            info.panel = this.currentPanel
            domTab.addEventListener('click', (e) => { this.select(info.domListItem); e.preventDefault(); e.stopPropagation()},false)
            this.currentPanel.header.appendChild(domTab)
            this.panelList[this.currentPanel.id].push(itemPath)
            this.openFiles[itemPath] = info
        }
        this.select(itemFile)
    }

    setRunInBackgroundInstance(runInBackground){
        this.runInBackground = runInBackground;
    }

    select(itemFile){
        const itemInfo = this.openFiles[itemFile.getFullPath()]
        Object.values(this.openFiles).filter(i=>i.panel===itemInfo.panel).forEach((fileInfo)=>{
            fileInfo.domTab.classList.remove('selected')
            fileInfo.domView.style.display = 'none'
            if(fileInfo.domListItem?._value)fileInfo.domListItem._value.style.color = null
        })
        if(itemFile.domListItem?._value)
        itemFile._value.style.color = 'var(--text-hover)'
        if (!itemFile.parent?._toggled)itemFile.parent?._toggleOrigin()
        const k = itemFile.getFullPath()
        this.openFiles[k].domTab.classList.add('selected')
        this.openFiles[k].domView.style.display = null
        const el = this.openFiles[k].domTab
        const wrapper = el.parentNode.parentNode
        const elLeft = el.offsetLeft + el.offsetWidth;
        const elParentLeft = wrapper.offsetLeft + wrapper.offsetWidth;
        if (elLeft >= elParentLeft + wrapper.scrollLeft) {
            wrapper.scrollLeft = elLeft - elParentLeft;
        } else if (elLeft <= wrapper.offsetLeft + wrapper.scrollLeft) {
            wrapper.scrollLeft = el.offsetLeft - wrapper.offsetLeft;
        }
        this.currentPanel = itemInfo.panel
    }

    close(itemFile) {
        if(this.isOpen(itemFile)){
            const k = itemFile.getFullPath()
            const l = Object.keys(this.openFiles)
            const isCurrent = this.openFiles[k].domTab.classList.contains('selected')
            
            if (isCurrent && itemFile._value) itemFile._value.style.color = null
            this.openFiles[k].remove()
            this.openFiles[k].domTab.remove()
            const panelId = this.openFiles[k].panel.id

            if (isCurrent){
                const currentListPanelItem = this.panelList[panelId]
                let i =currentListPanelItem.indexOf(k)
                if(i > 0)i--;else i++;
                if (currentListPanelItem.length > 1) this.select(this.openFiles[currentListPanelItem[i]].domListItem)
            }
            this.panelList[panelId].splice(this.panelList[panelId].indexOf(k),1)

            delete this.openFiles[k]
            this.cleanEmptyPanel()
        }
    }

    cleanEmptyPanel(){
        let isDelete = false;
        Object.keys(this.panelList).filter(k=>k!=="FVmainPanel"&&this.panelList[k].length===0).forEach(p=>{
            delete this.panelList[p]
            this.panelManager.removePanel(p)
            isDelete = true;
        })
        const listPanel = Object.keys(this.panelList)
        if(!listPanel.includes(this.currentPanel.id)) this.currentPanel = document.getElementById(listPanel[listPanel.length-1])
        if(isDelete) this.updateSizePanel()
    }

    isOpen(itemFile){
        return this.openFiles.hasOwnProperty(itemFile.getFullPath())
    }

    closeAllPathStartWith(path) {
        Object.keys(this.openFiles).forEach((p)=>{
            if(!p.startsWith(path)){
                this.currentPanel.contentTab.removeChild(this.openFiles[k].domView)
                this.openFiles[k].domTab.remove()
                delete this.openFiles[k]
            }
        })
    }

    renameAllPathStartWith(oldPath, newPath) {
        Object.keys(this.openFiles).forEach((p) => {
            if (p.startsWith(oldPath)) delete Object.assign(this.openFiles, { [newPath + p.slice(oldPath.length)]: this.openFiles[p] })[p];
        })
    }

    updateDomItem(item){
        const k = item.getFullPath()
        if(this.openFiles.hasOwnProperty(k)) this.openFiles[k].domListItem = item;
    }


    rename(itemFile, prevPath) {
        if (this.openFiles.hasOwnProperty(prevPath)){
            const domFileName = this.openFiles[prevPath].domTab.querySelector('span')
            domFileName.innerText = itemFile.valueText
            colorizeFile(itemFile.valueText, domFileName)
            delete Object.assign(this.openFiles, { [itemFile.getFullPath()]: this.openFiles[prevPath] })[prevPath]
        }
    }

}
class WelcomeUI {
    constructor(container) {
        this.container = container;
        container.id = "welcomePage"
        container.innerHTML = `<div class="carousel">
            <div class="slides">
                <div class="slide">
                    <div></div>
                    <div class="double-column">
                        <div class="column left-column">

                        </div>
                        <div class="column right-column">
                            <div class="details">
                                
                            </div>
                            <button class="examples-btn" data-examples="0">See more</button>
                        </div>
                    </div>
                    <div class="showOnStartup">
                        <input type="checkbox" id="showOnStartup"/>
                        <label for="showOnStartup">Show welcome page on startup</label>
                    </div>
                </div>
                <div class="slide">
                    <a class="return-btn">&lt; Welcome</a>
                    <div class="examples-content">

                    </div>
                </div>
            </div>
        </div>`
        this.loadContent().then(()=>{
            const showOnStartup = container.querySelector('#showOnStartup');
            showOnStartup.checked = localStorage.getItem('showWelcomePageOnStartup') !== 'false';
            showOnStartup.addEventListener('change', () => {
                localStorage.setItem('showWelcomePageOnStartup', showOnStartup.checked);
            })
            const slides = container.querySelector('.slides');
            const listItems = container.querySelectorAll('.list-item');
            const detailsContainer = container.querySelector('.details');
            const examplesButton = container.querySelector('.examples-btn');
            const returnButton = container.querySelector('.return-btn');
            const examplesContent = container.querySelector('.examples-content');
            const displaySection = (id) => {
                detailsContainer.innerHTML = this.content[id].detail;
                examplesButton.dataset.examples = this.content[id].hasOwnProperty('see more')?id:-1;
            }
            displaySection(0)
            listItems.forEach((item,i) => {
                if(i===0)item.classList.add('selected')
                item.addEventListener('click', ()=>{
                    this.container.querySelector('.selected')?.classList?.remove('selected')
                    displaySection(i)
                    item.classList.add('selected')
                });
            });

            examplesButton.addEventListener('click', () => {
                const id = parseInt(examplesButton.dataset.examples);
                examplesContent.innerHTML = this.content[id]['see more'];
                slides.style.transform = `translateX(-100%)`;
            });

            returnButton.addEventListener('click', () => {
                slides.style.transform = `translateX(0%)`;
            });
        });
    }

    async loadContent(){
        /*  
<!-- nextSECTION -->
<!-- detail -->
<!-- see more -->
<!-- nextSECTION -->
 */
        async function colorizeBlockCode(str) {
            // Split the input string by the <code> tags
            const parts = str.split(/(<code class\='block'>[\s\S]*?<\/code>)/);
          
            // Process each part
            for (let i = 0; i < parts.length; i++) {
              if (parts[i].startsWith("<code class='block'>") && parts[i].endsWith('</code>')) {
                const content = parts[i].slice(20, -7); // Extract the content inside <code> tags
                if (content.includes('\n')) { // Check if the content has newlines
                  const replacement = await codeEditorProvider.colorizeCodeToHtml(content,'python');
                  parts[i] = `<code class='block'>${replacement}</code>`;
                }
              }
            }
            return parts.join('');
          }
        this.content = [];
        (await colorizeBlockCode(welcomePageContent)).split('<!-- nextSECTION -->').forEach((section) => {
            const s = section.split(/\<\!\-\-\s*(.*?)\s*\-\-\>/gm)
            const sectionData = {}
            for(let i=1; i<s.length; i+=2){
                sectionData[s[i]] = s[i+1]
            }
            this.container.querySelector('.left-column').innerHTML += `<div class="list-item">${sectionData.title}</div>`
            this.content.push(sectionData)
        })
    }

    remove() {
        this.container.remove()
    }
}
class TerminalUI {
    constructor(container) {
        const theme = themeManager.variable[document.getElementById('theme-switch').checked?'dark':'light']['terminal']
        this.terminal = new Terminal({theme:{
            foreground: theme['white-bright'],
            background: theme['black'],
            selectionBackground: '#5DA5D533',
            selectionInactiveBackground: '#66646633',
            cursor:theme['white-bright'],
            cursorAccent:theme['white'],
            black: theme['black'],
            brightBlack: theme['black-bright'],
            red: theme['red'],
            brightRed: theme['red-bright'],
            green: theme['green'],
            brightGreen: theme['green-birght'],
            yellow: theme['yellow'],
            brightYellow: theme['yellow-birght'],
            blue: theme['blue'],
            brightBlue: theme['blue-birght'],
            magenta: theme['purple'],
            brightMagenta: theme['purple-birght'],
            cyan: theme['cyan'],
            brightCyan: theme['cyan-birght'],
            white: theme['white'],
            brightWhite: theme['white-bright'],
          },fontFamily: "Fira Code"});

        this.container = container
        container.style.width = "100%"
        container.style.height = "100%"//"calc(100% - 40px)"
        container.style.backgroundColor = theme['black']
        container.style.padding = "5px 0 40px 5px"
        const term = document.createElement('div')
        term.style.width = "100%"
        term.style.height = "100%"
        container.appendChild(term)
        const fitAddon = new FitAddon();
        this.terminal.loadAddon(fitAddon);
        this.terminal.loadAddon(new WebglAddon());
        this.terminal.open(term);
        this.socket = io();
        this.terminal.onResize(size => {
            this.socket.emit("resize", size);
        })
        fitAddon.fit()

        this.terminal.onData((data) => this.socket.emit("input", data));
        this.socket.on("output", (data) => this.terminal.write(data));
        this.socket.on("disconnect", () => {
            term.innerHTML = ""
        });

        let lastInterval;
        window.addEventListener('resize',(e)=>{
            if(lastInterval) clearTimeout(lastInterval)
            lastInterval = setTimeout(()=>fitAddon.fit(),500)
        })
        setTimeout(()=>this.terminal.focus(),500)
    }
  
  
  
    remove() {
      this.socket.disconnect();
      this.container.remove();
      delete this
    }
  }
class CsvEditor {
    constructor(container, pathGetter, content, setIsEdited, options) {
        this.fileEditorDOM = container
        this.fileEditorDOM.style = "height:100%; width:100%;"
        this.pathGetter = pathGetter
        this.contentOnServe = content
        this.setIsEdited = setIsEdited
        this.csvTable = document.createElement('csv-table')
        let header = document.createElement('div')
        header.className = "header-notebook table-info-container"
        let btn;
        if (options.save) {
            btn = document.createElement('a')
            btn.innerText = "Save"
            this.save = (cb) => {
                let path = this.pathGetter()
                let content = this.csvTable.getCSV()
                userFolder('write', { path, data: content }, (err) => {
                    if (err.err) quickToast.fire({ icon: 'error', title: 'save error' })
                    else { this.contentOnServe = content; this.setIsEdited(false) }
                    if (cb) cb(err.err)
                })
            }
            btn.addEventListener('click', () => this.save())
            header.appendChild(btn)
        }
        btn = document.createElement('a')
        btn.innerText = "Reload"
        btn.addEventListener('click', () => this.reload())
        header.appendChild(btn)
        this.btnReload = btn
/*
        let spacer = document.createElement('div')
        spacer.style.marginLeft = 'auto';
        header.appendChild(spacer)*/
        this.fileEditorDOM.appendChild(header)

        const wrapper = document.createElement('div')
        wrapper.className = 'cellsContainer'
        wrapper.appendChild(this.csvTable)
        this.fileEditorDOM.appendChild(wrapper)
        this.csvTable.setData(content);
        this.csvTable.cbEdit = this.setIsEdited;
        [...this.csvTable.getPageManager().children].forEach(d=>header.appendChild(d))

    }
    remove() {
        this.fileEditorDOM.parentElement.removeChild(this.fileEditorDOM)
    }
    reload() {
        userFolder('read', { path: this.pathGetter() }, (data) => {
            if (!data.err) {
                this.contentOnServe = data.data
                this.csvTable.setData(data.data)
                this.setIsEdited(false)
            } else {
                quickToast.fire({ icon: 'error', title: 'read file error : '+data.err })
            }
        })
    }
}

let idEditor = 0
class FileEditor {
    constructor(container, pathGetter, content, setIsEdited, runInBackground, options) {
        this.fileEditorDOM = container
        this.fileEditorDOM.style = "height:100%; width:100%; overflow-y:hidden"
        this.pathGetter = pathGetter
        this.contentOnServe = content
        this.setIsEdited = setIsEdited
        this.options = options
        let header = document.createElement('div')
        header.className = "header-notebook"
        let btn;
        if (options.save){
            btn = document.createElement('a')
            btn.innerText = "Save"
            btn.classList.add('icon-save')
            this.save = (cb) => {
                let path = this.pathGetter()
                let content = this.editor.getValue()
                userFolder('write', { path, data: content }, (err) => {
                    if (err.err) quickToast.fire({ icon: 'error', title: 'save error' })
                    else { this.contentOnServe = content; this.setIsEdited(false) }
                    if(cb)cb(err.err)
                })
            }
            btn.addEventListener('click', ()=>this.save())
            header.appendChild(btn)

        }

        if(options.reload){
            btn = document.createElement('a')
            btn.innerText = "Reload"
            btn.classList.add('icon-reload')
            btn.addEventListener('click', () => this.reload())
            header.appendChild(btn)
            this.btnReload = btn
        }


        if(options.addParameters){
            const paramSelector = document.createElement('select')
            paramSelector.innerHTML = '<option value="" disabled selected hidden>Add Parameters</option>'
            paramSelector.addEventListener('focus', function () {
                this.innerHTML = '<option value="" disabled selected hidden>Add Parameters</option>' + Parameters.getCurrentItemBuild().items.map((p, i) => ('<option value="' + i + '">' + p.titleText + '</option>')).join('')
            })
            paramSelector.addEventListener('change', () => {
                if (paramSelector.value) {
                    let paramItem = Parameters.getCurrentItemBuild().items[parseInt(paramSelector.value)]
                    const l = this.editor.getPosition().lineNumber+1;
                    const indent = this.editor.getModel().getValueInRange({startLineNumber: l,endLineNumber: l+1,startColumn: 1,endColumn: 1}).match(/^(\s*)/)[0].replace('\n','')
                    const range = {startLineNumber: l,endLineNumber: l,startColumn: 1,endColumn: 1}
                    this.editor.executeEdits("insert-parameters", [{identifier: { major: 1, minor: 1 }, range, text: ('params = ' + JSON.stringify({ input: Parameters.getParamFromItemParam(paramItem) }, null, 2)).replaceAll(/^/gm,indent)+'\n', forceMoveMarkers: true}]);
                }
            })
            header.appendChild(paramSelector)
        }

        let spacer = document.createElement('div')
        spacer.style.marginLeft = 'auto';
        header.appendChild(spacer)
        if(options.runInBackground){
            btn = document.createElement('a')
            btn.style = "background-color:var(--theme);color:#222"
            btn.innerText = "Run in background"
            btn.classList.add('icon-play')
            btn.addEventListener('click', () => {
                let path = this.pathGetter()
                fetchW('/api/nohup/' + Parameters.getCurrentItemBuild().token, {
                    method: 'POST', headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
                    body: JSON.stringify({ path })
                }).then(res => res.json()).then(data => {
                    if (data.err) quickToast.fire({ icon: 'error', title: 'run error' })
                    else runInBackground.refresh(data.token)
                    //refresh backgournd run manager
                })
            })
            this.btnRunBackground = btn
            header.appendChild(btn)
        }

        if (options.tokenKillnohup) {
            btn = document.createElement('a')
            btn.style = "background-color:#d13b4b;color:#222"
            btn.classList.add('icon-stop')
            btn.innerText = "Stop execution"
            btn.addEventListener('click', () => {
                btn.style.display = 'none'
                fetchW('/api/killnohup/' + options.tokenKillnohup, { // + itemRunInBackground.token
                    method: 'GET'
                }).then(res => res.json()).then(err => {
                    if (err.err) quickToast.fire({ icon: 'error', title: 'kill error' })
                    header.removeChild(btn)
                    runInBackground.refresh()
                })
            })
            this.btnStopBackground = btn
            header.appendChild(btn)
        }
        this.fileEditorDOM.appendChild(header)

        if(options.type==='img'){
            const imgDiv = document.createElement('div')
            imgDiv.className = 'cellsContainer'
            imgDiv.style = "overflow-x: hidden;"
            this.imgView = document.createElement('img')
            this.imgView.style = "transform: translate(-50%, 0px);margin-left: 50%;max-width: 100%;"
            imgDiv.appendChild(this.imgView)
            this.fileEditorDOM.appendChild(imgDiv)
            this.imgView.src = content
        } else {
            let fileEditor = document.createElement('div')
            this.idEditor = 'serverFileEditor'+idEditor
            fileEditor.id = this.idEditor
            fileEditor.className = 'cellsContainer'
            this.fileEditorDOM.appendChild(fileEditor)
            this.editor = CodeEditorProvider.createEditor('serverFileEditor'+idEditor, {
                lineNumbers: 'on', minimap: true, language: 'python',readOnly: options.readOnly
            });
            this.editor.setValue(content)
            this.editor.layout()
            fileEditor.addEventListener('focusout', () => {
                this.setIsEdited(this.editor.getValue('serverFileEditor'+idEditor)!==this.contentOnServe)
            })

            if (options.save)
            fileEditor.addEventListener('keydown', (evt) => {
                this.setIsEdited(this.editor.getValue('serverFileEditor'+idEditor)!==this.contentOnServe)
                if ((window.isMac ? evt.metaKey : evt.ctrlKey) && evt.key == 's') {
                    this.save();
                    evt.preventDefault();
                    evt.stopPropagation();
                }
            }, true)
            idEditor++
        }
    }

    remove(){
        if (this.idEditor)CodeEditorProvider.removeEditor(this.idEditor)
        this.fileEditorDOM.parentElement.removeChild(this.fileEditorDOM)
    }

    reload() {
        if(this.options.reload){
            userFolder('read', { path: this.pathGetter() }, (data) => {
                if (!data.err) {
                    this.contentOnServe = data.data
                    if(this.imgView) this.imgView.src = data.data
                    else if(this.editor) this.editor.setValue(data.data)
                    this.setIsEdited(false)
                } else {
                    quickToast.fire({ icon: 'error', title: 'read file error : '+data.err })
                }
            })
        }
    }
}

export class ResourceMonitor {
    constructor() {
        this.section = document.createElement('custom-section')
        this.section.classList.add('title')
        this.section.style = "position: sticky; top: 0; background-color: var(--bg-2); z-index: 1; border-bottom: 2px solid var(--bg-4);"
        this.section.titleText = "resource monitor"

        this.cpu = this._buildCursor("#12b8f1")
        this.mem = this._buildCursor("#12f1b8")

    }


    _buildCursor(color) {
        let container = document.createElement('div')
        container.style = "border-radius: 4px;background-color: var(--bg-input);margin: 7px 7px;color: #000;text-shadow: 0 0 2px #fff;font-weight: bolder;"
        let bar = document.createElement('div')
        bar.style = "border-radius: 4px;background-color:" + color + ";width:0%;padding: 2px 10px;text-align: center;white-space: nowrap;"
        bar.innerText = "0%"
        container.appendChild(bar)
        this.section.addItem(container)
        return bar
    }

    stopAutoRefresh() {
        if (this.interval) {
            clearInterval(this.interval);
            this.interval = undefined
        }
    }

    refresh() {
        if (document.hasFocus()){
            fetchW('/api/monitor', { method: 'GET' }).then(res => res.json()).then(data => {
                if(data.err) return;
                this.cpu.style.width = data.cpu + '%'
                this.cpu.innerText = data.cpu?.toFixed(2) + '% CPU'
                this.mem.style.width = (data.activeMem * 100 / data.totalMem) + '%'
                this.mem.innerText = (data.activeMem)?.toFixed(2) + ' / ' + data.totalMem?.toFixed(2) + ' Go'
            })
        }
    }

    startAutoRefresh(ms) {
        if (!this.interval) {
            this.refresh()
            this.interval = setInterval(() => {
                if (this.section.offsetParent)
                    this.refresh()
            }, ms)
        }
    }
} 



export class RunInBackground {
    constructor(filesViewer) {
        this.filesViewer = filesViewer;
        this.mainSection = document.createElement('custom-section')
        this.mainSection.toggleable = true
        this.mainSection.classList.add('title')
        this.mainSection.titleText = "run in background"
        this.mainSection.deletable = true
        this.mainSection.classList.add('title')
        let btnRefresh = document.createElement('a')
        btnRefresh.classList.add('icon-reload')
        btnRefresh.addEventListener('click', () => this.refresh())
        this.mainSection._btnSlot.appendChild(btnRefresh);
        this.mainSection.deletable = false
        this.sections = {}

    }

    stopAutoRefresh() {
        if (this.interval) {
            clearInterval(this.interval);
            this.interval = undefined
        }
    }

    refresh(autoOpenToken) {
        fetchW('/api/statesnohup', { method: 'GET' }).then(res => res.json()).then(data => {
            for(const section of Object.values(this.sections)) section.clear()
            if(data.err) return;
            for(const [secName,infos] of Object.entries(data.data)){
                for(let info of infos){
                    if (!this.sections.hasOwnProperty(secName)) {
                        this.sections[secName] = document.createElement('custom-section')
                        this.sections[secName].titleText = secName
                        this.sections[secName].toggleable = true
                        this.mainSection.addItem(this.sections[secName])
                    }
                    let item = document.createElement('custom-item')
                    item.isRunningBackground = true
                    if (secName !== 'running') {
                        item.deletable = () => {
                            fetchW('/api/removenohup/'+info.token, { method: 'GET' }).then(res => res.json()).then(err => {
                                if(err.err)this.refresh()
                                else this.filesViewer.close(item)
                            })
                        }
                    }
                    info.status = secName
                    item.info = info
                    item.getFullPath = () => './.backgroundRun/' + info.token + '.log'
                    item.valueText = info.fileRun + ' ' + (new Date(info.date)).toLocaleString("fr-FR")
                    this.sections[secName].addItem(item)
                    const open = () => {
                        if(this.filesViewer.isOpen(item)) this.filesViewer.openFiles[item.getFullPath()].reload()
                        else userFolder('read', { path: item.getFullPath() }, (data) => {
                                if (data.err) quickToast.fire({ icon: 'error', title: 'read file error' })
                                else this.filesViewer.open(item, data.data, {tokenKillnohup:(info.status==='running')?info.token:false,readOnly:true,reload:true})
                            })
                    }
                    item._value.addEventListener('click', open)
                    if(info.token === autoOpenToken){
                         this.filesViewer.open(item, '', {tokenKillnohup:(info.status==='running')?info.token:false,readOnly:true,reload:true})
                         setTimeout(open,3000)
                    }
                }
            }
        })
    }
}


export class RunningNotebooks {
    constructor() {
        this.mainSection = document.createElement('custom-section')
        this.mainSection.toggleable = true
        this.mainSection.classList.add('title')
        this.mainSection.titleText = "running notebooks"
        this.mainSection.deletable = true
        this.mainSection.classList.add('title')
        let btnRefresh = document.createElement('a')
        btnRefresh.classList.add('icon-reload')
        btnRefresh.addEventListener('click', () => this.refresh())
        this.mainSection._btnSlot.appendChild(btnRefresh);
        this.mainSection.deletable = false

    }

    stopAutoRefresh() {
        if (this.interval) {
            clearInterval(this.interval);
            this.interval = undefined
        }
    }

    refresh() {
        fetchW('/api/notebook/getMyNotebooksRunning', { method: 'GET' }).then(res => res.json()).then(data => {
            if(data.err) return;
            this.mainSection.clear()
            for(const notebook of data.data){
                    let item = document.createElement('custom-item')
                    item.deletable = () => {
                        let info = { event: "kill from runningNotebook panel" };
                        fetchW("api/notebook/kill/" + notebook.id, {
                            method: 'POST', headers: { 'Content-Type': 'application/json' }, body: JSON.stringify(info)
                        }).then((response) => response.json()).then(()=>this.refresh())
                    }
                    item.valueText = '' + (new Date(notebook.lastUse)).toLocaleString("fr-FR")
                    item._btnDel.insertAdjacentHTML('beforebegin', `<span style="border-radius:3px;background-color:#12b8f1;padding: 0 3px 0 3px;color: black;margin: 0 3px 0 auto;">${notebook.cpu}%</span><span style="border-radius:3px;background-color:#12f1b8;padding: 0 3px 0 3px;color: black;margin: 0 3px 0 3px;">${notebook.mem}%</span>`)
                    item._btnDel.style.marginLeft = 0
                    this.mainSection.addItem(item)
                }
        })
    }
}