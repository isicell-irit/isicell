import { isCppCodeValid } from "../utils/utilsVaraibleName";
import { longToast } from '../guiComponent/notification'
import SearchManager from "./searchManager";
import stateMachine from "../diagrams/stateMachine";
import behaviorDiagram from "../diagrams/behaviorDiagram";
import { create_UUID } from "../utils/misc";


/*
function genCode(SubDiagKey, currentId, flows) {
    let labels = {};

    function recursive(currentId, flows, lvl, parents) {
        const targetFlows = flows.filter(f => f.source.id === currentId);
        const sourceFlows = flows.filter(f => f.target.id === currentId);
        const sourceFlowsLoop = flows.filter(f => f.target.id === currentId && !parents.includes(f.source.id));
        const element = sourceFlows[0]?.target;
        let code = [];
        const tab = '\t';
        let label = '';

        if (parents.includes(currentId)) {
            console.log(parents)
            label = `${SubDiagKey}_${currentId}_${labels[currentId]}`;
            code.push({ code: `${tab.repeat(lvl)}goto ${label};\n`, type: 'goto', label, id: currentId });
            return { code };
        }

        if (element) {
            if (sourceFlowsLoop.length > 0) {
                labels[currentId] = (labels[currentId] || 0) + 1;
                label = `${SubDiagKey}_${currentId}_${labels[currentId]}`;
                code.push({ code: `${tab.repeat(lvl)}${label}:\n`, type: 'label', label, id: currentId });
            }

            if (element.$type === 'bpmn:Task') {
                code.push({ code: `${tab.repeat(lvl)}${element.get('call')};\n`, type: 'task', id: currentId });
            }

            if (element.$type === 'bpmn:Task' && targetFlows.length === 1) {
                const ret = recursive(targetFlows[0].target.id, flows, lvl, [...parents, currentId]);
                code.push(...ret.code);
                return { code, error: ret.error };
            }

            if (element.$type === 'bpmn:Gateway') {
                if (targetFlows.length === 0) {
                    return { code, error: { idElement: [currentId], text: "condition without outgoing" } };
                }

                if (!element.get('name')) {
                    return { code, error: { idElement: [currentId], text: "condition activity without condition code" } };
                }

                const condition = targetFlows[0].condition === 'NO' ? `!(${element.get('name')})` : element.get('name');

                if (targetFlows.length === 1) {
                    code.push({ code: `${tab.repeat(lvl)}if(${condition}){\n`, type: 'if', id: currentId });
                    const yesCond = recursive(targetFlows[0].target.id, flows, lvl + 1, [...parents, currentId]);
                    code.push(...yesCond.code, { code: `${tab.repeat(lvl)}}\n`, type: 'end-if', id: currentId });
                    return { code, error: yesCond.error };
                }

                if (targetFlows.length === 2) {
                    const yesCond = recursive(targetFlows.find(f => f.condition === 'YES').target.id, flows, lvl + 1, [...parents, currentId]);
                    const noCond = recursive(targetFlows.find(f => f.condition === 'NO').target.id, flows, lvl + 1, [...parents, currentId]);

                    const { yesCode, noCode, convergenceId } = simplifiedCode(yesCond.code, noCond.code);

                    if(noCode.length === 0){
                        code.push({ code: `${tab.repeat(lvl)}if(${element.get('name')}){\n`, type: 'if', id: currentId },
                                    ...yesCode,
                                    { code: `${tab.repeat(lvl)}}\n`, type: 'end-if', id: currentId });
                    } else if(yesCode.length === 0){
                        code.push({ code: `${tab.repeat(lvl)}if(!(${element.get('name')})){\n`, type: 'if', id: currentId },
                                    ...noCode,
                                    { code: `${tab.repeat(lvl)}}\n`, type: 'end-if', id: currentId });
                    } else {
                        code.push({ code: `${tab.repeat(lvl)}if(${element.get('name')}){\n`, type: 'if', id: currentId },
                        ...yesCode,
                        { code: `${tab.repeat(lvl)}} else {\n`, type: 'else', id: currentId },
                        ...noCode,
                        { code: `${tab.repeat(lvl)}}\n`, type: 'end-if', id: currentId });
                    }

                    if (convergenceId) {
                        const ret = recursive(convergenceId, flows, lvl, [...parents, currentId]);
                        code.push(...ret.code);
                        return { code, error: yesCond.error || noCond.error || ret.error };
                    }

                    return { code, error: yesCond.error || noCond.error };
                }

                return { code };
            }
        } else if (targetFlows.length === 1) {
            const ret = recursive(targetFlows[0].target.id, flows, lvl, [...parents, currentId]);
            code.push(...ret.code);
            return { code, error: ret.error };
        }
        return { code };
    }

    function simplifiedCode(yesCode, noCode) {
        const yesIds = yesCode.map(c => c.id);
        const noIds = noCode.map(c => c.id);
        let convergenceId = null;

        for (let id of yesIds) {
            if (noIds.includes(id)) {
                convergenceId = id;
                break;
            }
        }

        if (convergenceId) {
            let yesIndex = yesIds.indexOf(convergenceId);
            let noIndex = noIds.indexOf(convergenceId);
            const prevYesIds =  yesIds.slice(0,yesIndex)
            const prevNoIds =  noIds.slice(0,noIndex)
            yesIndex += yesIds.slice(yesIndex).reduce((acc, id, index) => prevYesIds.includes(id)?index:acc, 0);
            noIndex += noIds.slice(noIndex).reduce((acc, id, index) => prevNoIds.includes(id)?index:acc, 0);

            if(convergenceId === yesCode[yesIndex].id && convergenceId === noCode[noIndex].id)
            return {
                yesCode: yesCode.slice(0, yesIndex),
                noCode: noCode.slice(0, noIndex),
                convergenceId
            };
        }

        return { yesCode, noCode, convergenceId: null };
    }

    let { code, error } = recursive(currentId, flows, 0, [], []);
    const gotoLabelList = code.filter(c => c.type === 'goto').map(c => c.label);
    code = code.filter(c => c.type !== 'label' || gotoLabelList.includes(c.label)).map(c => c.code).join('');

    return { code, error };
}
*/






















function genCode(SubDiagKey, currentId, flows) {
    let labels = {};

    const getElement = (currentId) => {
        const sourceFlows = flows.filter(f => f.target.id === currentId)
        return [sourceFlows[0]?.target,sourceFlows,flows.filter(f => f.source.id === currentId)]
    }
    function recursive(currentId, lvl, parents) {
        
        let [element,sourceFlows,targetFlows] = getElement(currentId)
        if(sourceFlows.length === 0 && targetFlows.length === 1){
            currentId = targetFlows[0].target.id;
            [element,sourceFlows,targetFlows] = getElement(currentId)
        }
        let code = [];
        const indents = '\t'.repeat(lvl);
        let label = '';
        let error = '';
        while(element!==undefined){
            
            if (sourceFlows.length > 1 && !parents.includes(currentId)) {
                labels[currentId] = (labels[currentId] || 0) + 1;
                label = `${SubDiagKey}_${currentId}_${labels[currentId]}`;
                code.push({ code: `${indents}${label}:\n`, type: 'label', label, id: currentId });
            }

            if (parents.includes(currentId)) {
                label = `${SubDiagKey}_${currentId}_${labels[currentId]}`;
                code.push({ code: `${indents}goto ${label};\n`, type: 'goto', label, id: currentId });
                currentId = undefined
            } else if (element.$type === 'bpmn:Task') {
                code.push({ code: `${indents}${element.get('call')};\n`, type: 'task', id: currentId });
                parents.push(currentId);
                currentId = targetFlows[0]?.target?.id;
            } else if (element.$type === 'bpmn:Gateway') {
                const condition = targetFlows[0].condition === 'NO' ? `!(${element.get('name')})` : element.get('name');
                if (targetFlows.length === 0) {
                    error = error || { idElement: [currentId], text: "condition without outgoing" };
                    currentId = undefined
                } else if (!element.get('name')) {
                    error = error || { idElement: [currentId], text: "condition activity without condition code" };
                    currentId = undefined
                } else if (targetFlows.length === 1) {
                    code.push({ code: `${indents}if(${condition}){\n`, type: 'if', id: currentId });
                    const yesCond = recursive(targetFlows[0].target.id, lvl + 1, [...parents, currentId]);
                    code.push(...yesCond.code, { code: `${indents}}\n`, type: 'end-if', id: currentId });
                    error = error || yesCond.error;
                    currentId = undefined;
                } else if (targetFlows.length === 2) {
                    const yesCond = recursive(targetFlows.find(f => f.condition === 'YES').target.id, lvl + 1, [...parents, currentId]);
                    const noCond = recursive(targetFlows.find(f => f.condition === 'NO').target.id, lvl + 1, [...parents, currentId]);

                    const { yesCode, noCode, convergenceId } = simplifiedCode(yesCond.code, noCond.code);

                    if(noCode.length === 0){
                        code.push({ code: `${indents}if(${element.get('name')}){\n`, type: 'if', id: currentId },
                                    ...yesCode,
                                    { code: `${indents}}\n`, type: 'end-if', id: currentId });
                    } else if(yesCode.length === 0){
                        code.push({ code: `${indents}if(!(${element.get('name')})){\n`, type: 'if', id: currentId },
                                    ...noCode,
                                    { code: `${indents}}\n`, type: 'end-if', id: currentId });
                    } else {
                        code.push({ code: `${indents}if(${element.get('name')}){\n`, type: 'if', id: currentId },
                        ...yesCode,
                        { code: `${indents}} else {\n`, type: 'else', id: currentId },
                        ...noCode,
                        { code: `${indents}}\n`, type: 'end-if', id: currentId });
                    }
                    parents = [...new Set([...code.map(c=>c.id)])]
                    currentId = convergenceId;
                    error = error || yesCond.error || noCond.error
                }
            }
            if(currentId) [element,sourceFlows,targetFlows] = getElement(currentId)
            else element = undefined
        }
        return { code, error };
    }

    function simplifiedCode(yesCode, noCode) {
        const yesIds = yesCode.map(c => c.id);
        const noIds = noCode.map(c => c.id);
        let convergenceId = null;

        for (let id of yesIds) {
            if (noIds.includes(id)) {
                convergenceId = id;
                break;
            }
        }

        if (convergenceId) {
            let yesIndex = yesIds.indexOf(convergenceId);
            let noIndex = noIds.indexOf(convergenceId);
            const prevYesIds =  yesIds.slice(0,yesIndex)
            const prevNoIds =  noIds.slice(0,noIndex)
            yesIndex += yesIds.slice(yesIndex).reduce((acc, id, index) => prevYesIds.includes(id)?index:acc, 0);
            noIndex += noIds.slice(noIndex).reduce((acc, id, index) => prevNoIds.includes(id)?index:acc, 0);

            if(convergenceId === yesCode[yesIndex].id && convergenceId === noCode[noIndex].id)
            return {
                yesCode: yesCode.slice(0, yesIndex),
                noCode: noCode.slice(0, noIndex),
                convergenceId
            };
        }

        return { yesCode, noCode, convergenceId: null };
    }
/*
    function simplifiedCode(yesCode, noCode) {
        const yesIds = yesCode.map(c => c.id);
        const noIds = noCode.map(c => c.id);
        let convergenceId = null;
        let convergenceIdNo = null;

        for (let id of yesIds) {
            if (noIds.includes(id)) {
                convergenceId = id;
                break;
            }
        }
        for (let id of noIds) {
            if (yesIds.includes(id)) {
                convergenceIdNo = id;
                break;
            }
        }
        
        if(convergenceId !== convergenceIdNo && convergenceId !== null){
            let sYes = yesIds.indexOf(convergenceId)+yesIds.indexOf(convergenceId)
            let sNo = yesIds.indexOf(convergenceIdNo)+yesIds.indexOf(convergenceIdNo)
            if(sYes > sNo)
                convergenceId = convergenceIdNo
        }

        if (convergenceId) {
            let yesIndex = yesIds.indexOf(convergenceId);
            let noIndex = noIds.indexOf(convergenceId);
            const prevYesIds =  yesIds.slice(0,yesIndex)
            const prevNoIds =  noIds.slice(0,noIndex)
            yesIndex += yesIds.slice(yesIndex).reduce((acc, id, index) => prevYesIds.includes(id)?index:acc, 0);
            noIndex += noIds.slice(noIndex).reduce((acc, id, index) => prevNoIds.includes(id)?index:acc, 0);

            if(yesCode[yesIndex].id === noCode[noIndex].id)
            return {
                yesCode: yesCode.slice(0, yesIndex),
                noCode: noCode.slice(0, noIndex),
                convergenceId
            };
        }
        return { yesCode, noCode, convergenceId: null };
    }*/

    let { code, error } = recursive(currentId, 0, []);
    const gotoLabelList = code.filter(c => c.type === 'goto').map(c => c.label);
    code = code.filter(c => c.type !== 'label' || gotoLabelList.includes(c.label)).map(c => c.code).join('');

    return { code, error };
}

/**
 * Singleton class managing the function calls and definitions
 * for the different categories
 */
class FunctionManager {

    // Singleton and list initialisation
    constructor(){
        this.categories = {}
        this.infoHeader = {}
        this.updater = []
    }

    /**
     * Adds or creates a function in the manager and returns the id of the function
     * @param {*} category The category of the function
     * @param {*} id The id of the function (-1 if new)
     * @param {*} code The code of the function
     */
    addOrUpdateFunction(category,id,code){
        if(!this.categories[category]) this.categories[category] = {}
        if(id===-1) {
            id=create_UUID();
        }
        let info = this.extractHeaderCppFunction(code);

        this.setInfo(id,info)
        this.categories[category][id] = code;
        if (!isCppCodeValid(code))
            longToast.fire({
                icon: 'warning',
                title: 'It seems your C++ code for '+info.functionName + ' may be faulty.'
            })
        this.update();
        SearchManager.updateIndexFunction(id, category, info.functionName+(info.params.length>0?'(...)':'()'),code)
        return id
    }

    /**
     * Checks fot function prototype availability and modifies the info and code if necessary
     * @param {*} info the info of the function to check
     * @param {*} category the category of the function to check
     * @param {*} code the full code of the function to check
     */
    checkFunctionAvailability(id, category, code){
        let info = this.extractHeaderCppFunction(code);
        let regex = /(?:(?<value>.+)_(?<count>\d+$))|(?<value2>.+)/;
        let oldName = info.functionName.replaceAll(/[^\w\d_]/g, '\\$&');
        info.functionName = info.functionName.replaceAll(/[^\w\d_]/g,'_');

        let checkFuncNameInfo = info.functionName.match(regex).groups;
        checkFuncNameInfo.value = checkFuncNameInfo.value ? checkFuncNameInfo.value : checkFuncNameInfo.value2;
        let copyCount = -1;
        let nameExisting = false;
        for(let funcId in this.categories[category]){
            if(funcId !== id){
                let funcInfo = this.getInfo(funcId);
                let funcNameInfo = funcInfo.functionName.match(regex).groups;
                funcNameInfo.value = funcNameInfo.value ? funcNameInfo.value : funcNameInfo.value2;

                nameExisting |= (funcInfo.functionName === info.functionName);
                if(funcNameInfo.value === checkFuncNameInfo.value){
                    let count = (funcNameInfo.count === undefined) ? 0 : Number(funcNameInfo.count);
                    copyCount = Math.max(copyCount,count);
                }
            }
        }

        if(copyCount >= ((checkFuncNameInfo.count === undefined) ? -1 : Number(checkFuncNameInfo.count))){
            copyCount++;
            let checkedFuncName = info.functionName;
            if(nameExisting)
                checkedFuncName = checkFuncNameInfo.value+((copyCount>0) ? "_"+copyCount : "");

            let replaceRegex = new RegExp('(?<=\\s|\\*|&|^)'+oldName+'(?=\\(|<)','g');
            info.functionName = checkedFuncName;
            code = code.replaceAll(replaceRegex, checkedFuncName);
        }

        return {info, code};
    }

    /**
     * Add a function call the the updater
     * @param {*} fun The function to add to the updater
     */
    addUpdater(fun){
        this.updater.push(fun)
    }
    /** Calls all functions in the updater */
    update(){
        for(var fun of this.updater){
            fun()
        }
    }

    /**
     * Returns the code for a wanted function
     * @param {*} category The category to which the wanted function belongs
     * @param {*} id The id of the function
     */
    getCode(cathegory,id){
        return this.categories[cathegory][id]
    }

    /**
     * Returns the info for a wanted function
     * @param {*} id The id of the function
     */
    getInfo(id){
        return this.infoHeader[id]
    }

    /**
     * Sets the info for a function
     * @param {*} id The id of the function
     * @param {*} info The info to set
     */
    setInfo(id,info){
        return this.infoHeader[id] = info
    }
    /**
     * remove the info for a function
     * @param {*} id The id of the function
     */
    removeInfo(id) {
        delete this.infoHeader[id]
    }

    /**
     * Returns the call for the wanted function
     * @param {*} category The category of the function (obsolete as of now)
     * @param {*} id The id of the function
     * @param {*} prevFunCall The call know by the diagram before calling this function
     */
    getFunctionCall(category,id,prevFunCall=''){
        var info = this.getInfo(id)
        if(prevFunCall !== ''){ // If there was a previous call
            try { // Returns the new call without destroying return variables or such
                var regex = new RegExp(`(?<variable>.*)?${info.functionName}\\((?<params>.*)\\)`,'m');
                var res = regex.exec(prevFunCall).groups
                console.log(res)
                res.paramsArr = res.params.split(',').map((val,ind,arr)=>arr[ind] = val.trim());
                if(res.paramsArr[0] === "") res.paramsArr = [];

                var ret = ''
                //ReturnType
                if(info.returnType!=='void')
                    ret += (res.variable?res.variable:'');
                //FunctionName
                ret += info.functionName+'('
                //Parameters
                let paramDiff = info.params.length - res.paramsArr.length;
                if(paramDiff > 0){
                    let params = [...res.paramsArr];
                    for(let i=res.paramsArr.length; i<info.params.length; ++i)
                        params.push("_"+info.params[i]);
                    ret += params.join(', ');
                }
                else if(paramDiff < 0){
                    let params = [...res.paramsArr];
                    params = params.slice(0, info.params.length);
                    ret += params.join(', ');
                }
                else{
                    ret += res.paramsArr.join(', ');
                }
                //THE END
                ret += ')'
                return ret
            } catch(e) {
                console.error("Error in getFunctionCall", e);
                return undefined;
            }
        } else { // If it's the first time just return the call
            return info.functionName+'('+info.params.join(', ')+')'
        }
    }

    parseFunctionCall(call){
        var regex = / *(?:(?<variable>[^=]+) *[=| ])* *(?<functionName>\w+)\((?<params>.*)\)/m;
        var res = regex.exec(call).groups;
        res.params = res.params.split(',').map((val,ind,arr)=>arr[ind] = val.trim());
        return res;
    }

    /**
     * Returns the header of a C++ function
     * @param {*} code The C++ function to process
     */
    extractHeaderCppFunction(code){
        try {
            // var regex = /(?:\w* )*(?<returnType>.+) +(?<functionName>\w+) *\((?<params>.*)\)/m;
            var regex = /(inline +)?(?<returnType>.+)(?<pointer>[ \*]) *((?<namespace>.*)::)?(?<functionName>[\w_\d]+?) *\((?<params>.*?)\)/m; //? To take spaces in types into account ?
            var res = regex.exec(code).groups
            res.returnType = res.returnType + (res.pointer==='*'?'*':'')
            delete res.pointer
            // if(res.params)res.params = Array.from(res.params.matchAll(/[\w+|*]\s+(\w+)/gm), m => m[1]);
            if(res.params)res.paramsWithTypes = res.params.split(/\s*,\s*/).map(val=>val.match(/^(?<type>[\w|*|<|>\s]+[ |\*])(?<name>\w+)$/)?.groups);
            if(res.params)res.params = Array.from(res.params.matchAll(/(?<=\s|&|\*)\w+(?=\s*,|\s*$)/gm), m => m[0]);
            else res.params = []
            return res
        } catch {
            console.log(code)
            return undefined;
        }
    }

    /**
     * Returns the processed code {code,error:{idElements,text}}
     * @param {*} idStart The id of the first element of the graph
     * @param {*} flows The complete flow graph
     */
    generateCode(SubDiagKey,idStart,flows){
        var ret = genCode(SubDiagKey,idStart,flows)
        return {code:ret.code,error:ret.error,text:ret.text}
    }

    /**
     * Serialize all the functions in a category
     * @param {*} category The category to serialize
     */
    serializeCategory(category){
        let catJson = this.getCategory(category)
        let str = JSON.stringify(catJson)
        str = str.replaceAll('\ \ \ \ ', '\\t')
        return str
    }

    /**
     * Returns the functions in a category
     * @param {*} category The category to return
     */
    getCategory(category){
        if(this.categories[category]) return this.categories[category]
        else return {}
    }

    /**
     * Returns the functions in a category
     * @param {*} category The category to return
     */
    getCategoryFormatted(category){
        if(this.categories[category])
            return JSON.parse(this.serializeCategory(category))
        else
            return {}
    }

    /**
     * Completes the manager with the functions from the json
     * @param {*} category The category of the functions
     * @param {*} json The serialized json with all the functions from a category
     */
    loadJson(category,json){
        for(let id in json){
            this.addOrUpdateFunction(category,id,json[id])
        }
    }

    forAllFunctions(category, func){
        let functions = this.getCategory(category);
        for(let key of Object.keys(functions))
            func(key, functions[key]);
    }

    /**
     * Removes a function from the manager
     * @param {*} category The category of the function
     * @param {*} id The id of the function
     */
    deleteFunction(category,id){
        SearchManager.removeIndexFunction(id)
        delete this.categories[category][id]
    }

    /**
     * Clears all the functions from the manager
     */
    clear(){
        for(let category in this.categories){
            this.categories[category] = {}
        }
        SearchManager.clearIndexFunction()
    }

    refactorCode(oldValue, newValue, cat) {
        let callRegex = new RegExp("((?<=\\s|\\W|^)" + oldValue + "(?=\\s|\\W|$)(?!\\())", "g");
        if (newValue !== oldValue && oldValue !== '') {
            //* Update behaviorDiagram xml
            stateMachine.forEachElement(cat, (xml, code, element) => {
                let res = {};

                res['xml'] = xml.replaceAll(callRegex, newValue);
                res['code'] = code.replaceAll(callRegex, newValue);
                return res;
            }, false)

            //* Update Functions
            this.forAllFunctions(cat, (id, code) => {
                let newCode = code.replace(callRegex, newValue);
                if (newCode !== code)
                    this.addOrUpdateFunction(cat, id, newCode);
            })

            //* Update stateMachine Labels
            if (cat === 'cellFunction')
                stateMachine.forEachLabel((label) => {
                    let oldLabel = label.businessObject.name
                    let newLabel = oldLabel.replace(callRegex, newValue);

                    if (newLabel !== oldLabel) {
                        label.businessObject.name = newLabel;
                        stateMachine.modeling.updateProperties(label, {
                            name: newLabel
                        });
                    }
                })

            if (behaviorDiagram.currentState !== undefined && behaviorDiagram.category === cat)
                stateMachine.viewElement(behaviorDiagram.currentState, cat);
        }
    }
}

const functionManager = new FunctionManager();
export default functionManager;
