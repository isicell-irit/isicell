import CodeEditorProvider from "../guiComponent/codeEditorProvider";
import PanelManager from "../guiComponent/panelManager";
import { fetchW, streamJson } from "../utils/misc";
import Parameters from "./ParametersManager";
import FunctionManager from "./functionManager";
import '../css/explorer.css'

//helper functions, it turned out chrome doesn't support Math.sgn() 
function signum(x) {
    return (x < 0) ? -1 : 1;
}
function absolute(x) {
    return (x < 0) ? -x : x;
}

let nbColLeaf = 5;
let nbColTree = 5;

function drawPath(svg, path, startX, startY, endX, endY) {
    // get the path's stroke width (if one wanted to be  really precize, one could use half the stroke size)
    var stroke = parseFloat(path.getAttribute("stroke-width"));
    // check if the svg is big enough to draw the path, if not, set heigh/width
    if (svg.getAttribute("height") < endY) svg.setAttribute("height", endY);
    if (svg.getAttribute("width") < (startX + stroke + 100)) svg.setAttribute("width", (startX + stroke + 100));
    if (svg.getAttribute("width") < (endX + stroke + 100)) svg.setAttribute("width", (endX + stroke + 100));

    var deltaX = (endX - startX) * 0.15;
    var deltaY = (endY - startY) * 0.15;
    // for further calculations which ever is the shortest distance
    var delta = deltaY < absolute(deltaX) ? deltaY : absolute(deltaX);

    // set sweep-flag (counter/clock-wise)
    // if start element is closer to the left edge,
    // draw the first arc counter-clockwise, and the second one clock-wise
    var arc1 = 0; var arc2 = 1;
    if (startX > endX) {
        arc1 = 1;
        arc2 = 0;
    }
    // draw tha pipe-like path
    // 1. move a bit down, 2. arch,  3. move a bit to the right, 4.arch, 5. move down to the end 
    path.setAttribute("d", "M" + startX + " " + startY +
        " V" + (startY + delta) +
        " A" + delta + " " + delta + " 0 0 " + arc1 + " " + (startX + delta * signum(deltaX)) + " " + (startY + 2 * delta) +
        " H" + (endX - delta * signum(deltaX)) +
        " A" + delta + " " + delta + " 0 0 " + arc2 + " " + endX + " " + (startY + 3 * delta) +
        " V" + (endY - 10));
}

function connectElements(startElem, endElem, pathId, idChild) {
    var svg = document.getElementById('svg-tree')
    //var pathId = 'path_' + startElem.id + '-' + endElem.id
    var path = document.getElementById(pathId)
    if (path === null) {
        path = document.createElementNS("http://www.w3.org/2000/svg", 'path')
        path.id = pathId
        path.setAttribute('stroke', "var(--theme)")
        path.setAttribute('fill', "none")
        path.setAttribute('stroke-width', "5px")
        path.setAttribute('d', "M0 0")
        path.setAttribute('marker-end', 'url(#pointer)')
        path.dataset.child = idChild
        svg.appendChild(path)
    }
    var svgContainer = document.getElementById("exploreView");

    // if first element is lower than the second, swap!
    if (startElem.offsetTop > endElem.offsetTop) {
        var temp = startElem;
        startElem = endElem;
        endElem = temp;
    }

    // get (top, left) corner coordinates of the svg container   
    var svgTop = 0;//svgContainer.offsetTop;
    var svgLeft = 0;//svgContainer.offsetLeft;

    // calculate path's start (x,y)  coords
    // we want the x coordinate to visually result in the element's mid point
    var startX = startElem.offsetLeft + 0.5 * startElem.offsetWidth - svgLeft;    // x = left offset + 0.5*width - svg's left offset
    var startY = startElem.offsetTop + startElem.offsetHeight - svgTop;  
    // calculate path's end (x,y) coords
    var endX = endElem.offsetLeft + 0.5 * endElem.offsetWidth - svgLeft;
    var endY = endElem.offsetTop - svgTop - 5;
    // y = top offset + height - svg's top offset
    //console.log(startElem, endElem);
    //console.log(svgTop,svgLeft, startX, startY,endX, endY);
    // call function for drawing the path
    drawPath(svg, path, startX, startY, endX, endY);

}
function connectClean() {
    const svgTree = document.getElementById("svg-tree")
    if (svgTree) {
        svgTree.querySelectorAll('path[data-child]').forEach((p) => {
            if (document.getElementById(p.dataset.child) === null) p.parentElement.removeChild(p)
        })
    }
}



function connectAll() {
    const svgTree = document.getElementById("svg-tree")
    if(svgTree){
        svgTree.setAttribute("height", "0");
        svgTree.setAttribute("width", "0");
        // connect all the paths you want!
        document.querySelectorAll('.item-tree').forEach(function (item) {
            if (item.hasAttribute('data-parent') && (document.getElementById(item.dataset.parent) !== null)) {
                var parentItem = document.getElementById(item.dataset.parent)
                connectElements(item.querySelector('div'), parentItem.querySelector('div'), item.id + '-' + parentItem.id, item.id)
            }
        })
    }
}

window.addEventListener("resize", function (event) {
    // reset svg each time 
    connectAll();
});



function valueToString(value) {
    if (value == 0) return "0.00"
    const valueAbs = Math.abs(value)
    return (value > 0 ? '' : '-') + (valueAbs > 1 ? valueAbs.toFixed(2) : valueAbs.toFixed(-1 * Math.floor(Math.log10(valueAbs)) + 1))
}

function getArrow(viewBox, x, p, mp, deltaValue, text, value) {
    const x0 = viewBox[0] / 2// * baseRate
    const d = viewBox[0] * 0.025 * (x > 0 ? 1 : -1)
    const w = (viewBox[0] * 0.95 / 2) * x - d
    const x1 = x0 + w
    const h = viewBox[1] / mp
    const y0 = h * p
    const y1 = y0 + h
    let color, anchorText, anchorDelta, xDelta, sign,colorDelta='fill="var(--text-2)"';
    if (x < 0) {
        color = "#c72272"
        anchorText = "start"
        if (Math.abs(x) > 0.5) {
            anchorDelta = "middle"
            colorDelta=''
            xDelta = x0 + w / 2
        } else {
            anchorDelta = "end"
            xDelta = x1 + d * 2
        }
        sign = ''
        text = +valueToString(value) + '&nbsp;&nbsp;<tspan font-size="6">' + text + '</tspan>'
    } else {
        color = "#1cad58"
        anchorText = "end"
        if (Math.abs(x) > 0.5) {
            anchorDelta = "middle"
            colorDelta=''
            xDelta = x0 + w / 2
        } else {
            anchorDelta = "start"
            xDelta = x1 + d * 2
        }
        sign = '+'
        text = '<tspan font-size="6">' + text + '</tspan>&nbsp;&nbsp;' + valueToString(value)
    }
    return `<polygon points="${x0},${y1} ${x1},${y1} ${x1 + d},${y0 + h / 2} ${x1},${y0} ${x0},${y0}" fill="${color}" stroke="black" stroke-width="0.25"/>
                <text text-anchor="${anchorDelta}" ${colorDelta} x="${xDelta}" y="${y0 + 0.75 * h}" font-family="system-ui" font-size="10">${sign + valueToString(deltaValue)}</text>
                <text text-anchor="${anchorText}" x="${x0 - d}" y="${y0 + 0.75 * h}" font-family="system-ui" font-size="10" fill="var(--text)">${text}</text>`//${x0 + d},${y0 + h / 2}
}


function getSVGwidget(prevVal, range, newVal) {
    const keyRange = Object.keys(range)
    const viewBox = [200, 15 * keyRange.length]
    let x;
    let svg = `<svg viewBox="0 0 ${viewBox[0]} ${viewBox[1]}" xmlns="http://www.w3.org/2000/svg">`
    for (let i in keyRange) {
        const key = keyRange[i]
        if (newVal[key] >= prevVal[key]) {
            x = (newVal[key] - prevVal[key]) / (range[key][1] - prevVal[key])
        } else {
            x = -(prevVal[key] - newVal[key]) / (prevVal[key] - range[key][0])
        }
        svg += getArrow(viewBox, x, i, keyRange.length, newVal[key] - prevVal[key], key, newVal[key])
    }
    svg += '</svg>';

    var template = document.createElement('template');
    template.innerHTML = svg;
    return template.content.firstChild;
}








function resetRangeSelector() {
    document.getElementById('selected-item').querySelectorAll('li.range').forEach((li) => { li.parentElement.removeChild(li) })
    document.getElementById('selected-item').style.display = 'none';
}

function getSelectedRange() {
    const formRange = document.getElementById('selected-item')
    const names = formRange.querySelectorAll("span[name='nameparameters']")
    const mins = formRange.querySelectorAll("input[name='minparameters']")
    const maxs = formRange.querySelectorAll("input[name='maxparameters']")
    const range = {}
    for (let i = 0; i < names.length; ++i) {
        range[names[i].innerText] = [parseFloat(mins[i].value), parseFloat(maxs[i].value)]
    }
    return range
}

function updateDefaultValue(data) {
    const formRange = document.getElementById('selected-item')
    const names = formRange.querySelectorAll("span[name='nameparameters']")
    const defaultvalues = formRange.querySelectorAll("span[name='defaultvalue']")
    for (let i = 0; i < names.length; ++i) {
        let pathJSON = names[i].innerText.split('.')
        let defaultValue = data
        for (let key of pathJSON) defaultValue = defaultValue[key]
        defaultvalues[i].textContent = ' [default : ' + defaultValue + ']'
    }
}

function validRange() {
    var range = getSelectedRange()
    var keys = Object.keys(range)
    if (keys.length < 1) return false
    for (var key of keys) {
        if (isNaN(range[key][0])) return false
        if (isNaN(range[key][1])) return false
    }
    return true
}

function root() {
    document.getElementById('btnRootExplore').style.display = 'none'
    const content = document.createElement('div')
    const img = document.createElement('img')
    img.src = "img/loading-buffering2.gif"
    content.appendChild(img)
    const item = buildTreeElement(0, content, 'tree_root')
    const params = JSON.parse(CodeEditorProvider.getValue('editorJson'))
    params['exploration'] = { functionPYTHON: CodeEditorProvider.getValue('editorPython') }
    computeServerSide('tree_root',params, (data) => {
        if(document.getElementById('tree_root')){
            img.src = "data:image/png;base64," + data.image
            item.querySelector('input').data = data.input
            img.className = 'zoom'
            document.getElementById('selected-item').style.display = null;
        } else {
            document.getElementById('btnRootExplore').style.display = null
            document.getElementById('selected-item').style.display = 'none';
        }
    }, () => {
        document.getElementById('btnRootExplore').style.display = null
        document.getElementById('selected-item').style.display = 'none';
        item.remove()

    })
}
function test() {
    if (validRange()) {
        let params;
        let nb = parseInt(document.getElementById('numberPlot').value)
        let treeElement = document.querySelector('input[name="tree"]:checked')
        let idTreeParent = treeElement.parentElement.id
        let numRow = parseInt(treeElement.parentNode.parentNode.dataset.numrow)
        let leafChecked = document.querySelector('input[name="leaf_' + idTreeParent + '"]:checked')
        if (leafChecked === null) {
            params = { 'input': treeElement.data }
            run(treeElement.parentElement.id, params, getSelectedRange(), nb)
        } else {
            params = { 'input': leafChecked.data }
            const idParent = addTreeItem(leafChecked.parentElement.id, numRow)
            hideAllRowLeaf()
            run(idParent, params, getSelectedRange(), nb)
        }
        resetRangeSelector()
    }
}

function run(idTree, params, range, nb) {
    const paramsCurrent = { ...params }
    const flattenParams = JSON.flatten(paramsCurrent.input)
    paramsCurrent['exploration'] = { nb: nb, range: range, functionPYTHON: CodeEditorProvider.getValue('editorPython') }
    addEmptyRow(idTree, nb)
    document.getElementById('row_leaf_' + idTree).isRunning = true
    computeServerSide(idTree,paramsCurrent, (data) => {
        updateLeafFigure(idTree, data, flattenParams, range)
    },() => {
        document.getElementById('row_leaf_' + idTree).remove()
        document.getElementById('selected-item').style.display = null;
    })
}


function hideAllRowLeaf() {
    [...document.getElementById('fig-leaf').children].forEach(function (item) { item.style.display = 'none' })
}

function showRowLeaf(idTree) {
    document.getElementById('row_leaf_' + idTree).style.display = 'block';
}

function addTreeItem(leafId, numRow) {
    const currentNumRow = numRow + 1

    const leafItem = document.getElementById(leafId)
    const content = document.createElement('div')
    const svg = leafItem.querySelector('svg').cloneNode(true)
    const img = leafItem.querySelector('img').cloneNode(true)
    img.removeAttribute('id')
    content.appendChild(svg)
    content.appendChild(img)
    const item = buildTreeElement(currentNumRow, content, uuidv4(), leafItem.dataset.parent)
    item.querySelector('input').data = leafItem.querySelector('input').data
    connectAll()
    return item.id
}

function buildTreeElement(numRow, content, id, idParent) {
    var rowDom = document.getElementById('row_tree_' + numRow)
    if (rowDom === null) {
        rowDom = document.createElement('div')
        rowDom.id = 'row_tree_' + numRow
        rowDom.className = 'row-tree'
        rowDom.dataset.numrow = numRow
        document.getElementById('fig-tree').appendChild(rowDom);
    }
    var item = document.createElement('label')
    item.style.width = `calc(${100/nbColTree}% - ${18*(nbColTree-1)/nbColTree}px)`
    var input = document.createElement('input')
    input.type = 'radio'
    input.name = 'tree'
    input.checked = true
    input.addEventListener('change', function (event) {
        [...document.getElementById('fig-leaf').children].forEach(function (item) { item.style.display = 'none' })
        let rl = document.getElementById('row_leaf_' + this.parentElement.id)
        if (rl !== null) {
            rl.style.display = null
            let leafChecked = document.querySelector('input[name="leaf_' + this.parentElement.id + '"]:checked')
            if (leafChecked !== null) document.getElementById('selected-item').style.display = 'block';
            else document.getElementById('selected-item').style.display = 'none';
            CodeEditorProvider.setValue('editorJson', JSON.stringify({ input: this.data }, null, 2))
            updateDefaultValue(this.data)
        } else {
            console.log('row_leaf_' + this.parentElement.id, 'not exist')
        }
    })
    item.appendChild(input)
    item.appendChild(content)
    if (idParent) item.dataset.parent = idParent
    var a = document.createElement('a')
    a.innerHTML = '&#10006;'
    a.addEventListener('click', (e) => { cutTree(id);  })
    content.appendChild(a)
    var img = content.querySelector('img')
    if (idParent) img.className = 'zoom'
    content.addEventListener('mouseover', (e) => { if (e.target !== img) a.style.display = 'block' })
    content.addEventListener('mouseleave', (e) => { a.style.display = 'none' })
    item.className = 'item-tree'
    item.id = id
    if (numRow > 1) {
        var rowPrev = document.getElementById('row_tree_' + (numRow - 1))
        if (rowDom.children.length > 0 && rowPrev.children.length > 1) {
            var indexParent = Array.prototype.indexOf.call(rowPrev.children, document.getElementById(idParent))
            if (indexParent < rowPrev.children.length - 1) {
                var p;
                for (p = 0; p < rowDom.children.length; p++) {
                    if (rowDom.children[p].dataset.parent === rowPrev.children[indexParent + 1].id)
                        break;
                }
                if (p < rowDom.children.length) {
                    rowDom.insertBefore(item, rowDom.children[p]);
                } else {
                    rowDom.appendChild(item)
                }
            } else {
                rowDom.appendChild(item)
            }
        } else {
            rowDom.appendChild(item)
        }
    } else {
        rowDom.appendChild(item)
    }
    return item
}

function cutTree(id) {
    let deletable = true
    document.getElementById('fig-tree').querySelectorAll('label[data-parent="' + id + '"]').forEach((label) => {
        deletable&&=cutTree(label.id)
    })
    if(id!=='tree_root') deletable&&= !document.getElementById('row_leaf_' + id).isRunning
    if(deletable){
        var rowLeaf = document.getElementById('row_leaf_' + id)
        if (rowLeaf !== null) rowLeaf.parentElement.removeChild(rowLeaf)
        var treeItem = document.getElementById(id)
        treeItem.parentElement.removeChild(treeItem)
        if (id === 'tree_root') {
            document.getElementById('btnRootExplore').style.display = null
            document.getElementById('selected-item').style.display = 'none';
        }
        connectClean()
        connectAll()
    }
    return deletable
}

function addEmptyRow(idTree, nb) {
    const contentDiv = document.createElement('div')
    contentDiv.id = 'row_leaf_' + idTree
    contentDiv.className = 'row'
    document.getElementById('fig-leaf').append(contentDiv)
    for (let i = 0; i < nb; i++) {
        addEmptyImage(contentDiv, idTree, i)
    }
}

function addEmptyImage(contentDiv, idTree, numImage) {
    const id = getImageId(idTree, numImage)
    const label = document.createElement('label')
    label.style.width = `calc(${100/nbColLeaf}% - ${18*(nbColLeaf-1)/nbColLeaf}px)`
    label.id = id
    label.dataset.parent = idTree
    const input = document.createElement('input')
    const div = document.createElement('div')
    input.type = "radio"
    input.name = 'leaf_' + idTree
    input.id = 'input_' + id
    input.disabled = true
    var img = document.createElement('img')
    img.id = 'img_' + id
    img.src = "img/loading-buffering2.gif"
    input.addEventListener('change', function (event) {
        document.getElementById('selected-item').style.display = 'block';
        CodeEditorProvider.setValue('editorJson', JSON.stringify({ input: this.data }, null, 2))
        updateDefaultValue(this.data)
    })
    div.append(img)
    label.append(input)
    label.append(div)
    contentDiv.append(label)
}

function updateLeafFigure(idTree, data,flattenParams, range) {
    const id = getImageId(idTree, data.index)
    var inputDOM = document.getElementById('input_' + id)
    inputDOM.data = data.input
    inputDOM.disabled = false
    var imgDOM = document.getElementById('img_' + id)
    imgDOM.parentNode.insertBefore(getSVGwidget(flattenParams, range, JSON.flatten(data.input)), imgDOM);
    imgDOM.src = "data:image/png;base64," + data.image
    imgDOM.className = 'zoom'
}

function getImageId(idTree, numImage) {
    return 'leaf_' + idTree + '_' + numImage
}


function computeServerSide(idTree,paramsCurrent, callback, callbackErr) {
    exploreManager.pythonCode = CodeEditorProvider.getValue('editorPython')
    const domDebug = document.getElementById("explorePythonDebug")
    domDebug.style.display = 'none'
    connectAll()
    domDebug.textContent = ''
    
    streamJson('/api/runPython/' + exploreManager.tokenBuild, {
        method: 'POST',
        headers: {'Accept': 'application/json','Content-Type': 'application/json'},
        body: JSON.stringify(paramsCurrent)
    },{
        callBackouput:(jsonValue)=>{
            for (let v of jsonValue) {
                if(!v.err) callback(v)
                else {
                    domDebug.textContent += v.err+'\n'
                    domDebug.style.display = null
                    callbackErr()
                    connectAll()
                }
            }
        },
        callBackDone:()=>{
            if(idTree!=='tree_root') document.getElementById('row_leaf_' + idTree).isRunning = false
        }
    })
}
function uuidv4() {
    return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
        (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    );
}


class ExploreManager {

    constructor() {  }

    init(mainContainer, defaultData) {
        const sidePanel = document.createElement('div')
        sidePanel.id = 'exploreActivity'
        mainContainer.appendChild(sidePanel)

        PanelManager.createSplitterContainer('contentView', 'exploreGUI', false);
        this.mainPanelManager = new PanelManager("exploreGUI", false);
        const editorsPanel = this.mainPanelManager.createPanel("editorPythonJson");
        const viewPanel = this.mainPanelManager.createPanel("exploreView");
        this.mainPanelManager.setWidthPanel([400, 100], [true, false])
        document.getElementById("exploreGUI").style.overflowY = "auto";
        viewPanel.style.position = "relative";
        
        PanelManager.createSplitterContainer('editorPythonJson', 'editorPythonJsonSplit', true);
        this.editorPanelManager = new PanelManager("editorPythonJsonSplit", true);
        this.editorPanelManager.createPanel("editorPython");
        this.editorPanelManager.createPanel("editorJson");
        this.editorPanelManager.setWidthPanel([100, 100], [false, false])

        this.currentNode = null;


        CodeEditorProvider.createEditor('editorPython', { lineNumbers: 'on', minimap: false, language: 'python' });
        CodeEditorProvider.createEditor('editorJson', { lineNumbers: 'on', minimap: false, language: 'json' }); 
        
    }
    

    run(paramItem) {
        selectChangeNbCol
        this.clear()
        this.paramItem = paramItem;
        CodeEditorProvider.setValue('editorPython', this.pythonCode)

        CodeEditorProvider.addCompletionProvider('python', () => {
            let valideType = ['bool', 'double', 'int', 'void']
            valideType = [...valideType, ...valideType.map(vt => 'vector<' + vt + '>')]
            valideType = [...valideType, ...valideType.map(vt => 'vector<' + vt + '>'),...Object.keys(paramItem.parent.finalJson.enums||{})]
            return [...paramItem.parent.finalJson.cell.attributes.filter(item =>valideType.includes(item.type.replace(' ',''))).map((item) => {
                return {
                    label: 'simu.cells.get' + item.value.charAt(0).toUpperCase() + item.value.slice(1)+'()',
                    insertText: 'simu.cells.get' + item.value.charAt(0).toUpperCase() + item.value.slice(1) + '()',
                    documentation: ''
                }
            }),
              , { label: 'simu.getCells()', insertText:'simu.getCells()', documentation: '' },
                { label: 'simu.cells.getPosition()', insertText: 'simu.cells.getPosition()', documentation: '' },
                { label: 'simu.cells.getState()', insertText: 'simu.cells.getState()', documentation: '' },
                { label: 'simu.cells.getType()', insertText: 'simu.cells.getType()', documentation: '' },
                { label: 'simu.getCurrentStep()', insertText: 'simu.getCurrentStep()', documentation: '' },
                { label: 'simu.update(nbSteps)', insertText: 'simu.update()', documentation: '' },
                ...Object.values(paramItem.parent.finalJson.scenario.functions).map(i => FunctionManager.extractHeaderCppFunction(i)).filter(i => valideType.includes(i.returnType.replace(' ', ''))).map((item) => {
                    return {
                        label: 'simu.' + item.functionName + '(' + item.params.join(',') +')',
                        insertText: 'simu.' + item.functionName + '(' + item.params.join(',') + ')',
                        documentation: ''
                    }
                })]
        })
        CodeEditorProvider.setEditorCompletionProvider('editorPython', 'python')


        document.getElementById('btnRootExplore').onclick = root
        document.getElementById('btnRunExplore').onclick = test

        const selectNbColl = document.getElementById('selectChangeNbCol')
        let opt = ''
        for(let i=2; i<8;i++) opt+=`<option value="${i}">${i}</option>`
        selectNbColl.innerHTML = opt
        selectNbColl.value=nbColLeaf
        selectNbColl.onchange = ()=>this.changeNumberOfColumns(selectNbColl.value)

        document.getElementById('parameters-select').addEventListener('focus', function () {
            this.innerHTML = ''
            var params = JSON.flatten(JSON.parse(CodeEditorProvider.getValue('editorJson')).input)
            for (const p in params) {
                this.add(new Option(p, p))
            }
            this.value = undefined
        })

        document.getElementById('parameters-select').addEventListener('change', function (event) {
            let pathJSON = event.target.value.split('.')
            let defaultValue = this.currentNode?.data || JSON.parse(CodeEditorProvider.getValue('editorJson'))['input']
            for (const key of pathJSON) defaultValue = defaultValue[key]

            const li = document.createElement('li')
            li.className = 'range'
            li.innerHTML = '<div><span name="nameparameters">' + event.target.value + '</span><span name="defaultvalue"> [default : ' + defaultValue +']</span></div><input name="minparameters" type="text"><input name="maxparameters" type="text"><a onclick="this.parentNode.parentNode.removeChild(this.parentNode);">&#10006;</a>';
            document.getElementById('selected-item').insertBefore(li, this.parentNode)
        })

        document.getElementById('fig-leaf').addEventListener('click', function (e) {
            if (e.target.nodeName === 'DIV') {
                this.querySelectorAll('input:checked').forEach((item) => { item.checked = false; })
                document.getElementById('selected-item').style.display = 'none';
                this.currentNode = this.querySelector('input:checked')
            }
        })

        document.getElementById('fig-tree').addEventListener('click', function (e) {
            var checked = this.querySelector('input:checked')
            if (checked !== null) {
                if (checked.parentElement.contains(e.target)) {
                    this.currentNode = checked
                    CodeEditorProvider.setValue('editorJson', JSON.stringify({ input: checked.data }, null, 2))
                    updateDefaultValue(this.currentNode.data)
                }
            }
        })
        CodeEditorProvider.setValue('editorJson', JSON.stringify({ input: Parameters.getParamFromItemParam(paramItem) }, null, 2))

    }

    changeNumberOfColumns(nbCol){
        nbColLeaf = nbColTree = nbCol
        const newWidth = `calc(${100/nbCol}% - ${18*(nbCol-1)/nbCol}px)`
        document.getElementById('fig-leaf').querySelectorAll('label').forEach(e=>e.style.width=newWidth)
        document.getElementById('fig-tree').querySelectorAll('label').forEach(e=>e.style.width=newWidth)
        connectAll();
    }



    clear(){

        document.getElementById('exploreView').innerHTML = `<svg id="svg-tree" style="z-index: 0; position: absolute;" width="0" height="0"><defs><marker id="pointer" markerUnits="strokeWidth" markerWidth="10" markerHeight="10" orient="auto" refY="5" viewBox="0 0 30 30" refX="4"><path d="M 0 0 L 10 5 L 0 10 z" style="fill:var(--theme)!important"></path></marker></defs></svg>
<div id="explorePythonDebug" style="display:none"></div>
<button id="btnRootExplore">Start</button>
<select id="selectChangeNbCol"></select>
<div id="fig-tree"></div>
<hr>
<div id="fig-leaf"></div>

<ul id="selected-item" style="display: none;">
    <li>
        <select id="parameters-select"><option value="">--Please choose an option--</option></select>
    </li>
    <li>
        <button id="btnRunExplore" style="grid-column: 2;">Run</button><input type="text" id="numberPlot" value="10">
    </li>
</ul>`
    this.paramItem = undefined
    }

    get tokenBuild() {
        return this.paramItem.parent.token
    }

    get pythonCode() {
        if (this.paramItem.parent.pythonCode) return this.paramItem.parent.pythonCode
        else return `def plot(params):

    maxStep = params['input']['Scenario']['maxStep']
    dt = params['input']['Scenario']['dt']
    stepsList = list(range(maxStep))

    def recordStep(simu):
        return simu.cells.countState()

    statesData = Simu(params).compute(recordStep,stepsList).reset_index()
    statesData = statesData.melt(id_vars='step',var_name='state',value_name='count')

    statesData['step'] *= dt/3600
    statesData.rename(columns={'step':'time (h)'},inplace=True)

    dpi = 100
    fig, ax = plt.subplots(figsize = (450 / dpi, 300 / dpi), dpi = dpi)
    sns.lineplot(statesData,x='time (h)',y='count',hue='state',ax=ax)
    return fig`
    }
    set pythonCode(code) {
        return this.paramItem.parent.pythonCode = code
    }

}

const exploreManager = new ExploreManager();
export default exploreManager;