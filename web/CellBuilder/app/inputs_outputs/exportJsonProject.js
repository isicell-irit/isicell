import CppTypeManager from "../manager/CppTypeManager";
import ModuleManager from "../manager/moduleManager";
import CustomModulesManager from "../manager/customModulesManager";
import Parameters from "../manager/ParametersManager";
import NotebookManager from "../manager/notebookManager";
import FunctionManager from "../manager/functionManager";
import stateMachine from "../diagrams/stateMachine";
import behaviorDiagram from "../diagrams/behaviorDiagram";

export default async function exportJsonProject(withoutBuild = false) {
  const lastChangeTracked = Parameters.changeTracked
  Parameters.changeTracked = false;
  if(behaviorDiagram.currentEditorElement !== undefined || behaviorDiagram.currentEditorFunction !== undefined)
    behaviorDiagram.updateFunction();
  await behaviorDiagram.saveXML();
  let toSerialise = [{ sectionName: 'Cell Types', serializedTag: 'CellTypes' },
                     { sectionName: 'Cell Attributes', serializedTag: 'cellAttributes' },
                     { sectionName: 'Scenario Attributes', serializedTag: 'scenarioAttributes' }]
  const data = {
    'version': window.CURRENT_VERSION,
    'CppTypes': CppTypeManager.getUserTypes(),
    'CppEnums': CppTypeManager.getEnums(),
    'ModelValues': { Cell: document.getElementById('Cell Attributes').items.filter(i => i.hasTag('MODEL')).map(i => i.valueText),
                     Scenario: document.getElementById('Scenario Attributes').items.filter(i => i.hasTag('MODEL')).map(i => i.valueText)},
    'ViewerValues': document.getElementById('Cell Attributes').items.filter(i => i.hasTag('VIEWER')).map(i => i.valueText),
    'CustomModules': CustomModulesManager.getValues(),
    'moduleSelected': ModuleManager.getSelectedModulesNames(),
    'scenarioFunction': FunctionManager.getCategoryFormatted('scenarioFunction'),
    'cellFunction': FunctionManager.getCategoryFormatted('cellFunction'),
}
  for (let ts of toSerialise) {
    data[ts.serializedTag] = document.getElementById(ts.sectionName).items.filter(item => !item.hasTag('CORE')).map(i => { return { value: i.valueText, type: i.type } })
  }
  if (!withoutBuild) {
    data['build'] = Parameters.getData()
    data['notebooks'] = NotebookManager.getValues()
  }
  else {
    data['build'] = {}
    data['notebooks'] = {}
  }
  
  try {
    const { xml } = await stateMachine.modeler.saveXML({ format: true }); 
    data['subDiagram'] = stateMachine.subDiagram
    data['xml'] = xml
    return data;
  } catch (err) {
    console.error('Error happened saving XML: ', err);
  } finally {
    Parameters.changeTracked = lastChangeTracked
  }
  return undefined;
}