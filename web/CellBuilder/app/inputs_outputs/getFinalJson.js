import FunctionManager from "../manager/functionManager";
import ModuleManager from "../manager/moduleManager";
import CustomModulesManager from "../manager/customModulesManager";
import CppTypeManager from "../manager/CppTypeManager";
import stateMachine from "../diagrams/stateMachine";

export default function getFinalJson() {
    let transitions = stateMachine.getTransitions()
    transitions.forEach((v,i,arr) => {
      if(arr[i].from === 'Cell Instantiation') arr[i].from = 'CellInstantiation'; //TODO WHATTTTTT ??????
      arr[i].destinations.forEach((_v,_i,_arr) => {
        if(_arr[_i].to === 'Dead Cell') _arr[_i].to = 'DeadCell';
        if(_arr[_i].to === 'Cell Instantiation') _arr[_i].to = 'CellInstantiation';
        //if(_arr[_i].to === 'Dead cell') _arr[_i].to = 'DeadCell'; //? Will need to get rid of that
      })
    })
  let retJson = {
    moduleSelected: ModuleManager.getSelectedModulesNames(),
    customModule: CustomModulesManager.getValues(),
    enums: CppTypeManager.getEnums(),
    cellTypes: document.getElementById("Cell Types").items.map(item => item.valueText),
    scenario : {
      attributes : document.getElementById('Scenario Attributes').items.map(item=>{return{type:item.type,value:item.valueText,source:item.hasTag('CORE')?'core':'user'}}),//.filter(item=>item.source !== 'core'),
      codeInit: stateMachine.subDiagram['initialisation']?.code || '',
      codeLoop: stateMachine.subDiagram['loop']?.code || '',
      functions: FunctionManager.getCategoryFormatted('scenarioFunction'),
    },
    cell : {
      attributes: document.getElementById('Cell Attributes').items.map(item => { return { type: item.type, value: item.valueText, source: item.hasTag('CORE') ? 'core' : 'user' } }),//.filter(item=>item.source==='user'),
      initializationCode: stateMachine.getInitializeCode(),
      initializationTransitions: transitions.filter(t => t.from === 'CellInstantiation'),
      States: stateMachine.getStates(),
      Transitions: transitions.filter(t=> t.from !== 'CellInstantiation'),
      functions: FunctionManager.getCategoryFormatted('cellFunction')
    },
    paramInputs: {
      'Cell': document.getElementById('Cell Attributes').items.filter(i=>i.hasTag('MODEL')).map(i=>{return{type:i.type,value:i.valueText}}),
      'Scenario': document.getElementById('Scenario Attributes').items.filter(i => i.hasTag('MODEL')).map(i=>{return{ type: i.type, value: i.valueText }}),
    },
    CellAttributesRecorded: document.getElementById('Cell Attributes').items.filter(i => i.hasTag('VIEWER')).map(i => { return { type: i.type, value: i.valueText } })

  }


    return retJson
  }