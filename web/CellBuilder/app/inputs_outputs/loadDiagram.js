import clear from "../setups/clear";
import CppTypeManager from "../manager/CppTypeManager";
import ModuleManager from "../manager/moduleManager";
import FunctionManager from "../manager/functionManager";
import { isOlderVersion } from "../utils/misc";
import CustomModulesManager from "../manager/customModulesManager";
import { getTitleUserFonction, callBackCheckIntegrity } from '../utils/utilsVaraibleName';
import Parameters from "../manager/ParametersManager";
import getFinalJson from "./getFinalJson";
import NotebookManager from "../manager/notebookManager";
import SearchManager from "../manager/searchManager";
import { rgbToHex } from "../utils/color";
import stateMachine from "../diagrams/stateMachine";

function addItemFunction(c, idFunc, code) {
  let sec = document.getElementById(c.sectionName)
  let headerFunction = FunctionManager.extractHeaderCppFunction(code)
  if (headerFunction) {
    let item = sec.itemBuilder()
    item.type = headerFunction.returnType
    item.valueTextNoIntegrityCheck = headerFunction.functionName + '()'
    item.title = getTitleUserFonction(idFunc)
    item.setAttribute('data-idFunc', idFunc)
    item.dataset.action = 'create.plugin-function'
    item.classList.add('entry')
    item.dataset.name = headerFunction.functionName
    item.dataset.cat = c.category
    sec.addItem(item)
  }
}

export default async function loadDiagram(fileXML, withoutBuild = false) {
  const lastChangeTracked = Parameters.changeTracked
  Parameters.changeTracked = false
  clear(withoutBuild);
  SearchManager.clearIndex()

  try{
    const data = (typeof fileXML === "string")?JSON.parse(fileXML):JSON.parse(JSON.stringify(fileXML));
    const version = data['version']
    await stateMachine.importXML(data['xml'], data['subDiagram']);
    ModuleManager.importSelectedModules(data['moduleSelected'])
    CustomModulesManager.importCustomModules(data['CustomModules'])
    let categories = [{ sectionName: 'Scenario functions', category: 'scenarioFunction' },
                      { sectionName: 'Cell functions', category: 'cellFunction' }]
    for (let c of categories) {
      let sec = document.getElementById(c.sectionName)
      let coreItem = {}
      sec.items.forEach(item => { coreItem[item.getAttribute('data-idFunc')] = item })
      let functions = data[c.category] || {}
      FunctionManager.loadJson(c.category, functions);
      for (let idFunc in functions) {
        if (coreItem.hasOwnProperty(idFunc)) {
          let headerFunction = FunctionManager.extractHeaderCppFunction(functions[idFunc])
          coreItem[idFunc].valueTextNoIntegrityCheck = headerFunction.functionName + '()'
        } else addItemFunction(c, idFunc, functions[idFunc])
      }
    }
    let sec = document.getElementById('Cell Types')
    //document.getElementById('Cell Types').clear()
    data['CellTypes'].forEach(i => {
      const item = sec.itemBuilder()
      item.valueTextNoIntegrityCheck = i.value
      sec.addItem(item)
    })


    categories = [{ sectionName: 'Cell Attributes', serializedTag: 'cellAttributes' },
                  { sectionName: 'Scenario Attributes', serializedTag: 'scenarioAttributes' }]
    for (let c of categories) {
      sec = document.getElementById(c.sectionName)
      let serializedData = data[c.serializedTag];
      serializedData.forEach(itemS => {
        let item = sec.itemBuilder()
        item.valueTextNoIntegrityCheck = itemS.value
        if (itemS.type) item.type = itemS.type
        sec.addItem(item)
      })
    }
    await stateMachine.loadAllElements(true);
    CppTypeManager.importTypes(data['CppTypes'])
    CppTypeManager.importEnums(data['CppEnums'])

    data['ModelValues'].Cell = data['ModelValues'].Cell.filter(i => document.getElementById('Cell Attributes').items.find(item => item.valueText === i))
    data['ModelValues'].Scenario = data['ModelValues'].Scenario.filter(i => document.getElementById('Scenario Attributes').items.find(item => item.valueText === i))
    data['ViewerValues'] = data['ViewerValues'].filter(i => document.getElementById('Cell Attributes').items.find(item => item.valueText === i))

    data['ModelValues'].Cell.forEach(i => document.getElementById('Cell Attributes').items.find(item => item.valueText === i).addTag('MODEL'))
    data['ModelValues'].Scenario.forEach(i => document.getElementById('Scenario Attributes').items.find(item => item.valueText === i).addTag('MODEL'))
    data['ViewerValues'].forEach(i => document.getElementById('Cell Attributes').items.find(item => item.valueText === i).addTag('VIEWER'))
    document.getElementById('Cell Attributes').sort()
    document.getElementById('Scenario Attributes').sort()
    if (!withoutBuild) {
      if (isOlderVersion(version, '0.4.1')) {
        for (let i=0;i<data['build']['build'].length;i++){
          if (typeof data['build']['build'][i]['xml'] === 'object'){
            data['build']['build'][i]['xml']['notebooks'] = {}
          }
        }
      }
      if (isOlderVersion(version, '0.4.2')) {
        for (const build of data['build']['build']){
          const cellTypeFind = Object.fromEntries(build.finalJson.cell.attributes.map(a=>[a.value,a.type]))
          const scenarioTypeFind = Object.fromEntries(build.finalJson.scenario.attributes.map(a=>[a.value,a.type]))
          for(const param of build.params){
            const newConfig = {Cell:{},Scenario:{}}
            for(const [k,v] of Object.entries(param.config)){
              const listKeys = k.split('.')
              let attrName, attrType, cellType, cat = listKeys[0];
              if(cat==='Cell') { attrName = listKeys[2]; cellType = listKeys[1]; attrType = cellTypeFind[attrName]
              } else { attrName = listKeys[1]; cellType = undefined; attrType = scenarioTypeFind[attrName] }
              const isNumber = (attrType==='double'||attrType==='float'||attrType==='int')
              if(!newConfig[cat][attrName]) newConfig[cat][attrName]=isNumber?{type:attrType,min:Infinity,max:-Infinity}:(attrType==='Protocol'?{type:attrType,ymin:v.ymin,ymax:v.ymin,dt:v.dt,maxTime:v.maxTime}:{type:attrType})
              if(isNumber && !v.hide){
                if(newConfig[cat][attrName].min>v.min) newConfig[cat][attrName].min = v.min
                if(newConfig[cat][attrName].max<v.max) newConfig[cat][attrName].max = v.max
              }
              if(cat=='Cell' && cellType !== 'Common'){
                if(!newConfig[cat][attrName].hidden)newConfig[cat][attrName].hidden=[]
                if(v.hide)newConfig[cat][attrName].hidden.push(cellType)
              }
            }
            param.config = newConfig
          }
        }
      }
      if(isOlderVersion(version,'0.4.3')) {
        for (let i=0;i<data['build']['build'].length;i++){
          if (Array.isArray(data['build']['build'][i]['finalJson']['cell']['functions'])){
            data['build']['build'][i]['finalJson']['cell']['functions'] = Object.fromEntries(data['build']['build'][i]['finalJson']['cell']['functions'].map((f,i)=>['patchCellF'+i,f]))
            data['build']['build'][i]['finalJson']['scenario']['functions'] = Object.fromEntries(data['build']['build'][i]['finalJson']['scenario']['functions'].map((f,i)=>['patchScenarioF'+i,f]))
          }
        }
      }
      Parameters.loadData(data['build'])
      NotebookManager.importNotebooks(data['notebooks'])
    }
    Parameters.changeTracked = lastChangeTracked
    return
  } catch (e) {
    console.error(e)
  }

  await stateMachine.importXML(fileXML);
    //console.warn(warnings);
  
  let version = stateMachine.getSerialized('version', '');
  ModuleManager.importSelectedModules(stateMachine.getSerialized('moduleSelected'))
  CustomModulesManager.importCustomModules(stateMachine.getSerialized('CustomModules'))

  let categories = [{sectionName:'Scenario functions',category:'scenarioFunction'},
                    {sectionName:'Cell functions',category:'cellFunction'}]

  for (let c of categories) {
    let sec = document.getElementById(c.sectionName)
    sec.items.forEach(item => { if (!item.hasTag('CORE')) item.delete() })
    let coreItem = {}
    sec.items.forEach(item => { coreItem[item.getAttribute('data-idFunc')]=item })
    let functions = stateMachine.getSerialized(c.category,{})
    FunctionManager.loadJson(c.category,functions);
    if (isOlderVersion(version, '0.3.0') && c.category === 'cellFunction') {
      FunctionManager.addOrUpdateFunction(c.category,'commonBehaviorFunction','void commonBehavior() {\n\n}')
      functions['commonBehaviorFunction'] = 'void commonBehavior() {\n\n}'
    }
    for(let idFunc in functions){
      if (coreItem.hasOwnProperty(idFunc)) {
        let headerFunction = FunctionManager.extractHeaderCppFunction(functions[idFunc])
        coreItem[idFunc].valueTextNoIntegrityCheck = headerFunction.functionName + '()'
      } else addItemFunction(c,idFunc,functions[idFunc])

    }
  }

  

  let sec = document.getElementById('Cell Types')
  document.getElementById('Cell Types').clear()
  stateMachine.getSerialized('CellTypes').forEach(i => {
    const item = sec.itemBuilder()
    item.valueTextNoIntegrityCheck = i.value
    sec.addItem(item)
  })

    let viewerValues = {}
    let modelValues = {}
    let CppTypesValues = {}//stateMachine.getSerialized('CppTypes')
    if(!isOlderVersion(version, '0.1.0')){
      CppTypesValues = stateMachine.getSerialized('CppTypes')
      viewerValues = stateMachine.getSerialized('ViewerValues')
      modelValues = stateMachine.getSerialized('ModelValues')
    }
    categories = [{sectionName:'Cell Attributes',serializedTag:'cellAttributes'},
                  {sectionName:'Scenario Attributes',serializedTag:'scenarioAttributes'}]
    for(let c of categories){
      sec = document.getElementById(c.sectionName)
      sec.items.forEach(item => { if (!item.hasTag('CORE')) item.delete() })
      let serializedData = stateMachine.getSerialized(c.serializedTag); 
      //TODO  REMOVE WHEN OUT OF DATE
      if(isOlderVersion(version, '0.1.0')){
        modelValues[c.sectionName] = {}
        serializedData.forEach(item=> {
          if('initValue' in item){
            modelValues[c.sectionName][item.id] = {checked: true, name: item.value, initValue: item.initValue, description: item.description};
            delete item.initValue;
            delete item.description;
          }
          if('viewerInfo' in item){
            viewerValues[item.id] = {checked: true, ...item.viewerInfo};
            delete item.viewerInfo;
          }
        })
        let defaultNames = document.getElementById(c.sectionName).items.filter(item=>item.hasTag('CORE')).map(item=>item.valueText)
        serializedData = serializedData.filter(item=>!defaultNames.includes(item.value))
      }
      //TODO

      serializedData.forEach(itemS=> {
        // if(item.source === 'core' && defaultJson.Default[c.serializedTag].map(v=>  v.match(regexVariableDeclarationSplit).groups).reduce((acc,cur)=> acc||(cur.value === item.value),false))
        let item = sec.itemBuilder()
        item.valueTextNoIntegrityCheck = itemS.value
        if(itemS.type) item.type = itemS.type
        sec.addItem(item)
      })
    }
    //TODO  REMOVE WHEN OUT OF DATE
    if(isOlderVersion(version, '0.1.0')){
      // states and types
      viewerValues['type'] = stateMachine.getSerialized('TypeViewerColors');
      viewerValues['type'].checked = true;

      viewerValues['state'] = stateMachine.getSerialized('StateViewerColors');
      viewerValues['state'].checked = true;
      CppTypesValues = stateMachine.getSerialized('CppTypes').filter(type=>!CppTypeManager.getDefaultTypes().includes(type))
    }
    //TODO
    // stateMachine.getSerialized('cellFunction')
    
    //SettingsManager.setSettings(JSON.parse(stateMachine.getSerialized('Settings',"{}")));
  
  const mainElement = stateMachine.modeler.get('elementRegistry').get('Process_1');
  ['loop', 'initialisation'].forEach(sup => {
    stateMachine.subDiagram[sup] = { behaviorDiagram: mainElement.businessObject.get('behaviorDiagram' + sup), code: mainElement.businessObject.get('code' + sup), variablesList: JSON.parse(mainElement.businessObject.get('varaibleList' + sup) || '[]') };
  })
  stateMachine.modeling.updateProperties(mainElement, { behaviorDiagramloop: undefined, codeloop: undefined, variablesListloop: undefined, behaviorDiagraminitialisation: undefined, codeinitialisation: undefined, variablesListinitialisation: undefined })
  stateMachine.forEachElement('cellFunction', (xml, code, element) => {
    stateMachine.subDiagram[element.id] = { behaviorDiagram: element.businessObject.get('behaviorDiagram'), code: element.businessObject.get('code'), variablesList: JSON.parse(mainElement.businessObject.get('varaibleList') || '[]') };
    stateMachine.modeling.updateProperties(element, { behaviorDiagram: undefined,code:undefined,variablesList:undefined})
    return { xml: stateMachine.subDiagram[element.id].behaviorDiagram, code: stateMachine.subDiagram[element.id].code}
  })



    // Force load all existing activities in case they were never opened by the user
    await stateMachine.loadAllElements(true);

  if (isOlderVersion(version, '0.3.0')) {
    FunctionManager.addOrUpdateFunction("cellFunction",'commonBehaviorFunction','void commonBehavior() {\n\n}')
    stateMachine.forEachElement("cellFunction", (xml, code, element) => {
      if (element.type !== "bpmn:IntermediateCatchEvent")
        return { xml: xml.replace('name="State start"', 'name="State start" call="commonBehavior()" idFunc="commonBehaviorFunction"'), code: FunctionManager.getFunctionCall('cellFunction', 'commonBehaviorFunction') + ';\n' + code }
      else return { xml, code }
    }, false)
  }

  CppTypeManager.importTypes(CppTypesValues)
  CppTypeManager.importEnums(stateMachine.getSerialized('CppEnums'))
  //TODO  REMOVE WHEN OUT OF DATE
  if (isOlderVersion(version, '0.2.0')) {
    // states and types
    Object.keys(modelValues).forEach(cat => Object.values(modelValues[cat]).forEach(attr => {
      if (attr.checked) document.getElementById(cat).items.find(item => item.valueText === attr.name).addTag('MODEL')
    }));

    const colors = {}
    Object.values(viewerValues).forEach(attr => {
      //if (attr.checked) document.getElementById('Cell Attributes').items.find(item => item.valueText === attr.name).addTag('VIEWER')
      if (attr.checked) {
        const item = document.getElementById('Cell Attributes').items.find(item => item.valueText === attr.name)
        if(item)item.addTag('VIEWER')
        else console.warn('no find in Cell Attributes : ',attr.name)
        if (attr.type === 'continuous') colors[attr.name] = { min: attr.min, max: attr.max, palette: attr.colors, reverse: attr.reverse }
        if (attr.type === 'discrete') {
          colors[attr.name] = []
          Object.values(attr.colors).forEach(i => colors[attr.name].push([i.name, rgbToHex(i)])) //[i.name] = rgbToHex(i))
          colors[attr.name] = Object.fromEntries(colors[attr.name])
        }
      }
    });
    document.getElementById('Cell Attributes').items.find(item => item.valueText === 'type').removeTag('VIEWER')
    document.getElementById('Cell Attributes').sort()
    document.getElementById('Scenario Attributes').sort()

    const param = { Cell: {}, Scenario: {} }
    const config = {}
    Object.keys(modelValues).forEach(cat => Object.values(modelValues[cat]).forEach(attr => {
      if (attr.checked) {
        if (cat === 'Cell Attributes') {
          Object.entries(attr.initValue).forEach(([type, val]) => {
            if (!param.Cell[type]) param.Cell[type] = {}
            param.Cell[type][attr.name] = typeof val === 'string' ? (val !== '' ? parseFloat(val) : 1) : val
            config['Cell.' + type + '.' + attr.name] = { min: attr.description.min !== undefined ? attr.description.min : 0, max: attr.description.max !== undefined ? attr.description.max: (param.Cell[type][attr.name]+0.1)*10, hide: val === '' }
          })
        } else {
          param.Scenario[attr.name] = typeof attr.initValue === 'string' ? parseFloat(attr.initValue) : attr.initValue
          if (attr.description.min && attr.description.max) config['Scenario.' + attr.name] = { min: attr.description.min, max: attr.description.max, hide: false }
        }
      }
    }));
    setTimeout(()=>{
      const buildItem = Parameters.addNewBuild('import from last version', getFinalJson(), true)
      buildItem.color = colors
      const paramItem = buildItem.items[0]
      paramItem.param = param
      paramItem.config = config
    },1000)
  } else {//TODO
    modelValues.Cell.forEach(i => document.getElementById('Cell Attributes').items.find(item => item.valueText === i).addTag('MODEL'))
    modelValues.Scenario.forEach(i => document.getElementById('Scenario Attributes').items.find(item => item.valueText === i).addTag('MODEL'))
    viewerValues.forEach(i => document.getElementById('Cell Attributes').items.find(item => item.valueText === i).addTag('VIEWER'))
    document.getElementById('Cell Attributes').sort()
    document.getElementById('Scenario Attributes').sort()
    if (!withoutBuild) {
      if (isOlderVersion(version, '0.4.1')) {
        for (let i = 0; i < data['build']['build'].length; i++) {
          if (typeof data['build']['build'][i]['xml'] === 'object') {
            data['build']['build'][i]['xml']['notebooks'] = {}
          }
        }
      }
      Parameters.loadData(stateMachine.getSerialized('build'))
      NotebookManager.importNotebooks(stateMachine.getSerialized('notebooks'))
      //Parameters.loadData(data['build'])
      //NotebookManager.importNotebooks(data['notebooks'])
    }
  }
  //remove old serialized data
  stateMachine.modeler.get('elementRegistry').get('Process_1');
  stateMachine.modeling.updateProperties(stateMachine.modeler.get('elementRegistry').get('Process_1'), { 'variablesListtimeline':undefined, 'state':undefined, 'version':undefined ,'cellType': undefined, 'codeinit': undefined, 'dtConfig': undefined, 'variablesListinit':undefined,'timeline':undefined,'Settings': undefined, 'CellTypes': undefined, 'codetimeline': undefined, 'behaviorDiagramtimeline': undefined, 'CppEnums': undefined, 'CppTypes': undefined, 'TypeViewerColors': undefined, 'StateViewerColors': undefined, 'ViewerValues': undefined, 'ModelValues': undefined, 'cellAttributes': undefined, 'scenarioAttributes': undefined, 'moduleSelected': undefined, 'CustomModules': undefined, 'notebooks': undefined, 'scenarioFunction': undefined, 'cellFunction': undefined, 'build': undefined });

  Parameters.changeTracked = lastChangeTracked

//    ModelManager.setValues(modelValues)
    //ViewerManager.setValues(viewerValues)
    //ModelManager.rebuildDOMfromData();
    //ModelManager.updateDataFromDOM();
  }