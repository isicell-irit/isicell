import './css/app.css'
import stateMachine from './diagrams/stateMachine';
import behaviorDiagram from './diagrams/behaviorDiagram';
import { loadCustomListElement } from './guiComponent/customList';
import { loadCSV_tableElement } from './guiComponent/csvTable';
import ModuleManager from './manager/moduleManager'
import ViewerManager from './viewer/ViewerManager'
import CppTypeManager from './manager/CppTypeManager'
import CustomModulesManager from './manager/customModulesManager'
import ActivityManager from './manager/ActivityManager'
import {downloadFile, getLoader, fetchW} from './utils/misc';
import setupGlobalList from './setups/GlobalListSetup';
import setupLocalList from './setups/LocalListSetup';
import { panelManager, panelManagerContent } from './setups/PanelManagerSetup'
import setupModuleManager from './setups/ModuleManagerSetup'
import setupCompletion from './setups/CompletionSetup';
import exportJsonProject from './inputs_outputs/exportJsonProject';
import JSZip from 'jszip';
import clear from './setups/clear';
import Parameters from './manager/ParametersManager';
import ExploreManager from './manager/exploreManager';
import PanelManager from './guiComponent/panelManager';
import NotebookManager from './manager/notebookManager';
import SearchManager from './manager/searchManager';
import LoggingManager from './manager/LoggingManager';
import { drawAll } from './utils/diagramToImage';
import DiffManager from './manager/diffManager';
import projectManager from './manager/ProjectManager';
import demoManager from './manager/DemosManager';
import './guiComponent/parameters'
import './css/fontello.css'
import './css/logo.css'
import themeManager from './manager/ThemeManager';

const CURRENT_VERSION = '0.4.3'
window.CURRENT_VERSION = CURRENT_VERSION

console.log(`

 /$$$$$$  /$$$$$$  /$$  /$$$$$$            /$$ /$$
|_  $$_/ /$$__  $$|__/ /$$__  $$          | $$| $$
  | $$  | $$  \\__/ /$$| $$  \\__/  /$$$$$$ | $$| $$
  | $$  |  $$$$$$ | $$| $$       /$$__  $$| $$| $$
  | $$   \\____  $$| $$| $$      | $$$$$$$$| $$| $$
  | $$   /$$  \\ $$| $$| $$    $$| $$_____/| $$| $$
 /$$$$$$|  $$$$$$/| $$|  $$$$$$/|  $$$$$$$| $$| $$
|______/ \\______/ |__/ \\______/  \\_______/|__/|__/

                                     version`,CURRENT_VERSION)

window.addEventListener("dragover", function (e) {
  e.preventDefault();
}, false);
window.addEventListener("drop", function (e) {
  e.preventDefault();
}, false);

window.mobileAndTabletCheck = function () {
  let check = false;
  (function (a) { if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true; })(navigator.userAgent || navigator.vendor || window.opera);
  return check;
};

document.querySelector('.header > ul > li > img').addEventListener('click',()=>{
  if (!document.fullscreenElement) {
    document.documentElement.requestFullscreen();
  } else if (document.exitFullscreen) {
    document.exitFullscreen();
  }
})

loadCustomListElement()
loadCSV_tableElement()


//* global variables

//* Loader screen
let loader = getLoader();
loader.setText('Loading data. Please wait.');
loader.show();

document.getElementById('doxygenDoc').onclick = ()=>{
  window.open('/doc','popup','width=800,height=600');
  return false;
}

main()

//! Main function
async function main() {

  let defaultData = (await fetchW('/api/defaultJson').then((res) => res.json())).defaultJson
  LoggingManager.init(defaultData);

  //* Checks local storage for theming info and default to light theme
  if (localStorage.getItem('theming') === undefined) localStorage.setItem('theming', 'light')
  else if (localStorage.getItem('theming') === 'dark') document.getElementById('theme-switch').checked = true
  else if (localStorage.getItem('theming') === 'light') document.getElementById('theme-switch').checked = false
  themeManager.init()



  ActivityManager.addActivity('cells_behavior', 'globalList', 'visualProgrammingView',()=>stateMachine.showCellsDiagram(), undefined, undefined, true);
  ActivityManager.addActivity('scenario_behavior', 'globalList', 'visualProgrammingView',()=>stateMachine.showScenarioDiagram(), undefined, undefined);
  ActivityManager.addActivity('globallist_btn', 'globalList', 'visualProgrammingView',undefined, undefined, undefined);

  /**
   * IOABLE BEGIN
   */
  var currentCatIsCell = true
  var prevSetup = {model:'',viewer:''}
  ActivityManager.addActivity('ioable', 'globalList', 'visualProgrammingView', () => {
    document.getElementById('Cell Attributes').displayIO()
    document.getElementById('Scenario Attributes').displayIO()
    prevSetup.model = [...document.getElementById('Cell Attributes').items.filter(i => i.hasTag('MODEL')).map(i => i.valueText), ...document.getElementById('Scenario Attributes').items.filter(i => i.hasTag('MODEL')).map(i => i.valueText)].sort().join(',')
    prevSetup.viewer = [...document.getElementById('Cell Attributes').items.filter(i => i.hasTag('VIEWER')).map(i => i.valueText), ...document.getElementById('Scenario Attributes').items.filter(i => i.hasTag('VIEWER')).map(i => i.valueText)].sort().join(',')
    const valideType = ['double','int','bool','Protocol',...Object.keys(CppTypeManager.getEnums())]
    document.getElementById('Cell Attributes').items.filter(item => !valideType.includes(item.type)).forEach(item=>item.style.display='none')
    document.getElementById('Scenario Attributes').items.filter(item => !valideType.includes(item.type)).forEach(item => item.style.display = 'none')
    if (document.getElementById('Cell Attributes').style.display==='none') currentCatIsCell = false
    else currentCatIsCell = true;
    [...document.getElementById('globalList').children].forEach(i=>i.style.display = 'none')
    document.getElementById('Cell Attributes').style.display = null
    document.getElementById('Scenario Attributes').style.display = null
  }, () => {
    document.getElementById('Cell Attributes').items.forEach(item => item.style.display = null)
    document.getElementById('Scenario Attributes').items.forEach(item => item.style.display = null)
    document.getElementById('Cell Attributes').hideIO()
    document.getElementById('Cell Attributes').sort()
    document.getElementById('Scenario Attributes').hideIO();
    document.getElementById('Scenario Attributes').sort();

    const newModel = [...document.getElementById('Cell Attributes').items.filter(i => i.hasTag('MODEL')).map(i => i.valueText), ...document.getElementById('Scenario Attributes').items.filter(i => i.hasTag('MODEL')).map(i => i.valueText)].sort().join(',')
    const newViewer = [...document.getElementById('Cell Attributes').items.filter(i => i.hasTag('VIEWER')).map(i => i.valueText), ...document.getElementById('Scenario Attributes').items.filter(i => i.hasTag('VIEWER')).map(i => i.valueText)].sort().join(',')

    if (prevSetup.model !== newModel || prevSetup.viewer !== newViewer) Parameters.changeDetected() 
    document.getElementById('Cell Attributes').style.display = 'none'
    document.getElementById('Scenario Attributes').style.display = 'none'
    if (currentCatIsCell) ["Cell Types", "Cell Attributes", "Bodies Methods", "Cell Methods"].forEach(i=>document.getElementById(i).style.display = null)
    else ["Cell Types", "Scenario Attributes", "Plugins Methods", "Scenario Methods"].forEach(i => document.getElementById(i).style.display = null)
  }, () => { });

  /**
   * IOABLE END
   */

  behaviorDiagram.init("containerBehavior");
  SearchManager.init(managerContainer);
  // Sets the diagram panels (stateMachine knows about panelManager to open and close the behavior diagram)
  await stateMachine.init("containerState", panelManagerContent);

  ActivityManager.addActivity('search', 'SearchManager', 'visualProgrammingView');
  setupGlobalList(defaultData);
  setupLocalList(defaultData);
  behaviorDiagram.transfertEventPalette('localList')






  PanelManager.createSplitterContainer('managerContainer', 'moduleManagerSplit', false);
  const moduleManager = new PanelManager("moduleManagerSplit", false);
  const customModule = moduleManager.createPanel("customModuleContainer");
  const moduleSelector = moduleManager.createPanel("moduleSelectorContainer");
  moduleManager.setWidthPanel([30, 70], [false, false])
  //* Managers setup
  /**
   * Plugs the html link to showing the module selector
   */
  ModuleManager.init(moduleSelector, defaultData);
  CustomModulesManager.init(customModule, defaultData);

  const prevValueModule = {}
  ActivityManager.addActivity('module', 'moduleManagerSplit', 'visualProgrammingView', () => {
    CustomModulesManager.refreshCollapsedList()
    prevValueModule.custom = JSON.stringify(CustomModulesManager.getValues());
    prevValueModule.selected = JSON.stringify(ModuleManager.getDataSelectedModules());
  }, () => {
    document.getElementById('pluginEditorView').style.display = 'none';
    if (prevValueModule.selected !== JSON.stringify(ModuleManager.getDataSelectedModules())  || prevValueModule.custom !==JSON.stringify(CustomModulesManager.getValues()))
      Parameters.changeDetected()
    delete prevValueModule.custom
    delete prevValueModule.selected
    CustomModulesManager.unselectFile()
    CustomModulesManager.update()
  }, () => { ModuleManager.updateSelectedModules() });
  setupModuleManager();


  DiffManager.init(managerContainer);
  ActivityManager.addActivity('diff', 'diffActivity', 'diffEditor',()=>DiffManager.onClickActivity());


  /**
   * Plugs the html link to showing the type manager
   */
  CppTypeManager.init(managerContainer, defaultData);
  ActivityManager.addActivity('type', 'CppTypeManager', 'visualProgrammingView', undefined, undefined, () => { CppTypeManager.updateTypes() });


  Parameters.init(managerContainer, defaultData)
  ActivityManager.addActivity('parambtn', 'sideMenuParameters', 'parametersGUI', () => { Parameters.onClickActivity() });

  ViewerManager.init(managerContainer, defaultData)
  ActivityManager.addActivity('viewer', 'viewerActivity', 'contentViewer', () => { setTimeout(() =>window.dispatchEvent(new Event('resize')),100) });

  
  ExploreManager.init(managerContainer)
  ActivityManager.addActivity('explore', 'exploreActivity', 'exploreGUI', () => { panelManager.minimize(['managerContainer']);setTimeout(() =>window.dispatchEvent(new Event('resize')),100); }, () => { panelManager.unminimize(['managerContainer']); });
  
  NotebookManager.init(managerContainer)
  ActivityManager.addActivity('notebook', 'notebookActivity', 'notebookGUI', () => { NotebookManager.onClickActivity() }, () => { NotebookManager.onLeaveActivity() });

  ActivityManager.addActivity('projectManager_btn', '', 'projectManagerGUI',()=>panelManager.minimize(['managerContainer'],1,true),()=>panelManager.unminimize(['managerContainer'],1,true));

  demoManager.init(defaultData);
  ActivityManager.addActivity('viewDemos','','DemosManagerGUI',()=>panelManager.minimize(['managerContainer'],1,true),()=>panelManager.unminimize(['managerContainer'],1,true));


  setupCompletion(defaultData);

  var saveBtn = document.getElementById('save');
  saveBtn.onclick = ()=>projectManager.save()
  let platform = navigator?.userAgentData?.platform || navigator?.platform || 'unknown'
  window.isMac = platform.match(/Mac/i) ? true : false;
  document.addEventListener("keydown", function (e) {
    if ((window.isMac ? e.metaKey : e.ctrlKey) && e.key == 's') {
      e.preventDefault();
      projectManager.save()
    }
  }, false);

 

  //??DOWNLOADSSETUP
  /* When the user clicks on the button,
  toggle between hiding and showing the dropdown content */
  var downloadBtn = document.getElementById('export_btn');
  downloadBtn.onclick = () => {
    document.getElementById("myDropdown").classList.toggle("show");
  }
  //! Close the dropdown menu if the user clicks outside of it
  window.onclick = function (event) {
    if (!event.target.matches('.dropbtn')) {
      var dropdowns = document.getElementsByClassName("dropdown-content");
      var i;
      for (i = 0; i < dropdowns.length; i++) {
        var openDropdown = dropdowns[i];
        if (openDropdown.classList.contains('show')) {
          openDropdown.classList.remove('show');
        }
      }
    }
  }
  //! Embeds the serialized XML of the diagram in a download link on left on the button
  var downloadDiagBtn = document.getElementById('download_diag');
  async function downloadDiag(ev) {
    const xml = await exportJsonProject();

    var zip = JSZip();
    zip.file("diag.xml", JSON.stringify(xml));
    zip.generateAsync({ type: "base64" }).then((base64) => {
      downloadFile("data:application/zip;base64," + base64, "diagram.isidiag")
    });
    document.getElementById("myDropdown").classList.remove("show");
  }
  downloadDiagBtn.onclick = downloadDiag;
  //! Returns the stateMachine diagram formated for export in drawio

  document.getElementById('export_svg').onclick = async () => {
    drawAll('svg')
    document.getElementById("myDropdown").classList.remove("show");
  }
  document.getElementById('export_png').onclick = async () => {
    drawAll('png',2)
    document.getElementById("myDropdown").classList.remove("show");
  }
  //?? END DOWNLOADS

  //??LOADFILESETUP
  //! load button and hidden file input
  var openLink = document.getElementById('load');
  var fileInput = document.getElementById('upload_input');

  // fileInput returns a file name on change
  fileInput.onchange = function (e) {
    var file = e.target.files[0];
    projectManager.loadIsidiagFile(file);

    e.target.value = "" // Allows to reopen the same diagram twice in a row
  }
  // Trigger the hidden file input on button click
  openLink.onclick = function (e) {
    e.preventDefault();
    fileInput.click();
  }
  //??LOADFILE DRAGANDDROP
  //! Drag and drop isidiag
  
  let mainContent = document.getElementById('visualProgrammingView');
  mainContent.ondrop = (ev) => {
    // Prevent default behavior (Prevent file from being opened)
    ev.preventDefault();
    window.dropping = false;
    loader.hide();
    loader.pointerEvents('all');
    console.log(ev)
    if (ev.dataTransfer.items && ev.dataTransfer.items.length > 0) {
      // Use DataTransferItemList interface to access the file
      // If dropped item is not a file, reject it
      if (ev.dataTransfer.items[0].kind === 'file') {
        var file = ev.dataTransfer.items[0].getAsFile();
        console.log('... file.name = ' + file.name);
        projectManager.loadIsidiagFile(file);
      }
    } else {
      if(ev.dataTransfer.files[0]){
      // Use DataTransfer interface to access the file(s)
      console.log('... file.name = ' + ev.dataTransfer.files[0].name);
        projectManager.loadIsidiagFile(file);
      }
    }
  }
  mainContent.ondragover = (ev) => {
    console.log('File(s) in drop zone');
    if (!window.dropping && (ev.dataTransfer.items[0] || ev.dataTransfer.files[0])) {
      window.dropping = true;
      loader.setText('Drop an isidiag to load a diagram.');
      loader.show();
      loader.pointerEvents('none');
    }
    // Prevent default behavior (Prevent file from being opened)
    ev.preventDefault();
  }
  //??CLEAR
  //! Reset everything button
  document.getElementById('clear').onclick = () => {
    clear();
    panelManagerContent.minimize(['panelLocalList', behaviorDiagram.mainContainer.id], -1);
  }
  await projectManager.loadAtStart()
  loader.hide();

}
