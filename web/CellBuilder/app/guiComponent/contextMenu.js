/**
 * ModalWindows class
 * 
 */
const background = document.createElement('div')
background.style.display = 'none'
background.classList.add('context-menu')
document.body.appendChild(background)
export default class ContextMenu {

    constructor(entries) {
        this.target = undefined
        this.cb_cancel = undefined
        this.cb_open = undefined
        this.div = document.createElement('div')
        const ul = document.createElement('ul')

        for (const [name, [logo, clickAction]] of Object.entries(entries)) {
            const li = document.createElement('li')
            li.classList.add(logo)
            //li.innerHTML = `<i class="${logo}">${name}`
            li.textContent = name
            li.addEventListener('click',(e)=>{
                e.stopPropagation();
                if(!li.classList.contains('disabled')){
                    clickAction(this.target);
                    this.hide();
                }
            },false)
            ul.appendChild(li)
        }
        this.div.appendChild(ul)

        var self = this;
        this.eventOnContextMenu = function (e) {
            e.preventDefault();
            self.target = this
            if(self.cb_open)self.cb_open(this)
            self.show(e.pageX,e.pageY)
        }
    }


    show(x,y){
        background.appendChild(this.div)
        background.onclick = () => { this.hide(this.cb_cancel) }
        background.oncontextmenu = (e) => { e.preventDefault();this.hide(this.cb_cancel) }


        background.style.display = null
        let menuWidth = this.div.offsetWidth;
        let menuHeight = this.div.offsetHeight;
        this.div.style.left = (window.innerWidth - x < menuWidth) ? x - menuWidth  + "px":x + "px";
        this.div.style.top = (window.innerHeight - y < menuHeight)? y - menuHeight + "px":y + "px";
    }

    hide(cb) {
        background.style.display = 'none'
        background.innerHTML = ""
        if(cb)cb()
    }
}