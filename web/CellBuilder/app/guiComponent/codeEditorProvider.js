import * as monaco from 'monaco-editor';
import { init } from '../utils/MonacoUtils'
import { getCompletion } from '../setups/CppCompletion';

init()
class AI_Completion {
    constructor(){
        this.apiKey = atob('ZDQ5OTU0ZWItY2ZiYS00OTkyLTk4MGYtZDhmYjM3ZjBlOTQy')
        this.url = atob('aHR0cHM6Ly93ZWItYmFja2VuZC5jb2RlaXVtLmNvbS9leGEubGFuZ3VhZ2Vfc2VydmVyX3BiLkxhbmd1YWdlU2VydmVyU2VydmljZS9HZXRDb21wbGV0aW9ucw==')
        this.extensionName = atob('QGNvZGVpdW0vcmVhY3QtY29kZS1lZGl0b3I=')
        this.id_session = `react-editor-${'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
            const r = (Math.random() * 16) | 0;
            return (c === 'x' ? r : (r & 0x3) | 0x8).toString(16);
          })}`;
        window.updateCompletionAPI = (url,apiKey)=>{this.url=url,this.apiKey=apiKey}
        this.inlineCompletionsProvider = {
            freeInlineCompletions: ()=>{},
            provideInlineCompletions: async (document, position,context) => {
                if(context.triggerKind==0) return {items:[]}
                const text = document.getValue();
                const offset = document.getOffsetAt(position)
                const completions = (await this.getCompletion(text,offset,document.getLanguageId())).completionItems || []
                const final = {items:completions.map(c=>{
                    const pStart = document.getPositionAt(c.range.startOffset)
                    const pEnd = document.getPositionAt(c.range.endOffset)
                    return { insertText: c.completion.text, range: {
                        startLineNumber: pStart.lineNumber,
                        endLineNumber: pEnd.lineNumber,
                        startColumn: pStart.column,
                        endColumn: pEnd.column
                    }, completionId: c.completion.completionId }
                })}
                return final
            },
        }
    }

    async getCompletion(text,cursorOffset,language){
        if(!this.url || !this.apiKey) return {completionItems:[]}
        try {
            return await (await fetch(this.url, {
                "method": "POST",
                "credentials": "same-origin",
                "redirect": "error",
                "mode": "cors",
            headers: {"Content-Type": "application/json",
                    'Authorization': `Basic ${this.apiKey}-${this.id_session}`},
            body: JSON.stringify({
                "metadata": {
                    "ideName": "web",
                    "ideVersion": "unknown",
                    "extensionName": this.extensionName,
                    "extensionVersion": "1.0.10",
                    "apiKey": this.apiKey,
                    "sessionId": this.id_session
                },
                "document": {
                    text,
                    "editorLanguage": language,
                    "language": "LANGUAGE_"+language.toUpperCase(),
                    cursorOffset,
                    "lineEnding": "\n"
                },
                "editorOptions": {
                    "tabSize": "4",
                    "insertSpaces": true
                }
            })
            })).json()
        } catch (error) {
            console.error(error);
            return {completionItems:[]}
        }
    }
}

const aic = new AI_Completion()



function backtrackValidePosition(model, position, hover=false) {
    const startCol = hover?model.getWordAtPosition(position)?.endColumn:position.column
    if(!startCol) return [false,false]
    const code = model.getValueInRange({startLineNumber: position.lineNumber, endLineNumber: position.lineNumber, startColumn: 0, endColumn: startCol})
    let sequenceWord = ''
    let pC = 0;
    let pB = 0;
    let nbB = 0;
    let i;
    for(i=code.length-1;i>=0;i--){
        const c = code[i]
        if(c==')') pC--;
        if(c==']') pB--;
        else if(pC===0 && !/\w|\./.test(c)) break;
        else if(c=='(') pC++;
        else if(c=='[') {
            pB++;
            if(pB==0)nbB++
        } else if(pC===0) sequenceWord = c+sequenceWord;
    }
    return [sequenceWord,{startLineNumber: position.lineNumber, endLineNumber: position.lineNumber, startColumn: i+2, endColumn: startCol}]
}

function backtrackValidePosition2(model, position, hover=false) {
    const startCol = hover?model.getWordAtPosition(position)?.endColumn:position.column
    if(!startCol) return [false,false,false]
    const code = model.getValueInRange({startLineNumber: position.lineNumber, endLineNumber: position.lineNumber, startColumn: 0, endColumn: startCol})
    let sequenceWord = []
    let sequenceBracket = []
    let word = ''
    let pC = 0;
    let pB = 0;
    let nbB = 0;
    let i;
    for(i=code.length-1;i>=0;i--){
        const c = code[i]
        if(c==')') pC--;
        if(c==']') pB--;
        else if(c=='[') {
            pB++;
            if(pB==0)nbB++
        }
        else if((pC===0&&pB==0) && !/\w|\./.test(c)) break;
        else if(c=='(') pC++;
        else if(pC===0&&pB==0&&c=='.'){
            sequenceWord.unshift(word);
            sequenceBracket.unshift(nbB);
            word=''
            nbB = 0
        } else if(pC===0&&pB==0) word = c+word;
    }
    sequenceWord.unshift(word);
    sequenceBracket.unshift(nbB);
    return [sequenceWord,sequenceBracket,{startLineNumber: position.lineNumber, endLineNumber: position.lineNumber, startColumn: i+2, endColumn: startCol}]
}

class CodeEditorProvider {

    // Instance and attributes initialisation
    constructor() {

        this.completionProvider = {} // All completions
        this.currentCompletion = {} // All available completions fetched
        this.listVariable = 0;
        this.monaco = {}; // List of the editors
        this.callBackMonolineEnter = {} // The function to execute on Enter for each editor
        this.registerCompletion();
        window.TEST = this
    }


    /**
     * Registers a completion provider
     * @param {*} name Name of the completion category
     * @param {*} provider Function returning a list of {label,insertText,documentation}
     */
    addCompletionProvider(name, provider) {
        this.completionProvider[name] = provider;
    }

    /**
     * Remove a completion provider
     * @param {*} name Name of the completion category
     */
    removeCompletionProvider(name) {
        delete this.completionProvider[name];
    }
    /**
     * Affects completion categories to an editor
     * @param {*} name Name of the editor
     * @param {*} provider List of the completion provider category
     */
    setEditorCompletionProvider(name, provider) {
        this.monaco[name].provider = provider
    }

    /**
     * Returns the content of an editor
     * @param {*} name The name of the editor
     */
    getValue(name) {
        return this.monaco[name].editor.getValue()
    }

    /**
     * Sets the value of an editor
     * @param {*} name The name of the editor
     * @param {*} value The value to set
     */
    setValue(name, value) {
        this.monaco[name].editor.setValue(value)
    }

    setDiffValue(name, newValue, oldValue,language) {
        this.monaco[name].editor.setModel({
            original: monaco.editor.createModel(oldValue,language),
            modified: monaco.editor.createModel(newValue,language),
        })
    }
    removeEditor(idContainer){
        this.monaco[idContainer].editor.dispose()
        delete this.monaco[idContainer]
    }

    /**
     * Creates a new editor
     * @param {*} idContainer The id of the container in which the editor is created
     * @param {*} options The editor options {lineNumbers:"on"/"off",monoline:true/false,provider:['prov1','prov2']}
     */
    createEditor(idContainer, options = {}) {
        this.callBackMonolineEnter[idContainer] = () => { } // Default
        var containerDom = document.getElementById(idContainer)

        // Sets the provider if passed in param
        this.monaco[idContainer] = {
            provider: options.provider
        };

        // Updates the available completion a focus of the editor
        containerDom.addEventListener('focusin', () => {
            this.updateCurrentCompletion(this.monaco[idContainer].provider)
        })

        // Applies the on Enter callback if the editor is monoline
        if (options.monoline) {
            containerDom.addEventListener("keydown", (event) => {
                if (event.key === 'Enter' && event.target.getAttribute("aria-activedescendant") === null) {
                    event.preventDefault();
                    event.stopPropagation();
                    this.callBackMonolineEnter[idContainer](this.monaco[idContainer].editor.getValue());
                }
            })
        }

        const optionsEditor = {
            value: '',
            language: options.language ? options.language : 'cpp',
            fontLigatures: true,
            readOnly: options.readOnly ? options.readOnly : false,
            fontFamily: 'Fira Code',
            minimap: {
                enabled: options.minimap ? options.minimap : false
            },
            lineNumbers: options.lineNumbers ? options.lineNumbers : 'off',
            glyphMargin: false,
            folding: false,
            fontSize: 12,
            lineHeight: options.monoline?25:18,
            padding: options.padding ? options.padding :(options.monoline ? { top: 0, bottom: 0 } : { top: 10, bottom: 10 }),
            renderWhitespace: "boundary",
            scrollBeyondLastLine: true,
            overviewRulerLanes: 0,
            overviewRulerBorder: false,
            hideCursorInOverviewRuler: true,
            automaticLayout: true,
            "bracketPairColorization.enabled": true,
            inlineSuggest: {showToolbar: "always",keepOnBlur: true},
            scrollBeyondLastLine: false,
            scrollbar: {
                useShadows: true,
                verticalScrollbarSize: 10,
                horizontalScrollbarSize: 10,
                vertical: options.scrollVertical?options.scrollVertical:"auto",
                horizontal: "auto",
                handleMouseWheel: true,
                alwaysConsumeMouseWheel: false
            },
            renderSideBySide: options.renderSideBySide? options.renderSideBySide:false,
            stickyScroll: {
                enabled: true,
                maxLineCount: 5
            },
            formatOnPaste: true,
            formatOnType: true,
            theme: (document.getElementById('theme-switch').checked) ? 'dark' : 'light'
        }

        // Creates the editor in the containerDom with a bunch a params
        if(options.diff) this.monaco[idContainer].editor = monaco.editor.createDiffEditor(containerDom, optionsEditor);
        else this.monaco[idContainer].editor = monaco.editor.create(containerDom, optionsEditor);

        if(optionsEditor.language === 'python'){
            this.monaco[idContainer].editor.addAction({
                // An unique identifier of the contributed action.
                id: "comment-multi-lines",

                // A label of the action that will be presented to the user.
                label: "comment multi lines",
                keybindings: [
                    monaco.KeyMod.CtrlCmd | monaco.KeyMod.Shift | monaco.KeyCode.Slash,
                    monaco.KeyMod.CtrlCmd | monaco.KeyCode.Slash
                ],
                precondition: null,keybindingContext: null,contextMenuGroupId: "navigation",contextMenuOrder: 1.5,
                run: (ed) => {
                    let range = ed.getSelection()
                    if(range.endLineNumber-range.startLineNumber>0){
                        range = {startLineNumber: range.startLineNumber,endLineNumber: range.endLineNumber+1,startColumn: 1,endColumn: 1}
                        let text = ed.getModel().getValueInRange(range)
                        if(/^\s*#/.test(text)) text = text.replaceAll(/^(\s*)(#\s?)/gm,'$1')
                        else text = '#'+text.replaceAll(/^(?!\s*\n|\s*#)/gm,'#')
                        ed.executeEdits("comment", [{identifier: { major: 1, minor: 1 }, range, text, forceMoveMarkers: true}]);
                    }
                },
            });
        }
        if(options.callbackGlobalSearch){
            this.monaco[idContainer].editor.addAction({
                // An unique identifier of the contributed action.
                id: "global-search-notebook",

                // A label of the action that will be presented to the user.
                label: "global search notebook",
                keybindings: [
                    monaco.KeyMod.CtrlCmd | monaco.KeyCode.KeyF
                ],
                precondition: null,keybindingContext: null,contextMenuGroupId: "navigation",contextMenuOrder: 1.5,
                run: (ed) => {
                    let range = ed.getSelection()
                    if(range.startLineNumber===range.endLineNumber)options.callbackGlobalSearch(range)
                },
            });
        }
        this.monaco[idContainer].editor.addAction({
            // An unique identifier of the contributed action.
            id: "code-completion",

            // A label of the action that will be presented to the user.
            label: "AI completion",
            keybindings: [
                monaco.KeyMod.Alt | monaco.KeyCode.KeyC
            ],
            precondition: null,keybindingContext: null,contextMenuGroupId: "navigation",contextMenuOrder: 1.5,
            run: (ed) => {
                let range = ed.getSelection()
                if(range.startLineNumber===range.endLineNumber) this.monaco[idContainer].editor.trigger('', 'editor.action.inlineSuggest.trigger', '');
            },
        });

        return this.monaco[idContainer].editor
    }

    /**
     * Registers the proper completion to the monaco editors using currentCompletion
     */
    registerCompletion() {
        if (Object.keys(this.monaco).length === 0) {
            monaco.languages.registerInlineCompletionsProvider('cpp',aic.inlineCompletionsProvider)
            monaco.languages.registerHoverProvider('cpp', {
                provideHover: (model, position) => {
                    let contextTypes = this.currentCompletion?.variablesType
                    if(!contextTypes) return
                    let contextVariables = this.currentCompletion.variables
                    let template = this.currentCompletion.template
                    let thisHasRoot = this.currentCompletion.thisHasRoot===true
                    return getCompletion(model,position,false,contextTypes,contextVariables||{},template,thisHasRoot)
                }
            })
            monaco.languages.registerCompletionItemProvider('cpp', {
                triggerCharacters: ['.', '>', ':'],
                provideCompletionItems: (model, position,context) => {
                    if(['>', ':'].includes(context.triggerCharacter)) {
                        const token = model.getValueInRange({startLineNumber: position.lineNumber, endLineNumber: position.lineNumber, startColumn: position.column-2, endColumn: position.column})
                        if(token!=='::'  && token!=='->') return
                    }
                    console.log(this.currentCompletion)
                    let contextTypes = this.currentCompletion?.variablesType
                    if(!contextTypes) return
                    let contextVariables = this.currentCompletion.variables
                    let template = this.currentCompletion.template
                    let thisHasRoot = this.currentCompletion.thisHasRoot===true
                    return getCompletion(model,position,true,contextTypes,contextVariables||{},template,thisHasRoot)
                }
            })
            monaco.languages.registerInlineCompletionsProvider('python',aic.inlineCompletionsProvider)
            monaco.languages.registerHoverProvider('python', {
                provideHover: (model, position) => {
                    let [sequenceWord,range] = backtrackValidePosition(model, position,true)
                    if(sequenceWord===false) return {contents:[]}
                    if(this.currentCompletion?.notebook){
                        sequenceWord = sequenceWord.split('.')
                        let lastToken = sequenceWord.pop()
                        if(sequenceWord.length===0){
                            const variables = Object.keys(this.currentCompletion.variables).filter(v=>v.startsWith(lastToken))
                            return {contents:variables.map(v=>{
                                const type = this.currentCompletion.variables[v]
                                if(typeof type === 'string'){
                                    const info = this.currentCompletion.variablesType[type]?.current
                                    return info?.documentation
                                } else {
                                    return type?.documentation
                                }
                            })}
                        }
                        let type = this.currentCompletion.variables[sequenceWord[0]]
                        if(typeof type === 'object')type = type.return_type
                        let i = 1
                        while(i<sequenceWord.length && type){
                            type = this.currentCompletion.variablesType[type]?.members.find((m)=>m.label===sequenceWord[i])?.return_type
                            i++
                        }
                        if(!type) return {range,contents:[]}
                        //const prefix = sequenceWord.length>0?sequenceWord.join('.')+'.':''
                        //return {suggestions:JSON.parse(JSON.stringify(this.currentCompletion.variablesType[type].members.filter((m)=>m.label.startsWith(lastToken)))).map(m=>{m.insertText=prefix+m.insertText;return m})}
                        return {contents:[this.currentCompletion.variablesType[type]?.members.find((m)=>m.label === lastToken)?.documentation]}

                    } else {
                        const depth = sequenceWord.split('.').length
                        let suggestions = this.currentCompletion.length?this.currentCompletion.filter(w => w.insertText.replace('()','')===sequenceWord && w.depth === depth):[]
                        const tmp = {
                            range: range,
                            contents: suggestions.map(v=>(v.documentation))
                        };
                        //console.log(sequenceWord,depth,tmp)
                        return tmp
                    }
                }
            });
            monaco.languages.registerCompletionItemProvider('python', {
                provideCompletionItems: (model, position) => {

                    let [sequenceWord,sequenceBracket,range] = backtrackValidePosition2(model, position)
                    if(sequenceWord===false) return {suggestions:[]}
                    if(this.currentCompletion?.notebook){
                        let lastToken = sequenceWord.pop()
                        if(sequenceWord.length===0){
                            const variables = Object.keys(this.currentCompletion.variables).filter(v=>v.startsWith(lastToken))
                            return {suggestions:variables.map(v=>{
                                const type = this.currentCompletion.variables[v]
                                if(typeof type === 'string'){
                                    const info = this.currentCompletion.variablesType[type]?.current
                                    return {label:v,insertText:v,documentation:info?.documentation,kind:info?.kind}
                                } else {
                                    return type
                                }
                            })}
                        }
                        let type = this.currentCompletion.variables[sequenceWord[0]]
                        if(typeof type === 'object')type = type.return_type
                        const checkVector = (i)=>{
                            while(sequenceBracket[i]>0){
                                if(/vector/i.test(type)){
                                    type = type.replace(/vector/i,'')
                                    sequenceBracket[i]--
                                } else {
                                    type=''
                                    break;
                                }
                            }
                        }
                        checkVector(0)
                        //console.log(sequenceWord,lastToken)
                        let i = 1;
                        while(i<sequenceWord.length && type){
                            type = this.currentCompletion.variablesType[type]?.members.find((m)=>m.label===sequenceWord[i])?.return_type
                            checkVector(i)
                            i++
                        }
                        if(!type) return {suggestions:[]}
                        let suggestions = this.currentCompletion.variablesType[type]?.members.filter((m)=>m.label.startsWith(lastToken))
                        suggestions = suggestions?JSON.parse(JSON.stringify(suggestions)):[]
                        //console.log(suggestions)
                        return {suggestions}                        
                    } else {
                        //console.log(sequenceWord,' / ',depth)
                        let suggestions = this.currentCompletion.filter(w => w.insertText.startsWith(sequenceWord))
                        //console.log(suggestions)
                        var suggest = {
                            suggestions: suggestions.map((item) => {
                                //console.log(item)
                                item.range = range
                                item.insertTextRules = monaco.languages.CompletionItemInsertTextRule.InsertAsSnippet
                                if(!item.kind)
                                    if (item.insertText?.endsWith(')'))
                                        item.kind = monaco.languages.CompletionItemKind.Function
                                    else if (item.insertText?.includes('.'))
                                        item.kind = monaco.languages.CompletionItemKind.Property
                                    else item.kind = monaco.languages.CompletionItemKind.Variable
                                return item
                            })
                        }
                        return suggest;
                    }
                }
            })
        }
    }

    setError(id,markers){
        monaco.editor.setModelMarkers(this.getEditor(id).getModel(), 'cppError', markers)
    }
    setLanguage(id,language){
        monaco.editor.setModelLanguage(this.getEditor(id).getModel(), language);
    }

    async colorizeCodeToHtml(code,language){
        return await monaco.editor.colorize(code,language)
    }

    /**
     * Returns an editor
     * @param {*} id The id of the editor
     */
    getEditor(id) {
        return this.monaco[id].editor
    }

    updateCurrentCompletion(provider) {
        this.currentCompletion = provider?this.completionProvider[provider]():[]
    }

    /**
     * Sets the callback function on Enter for an editor
     * @param {*} id The editor id
     * @param {*} callback The callback function with the editor content as a parameter
     */
    setCallBackMonolineEnter(id, callback) {
        this.callBackMonolineEnter[id] = callback;
    }

    /**
     * Sets the callback function on Enter for an editor
     * @param {*} id The editor id
     * @param {*} callback The callback function with the editor content as a parameter
     */
    TriggerCallBackMonolineEnter(id, value = undefined) {
        this.callBackMonolineEnter[id](value === undefined ? this.monaco[id].editor.getValue() : value);
    }
}

const codeEditorProvider = new CodeEditorProvider()
window.codeEditorProvider = codeEditorProvider
export default codeEditorProvider;