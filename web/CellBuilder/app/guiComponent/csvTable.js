import { fetchW } from "../utils/misc"
import '../css/csvTable.css'
export function loadCSV_tableElement() {
    class CSVTable extends HTMLElement {
        static get observedAttributes() {
            return ['src', 'no-headers']
        }

        attributeChangedCallback(name, oldValue, newValue) {
            if (!this.__initialized) { return }
            if (oldValue !== newValue) {
                if (name === 'no-headers') {
                    this.noHeaders = newValue
                } else {
                    this[name] = newValue
                }
            }
        }

        get src() { return this.getAttribute('src') }
        set src(value) {
            this.setAttribute('src', value)
            this.setSrc(value)
        }

        get value() { return this.__data }
        set value(value) {
            this.setValue(value)
        }

        get noHeaders() { return this.hasAttribute('no-headers') }
        set noHeaders(value) {
            const noHeaders = this.hasAttribute('no-headers')
            if (noHeaders) {
                this.setAttribute('no-headers', '')
            } else {
                this.removeAttribute('no-headers')
            }
            this.setNoHeaders(noHeaders)
        }

        constructor() {
            super()
            this.__initialized = false
            this.__headers = true
            this.__data = []
            this.__rowsPerPage = 50
            this.__page = 0
            this.__sep = ','
        }

        async connectedCallback() {
            this.__table = document.createElement('table')
            this.appendChild(this.__table)
            if (this.hasAttribute('no-headers')) {
                this.__headers = false
            }

            if (this.hasAttribute('src')) {
                this.setSrc()
            }

            this.__initialized = true
        }

        async setSrc() {
            if (this.hasAttribute('src')) {
                const rawCSV = await this.fetchSrc(this.src)
                this.setData(rawCSV)
            }
        }

        setData(txt) {
            this._rawData = txt
            this.parse()
        }

        set sep(s) {
            if (this.__sep !== s) {
                this.__sep = s
                this.parse()
            }
        }

        parse() {
            this.__dataHeader = []
            this.__data = {}
            if (this._rawData) {
                let rawRows = this._rawData.split('\n')
                rawRows.forEach((r, i) => {
                    if(r.trim() !=='')
                        if (i === 0) this.__dataHeader = r.split(this.__sep)
                        else this.__data[i - 1] = r.split(this.__sep)
                })
                this.sortOn('id')
            }
        }

        getCSV() {
            let csv = this.__dataHeader.join(this.__sep)
            csv = csv + '\n' + Object.values(this.__data).map(r => r.join(this.__sep)).join('\n')
            return csv
        }

        async fetchSrc(src) {
            const response = await fetchW(src)
            if (response.status !== 200) throw Error(`ERR ${response.status}: ${response.statusText}`)
            return response.text()
        }

        setValue(value) {
            this.__data = parse(value)
            this.render()
        }

        setNoHeaders(noHeaders) {
            this.__headers = !noHeaders
            this.render()
        }
        sortOn(colNum) {
            if (this.__sortOn === colNum) this.__sortAsc = !this.__sortAsc
            else this.__sortAsc = false
            if (colNum === 'id') {
                this.__listIndex = Object.keys(this.__data).sort((a, b) => b - a)
                if (!this.__sortAsc) this.__listIndex = this.__listIndex.reverse();
            } else {
                if (this.__sortAsc) this.__listIndex = Object.entries(this.__data).sort(([, a], [, b]) => b[colNum] - a[colNum]).map(([i,]) => i)
                else this.__listIndex = Object.entries(this.__data).sort(([, a], [, b]) => a[colNum] - b[colNum]).map(([i,]) => i)
            }
            this.__sortOn = colNum
            this.render()
        }

        set rowsPerPage(n) {
            this.__rowsPerPage = n
            if (this.page >= this.maxPage)
                this.__page = this.maxPage-1
            this.render()
        }
        get rowsPerPage() {
            return this.__rowsPerPage
        }
        set page(n) {
            if (n !== this.__page){
                this.__page = n
                this.render()
            }
        }
        get page() {
            return this.__page
        }
        get maxPage() {
            return Math.ceil(this.__listIndex.length / this.rowsPerPage)
        }

        updateTextInfo() {
            if (this.__textInfo) this.__textInfo.innerText = `Showing ${this.page * this.rowsPerPage} to ${Math.min((this.page + 1) * this.rowsPerPage, this.__listIndex.length)} of ${this.__listIndex.length} entries`
        }
        updatePage() {
            if (this.__pagination) {
                let minP, maxP;
                if (this.maxPage < 7) {
                    minP = 0
                    maxP = this.maxPage - 1
                } else {
                    minP = Math.max(0, Math.min(this.page - 4, this.maxPage - 9))
                    maxP = Math.min(this.maxPage - 1, Math.max(this.page + 4, minP + 8))
                }
                this.__pagination.innerHTML = ''
                for (let i = minP; i <= maxP; i++) {
                    const p = document.createElement('a')
                    p.page = i
                    p.innerText = i + 1
                    if (this.page === i) p.className = 'current'
                    this.__pagination.appendChild(p)
                }
            }
        }

        getPageManager() {
            if (!this.__containerPageManager) {
                this.__containerPageManager = document.createElement('div')
                this.__containerPageManager.className = 'table-info-container'
                this.__textInfo = document.createElement('span')
                this.__textInfo.style.marginRight = 'auto'
                this.__containerPageManager.appendChild(this.__textInfo)
                const labelInputRowsPerPage = document.createElement('span')
                labelInputRowsPerPage.innerText = 'Rows per page:'
                this.__containerPageManager.appendChild(labelInputRowsPerPage)
                const inputRowsPerPage = document.createElement('input')
                inputRowsPerPage.type = 'text'
                inputRowsPerPage.value = this.rowsPerPage

                const valideRowsPerPage = () => {
                    const nb = Number(inputRowsPerPage.value)
                    if (nb) this.rowsPerPage = nb
                    else inputRowsPerPage.value = this.rowsPerPage
                }
                inputRowsPerPage.addEventListener('keyup', (e) => { if (e.key === 'Enter') valideRowsPerPage() })
                inputRowsPerPage.addEventListener('focusout', () => valideRowsPerPage())

                this.__containerPageManager.appendChild(inputRowsPerPage)
                const labelPage = document.createElement('span')
                labelPage.innerText = 'Page:'
                this.__containerPageManager.appendChild(labelPage)
                this.__pagination = document.createElement('span')
                this.__pagination.className = 'pagination'
                this.__containerPageManager.appendChild(this.__pagination)
                this.__pagination.addEventListener('click', (e) => {
                    this.page = e.target.page
                })
                this.updateTextInfo()
                this.updatePage()
            }
            return this.__containerPageManager
        }

        render() {
            const table = document.createElement('table')

            const thead = document.createElement('thead')
            thead.addEventListener('click', (e) => {
                if (e.target.nodeName === 'TH') this.sortOn(e.target.col)
                else if (e.target.nodeName === 'SPAN') this.sortOn(e.target.parentElement.col)
            })
            const tr = document.createElement('tr')
            const th = document.createElement('th')
            const sp = document.createElement('span')
            sp.innerText = 'id'
            th.col = 'id'
            th.appendChild(sp)
            if (this.__sortOn === 'id') th.className = this.__sortAsc ? 'headerSortUp' : 'headerSortDown'
            tr.appendChild(th)
            this.__dataHeader.forEach((header, i) => {
                const th = document.createElement('th')
                const sp = document.createElement('span')
                sp.innerText = header
                th.col = i
                th.appendChild(sp)
                if (i === this.__sortOn) th.className = this.__sortAsc ? 'headerSortUp' : 'headerSortDown'
                tr.appendChild(th)
            })
            thead.append(tr)
            table.appendChild(thead)

            const tbody = document.createElement('tbody')
            const iStart = this.__page * this.__rowsPerPage
            const iEnd = (this.__page + 1) * this.__rowsPerPage
            for (let i = iStart; i < iEnd && i < this.__listIndex.length; i++) {

                const index = this.__listIndex[i]
                const row = this.__data[index]
                const tr = document.createElement('tr')
                const td = document.createElement('td')
                td.innerText = index
                tr.appendChild(td)
                row.forEach((cell, j) => {
                    const td = document.createElement('td')
                    td.row = index
                    td.col = j
                    td.innerText = cell
                    tr.appendChild(td)
                })
                tbody.appendChild(tr)
            }
            tbody.addEventListener('dblclick', (e) => {
                if (e.target.nodeName === 'TD' && e.target.row !== undefined) {
                    let input = document.createElement('input')
                    input.type = 'text'
                    input.style.width = e.target.getBoundingClientRect().width+'px'
                    const valuePrev = e.target.innerHTML
                    input.value = valuePrev
                    e.target.innerHTML = ''
                    e.target.className = 'editing'
                    e.target.appendChild(input)
                    const valide = () => {
                        const value = input.value
                        if (this.cbEdit && value !== valuePrev) this.cbEdit(true)
                        //e.target.removeChild(input)
                        e.target.innerText = value
                        this.__data[e.target.row][e.target.col] = value
                        e.target.className = null

                    }
                    input.focus()
                    input.addEventListener('keyup', (e) => { if (e.key === 'Enter') valide() })
                    input.addEventListener('focusout', () => valide())
                }
            }, false)
            table.appendChild(tbody)

            this.removeChild(this.__table)
            this.__table = table
            this.appendChild(this.__table)
            this.updateTextInfo()
            this.updatePage()
        }
    }

    customElements.define('csv-table', CSVTable)
}