import '../css/screenSpliter.css'
/**
 * This class manages the panels logic
 * Each manager manages of row/col of panels
 */

export default class PanelManager {

    /**
     * Constructor of the PanelManager class
     * @param {*} idContainer Id of the html element in which to put the panels
     * @param {*} isHorizontal Are the panels to be arranged horizontally
     * @param {*} minSize Minimum size authorized for the manager
     */
    constructor(idContainer, isHorizontal = true, minSize = 0) {
        this.container = document.getElementById(idContainer)
        this.listIdPanel = []
        this.listIdVisiblePanel = []
        this.isHorizontal = isHorizontal
        this.minSize = minSize
        this.lastPosition={}
        this.locked={}
        this.currentResize = false
        window.addEventListener('resize',(e)=>{
            if (!this.currentResize && this.listIdVisiblePanel.length>1 && this.container.offsetParent!==null) {
                let pxSize = this.listIdVisiblePanel.map((i) => {
                    let el = document.getElementById(i)
                    return { px: (el.dataset.pxMode === 'on') ? (this.isHorizontal ? Number(el.style.width.slice(0, -2)) : Number(el.style.height.slice(0, -2))) : 0, pc: (el.dataset.pxMode === 'off') ? (this.isHorizontal ? Number(el.style.width.slice(0, -1)) : Number(el.style.height.slice(0, -1))) : 0 }
                })
                let pxSize2 = pxSize.reduce((p,i) => {return {px:p.px+i.px,pc:p.pc+i.pc}},{px:0,pc:0})
                pxSize = pxSize.map((i) => i.px > 0 ? i.px : i.pc / pxSize2.pc*(this.getContainerSpace()-pxSize2.px))
                for(let i in this.listIdVisiblePanel){
                    let el = document.getElementById(this.listIdVisiblePanel[i])
                    this.updatePanelSize(el,pxSize[i])
                }
            }
        })
    }

    /**
     * Returns the dom element for a splitter container
     * @param {*} idParent Id of the parent container in which to put the panels
     * @param {*} id Id of the new splitter container
     * @param {*} isHorizontal Are the panels to be arranged horrizontally
     */
    static createSplitterContainer(idParent,id,isHorizontal){
        var dom = document.createElement("div")
        if(isHorizontal) dom.className = "splitter-h"
        else dom.className = "splitter-v"
        dom.id = id;
        document.getElementById(idParent).appendChild(dom)
        return dom;
    }

    /**
     * Returns the dom for a new panel
     * @param {*} idPanel The id of the panel to create
     */
    createPanel(idPanel,pxMode='off'){
        // Creates and completes the panel dom element
        var panel = document.createElement('div');
        panel.id = idPanel
        panel.dataset.pxMode=pxMode
        if(this.isHorizontal) panel.classList.add("panel-split-h");
        else panel.classList.add('panel-split-v')
        var separator = this.createSeparator();
        // Apends the panel to the panel manager
        this.container.appendChild(panel);
        //
        this.addPanel(idPanel,separator);
        panel.separator = separator
        // Returns the panels dom
        return panel;
    }

    removePanel(idPanel){
        const panel = document.getElementById(idPanel)
        if(panel){
            panel.separator.remove()
            panel.remove()
            this.listIdPanel.splice(this.listIdPanel.indexOf(idPanel),1)
        }
    }

    /**
     * Registers the panel in the manager
     * @param {*} idPanel The id of the panel
     * @param {*} separator The separator next to the panel
     * @param {*} callback Optional callback function
     */
    addPanel(idPanel,separator,callback){
        if(this.listIdPanel.length>0){ // Generates separator events for the last panel
            this.generateEvent(this.getLastPanel(),separator,document.getElementById(idPanel),callback)
        }
        this.listIdPanel.push(idPanel); // Doesn't add a separator if there is only one element
    }

    /**
     * Returns the separator dom element
     */
    createSeparator(){
        if(this.listIdPanel.length>0){
            var separator = document.createElement('div');
            if(this.isHorizontal)separator.classList.add("splitter-separator-h");
            else separator.classList.add("splitter-separator-v")
            this.container.appendChild(separator);
            return separator;
        }
    }

    /**
     * Sets the size of the panels (width if horiozontal, height if vertical)
     * @param {*} sizePanel List of the sizes for each panel
     * @param {*} pxMode List of booleans for pixel or percentage sizes for each panel
     */
    setWidthPanel(sizePanel, pxMode) {
        this.listIdVisiblePanel = this.getListIdVisiblePanels()
        var pxTotal = 0;
        for(var i=0;i<sizePanel.length && i<this.listIdPanel.length;i++){
            if(pxMode[i]) pxTotal+=sizePanel[i]
        }
        for(var i=0;i<sizePanel.length && i<this.listIdPanel.length;i++){
            var panelDom = document.getElementById(this.listIdPanel[i])
            if(pxMode[i]){
                if(this.isHorizontal) panelDom.style.width = sizePanel[i]+"px"
                else panelDom.style.height = sizePanel[i] + "px"
                panelDom.dataset.pxMode = 'on'
            } else {
                if(this.isHorizontal) panelDom.style.width = sizePanel[i]*(1-pxTotal/this.getContainerSpace())+"%"
                else panelDom.style.height = sizePanel[i] * (1 - pxTotal / this.getContainerSpace()) + "%"
                panelDom.dataset.pxMode = 'off'
            }
        }
        this.listPixelMode = pxMode
        // window.dispatchEvent(new Event('resize'));
    }

    /**
     * Returns a list with the width/height of each panel in the manager
     * @param {*} pxMode Boolean for pixel values or percentage values 
     */
    getWidthPanel(pxMode){
        var total = 0;
        var width = []
        for(var idPanel of this.listIdPanel){
            var w;
            if(this.isHorizontal)w = document.getElementById(idPanel).offsetWidth
            else w = document.getElementById(idPanel).offsetHeight
            width.push(w)
            total+=w
        }
        if(pxMode)return width
        var widthPc = []
        for(var w of width){
            widthPc.push(w/total*100)
        }
        return widthPc
    }

    /**
     * Minimizes panels
     * @param {*} idPanels List of the id of the panels to minimize
     * @param {*} direction 1 -> Left to right / Up to down ; -1 -> Right to left / Down to Up
     * @param {*} withLock Locks resize
     */
    minimize(idPanels,direction,withLock=true){
        for(var idPanel of idPanels){
            if(this.lastPosition[idPanel] === undefined) this.memorise(idPanel);
            if(withLock) this.locked[this.listIdVisiblePanel[this.listIdVisiblePanel.indexOf(idPanel)+direction]]=true
            this.changeSize(idPanel,0,direction)
        }
    }

    /**
     * Unminimizes panels
     * @param {*} idPanels List of id of the panels to unminimize
     * @param {*} direction 1 -> Left to right / Up to down ; -1 -> Right to left / Down to Up
     * @param {*} withUnLock Unlocks resize
     */
    unminimize(idPanels,direction,withUnLock=true){
        for(var idPanel of idPanels){
            if(this.lastPosition[idPanel]) this.changeSize(idPanel,this.lastPosition[idPanel],direction)
        }
        for(var idPanel of idPanels){
            delete this.lastPosition[idPanel]
            if(withUnLock) delete this.locked[this.listIdVisiblePanel[this.listIdVisiblePanel.indexOf(idPanel)+direction]]
        }
    }

    /**
     * Saves the size of a panel
     * @param {*} idPanel The id of the panel
     */
    memorise(idPanel){
        var f = document.getElementById(idPanel)
        if(this.isHorizontal) this.lastPosition[idPanel] = f.offsetWidth
        else  this.lastPosition[idPanel] = f.offsetHeight
    }

    /**
     * Changes the size of a panel taking into consideration the panels around it
     * @param {*} idPanel The id of the panel to resize
     * @param {*} size The new size of the panel
     * @param {*} direction 1 -> Left to right / Up to down ; -1 -> Right to left / Down to Up
     */
    changeSize(idPanel,size,direction=1){
        this.listIdVisiblePanel = this.getListIdVisiblePanels()
        size = Math.max(size,this.minSize)
        var i = this.listIdVisiblePanel.indexOf(idPanel)
        var currentPanel = document.getElementById(idPanel)
        var nextPanel;
        var isLastVisible = true;

        // Tests whether this panel is the last panel unminimized
        if(i<this.listIdVisiblePanel.length-1) { // If it is not the last panel
            let j = i+1;
            let fnext = document.getElementById(this.listIdVisiblePanel[j])
            while(j<this.listIdVisiblePanel.length && fnext.style.display!=='none' && isLastVisible){
                isLastVisible = false;
                j+=1
                fnext = document.getElementById(this.listIdVisiblePanel[j])
            }
        }
        // Looks for nextPanel the last visible panel
        if(!isLastVisible){ // If it's not the last visible get the last visible to "nextPanel"
            nextPanel = document.getElementById(this.listIdVisiblePanel[i+direction])
            let j=1
            while(!(nextPanel.style.display!=='none')){
                nextPanel = document.getElementById(this.listIdVisiblePanel[i+direction*j])
                j+=1
            }
        } else { // If it is the last visible get the previous visible panel to "nextPanel"
            nextPanel = document.getElementById(this.listIdVisiblePanel[i-1])
            let j=1
            while(!(nextPanel.style.display!=='none')){
                nextPanel = document.getElementById(this.listIdVisiblePanel[i-j])
                j+=1
            }
        }
        // Changes the size
        this.updatePanelSize(nextPanel, this.isHorizontal ? (nextPanel.offsetWidth + (currentPanel.offsetWidth - size)) : (nextPanel.offsetHeight + (currentPanel.offsetHeight - size)))
        this.updatePanelSize(currentPanel, size)
        // window.dispatchEvent(new Event('resize'));
    }

    /**
     * Generates the resize events for a specific panel
     * @param {*} first The id of the panel that was already created
     * @param {*} separator The newly created separator to link to the first panel
     * @param {*} second The newly created panel id
     * @param {*} callback function called on panel resize with first and second as parameters
     */
    generateEvent(first, separator, second,callback){
        var   md;
        if(separator){
            separator.onmousedown = separator.ontouchstart = (e)=>{ // When a separator is clicked
                this.currentResize = true
                md = {e,
                    firstWidth:  first.offsetWidth,
                    firstHeight:  first.offsetHeight,
                    secondWidth: second.offsetWidth,
                    secondHeight: second.offsetHeight
                };
                if(this.locked[second.id] === undefined){ // If it's dragged -> resize
                    if(!e.touches){
                        document.onmousemove = (e) => {
                            var delta = {x: Math.min(Math.max(e.clientX - md.e.clientX, -md.firstWidth+this.minSize),md.secondWidth-this.minSize),
                                        y: Math.min(Math.max(e.clientY - md.e.clientY, -md.firstHeight + this.minSize), md.secondHeight - this.minSize)};
                            this.updatePanelSize(first, this.isHorizontal ? (md.firstWidth + delta.x) : (md.firstHeight + delta.y))
                            this.updatePanelSize(second, this.isHorizontal ? (md.secondWidth - delta.x) : (md.secondHeight - delta.y))
                            if(callback!==undefined) {
                                callback(first,second)
                            }
                            window.dispatchEvent(new Event('resize'));
                        }
                        document.onmouseup = () => { // If it's just clicked -> hide
                            document.onmousemove = document.onmouseup = null;
                        }
                    } else {
                        separator.ontouchmove = (e) => {
                            var delta = {
                                x: Math.min(Math.max(e.touches[0].clientX - md.e.touches[0].clientX, -md.firstWidth + this.minSize), md.secondWidth - this.minSize),
                                y: Math.min(Math.max(e.touches[0].clientY - md.e.touches[0].clientY, -md.firstHeight + this.minSize), md.secondHeight - this.minSize)
                            };
                            this.updatePanelSize(first, this.isHorizontal ? (md.firstWidth + delta.x) : (md.firstHeight + delta.y))
                            this.updatePanelSize(second, this.isHorizontal ? (md.secondWidth - delta.x) : (md.secondHeight - delta.y))
                            if (callback !== undefined) {
                                callback(first, second)
                            }
                            window.dispatchEvent(new Event('resize'));
                        }
                        separator.ontouchend = () => { // If it's just clicked -> hide
                            separator.ontouchmove = separator.ontouchend = null;
                        }
                    }
                }
            }
            separator.onmouseup = (e)=>{
                this.currentResize = false
            }
            separator.ontouchend = () => { // If it's just clicked -> hide
                this.currentResize = false
            }
        }
    }


    updatePanelSize(domPanel, sizePx) {
        if (this.isHorizontal) {
            domPanel.style.width = domPanel.dataset.pxMode === 'off' ? (sizePx / this.getContainerSpace() * 100) + "%" : sizePx + "px";
        } else {
            domPanel.style.height = domPanel.dataset.pxMode === 'off' ? (sizePx / this.getContainerSpace() * 100) + "%" : sizePx + "px";
        }
    }

    getSeparatorSpace(){
        let separatorTotalSize = ((this.isHorizontal) ? Array.from(this.container.querySelectorAll('.splitter-separator-h:not([style*="display:none"]:not([style*="display: none"])')).reduce((prevVal, currVal)=>{
            return prevVal + currVal.offsetWidth;
        }, 0) : Array.from(this.container.querySelectorAll('.splitter-separator-v:not([style*="display:none"]:not([style*="display: none"])')).reduce((prevVal, currVal)=>{
            return prevVal + currVal.offsetHeight;
        }, 0));
        return separatorTotalSize;
    }

    getContainerSpace(){
        return ((this.isHorizontal) ? this.container.offsetWidth : this.container.offsetHeight);
    }

    /**
     * Returns the id of the last panel added to the manager
     */
    getLastPanel(){
        return document.getElementById(this.listIdPanel[this.listIdPanel.length-1])
    }

    getListIdVisiblePanels(){
        return this.listIdPanel.filter((idPanel)=>{
            // return document.getElementById(idPanel).getElementsByClassName("active").length == 0
            return document.getElementById(idPanel).style.display !== "none"
        })
    }

}