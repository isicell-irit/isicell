import themeManager from "../manager/ThemeManager";

    class TimeLine {
	constructor(parent,data){
		// Get a reference to the needed elements.
		this.canvas = document.createElement("CANVAS"); //document.getElementById('mainCanvas');
		//canvas.style.width = "100%";
		//canvas.style.height = "100%";
		this.canvas.width = "200";
		this.canvas.height = "150";
		parent.style.height = "150px";
		parent.appendChild(this.canvas);
		this.context= 0;
		this.data= data;

		this.width = 10;  // These will be corrected later.
		this.height = 10;
		this.xRange= 0;
		this.xMin= 0;
		this.xMax= 0;
		this._xMid= 0;
		this.yRange= 0;
		this.yMin= 0;
		this.yMax= 0;
		this._yMid= 0;
    this._resMax = 20;

		this.slideButtonDown = false
		this.inMove = false;
		this.lastMouseX = 0;
		this.lastMouseY = 0;
		this.dt = 0.125;
		this.maxTime = 72;
    this.indexDataHover = -1;

		// Check for the canvas element, and get its context.
		if (this.canvas && this.canvas.getContext) {
			// You can only initialize one context per canvas.
			this.context = this.canvas.getContext('2d');
			if (this.context) {
				// Got the context.  Now we can start drawing.
				this.setZoom(7, 5, 13,10);
				this.plot();
			}
		}

	window.addEventListener('resize', (e)=>{this.resizeCanvas()});
	this.canvas.oncontextmenu = function() { return false; };
	window.addEventListener("mousemove", (e)=>{this.slideCanvas(e)}, false);
	window.addEventListener("mouseup", (e)=>{this.releaseCanvas(e)}, false);
	this.canvas.addEventListener("mousedown", (e)=>{this.clickCanvas(e)}, false);
	this.canvas.addEventListener("wheel", (e)=>{e.preventDefault();this.zoomCanvas(e)}, false);
    document.getElementById('theme-switch').addEventListener('change',()=>{this.plot()})
	}
	// This function is used to set a new position and zoom level.
	//
	setZoom(xMid, yMid, new_X_range, new_Y_range) {
		this._xMid = xMid;
		this._yMid = yMid;
		this.xRange = new_X_range;
		this.xMin = xMid - (this.xRange * 0.5);
    this.xMax = xMid + (this.xRange * 0.5);
    if (this.xRange > this.maxTime) this.xRange = this.maxTime;
    if(this.xMax>this.maxTime) {
      this.xMax = this.maxTime;
      this.xMin = this.xMax-this.xRange;
    }
		if(this.xMin<0){
			this.xMax = this.xRange;
			this.xMin = 0;
		}
		this.yRange = new_Y_range;
		
		this.yMin = yMid - (this.yRange * 0.5);
		this.yMax = yMid + (this.yRange * 0.5);
		/*try {
			this._yAspect = this.yRange / (this.xRange * this.canvas.clientHeight / this.canvas.clientWidth);
		}
		catch (ex) { this._yAspect = 1; }*/
	}

	// This function helps resize the canvas.
	//
	resizeCanvas() {
		this.canvas.width  = 0;
		this.canvas.height = this.canvas.parentElement.clientHeight;
		this.canvas.width  = this.canvas.parentElement.clientWidth;
		/*if (this._yAspect) {
			this.yRange = this._yAspect * this.xRange * this.canvas.clientHeight / this.canvas.clientWidth;
			this.yMin = this._yMid - (this.yRange * 0.5);
			this.yMax = this._yMid + (this.yRange * 0.5);
		}*/
		this.plot();
	}

	// This is here to remove roundoff errors in the grid labels.
	// I was seeing label names like 1.500000000000002.
	//
	makeLabel(val) {
		var label = Math.abs(Math.round(val * 10000000)).toString();
		if (label.indexOf('e', 0) >= 0) {
			label = val.toString();
		}
		else {
			while (label.length < 8) {
				label = "0" + label;
			}
			if (val < 0) {
				label = "-" + label;
			}
			var dotPos = label.length - 7;
			label = label.substr(0, dotPos) + "." + label.substr(dotPos);
			while ((label[label.length - 1] == '0') && (label[label.length - 2] != '.')) {
				label = label.substr(0, label.length - 1);
			}
		}
		return label;
	}

	// This is the main plotting function
	//
	plot() {

		this.grid();  // Draw the background first.
		this.context.fillStyle = '#52c8ff';
		this.context.strokeStyle = '#000';

		//this.context.beginPath();
    var dot, yRounded;
		for(var i=0;i < this.data.length;i++){
      dot = this.data[i]
			this.context.beginPath();
      if(i == this.indexDataHover){
        //this.context.fillStyle = '#1ed36f';
        this.context.fillStyle = '#ff52c8';
        this.context.arc((((dot[0]-this.xMin) /this.xRange ) * this.width), (((this.yMax - dot[1]) * this.height) / this.yRange), 5, 0, 2 * Math.PI);
        this.context.fill();
        this.context.stroke();
    		this.context.fillStyle = '#52c8ff';

      } else {
        this.context.arc((((dot[0]-this.xMin) /this.xRange ) * this.width), (((this.yMax - dot[1]) * this.height) / this.yRange), 5, 0, 2 * Math.PI);
        this.context.fill();
        this.context.stroke();
      }
			this.context.closePath();
		}
    if(this.indexDataHover>=0 && this.indexDataHover<this.data.length){
			this.context.beginPath();
      this.context.fillStyle = '#ff52c8';
      //this.context.fillStyle = '#1ed36f';
      dot = this.data[this.indexDataHover]
      if(dot[1]!==0){
        yRounded = 1/(Math.pow(10, Math.floor(Math.log(Math.abs(dot[1])) / Math.LN10))/100)
        yRounded = Math.floor(dot[1]*yRounded)/yRounded;
      } else yRounded = 0
      let offsetY = ((this.yMax - dot[1]) * this.height) / this.yRange < 20 ? 20 : -5;
      this.context.fillText('(' + dot[0] + ',' + yRounded + ')', ((dot[0] - this.xMin) * this.width) / this.xRange - 5, ((this.yMax - dot[1]) * this.height) / this.yRange + offsetY);
			this.context.closePath();
    }
	}

	// Create a blue grid as a background
	//
	grid() {
		// Set the canvas' internal image size to match the actual
		// size it takes on the web page, and clear canvas.
		this.height = this.canvas.height = this.canvas.parentElement.clientHeight;
		this.width  = this.canvas.width = this.canvas.parentElement.clientWidth;

		// Because we reset the width & height above, the canvas clears itself.
		// This is required by the draft spec, otherwise we could clear manually:
    var theme = themeManager.variable[document.getElementById('theme-switch').checked?'dark':'light']['ui']
    this.context.fillStyle = theme['bg-0'] //'#222'; // white background
		this.context.fillRect(0, 0, this.width, this.height);

		// Compute numeric range of 14 pixels, the smallest grid square.
		var rangeMin = this.xRange * this._resMax / this.width;
		// Find the first round number above that range
		var gridRange = Math.pow(10, Math.ceil(Math.log(rangeMin) / Math.LN10));
		// See if we can reduce it by half (0.5) or one-fifth (0.2)

    if(rangeMin <= this.dt) gridRange = this.dt
		else if ((gridRange * 0.2) > rangeMin) {
			gridRange *= 0.2;
		}
		else if ((gridRange * 0.5) > rangeMin) {
			gridRange *= 0.5;
		}
    var lineColorOrigin = theme['text-hover'] //'#fff';
    var lineColorUnlabeled = theme['text'] //'#999';
    var lineColorLabeled = theme['theme'] //'#82d8ff';

		// Setup some drawing options
		this.context.lineWidth = 1;
		this.context.textBaseline = 'bottom';

		// Draw the X grid lines
		//
		var index = Math.floor(this.xMin / gridRange);
		var xGrid = index * gridRange;
		for (; xGrid <= this.xMax; xGrid = index * gridRange) {
			var xPos = (((xGrid - this.xMin) * this.width) / this.xRange);
			if (Math.abs(index) < 1e-9) {
				// Mark the origin of the grid
				if (xPos > 20) {
					this.context.fillStyle = lineColorOrigin;
					this.context.font = 'bold 13px sans-serif';
					this.context.fillText("0.0", xPos + 2, this.height - 2);
				}

				this.context.strokeStyle = lineColorOrigin;
			}
			else if ((index % 5) != 0) {
				// Draw an unlabeled grid line
				this.context.strokeStyle = lineColorUnlabeled;
			}
			else {
				// Draw and label a grid line
				if (xPos > 20) {
					this.context.fillStyle = lineColorLabeled;
					this.context.font = 'bold 13px sans-serif';
					this.context.fillText(this.makeLabel(xGrid), xPos + 2, this.height - 2);
				}

				this.context.strokeStyle = lineColorLabeled;
			}

			// Draw the actual line
			this.context.beginPath();
			this.context.moveTo(xPos, 0);
			this.context.lineTo(xPos, this.height);
			this.context.stroke();
			this.context.closePath();

			++index;
		}

		// Find the Y gridrange
		var rangeMin = this.yRange * this._resMax / this.height;
		var gridRange = Math.pow(10, Math.ceil(Math.log(rangeMin) / Math.LN10));
		if ((gridRange * 0.2) > rangeMin) {
			gridRange *= 0.2;
		}
		else if ((gridRange * 0.5) > rangeMin) {
			gridRange *= 0.5;
		}

		// Draw the Y grid lines
		//
		index = Math.floor(this.yMin / gridRange);
		var yGrid = index * gridRange;
		for (; yGrid <= this.yMax; yGrid = index * gridRange) {
			var yPos = (((this.yMax - yGrid) * this.height) / this.yRange);
			if (Math.abs(index) < 1e-9) {
				// Mark the origin of the grid
				this.context.fillStyle = lineColorOrigin;
				this.context.font = 'bold 13px sans-serif';
				this.context.fillText("0.0", 2, yPos);

				this.context.strokeStyle = lineColorOrigin;
			}
			else if ((index % 5) != 0) {
				// Draw an unlabeled grid line
				this.context.strokeStyle = lineColorUnlabeled;
			}
			else {
				// Draw and label a grid line
				this.context.fillStyle = lineColorLabeled;
				this.context.font = 'bold 13px sans-serif';
				this.context.fillText(this.makeLabel(yGrid), 2, yPos);

				this.context.strokeStyle = lineColorLabeled;
			}

			// Draw the actual line
			this.context.beginPath();
			this.context.moveTo(0, yPos);
			this.context.lineTo(this.width, yPos);
			this.context.stroke();
			this.context.closePath();

			++index;
		}
	}

	// Good reference site: http://www.w3schools.com/jsref/event_onmousedown.asp
	//                 and: http://www.w3schools.com/jsref/dom_obj_event.asp

	// Capture "onmousedown" events, and record the mouse button and position.
	//
	clickCanvas(e) {
		if (!e) var e = window.event;
		// e.button: 0 is left, 1 is middle, 2 is right.
		if (e.button == 0) {
			this.slideButtonDown = true;
		}
		this.lastMouseX = e.clientX;
		this.lastMouseY = e.clientY;

		if (this.slideButtonDown) {
			e.preventDefault();
			e.stopPropagation();
			return false;
		} else {
			return true;
		}
	}

	// Turn off sliding/zooming if the mouse button is released, or the mouse leaves the canvas.
	//
	releaseCanvas(e) {
		const rect = this.canvas.getBoundingClientRect();
    if (!this.inMove && this.isInCanvas(e.clientX, e.clientY, rect)){
			if(e.button === 0){
				var x = Math.round((this.xMin+(e.clientX-rect.left) / this.width * this.xRange)/this.dt)*this.dt;
				var y = (this.yMax-(e.clientY-rect.top) / this.height * this.yRange);
				var indexDot = this.data.findIndex((dot)=>dot[0]==x)
        console.log("releaseCanvas : ("+x+", "+y+")")
				if(indexDot!=-1)this.data[indexDot][1] = y
				else if(x>=0) this.data.push([x,y]);
        this.data.sort((a, b) => { return a[0] - b[0] })
        this.indexDataHover = this.data.findIndex((dot)=>dot[0]==x)
				//this.data = Object.fromEntries(Object.entries(this.data).sort(([a,],[b,]) => parseFloat(a)-parseFloat(b)));
			} else if(e.button === 2 && this.indexDataHover>=0){
				//const rect = this.canvas.getBoundingClientRect();
				//var x = Math.round((this.xMin+(e.clientX-rect.left) / this.width * this.xRange)/this.dt)*this.dt;
				//var indexDot = this.data.findIndex((dot)=>dot[0]==x)
				//if(indexDot != -1) this.data.splice(indexDot, 1);
				this.data.splice(this.indexDataHover, 1);
        this.indexDataHover = -1;
			}
		}
			this.plot();
		this.slideButtonDown = false;
		this.inMove = false;
	}

	// On mouse move events, if a button is known to be down, slide or zoom the canvas.
	//
	slideCanvas(e) {
		var didSomething = false;
    var lastIndexDataHover = this.indexDataHover;
    this.indexDataHover = -1;
    const rect = this.canvas.getBoundingClientRect();
		if (!e) { e = window.event; }
		if (this.slideButtonDown && lastIndexDataHover < 0) {
			this.inMove = true;
			var slideX = (e.clientX - this.lastMouseX) * this.xRange / this.width;
			var slideY = (e.clientY - this.lastMouseY) * this.yRange / this.height;
      if(slideX>0 || this.xMax<this.maxTime){
        if(this.xMax-slideX>=this.maxTime){
          slideX = this.maxTime-this.xMax;
          this.xMin += slideX
          this.xMax = this.maxTime;
        } else {
          this.xMin -= slideX
          this.xMin = this.xMin<0?0:this.xMin;
          this.xMax = this.xMin + this.xRange;
        }
        /*
        this.yMin += slideY;
        this.yMin = this.yMin<0?0:this.yMin;
        this.yMax = this.yMin + this.yRange;*/
        this._xMid = (this.xMin + this.xMax) * 0.5;
        this._yMid = (this.yMin + this.yMax) * 0.5;

        this.lastMouseX = e.clientX;
        this.lastMouseY = e.clientY;
        this.plot();
      }
        didSomething = true;
		} else if(this.slideButtonDown && lastIndexDataHover>=0){
			this.inMove = true;
			var y = (this.yMax-(e.clientY-rect.top) / this.height * this.yRange);
      this.data[lastIndexDataHover][1] = y;
      this.indexDataHover = lastIndexDataHover;
      didSomething = true;
      this.plot()
    } else if(this.isInCanvas(e.clientX,e.clientY,rect)){
      var x = Math.round((this.xMin+(e.clientX-rect.left) / this.width * this.xRange)/this.dt)*this.dt;
			var yinf = (this.yMax-(e.clientY+7-rect.top) / this.height * this.yRange);
			var ysup = (this.yMax-(e.clientY-7-rect.top) / this.height * this.yRange);
      for(var i=0; i<this.data.length;i++){
        if(this.data[i][0]==x){
          if(this.data[i][1]>yinf && this.data[i][1]<ysup) this.indexDataHover = i;
          break;
        }
      }
      if(this.indexDataHover !== lastIndexDataHover){
          this.plot();
      }
    }

		if (didSomething) {
			e.preventDefault();
			e.stopPropagation();
			return false;
		} else {
			return true;
		}
	}

  isInCanvas(x, y, rect) {
    if(!rect) rect = this.canvas.getBoundingClientRect();
    return x > rect.left && x < rect.left + rect.width && y > rect.top && y < rect.top + rect.height
  }

	zoomCanvas(e) {
		if (!e) { e = window.event; }
		var scale = 1+e.deltaY*0.001;
    this.changeScale(scale, e.clientX)
    this.lastMouseX = e.clientX;
    this.lastMouseY = e.clientY;
	}

  changeScale(scale,xP){
    var new_X_range;

    if (scale < 1.0) {
      new_X_range = Math.max(this.width / this._resMax * this.dt, (this.xRange * scale));
      new_X_range = Math.min(this.maxTime, new_X_range);
    }
    else {
      new_X_range = Math.min(this.maxTime, (this.xRange * scale));
      new_X_range = Math.max(this.width / this._resMax * this.dt, new_X_range);
    }
    var xMid;
    if(xP) xMid = (this.xMin + (xP - this.canvas.getBoundingClientRect().left) * (this.xRange - new_X_range) / this.width) + new_X_range * 0.5;
    else xMid = new_X_range/2

    this.setZoom(xMid, this.yRange / 2, new_X_range, this.yRange);
    this.plot();
  }

  setYmaxZoom(yMax){
    this.setZoom(this.xMin+this.xRange/2, (yMax+this.yMin)/2, this.xRange, yMax-this.yMin);
  }
  setYminZoom(yMin){
    this.setZoom(this.xMin+this.xRange/2, (this.yMax+yMin)/2, this.xRange, this.yMax-yMin);
  }
  setDt(dt){
    this.dt = dt;
    this.changeScale(1)
  }
  setMaxtime(maxTime){
    this.maxTime = maxTime;
    this.setZoom(maxTime / 2, (this.yMax + this.yMin) / 2, maxTime, this.yMax - this.yMin);
  }

}


    customElements.define('parameter-widget', class extends HTMLElement {
        constructor() {
            super();
            const style = document.createElement('style');

            style.textContent = `

* {
    box-sizing: border-box;
    margin: 0;
    outline: none;
}
:host{
    background-color: var(--bg-3);
    font-family: -apple-system, BlinkMacSystemFont, "Segoe WPC", "Segoe UI", "HelveticaNeue-Light", system-ui, "Ubuntu", "Droid Sans", sans-serif;
    display: flex;
    flex-flow: column;
    height: 100%;
    overflow: auto;
}
.parameters-container {
    width: 100%;
    height: auto;
    padding: 20px !important;
    display: flex;
    flex-flow: row wrap;
    align-items: stretch;
    align-content: flex-start;
    gap:20px;
    background-color: var(--bg-3);
}
.param-widget {
    min-width: min(400px,100%);
    flex: 1 0 0;
    border-radius: 10px;
    background-color: var(--bg-0);
    padding: 10px !important;
    position: relative;
    height: fit-content;
}
.param-widget-large {
    min-width: min(700px,100%);
    flex: 1 0 0;
    border-radius: 10px;
    background-color: var(--bg-0);
    padding: 10px !important;
    position: relative;
    height: fit-content;
}
.name {
    background-color: var(--bg-0);
    color: var(--text-2);
    border-radius: 5px;
    position: absolute;
    padding: 5px;
    top: -15px;
    border: 1px solid var(--border);
    left:15px;
    height: 30px;
    width: fit-content;
    font-weight: bold;
}
.slider-container{
    color: var(--text-2);
    display: grid;
    grid-template-columns: fit-content(50%) auto 15px;
    gap:3px 0px;
    text-overflow: ellipsis;
    place-items: stretch;
    padding-top: 8px;
}

.slider-container-group{
    color: var(--text-2);
    display: grid;
    grid-template-columns: fit-content(50%) auto;
    gap:3px 0px;
    text-overflow: ellipsis;
    place-items: stretch;
    padding-top: 8px;
}
.checkbox-container input {
    height: 24px;
    width: 24px;
}
.slider-container span:has(+ *:hover),
.slider-container span:has(+ * + *:hover),
.slider-container span:hover + *,
.slider-container span + *:has(+ *:hover),
.slider-container span:hover + * + *,
.slider-container span + *:hover + *,
.slider-container > *:hover {
    filter: brightness(1.2) !important;
}
.slider-container-group > *:hover,
.slider-container-group span:hover + *,
.slider-container-group span:has(+ *:hover) {
    filter: brightness(1.2) !important;
}
input[type="text"]:hover {
    filter: brightness(1.2);
}
.slider-container > *:nth-child(6n + 4 of *:not(.notuse):not(.min-max)),
.slider-container > *:nth-child(6n + 5 of *:not(.notuse):not(.min-max)),
.slider-container > *:nth-child(6n + 6 of *:not(.notuse):not(.min-max)) {
  filter: invert(0.05);
}

.slider-container-group > *:nth-child(4n + 3 of *:not(.notuse):not(.visibility-wraper):not(.min-max)),
.slider-container-group > *:nth-child(4n + 4 of *:not(.notuse):not(.visibility-wraper):not(.min-max)) {
    filter: invert(0.05);
}
.notuse {
    display:none !important;
}
.slider-container span,.slider-container-group span {
    grid-column: 1;
    overflow: hidden;
    text-overflow: ellipsis;
    text-align: end;
    padding: 3px 10px 0 0;
    background-color: var(--bg-0);
}
.min-max {
    margin-left: 75px;
    grid-column: 2;
    display: flex;
    justify-content: space-between;
}
.min-max input:first-child{
    text-align: start;
}

input[type="text"] {
    height: 24px;
    width: 65px;
    background-color: var(--bg-input);
    color: var(--text-2);
    border:none;
    text-align: end;
    padding: 5px;
}
input[type='checkbox']{
    height: 24px;
    width: 24px;
    accent-color:var(--theme);
    background-color: var(--bg-0);
}
.checkbox-wraper{
    background-color: var(--bg-0);
}
.slider-wraper{
    background-color: var(--bg-0);
    display: flex;
}
.slider-wraper input:first-child{
    margin-right:10px;
}
.visibility-wraper{
    background-color: var(--bg-0);
    padding-left:10px;
    width: 22px;
    cursor: pointer;
}
.slider-container-group .visibility-wraper {
    display:none;
}
.icon-hide {
    background: #b50c3b;
    display: block;
  text-decoration: none;
  transform: translate(-50%, 0%);
  margin-top:6px;
    width: 17px;
    height: 13px;
  -webkit-mask-image: url('data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512"><path d="M320 400c-75.85 0-137.25-58.71-142.9-133.11L72.2 185.82c-13.79 17.3-26.48 35.59-36.72 55.59a32.35 32.35 0 0 0 0 29.19C89.71 376.41 197.07 448 320 448c26.91 0 52.87-4 77.89-10.46L346 397.39a144.13 144.13 0 0 1-26 2.61zm313.82 58.1l-110.55-85.44a331.25 331.25 0 0 0 81.25-102.07 32.35 32.35 0 0 0 0-29.19C550.29 135.59 442.93 64 320 64a308.15 308.15 0 0 0-147.32 37.7L45.46 3.37A16 16 0 0 0 23 6.18L3.37 31.45A16 16 0 0 0 6.18 53.9l588.36 454.73a16 16 0 0 0 22.46-2.81l19.64-25.27a16 16 0 0 0-2.82-22.45zm-183.72-142l-39.3-30.38A94.75 94.75 0 0 0 416 256a94.76 94.76 0 0 0-121.31-92.21A47.65 47.65 0 0 1 304 192a46.64 46.64 0 0 1-1.54 10l-73.61-56.89A142.31 142.31 0 0 1 320 112a143.92 143.92 0 0 1 144 144c0 21.63-5.29 41.79-13.9 60.11z"/></svg>');
}
.icon-show {
    background: #44aa5f;
    display: block;
  text-decoration: none;
  margin-top:6px;
  transform: translate(-50%, 0%);
  width: 16px;
    height: 14px;
  -webkit-mask-image: url('data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path d="M288 80c-65.2 0-118.8 29.6-159.9 67.7C89.6 183.5 63 226 49.4 256c13.6 30 40.2 72.5 78.6 108.3C169.2 402.4 222.8 432 288 432s118.8-29.6 159.9-67.7C486.4 328.5 513 286 526.6 256c-13.6-30-40.2-72.5-78.6-108.3C406.8 109.6 353.2 80 288 80zM95.4 112.6C142.5 68.8 207.2 32 288 32s145.5 36.8 192.6 80.6c46.8 43.5 78.1 95.4 93 131.1c3.3 7.9 3.3 16.7 0 24.6c-14.9 35.7-46.2 87.7-93 131.1C433.5 443.2 368.8 480 288 480s-145.5-36.8-192.6-80.6C48.6 356 17.3 304 2.5 268.3c-3.3-7.9-3.3-16.7 0-24.6C17.3 208 48.6 156 95.4 112.6zM288 336c44.2 0 80-35.8 80-80s-35.8-80-80-80c-.7 0-1.3 0-2 0c1.3 5.1 2 10.5 2 16c0 35.3-28.7 64-64 64c-5.5 0-10.9-.7-16-2c0 .7 0 1.3 0 2c0 44.2 35.8 80 80 80zm0-208a128 128 0 1 1 0 256 128 128 0 1 1 0-256z"/></svg>');
}
.hide {
    visibility:hidden;
}
.slider{
    height: 24px;
    width: 100%;
    background-color: var(--bg-input);
    border-left: 1px solid var(--border);
    border-right: 1px solid var(--border);
    cursor: col-resize;
    min-width: 100px;
    user-select: none;
}
.slider .slider-position {
    height: 24px;
    border-right: 2px solid var(--border);
    background-color:var(--theme);
}
.iffg {
    margin-left: 5px;
    margin-top: 5px;
    position: relative;
    display: flex;
    width: 100%;
}
.iffg span {
    text-align: right;
    margin-top: 5px;
    width: 100%;
    font-size: 12px;
    overflow: unset !important;
    text-overflow: unset !important;
}
.toggle-show {
    background-color:var(--bg-0);
    position: absolute;
    right: -7px;
    top: -14px;
    width: 26px;
    height:26px;
    border-radius:13px;
    border: 1px solid var(--border);
    cursor: pointer;
}

.toggle-show:hover {
    background-color:var(--theme);
}
.toggle-show:hover .icon-show {
  background: var(--bg-0);
}
.toggle-show .icon-show {
  transform: translate(23%, -10%);
  background: var(--theme);
}
.header {
    height:40px;
    width:100%;
    position: sticky;
    z-index:1;
    top: 0px;
    background-color: var(--bg-1);
    padding: 6px 20px;
    border-bottom: 2px solid var(--border);
    display: flex;
}
.header input{
    width: 60%;
    border-radius: 15px;
    height: 27px;
    font-size: 17px;
    margin-left: 40%;
    transform: translate(-40%, 0);
    text-align: center;
    border: 1px solid var(--border);
}
.header .btn {
    background-color: var(--theme);
    color: #222;
    white-space: nowrap;
    height: 25px;
    line-height: 23px;
    width: 25px;
    text-align: center;
    border-radius: 5px;
    border: 1px solid var(--border);
    cursor: pointer;
}
`;
            //const shadow = this.attachShadow({ mode: 'open' })
            this.shadow = this.attachShadow({ mode: 'closed' })
            // Attach the created elements to the shadow dom
            this.shadow.appendChild(style);

            this.header = document.createElement('div')
            this.header.className = 'header'
            this.header.innerHTML = `<input type="text" placeholder="parameters filter"><a class="btn">{ }</a><a class="btn"> = </a>`
            this.searchBar = this.header.querySelector('input')
            this.searchBar.onkeyup = ()=>{this.search(this.searchBar.value)}
            this.shadow.appendChild(this.header);
            const [btnExport,btnChangeView] = this.header.querySelectorAll('.btn')
            btnExport.onclick = ()=>{
                if(!this.json) return; 
                this.callbackExportJson(this.getJsonParameter())
            }
            this.btnChangeView = btnChangeView
            btnChangeView.onclick = ()=>{
                this._generateDOM(!this.forceByType)
            }

            this.parametersContainer = document.createElement('div')
            this.parametersContainer.className = 'parameters-container'
            this.shadow.appendChild(this.parametersContainer);
        }

        _setupByType(json,parameterName,color,config) {
            const root = document.createElement('div')
            root.className = 'param-widget'
            root.setAttribute('param',parameterName)
            const container = document.createElement('div')
            container.className = 'slider-container-group'
            root.appendChild(container)
            const nameDOM = document.createElement('div')
            nameDOM.className = "name"
            nameDOM.innerText = parameterName
            root.appendChild(nameDOM)
            const btnToggleShow = document.createElement('a')
            btnToggleShow.innerHTML = '<i class="icon-show"></i>'
            btnToggleShow.className = "toggle-show"
            btnToggleShow.onclick = ()=>{if(container.className === 'slider-container')updateVisibility();else showAll()}
            root.appendChild(btnToggleShow)
            if(!config.hidden)config.hidden = []
            const updateVisibility = ()=>{
                for(const type in json){
                    container.querySelectorAll(`[celltype="${type}"]`).forEach(el=>{if(config.hidden.includes(type))el.classList.add('notuse')})
                }
                container.className = "slider-container-group"
            }
            const showAll = ()=>{
                container.querySelectorAll(`[celltype]`).forEach(el=>el.classList.remove('notuse'))
                container.className = "slider-container"
            }

            if(config.type==='double' || config.type ==='float' || config.type ==='int'){
                let html = `<div class="min-max"><input type="text" value="${config?.min || 0}"><input type="text" value="${config?.max || 1}"></div>`
                for (const type in json) {
                    if (json[type][parameterName] === undefined && !config.hidden.includes(type)) config.hidden.push(type)
                    //if (type!=='Common') config.hidden.push(type)
                    html += `<span celltype="${type}" titlte=${type}>${type}</span><div class="slider-wraper" celltype="${type}"><input type="text" value="${json[type][parameterName] || 0}"><div class="slider"><div class="slider-position" style="background-color:${color[type]}"></div></div></div><a celltype="${type}" class="visibility-wraper" ${type === 'Common'?'style="visibility:hidden"':'' }><i celltype="${type}" class="icon-${config.hidden.includes(type) ? 'show' : 'hide'}"></i></a>`
                }
                container.innerHTML = html
                const [minE, maxE] = container.querySelector('.min-max').querySelectorAll('input');
                container.querySelector('.min-max').addEventListener('keyup', (e) => {
                    if (e.target === minE) config.min = Number(minE.value)
                    else if (e.target === maxE) config.max = Number(maxE.value)
                    if(config.type === 'int'){
                         minE.value = config.min = Math.round(config.min)
                         maxE.value = config.max = Math.round(config.max)
                    }
                    sliders.forEach(s => s.update())
                })
                const sliders = container.querySelectorAll('.slider')
                container.querySelectorAll('.slider-wraper > input').forEach((inputParameter, i) => {
                    const slider = sliders[i]
                    const type = inputParameter.parentElement.getAttribute('celltype')
                    slider.setValue = (pc) => slider.firstElementChild.style.width = (pc < 0 ? 0 : pc > 1 ? 1 : pc) * 100 + '%';
                    slider.update = () => slider.setValue((Number(inputParameter.value) - config.min) / (config.max - config.min))
                    slider.updateFromClick = (e) => {
                        if (e.touches) e = e.touches[0];
                        const rect = slider.getBoundingClientRect();
                        let pc = (e.clientX - rect.left) / rect.width; pc = (pc < 0 ? 0 : pc > 1 ? 1 : pc);
                        if(config.type === 'int') json[type][parameterName] = inputParameter.value = Math.round(config.min + pc * (config.max - config.min))
                        else json[type][parameterName] = inputParameter.value = config.min + pc * (config.max - config.min)
                        slider.update()
                    }
                    inputParameter.addEventListener('keyup', () => {
                        if(config.type === 'int') json[type][parameterName] = Math.round(Number(inputParameter.value));
                        else json[type][parameterName] = Number(inputParameter.value);
                        slider.update();
                    })
                    slider.update()
                })
                sliders.forEach(s => {
                    s.onmousedown = s.ontouchstart = (e) => {
                        s.updateFromClick(e);
                        document.ontouchmove = document.onmousemove = (e) => { s.updateFromClick(e); }
                        document.onmouseup = document.ontouchend = () => { document.ontouchmove = document.onmousemove = document.onmouseup = document.ontouchend = null }
                    }
                })
            } else if(config.type === 'bool'){
                let html = ''
                for (const type in json) {
                    if (json[type][parameterName] === undefined && !config.hidden.includes(type)) config.hidden.push(type)
                    html += `<span celltype="${type}" titlte=${type}>${type}</span><div class="checkbox-wraper" celltype="${type}"><input type="checkbox" style="accent-color: ${color[type]};" ${json[type][parameterName]?'checked':''}></div><a celltype="${type}" class="visibility-wraper" ${type === 'Common'?'style="visibility:hidden"':'' }><i celltype="${type}" class="icon-${config.hidden.includes(type) ? 'show' : 'hide'}"></i></a>`
                }
                container.innerHTML = html
                container.querySelectorAll('input').forEach(i=>{
                    const type = i.parentElement.getAttribute('celltype')
                    i.onchange=()=>json[type][parameterName] = i.checked
                })
            }

            const btnHide = container.querySelectorAll("a[class='visibility-wraper']").forEach(b=>{
                b.onclick = ()=>{
                    const type=b.getAttribute('celltype');
                    const idx=config.hidden.indexOf(type);
                    if(idx>=0)config.hidden.splice(idx,1);
                    else config.hidden.push(type);
                    b.firstChild.classList.toggle('icon-show')
                    b.firstChild.classList.toggle('icon-hide')
                }
            })
            updateVisibility()
            this.parametersContainer.appendChild(root);
        
        }


        _setupGroup(json,type,config,isHiddable=false) {
            const root = document.createElement('div')
            root.className = 'param-widget-large'
            const container = document.createElement('div')
            container.className = 'slider-container-group'
            root.appendChild(container)
            const nameDOM = document.createElement('div')
            nameDOM.className = "name"
            nameDOM.innerText = type
            root.appendChild(nameDOM)
            let html = ''
            for (const param in json) {
                if(config[param].type==='double' || config[param].type ==='float' || config[param].type ==='int'){
                    html += `<span title="${param}" param="${param}">${param}</span><div class="slider-wraper" param="${param}"><input type="text" value="${json[param] || 0}"><input type="text" value="${config[param].min || 0}"><div class="slider"><div class="slider-position"></div></div><input type="text" value="${config[param].max || 0}"></div>`
                } else if(config[param].type === 'bool'){
                    html += `<span title="${param}" param="${param}">${param}</span><input type="checkbox" param="${param}"  ${json[param]?'checked':''}>`
                } else if(config[param].type === 'Protocol'){
                    html += `<span title="${param}" param="${param}">${param}<div class="iffg"><span>yMin</span><input type="text" class="iff" value="${config[param].ymin}"></div><div class="iffg"><span>yMax</span><input type="text" class="iff" value="${config[param].ymax}"></div><div class="iffg"><span>dt</span><input type="text" class="iff"  value="${config[param].dt}"></div><div class="iffg"><span>maxTime</span><input type="text" class="iff"  value="${config[param].maxTime}"></div></span></div><div param="${param}" class='timeline'></div>`
                }
                if(isHiddable){
                    html += `<a param="${param}" class="visibility-wraper" ${type === 'Common'?'style="visibility:hidden"':'' }><i param="${param}" class="icon-${config[param].hidden.includes(type) ? 'show' : 'hide'}"></i></a>`
                }
            }
                container.innerHTML = html
                
            if(isHiddable){
                const btnToggleShow = document.createElement('a')
                btnToggleShow.innerHTML = '<i class="icon-show"></i>'
                btnToggleShow.className = "toggle-show"
                btnToggleShow.onclick = ()=>{if(container.className === 'slider-container')updateVisibility();else showAll()}
                root.appendChild(btnToggleShow)
                //if(!config.hidden)config.hidden = []
                const updateVisibility = ()=>{
                    for(const param in json){
                        container.querySelectorAll(`[param="${param}"]`).forEach(el=>{if(config[param].hidden.includes(type))el.classList.add('notuse')})
                    }
                    container.className = "slider-container-group"
                }
                const showAll = ()=>{
                    container.querySelectorAll(`[param]`).forEach(el=>el.classList.remove('notuse'))
                    container.className = "slider-container"
                }

                container.querySelectorAll("a[class='visibility-wraper']").forEach(b=>{
                    b.onclick = ()=>{
                        const param = b.getAttribute('param');
                        const idx = config[param].hidden.indexOf(type);
                        if(idx>=0)config[param].hidden.splice(idx,1);
                        else config[param].hidden.push(type);
                        b.firstChild.classList.toggle('icon-show')
                        b.firstChild.classList.toggle('icon-hide')
                    }
                })
                updateVisibility()
            }
                container.querySelectorAll('.timeline').forEach(t=>{
                    const param = t.getAttribute('param')
                   t.tl = new TimeLine(t,json[param])
                   t.tl.setYminZoom(config[param].ymin);
                   t.tl.setYmaxZoom(config[param].ymax);
                   t.tl.setDt(config[param].dt);
                   t.tl.setMaxtime(config[param].maxTime)
                   setTimeout(()=>t.tl.plot(),10)
                   const [yMin,yMax,dt,maxTime] = t.previousElementSibling.querySelectorAll('input')
                   yMin.onkeyup = () =>{ config[param].ymin = Number(yMin.value); t.tl.setYminZoom(config[param].ymin);}
                   yMax.onkeyup = () =>{ config[param].ymax = Number(yMax.value); t.tl.setYmaxZoom(config[param].ymax);}
                   dt.onkeyup = () =>{ config[param].dt = Number(dt.value); t.tl.setDt(config[param].dt);}
                   maxTime.onkeyup = () =>{ config[param].maxTime = Number(maxTime.value); t.tl.setMaxtime(config[param].maxTime);}
                })

                
     
                const sliders = container.querySelectorAll('.slider')
                container.querySelectorAll('.slider-wraper > input:first-child').forEach((inputParameter, i) => {
                    const slider = sliders[i]
                    const param = inputParameter.parentElement.getAttribute('param')

                    const [minE, maxE] = inputParameter.parentElement.querySelectorAll('input:not(:first-child)');
                    minE.onkeyup = (e) => {
                        if(minE.value!==''){
                            if(config[param].type === 'int') config[param].min = Math.round(minE.value)
                            else config[param].min = minE.value
                            this.parametersContainer.querySelectorAll(`.slider-wraper[param=${param}] > input:nth-child(2 of input)`).forEach(e=>{e.value=config[param].min;e.parentElement.querySelector('.slider').update()})
                        }
                    }
                    maxE.onkeyup = (e) => {
                        if(maxE.value!==''){
                            if(config[param].type === 'int') config[param].max = Math.round(maxE.value)
                            else config[param].max = maxE.value
                            this.parametersContainer.querySelectorAll(`.slider-wraper[param=${param}] > input:nth-child(3 of input)`).forEach(e=>{e.value=config[param].max;e.parentElement.querySelector('.slider').update()})
                        }
                    }

                    slider.setValue = (pc) => slider.firstElementChild.style.width = (pc < 0 ? 0 : pc > 1 ? 1 : pc) * 100 + '%';
                    slider.update = () => slider.setValue((Number(inputParameter.value) - config[param].min) / (config[param].max - config[param].min))
                    slider.updateFromClick = (e) => {
                        if (e.touches) e = e.touches[0];
                        const rect = slider.getBoundingClientRect();
                        let pc = (e.clientX - rect.left) / rect.width; pc = (pc < 0 ? 0 : pc > 1 ? 1 : pc);
                        if(config[param].type === 'int') json[param] = inputParameter.value = Math.round(config[param].min + pc * (config[param].max - config[param].min))
                        else json[param] = inputParameter.value = config[param].min + pc * (config[param].max - config[param].min)
                        slider.update()
                    }
                    inputParameter.onkeyup= () => {
                        if(config[param].type === 'int') json[param] = Math.round(Number(inputParameter.value));
                        else json[param] = Number(inputParameter.value);
                        slider.update();
                    }
                    slider.update()
                })
                sliders.forEach(s => {
                    s.onmousedown = s.ontouchstart = (e) => {
                        s.updateFromClick(e);
                        document.ontouchmove = document.onmousemove = (e) => { s.updateFromClick(e); }
                        document.onmouseup = document.ontouchend = () => { document.ontouchmove = document.onmousemove = document.onmouseup = document.ontouchend = null }
                    }
                })
                container.querySelectorAll("input[type='checkbox']").forEach(i=>{
                    const param = i.getAttribute('param')
                    i.onchange=()=>json[param] = i.checked
                })
            this.parametersContainer.appendChild(root);

        }

        setup(json,config,color,callbackExportJson){
            this.callbackExportJson = callbackExportJson
            this.listParam = [...Object.keys(json['Cell']['Common']),...Object.keys(json['Scenario'])]
            this.json = json
            this.config = config
            this.color = color
            this.btnChangeView.style.display = Object.keys(json['Cell']).length>1?null:'none'
            /*
            for(const p in json['Cell']['Common'])
                for(const t in json['Cell'])
                    if(json['Cell'][t][p]===undefined)json['Cell'][t][p]=0*/
            this._generateDOM()
            this._jsonBackup = JSON.stringify(json)
            this._configBackup = JSON.stringify(config)
        }

        get changeDetected(){
            const currentJson = JSON.stringify(this.json)
            const currentConfig = JSON.stringify(this.confg)
            const ret = currentJson!==this._jsonBackup || currentConfig!==this._configBackup
            this._jsonBackup = currentJson
            this._configBackup = currentConfig
            return ret
        }

        _generateDOM(forceByType=false){
            if(!this.json) return; 
            this.forceByType = forceByType
            this.parametersContainer.innerHTML = ''
            const paramsEditor = document.createElement('div')
            paramsEditor.className = "parameters-container"

            if(Object.keys(this.json['Cell']).length>1){
                if(forceByType){
                    for(const type in this.json['Cell']){
                        this._setupGroup(this.json['Cell'][type],type,this.config['Cell'],type!=='Common')
                    }
                } else {
                    for(const cellAttr in this.json['Cell']['Common']){
                        this._setupByType(this.json['Cell'],cellAttr,this.color,this.config['Cell'][cellAttr])
                    }
                    const spacer = document.createElement('div')
                    spacer.style.width = '100%'
                    this.parametersContainer.appendChild(spacer)
                }
            } else {
                this._setupGroup(this.json['Cell']['Common'],'Cell',this.config['Cell'])
            }
            this._setupGroup(this.json['Scenario'],'Scenario',this.config['Scenario'])
            this.search(this.searchBar.value)
        }

        clear(){
            this.parametersContainer.innerHTML = ''
            this.json = this.config = this.color = null
        }

        search(text){
            this.parametersContainer.querySelectorAll('[param]').forEach(p=>p.style.display=null)
            if(text!=='')
                this.listParam.filter(ps=>!ps.toLowerCase().includes(text.toLowerCase())).forEach(ps=>this.parametersContainer.querySelectorAll(`[param="${ps}"]`).forEach(p=>p.style.display='none'))
                this.parametersContainer.querySelectorAll('.timeline').forEach(t=>t.tl.plot())
        }

        getJsonParameter(json,config){
            if(!json)json=this.json
            if(!json) return {}
            if(!config)config=this.config
            const cleanJson = JSON.parse(JSON.stringify(json))
            if(Object.keys(json['Cell']).length>1) {
                for(const param in json['Cell']['Common']){
                    if(config['Cell'][param]!==undefined)
                    for(const type of config['Cell'][param].hidden){
                        delete cleanJson['Cell'][type][param]
                    }
                }
            }
            return cleanJson
        }

    });