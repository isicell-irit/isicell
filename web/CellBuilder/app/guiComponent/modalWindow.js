import '../css/modal.css'
/**
 * ModalWindows class
 * 
 */
export default class ModalWindows {

    /**
     * The constructor of the modal window
     * @param {*} header Header title of the window
     * @param {*} content HTML content of the window
     * @param {*} callbackClose The callback function to call if the close button is clicked
     * @param {*} callbackFocusOut The callback function to call if the user clicks outside of the window
     */
    constructor(header,content,callbackFocusOut=()=>{},callbackClose=()=>{},options={closeBtn : true}) {
        this.content = content
        this.content.classList.add('modal-content')
        this.callbackClose = callbackClose
        this.callbackFocusOut = callbackFocusOut
        this.main = document.createElement('div')
        this.main.className = 'modal'
        this.main.style.zIndex = 100;
        this.contentAndHeader = document.createElement('div')
        this.contentAndHeader.className = 'modal-header'
        var spanHeader = document.createElement('span')
        spanHeader.innerText = header;
        if(options.closeBtn){
            var spanClose = document.createElement('span')
            spanClose.className = 'close'
            spanClose.innerHTML = '&times;'
        }
        this.contentAndHeader.appendChild(spanHeader)
        if(options.closeBtn)this.contentAndHeader.appendChild(spanClose)
        this.contentAndHeader.appendChild(content)
        this.main.appendChild(this.contentAndHeader)
        document.body.appendChild(this.main)

        if(options.closeBtn) spanClose.onclick = ()=>{this.callbackClose(this);this.main.style.display = "none"}; // What happens if the user clicks the close button
    }

    /**
     * Shows the modal window
     */
    show(){
        window.onclick = (event)=>{ // If the user clicks outside the window
            if (event.target === this.main) {
                this.callbackFocusOut(this); // call cancel callback
                this.main.style.display = "none"; // hide the window
            }
        }
        this.main.style.display = "block";
        this.content.focus();
    }

    /**
     * Hides the modal window
     */
    hide(){
        this.main.style.display = "none";
    }

}