import Toastr from 'toastr2';
import '../css/notification.css'
const toastr = new Toastr({
    "closeButton": true,
    "debug": false,
    "newestOnTop": true,
    "progressBar": true,
    "positionClass": "toast-bottom-left",
    "preventDuplicates": true,
    "onclick": null,
    "showDuration": 300,
    "hideDuration": 1000,
    "timeOut": 5000,
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut",
    "closeOnHover": false,
    iconClasses: {
        error: 'icon-cancel',
        info: 'icon-eye',
        success: 'icon-ok',
        warning: 'icon-attention',
      },
  });

const quickToast = {
    fire:(opt)=>{
        toastr[opt.icon](opt['title'],opt.icon,{timeOut:5000})
    }
}
const longToast = {
    fire:(opt)=>{
        toastr[opt.icon](opt['title'],opt.icon,{timeOut:15000})
    }
}

export {toastr,quickToast,longToast}