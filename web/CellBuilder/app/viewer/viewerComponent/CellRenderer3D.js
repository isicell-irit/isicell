import * as THREE from 'three';
import ViewerManager from '../ViewerManager.js';

export default class CellRenderer {
    constructor(world, qualitySphere){
        this.data;
        this.dummy = new THREE.Object3D();
        this.ShapeSphere = new THREE.SphereGeometry(1, qualitySphere, qualitySphere);
        this.maxCell = 50_000; //TODO: Rework
        this.cut = {x:250,y:250,z:250,enable:false};
        this.material = new THREE.MeshMatcapMaterial({color: '#000000'});
        this.material.vertexTangents = true;
        this.material.tangentSpaceNormalMap = true;

        const textureLoader = new THREE.TextureLoader()
        this.material.map = textureLoader.load("textures/cellTexture.jpg");
        //console.log(this.material.map )
        this.material.normalMap = textureLoader.load("textures/cellNormalMap.jpg");
        this.material.normalMap.minFilter = THREE.LinearMipMapLinearFilter;
        this.material.normalMap.magFilter = THREE.LinearFilter;
        this.addShader();
        this.mesh = new THREE.InstancedMesh( this.ShapeSphere, this.material, this.maxCell );
        //this.mesh.instanceMatrix.setUsage( THREE.DynamicDrawUsage ); // will be updated every frame
        world.add( this.mesh );
    }
    
    setColor(infosColor){
  try {
      let color = ViewerManager.colorize(infosColor)
      //color = [...color, ...new Array(this.maxCell*3 - color.length)]
      this.mesh.geometry.setAttribute('instanceColor', new THREE.InstancedBufferAttribute(new Float32Array(color), 3));
    }catch(e){
        console.error(e);
      }
    }

    updateCells(data){
      this.data = data;
      this.refresh();
    }

    refresh(){
      try{
        this.mesh.count = this.data.x.length
        this.setColor(this.data.info);
        for (let i = 0; i < this.data.x.length; i++) { //&& i<this.maxCell
          if (!this.cut.enable || (this.data.x[i] < this.cut.x && this.data.y[i] < this.cut.y && this.data.z[i] < this.cut.z))
            this.setVisibleCell(i,this.data.x[i],this.data.y[i],this.data.z[i],this.data.radius[i]);
          else this.setUnvisibleCell(i);
        }
        for(let i=this.data.x.length; i<this.maxCell ;i++){
          this.setUnvisibleCell(i);
        }
        this.mesh.instanceMatrix.needsUpdate = true;
      }catch(e){
        console.error(e);
      }
    }

    setVisibleCell(id,x,y,z,r){
      this.dummy.scale.set(r,r,r);
      this.dummy.position.set(x,y,z );
      this.dummy.updateMatrix();
      this.mesh.setMatrixAt( id , this.dummy.matrix );
    }

    setUnvisibleCell(id){
      this.dummy.scale.set(0,0,0);
      //this.dummy.position.set( 9999, 9999, 9999 );
      //this.dummy.rotation.y = Math.PI*Math.random();
      //this.dummy.rotation.z = this.dummy.rotation.y * 2;
      this.dummy.updateMatrix();
      this.mesh.setMatrixAt( id , this.dummy.matrix );
    }

    applyCut(cut){
      try{
        this.cut = cut;
        this.refresh();
      }catch{
        console.error(e);
      }
    }

      addShader(){
        
        this.material.onBeforeCompile = function ( shader ) {

          shader.vertexShader = `
#define MATCAP
varying vec3 vViewPosition;
#ifndef FLAT_SHADED
	varying vec3 vNormal;
#endif
attribute vec3 instanceColor;
varying vec3 vInstanceColor;
varying vec3 vTangent;
varying vec3 vBitangent;
varying vec3 cameraPosition2;
varying highp vec3 surfacePosition;
#include <common>
#include <uv_pars_vertex>
#include <color_pars_vertex>
#include <displacementmap_pars_vertex>
#include <fog_pars_vertex>
#include <morphtarget_pars_vertex>
#include <skinning_pars_vertex>
#include <logdepthbuf_pars_vertex>
#include <clipping_planes_pars_vertex>
void main() {
	#include <uv_vertex>
	#include <color_vertex>
	#include <beginnormal_vertex>
	#include <morphnormal_vertex>
	#include <skinbase_vertex>
	#include <skinnormal_vertex>
	#include <defaultnormal_vertex>
	#ifndef FLAT_SHADED
		vNormal = normalize( transformedNormal );
	#endif
	#include <begin_vertex>
	vInstanceColor = instanceColor;
	vNormal = normal;
	cameraPosition2 = -projectionMatrix[3].xyz * mat3(projectionMatrix);
	surfacePosition = vec3(modelMatrix * vec4(position, 1.0));
	#include <morphtarget_vertex>
	#include <skinning_vertex>
	#include <displacementmap_vertex>
	#include <project_vertex>
	#include <logdepthbuf_vertex>
	#include <clipping_planes_vertex>
	#include <fog_vertex>
	vViewPosition = - mvPosition.xyz;
}`

          shader.fragmentShader = `
#define MATCAP
uniform vec3 diffuse;
uniform float opacity;
uniform sampler2D matcap;
varying vec3 vViewPosition;
#ifndef FLAT_SHADED
	varying vec3 vNormal;
#endif
varying vec3 vInstanceColor;
varying vec3 cameraPosition2;
varying vec3 surfacePosition;
varying vec3 vTangent;
varying vec3 vBitangent;
uniform mat3 normalMatrix;
struct DirectionalLight2 {
	vec3 direction;
	vec3 color;
	float intensity;
};
const int nbLights = 4;
vec4 mixV4(vec4 v1, vec4 v2, float i) { return v1 + (v2 - v1) * i; }
vec3 mixV3(vec3 v1, vec3 v2, float i) { return v1 + (v2 - v1) * i; }
vec3 superpose(vec3 v1, vec4 v2, float opacity) {
	return mixV3(v1, vec3(v2), v2.a * opacity);
}
#include <common>
#include <color_pars_fragment>
#include <uv_pars_fragment>
#include <map_pars_fragment>
#include <alphamap_pars_fragment>
#include <fog_pars_fragment>
#include <bumpmap_pars_fragment>
#include <normalmap_pars_fragment>
#include <logdepthbuf_pars_fragment>
#include <clipping_planes_pars_fragment>
void main() {
	#include <clipping_planes_fragment>
	vec4 diffuseColor = vec4( diffuse * vInstanceColor, opacity );
	#include <logdepthbuf_fragment>
	#include <map_fragment>
	#include <color_fragment>
	#include <alphamap_fragment>
	#include <alphatest_fragment>
	#include <normal_fragment_begin>
	#include <normal_fragment_maps>
	vec3 viewDir = normalize( vViewPosition );
	vec3 x = normalize( vec3( viewDir.z, 0.0, - viewDir.x ) );
	vec3 y = cross( viewDir, x );
	vec2 uv = vec2( dot( x, normal ), dot( y, normal ) ) * 0.495 + 0.5;
	#ifdef USE_MATCAP
		vec4 matcapColor = texture2D( matcap, uv );
	#else
		vec4 matcapColor = vec4( vec3( mix( 0.2, 0.8, uv.y ) ), 1.0 );
	#endif
	vec3 outgoingLight = diffuseColor.rgb * matcapColor.rgb;
		DirectionalLight2 lights2[nbLights];
	lights2[0].direction = vec3(0.661438,-0.75,0);
	lights2[0].color = vec3(1.0);
	lights2[0].intensity = 0.7;
	lights2[1].direction = vec3(-0.841789,-0.297529,0.450408);
	lights2[1].color = vec3(1.0);
	lights2[1].intensity = 0.5;
	lights2[2].direction = vec3(-0.2011,0.264352,-0.943227);
	lights2[2].color = vec3(1.0);
	lights2[2].intensity = 0.7;
	lights2[3].direction = vec3(0.381145,0.782907,0.491717);
	lights2[3].color = vec3(1.0);
	lights2[3].intensity = 0.52;
	vec3 tangentSpaceNormal = texture2D(normalMap, uv).yxz * 2.0 - 1.0;
	tangentSpaceNormal = normalize(tangentSpaceNormal);
	vec3 n = normalize(vNormal);              // Z
	vec3 t = normalize(cross(n, vec3(0.0, 0.0, 1.0)));  // X
	vec3 b = cross(n, t);                               // Y
	mat3 basis = mat3(t, b, n);
	vec3 eyespaceNormal1 = basis * tangentSpaceNormal;
	eyespaceNormal1 = normalize(normalMatrix * eyespaceNormal1);
	vec3 eyespaceNormal0 = normalize(normalMatrix * vNormal);
	vec3 surfaceToCamera = normalize(cameraPosition2 - surfacePosition);
	float diffuseCoef =min(1.0, max(0.0, abs(dot(eyespaceNormal0,surfaceToCamera))));
	float diffuseCoefMap = min(1.0, max(0.0, abs(dot(eyespaceNormal1, surfaceToCamera))));
	vec3 surfaceColor = vInstanceColor.rgb;
	surfaceColor *= sampledDiffuseColor.rgb;
	vec3 reducedColor = mix(surfaceColor + vec3(0.1), vec3((surfaceColor - 0.63) * 1.15 + 0.5),max(0.0, min(0.8, diffuseCoef * diffuseCoef))); 
	vec3 normalColor = mixV3(vec3(1.0), vec3(0.0),max(0.0,min(1.0,diffuseCoefMap *diffuseCoefMap))); 
	vec4 whiteHalo = mixV4(vec4(1.0), vec4(1.0, 1.0, 1.0, 0.0),diffuseCoef * diffuseCoef);
	whiteHalo *= whiteHalo;
	vec3 final = mixV3(reducedColor, (vec3(0.2) + reducedColor) * normalColor.r, 0.1);
	final = superpose(final, whiteHalo, 0.05);
	final = superpose(final, whiteHalo * vec4(normalColor, 1.0), 0.32);
	vec4 colorLinear;
	vec4 specColor = vec4(vec3(0.5),1.0);
	for (int i = 0; i < nbLights; ++i) {
		vec3 lightDir = normalize(lights2[i].direction);
		float lambertian = max(dot(lightDir, eyespaceNormal1), 0.0);
		colorLinear += lambertian * specColor * vec4(lights2[i].color, 1.0) * lights2[i].intensity;
	}
	gl_FragColor = vec4(mixV3(final , final+ final*colorLinear.rgb, 0.5), opacity);
}`

          //console.log( shader.uniforms );
          //console.log( shader.vertexShader );
          //console.log( shader.fragmentShader );
        };
      }
}
