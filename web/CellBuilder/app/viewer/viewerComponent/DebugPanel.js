
export default class DebugPanel {
    constructor(parent){
        this.displayed = false;
        this.container = document.createElement("div");
        parent.appendChild(this.container)
        this.container.classList.add('debugView');
    }

    write(data,err){
        const line = document.createElement('p')
        line.innerText = data
        if (err) {
            line.style.color = '#FF6188'
            line.style.fontWeight = 'bold'
        }
        this.container.appendChild(line)
        line.scrollIntoView()
    }

    reset() {
        this.container.innerHTML = ""
    }

}