import * as PIXI from 'pixi.js';
import '@pixi/unsafe-eval';
import CellRenderer2D from './CellRenderer2D.js';

export default class Viewer {

  constructor(container) {
    this.container = container;
    this.cellRenderer;
    this.idleTime = 0;
    this.idle = false;
    this.paused = false;
    this.dataUpdated = true;
    this.capturer = null;
    this.idleInterval = setInterval(()=>{
      if(this.idle){
        this.paused = (this.paused || (this.idleTime++ > 6));
      }
      else{
        this.idleTime = 0;
        this.paused = false;
      }
    }, 10000); // 1 minute
    window.addEventListener('blur', ()=>{this.idle = true;})
    window.addEventListener('focus', ()=>{this.idle = false;this.idleTime = 0;})

    let canvasDOM = document.createElement("canvas")
    window.addEventListener('resize', () => {
        canvasDOM.width = 1;
        canvasDOM.height = 1;
        if(canvasDOM.parentElement){
          canvasDOM.width = canvasDOM.parentElement.clientWidth;
          canvasDOM.height = canvasDOM.parentElement.clientHeight;
        }
    })
    this.app = new PIXI.Application({resizeTo:this.container,view:canvasDOM,backgroundColor:0xf8f8ff});
    this.container.appendChild(canvasDOM)

    this.cellRenderer = new CellRenderer2D(this.app.stage);
    this.createControls();
    canvasDOM.width = canvasDOM.parentElement.clientWidth;
    canvasDOM.height = canvasDOM.parentElement.clientHeight;
  }

  updateView(data){
    try{
      this.cellRenderer.updateCells(data);

      requestAnimationFrame(()=>{
        if (!this.paused && document.getElementById('contentViewer').style.display!=='none'){
          if(this.capturer) this.capturer.addFrame(this.app.view);
        }});
    }catch(e){
      console.error(e);
    }
  }

  update(){
  }

  createControls(){
    var self = this;
    var domElement = this.app.view;
    var mouse = {x:0,y:0};
    var touches = {};
    var distance = 0;

    domElement.addEventListener('mousedown', mousedown, false);
    domElement.addEventListener('mousewheel', mousewheel, false);
    domElement.addEventListener('wheel', mousewheel, false);

    domElement.addEventListener('touchstart', touchstart, false);
    domElement.addEventListener('touchmove', touchmove, false);
    domElement.addEventListener('touchend', touchend, false);
    domElement.addEventListener('touchcancel', touchend, false);

    function mousedown(e) {
      mouse.x = e.clientX;
      mouse.y = e.clientY;
      window.addEventListener('mousemove', mousemove, false);
      window.addEventListener('mouseup', mouseup, false);
    }

    function mousemove(e) {
      var dx = e.clientX - mouse.x;
      var dy = e.clientY - mouse.y;
      self.app.stage.x += dx;
      self.app.stage.y += dy;
      mouse.x = e.clientX;
      mouse.y = e.clientY;
    }

    function mouseup(e) {
      window.removeEventListener('mousemove', mousemove, false);
      window.removeEventListener('mouseup', mouseup, false);
    }

    function mousewheel(e) {
      var s = 1+((e.wheelDeltaY || - e.deltaY) / 500);
      var worldPos = {x: (e.clientX - self.app.stage.x) / self.app.stage.scale.x, y: (e.clientY - self.app.stage.y)/self.app.stage.scale.y};
      var newScale = {x: self.app.stage.scale.x * s, y: self.app.stage.scale.y * s};
      var newScreenPos = {x: (worldPos.x ) * newScale.x + self.app.stage.x, y: (worldPos.y) * newScale.y + self.app.stage.y};

      if(newScale.x>0.1 && newScale.x<5){
        self.app.stage.x -= (newScreenPos.x-e.clientX) ;
        self.app.stage.y -= (newScreenPos.y-e.clientY) ;
        self.app.stage.scale.x = newScale.x;
        self.app.stage.scale.y = newScale.y;
      }
    }

    function touchstart(e) {
      switch (e.touches.length) {
        case 2:
          pinchstart(e);
          break;
        case 1:
          panstart(e)
          break;
      }
    }

    function touchmove(e) {
      switch (e.touches.length) {
        case 2:
          pinchmove(e);
          break;
        case 1:
          panmove(e)
          break;
      }
    }

    function touchend(e) {
      touches = {};
      var touch = e.touches[ 0 ];
      if (touch) {  // Pass through for panning after pinching
        mouse.x = touch.clientX;
        mouse.y = touch.clientY;
      }
    }

    function panstart(e) {
      var touch = e.touches[ 0 ];
      mouse.x = touch.clientX;
      mouse.y = touch.clientY;
    }

    function panmove(e) {
      var touch = e.touches[ 0 ];
      var dx = touch.clientX - mouse.x;
      var dy = touch.clientY - mouse.y;
      self.app.stage.x += dx;
      self.app.stage.y += dy;
      mouse.x = touch.clientX;
      mouse.y = touch.clientY;
    }

    function pinchstart(e) {
      for (var i = 0; i < e.touches.length; i++) {
        var touch = e.touches[ i ];
        touches[ touch.identifier ] = touch;
      }
      var a = touches[ 0 ];
      var b = touches[ 1 ];
      var dx = b.clientX - a.clientX;
      var dy = b.clientY - a.clientY;
      distance = Math.sqrt(dx * dx + dy * dy);
      mouse.x = dx / 2 + a.clientX;
      mouse.y = dy / 2 + a.clientY;
    }

    function pinchmove(e) {
      for (var i = 0; i < e.touches.length; i++) {
        var touch = e.touches[ i ];
        touches[ touch.identifier ] = touch;
      }
      var a = touches[ 0 ];
      var b = touches[ 1 ];
      var dx = b.clientX - a.clientX;
      var dy = b.clientY - a.clientY;
      var d = Math.sqrt(dx * dx + dy * dy);
      var s = (d - distance)/500;
      var worldPos = {x: (a.clientX - self.app.stage.x) / self.app.stage.scale.x, y: (a.clientY - self.app.stage.y)/self.app.stage.scale.y};
      var newScale = {x: self.app.stage.scale.x * s, y: self.app.stage.scale.y * s};
      var newScreenPos = {x: (worldPos.x ) * newScale.x + self.app.stage.x, y: (worldPos.y) * newScale.y + self.app.stage.y};

      if(newScale.x>0.1 && newScale.x<5){
        self.app.stage.x -= (newScreenPos.x-a.clientX) ;
        self.app.stage.y -= (newScreenPos.y-a.clientY) ;
        self.app.stage.scale.x = newScale.x;
        self.app.stage.scale.y = newScale.y;
      }
      distance = d;
    }
  }



}
