import Plotly from 'plotly.js-dist'
import ViewerManager from '../ViewerManager';
import '../../css/viewerPlots.css'
function generateHeader(parent) {
    let container = document.createElement('div')
    container.classList.add('plot-config')
    parent.appendChild(container)
    let typeSelection = document.createElement('select')
    typeSelection.innerHTML = '<option>scatter</option><option>histogram</option><option>violin</option><option>histogram2dcontour</option>'
    container.appendChild(typeSelection)
    let main = document.createElement('div')
    container.appendChild(main)
    let dataMain = []
    traceScatter(main, dataMain, true)
    typeSelection.onchange = () => {
        main.innerHTML = ''
        dataMain = []
        if (typeSelection.value === "histogram") {
            traceHistogram(main, dataMain, true)
        } else if (typeSelection.value === "scatter") {
            traceScatter(main, dataMain, true)
        } else if (typeSelection.value === "violin") {
            traceViolin(main, dataMain, true)
        } else if (typeSelection.value === "histogram2dcontour") {
            traceHistogram2dcontour(main, dataMain, true)
        }
    }
    let btnGenerateTraces = document.createElement('a')
    container.appendChild(btnGenerateTraces)
    btnGenerateTraces.textContent = "Generate Trace"
    btnGenerateTraces.classList.add('common-button')
    btnGenerateTraces.style.display = 'block'
    btnGenerateTraces.style.marginBottom = '20px'
    btnGenerateTraces.onclick = () => {
        container.style.display = 'none'
        let dataMain0 = dataMain[0]
        let datas = [];
        let datasTemp = [];
        let layout = { xaxis: {}, yaxis: {} };
        let discrets = Object.keys(ViewerManager.colors).filter(e => ViewerManager.colors[e].palette == undefined)
        let notFind = true;
        for (let discret of discrets) {
            if (dataMain0.metadata.on === discret) {
                Object.keys(ViewerManager.colors[discret]).sort().forEach(s => {
                    const mainDataCopy = JSON.parse(JSON.stringify(dataMain0))
                    if (mainDataCopy.marker) mainDataCopy.marker.color = ViewerManager.colors[discret][s]
                    mainDataCopy.metadata.name = s
                    datasTemp.push(mainDataCopy)
                })
                notFind = false;
                break;
            }
        }
        if(notFind) {
            const mainDataCopy = JSON.parse(JSON.stringify(dataMain0))
            delete mainDataCopy.metadata.on
            datasTemp.push(mainDataCopy)
        }
        let plotContainer = document.createElement('div')
        let contentTrace = document.createElement('div')
        contentTrace.classList.add('plot-config')
        plotContainer.appendChild(contentTrace)
        let btnAddTrace = document.createElement('a')
        btnAddTrace.classList.add('common-button')
        btnAddTrace.style.display = 'block'
        btnAddTrace.style.marginBottom = '20px'
        btnAddTrace.textContent = "add trace"
        plotContainer.appendChild(btnAddTrace)
        let contentLayout = document.createElement('div')
        contentLayout.classList.add('plot-config')
        plotContainer.appendChild(contentLayout)

        let funcLayout,funcTrace;
        if (typeSelection.value === 'histogram') {
            funcLayout = layoutHistogram;
            funcTrace = traceHistogram;
        } else if (typeSelection.value === 'scatter') {
            funcLayout = layoutScatter;
            funcTrace = traceScatter;
        } else if (typeSelection.value === 'violin') {
            funcLayout = layoutViolin;
            funcTrace = traceViolin;
        } else if (typeSelection.value === 'histogram2dcontour') {
            funcLayout = layoutHistogram2dcontour;
            funcTrace = traceHistogram2dcontour;
        }
        for (let d of datasTemp) funcTrace(contentTrace, datas, false, d)
        funcLayout(contentLayout, layout, { xaxis: { title: dataMain0.metadata.x }, yaxis: { title: dataMain0.metadata.y } })
        btnAddTrace.onclick = () => {
            funcTrace(contentTrace, datas, false, { metadata: { on: datasTemp[0].metadata.on } })
        }
        let btnValidePlot = document.createElement('a')
        btnValidePlot.classList.add('common-button')
        btnValidePlot.style.display = 'block'
        btnValidePlot.style.marginBottom = '20px'
        btnValidePlot.textContent = "Valide Plot"
        btnValidePlot.onclick = () => {
            container.style.display = null
            datas.forEach(t => {
                if (t.metadata.name) t.name = t.metadata.name
            })
            ViewerManager.plots.addPlot({ data: datas, layout: layout })
            plotContainer.remove()
        }
        plotContainer.appendChild(btnValidePlot)
        parent.appendChild(plotContainer)
    }
}

function createDomElem(container, labelTxt, type, src, key, defaultValue, content) {
    let label = document.createElement('label')
    let spanLabel = document.createElement('span')
    spanLabel.textContent = labelTxt
    label.appendChild(spanLabel)
    let dom;
    if (type === 'select') {
        dom = document.createElement('select')
        dom.innerHTML = content.map(v => "<option>" + v + "</option>").join('')
        dom.value = defaultValue ? defaultValue : content[0]
        src[key] = defaultValue ? defaultValue : content[0]
    } else {
        dom = document.createElement('input')
        dom.type = type
        if (type === 'number') dom.step = 0.1
        if (type === 'checkbox') dom.checked = defaultValue
        else dom.value = defaultValue
        src[key] = defaultValue
    }
    if (type === 'checkbox') dom.onchange = () => src[key] = dom.checked
    else dom.onchange = () => src[key] = dom.value
    label.appendChild(dom)
    container.appendChild(label)
    return dom
}

function layoutScatter(container, layout, defaultOptions = { xaxis: {}, yaxis: {} }) {
    let main = document.createElement('div')
    main.classList.add('plot-config-row')
    let row = document.createElement('div')
    createDomElem(row, 'x title', 'text', layout.xaxis, 'title', defaultOptions.xaxis.title ? defaultOptions.xaxis.title : "Value")
    createDomElem(row, 'y title', 'text', layout.yaxis, 'title', defaultOptions.yaxis.title ? defaultOptions.yaxis.title : "Count")
    main.appendChild(row)
    container.appendChild(main)
}
function traceScatter(container, data, isMain, defaultOptions = { metadata: {} }) {
    let trace = {
        type: 'scatter',
        metadata: {},
        marker: {}
    }
    let main = document.createElement('div')
    main.classList.add('plot-config-row')
    let row = document.createElement('div')
    defaultOptions.metadata.x = defaultOptions.metadata.x ? defaultOptions.metadata.x : 'time'
    let xSelect = createDomElem(row, 'x', 'select', trace.metadata, 'x', defaultOptions.metadata.x, ['time', ...ViewerManager.continousInfo])
    createDomElem(row, 'y', 'select', trace.metadata, 'y', defaultOptions.metadata.y, ['',...ViewerManager.continousInfo])
    let timeFunc = createDomElem(row, 'func', 'select', trace.metadata, 'func', defaultOptions.metadata.func, ['count','avg','prop'])
    if (defaultOptions.metadata.x !== 'time') timeFunc.parentElement.style.display = 'none'
    xSelect.addEventListener('change', ()=>{
        if(xSelect.value === 'time'){
            timeFunc.parentElement.style.display = null
        } else {
            timeFunc.parentElement.style.display = 'none'
        }
    })
    if (!isMain) {
        if (defaultOptions.metadata.on) {
            trace.metadata.on = defaultOptions.metadata.on
            createDomElem(row, 'filter ' + defaultOptions.metadata.on, 'select', trace.metadata, 'name', defaultOptions.metadata?.name, ["", ...Object.keys(ViewerManager.colors[defaultOptions.metadata.on]).sort()])
        }
        createDomElem(row, 'color', 'color', trace.marker, 'color', defaultOptions?.marker?.color || "#10bfd2")
    } else {
        const discrets = Object.keys(ViewerManager.colors).filter(e => ViewerManager.colors[e].palette == undefined)
        createDomElem(row, 'on', 'select', trace.metadata, 'on', "all", ["all", ...discrets])
    }
    createDomElem(row, 'mode', 'select', trace, 'mode', defaultOptions.mode, ["lines", "markers", "lines+markers"])
    if (!isMain) {
        close = document.createElement('a')
        close.classList.add('close')
        close.onclick = () => {
            main.remove()
            data.splice(data.indexOf(trace), 1)
        }
        row.appendChild(close)
    }
    main.appendChild(row)
    container.appendChild(main)
    data.push(trace)
}

function layoutHistogram(container, layout, defaultOptions = { xaxis: {}, yaxis: {} }) {
    let main = document.createElement('div')
    main.classList.add('plot-config-row')
    let row = document.createElement('div')
    createDomElem(row, 'x title', 'text', layout.xaxis, 'title', defaultOptions.xaxis.title ? defaultOptions.xaxis.title : "Value")
    createDomElem(row, 'y title', 'text', layout.yaxis, 'title', defaultOptions.yaxis.title ? defaultOptions.yaxis.title : "Count")
    createDomElem(row, 'barmode', 'select', layout, 'barmode', 'overlay', ["stack", "group", "overlay", "relative"])
    createDomElem(row, 'bargap', 'number', layout, 'bargap', defaultOptions.bargap ? defaultOptions.bargap : 0.05)
    main.appendChild(row)
    container.appendChild(main)
}
function traceHistogram(container, data, isMain, defaultOptions = { xbins: {}, metadata: {}, marker: {}, cumulative: {} }) {
    let trace = {
        type: 'histogram',
        xbins: {},
        metadata: {},
        marker: {},
        cumulative: {},
    }
    let main = document.createElement('div')
    main.classList.add('plot-config-row')
    let row = document.createElement('div')
    createDomElem(row, 'x', 'select', trace.metadata, 'x', defaultOptions.metadata.x, ViewerManager.continousInfo)
    createDomElem(row, 'y', 'select', trace.metadata, 'y', defaultOptions.metadata.y, ["", ...ViewerManager.continousInfo])
    if (!isMain) {
        if (defaultOptions.metadata.on) {
            trace.metadata.on = defaultOptions.metadata.on
            createDomElem(row, 'filter ' + defaultOptions.metadata.on, 'select', trace.metadata, 'name', defaultOptions.metadata?.name, ["", ...Object.keys(ViewerManager.colors[defaultOptions.metadata.on]).sort()])
        }
        createDomElem(row, 'color', 'color', trace.marker, 'color', defaultOptions.marker.color ? defaultOptions.marker.color : "#10bfd2")
    } else {
        createDomElem(row, 'on', 'select', trace.metadata, 'on', "all", ["all", ...Object.keys(ViewerManager.colors).filter(e => ViewerManager.colors[e].palette == undefined)])
    }
    if (!isMain) {
        close = document.createElement('a')
        close.classList.add('close')
        close.onclick = () => {
            main.remove()
            data.splice(data.indexOf(trace), 1)
        }
        row.appendChild(close)
    }
    main.appendChild(row)

    let advanced = document.createElement('div')
    advanced.classList.add('advanced')
    createDomElem(advanced, 'bins size', 'number', trace.xbins, 'size', defaultOptions.xbins.size ? defaultOptions.xbins.size : 0.1)
    createDomElem(advanced, 'opacity', 'number', trace, 'opactiy', defaultOptions.opacity ? defaultOptions.opacity : 0.6)
    createDomElem(advanced, 'normalize', 'select', trace, 'histnorm', defaultOptions.histnorm ? defaultOptions.histnorm : "", ["", "percent", "probability", "density", "probability density"])
    createDomElem(advanced, 'hist func', 'select', trace, 'histfunc', defaultOptions.histfunc ? defaultOptions.histfunc : "count", ["count", "sum", "avg", "min", "max"])
    createDomElem(advanced, 'cumulative', 'checkbox', trace.cumulative, 'enabled', defaultOptions.cumulative.enabled ? defaultOptions.cumulative.enabled : false)
    main.appendChild(advanced)

    let more = document.createElement('a')
    more.classList.add('more')
    more.onclick = () => {
        advanced.classList.toggle('advanced')
    }
    main.appendChild(more)

    container.appendChild(main)
    data.push(trace)
}


function layoutViolin(container, layout, defaultOptions = { xaxis: {}, yaxis: {} }) {
    let main = document.createElement('div')
    main.classList.add('plot-config-row')
    let row = document.createElement('div')
    createDomElem(row, 'x title', 'text', layout.xaxis, 'title', defaultOptions.xaxis.title ? defaultOptions.xaxis.title : "Value")
    createDomElem(row, 'y title', 'text', layout.yaxis, 'title', defaultOptions.yaxis.title ? defaultOptions.yaxis.title : "Count")
    layout.yaxis.zeroline = false
    main.appendChild(row)
    container.appendChild(main)
}
function traceViolin(container, data, isMain, defaultOptions = { box: {}, metadata: {}, marker: {} }) {
    let trace = {
        type: 'violin',
        box: {},
        metadata: {},
        marker: {},
    }
    let main = document.createElement('div')
    main.classList.add('plot-config-row')
    let row = document.createElement('div')
    createDomElem(row, 'y', 'select', trace.metadata, 'y', defaultOptions.metadata.y, ViewerManager.continousInfo)
    if (!isMain) {
        if (defaultOptions.metadata.on) {
            trace.metadata.on = defaultOptions.metadata.on
            createDomElem(row, 'filter ' + defaultOptions.metadata.on, 'select', trace.metadata, 'name', defaultOptions.metadata?.name, ["", ...Object.keys(ViewerManager.colors[defaultOptions.metadata.on]).sort()])
        }
        createDomElem(row, 'color', 'color', trace.marker, 'color', defaultOptions.marker.color ? defaultOptions.marker.color : "#10bfd2")
    } else {
        createDomElem(row, 'on', 'select', trace.metadata, 'on', "all", ["all", ...Object.keys(ViewerManager.colors).filter(e => ViewerManager.colors[e].palette == undefined)])
    }
    if (!isMain) {
        close = document.createElement('a')
        close.classList.add('close')
        close.onclick = () => {
            main.remove()
            data.splice(data.indexOf(trace), 1)
        }
        row.appendChild(close)
    }
    main.appendChild(row)

    let advanced = document.createElement('div')
    advanced.classList.add('advanced')
    createDomElem(advanced, 'points', 'select', trace, 'points', defaultOptions.points ? defaultOptions.points : 'false', ['false', "all", "outliers", "suspectedoutliers"])
    createDomElem(advanced, 'jitter', 'select', trace, 'jitter', defaultOptions.jitter ? defaultOptions.jitter : '0', ['0', '1'])
    createDomElem(advanced, 'opacity', 'number', trace, 'opactiy', defaultOptions.opacity ? defaultOptions.opacity : 0.6)
    createDomElem(advanced, 'box', 'checkbox', trace.box, 'visible', defaultOptions.box.visible!==undefined ? defaultOptions.box.visible : true)
    main.appendChild(advanced)

    let more = document.createElement('a')
    more.classList.add('more')
    more.onclick = () => {
        advanced.classList.toggle('advanced')
    }
    main.appendChild(more)

    container.appendChild(main)
    data.push(trace)
}
function layoutHistogram2dcontour(container, layout, defaultOptions = { xaxis: {}, yaxis: {} }) {
    let main = document.createElement('div')
    main.classList.add('plot-config-row')
    let row = document.createElement('div')
    createDomElem(row, 'x title', 'text', layout.xaxis, 'title', defaultOptions.xaxis.title ? defaultOptions.xaxis.title : "Value")
    createDomElem(row, 'y title', 'text', layout.yaxis, 'title', defaultOptions.yaxis.title ? defaultOptions.yaxis.title : "Count")
    main.appendChild(row)
    container.appendChild(main)
}
function traceHistogram2dcontour(container, data, isMain, defaultOptions = { metadata: {} }) {
    let trace = {
        type: 'histogram2dcontour',
        metadata: {},
    }

    let main = document.createElement('div')
    main.classList.add('plot-config-row')
    let row = document.createElement('div')
    createDomElem(row, 'x', 'select', trace.metadata, 'x', defaultOptions.metadata.x, ViewerManager.continousInfo)
    createDomElem(row, 'y', 'select', trace.metadata, 'y', defaultOptions.metadata.y, ViewerManager.continousInfo)
    if (!isMain) {
        if (defaultOptions.metadata.on) {
            trace.metadata.on = defaultOptions.metadata.on
            createDomElem(row, 'filter ' + defaultOptions.metadata.on, 'select', trace.metadata, 'name', defaultOptions.metadata?.name, Object.keys(ViewerManager.colors[defaultOptions.metadata.on]).sort())
        }
    } else {
        createDomElem(row, 'on', 'select', trace.metadata, 'on', "all", ["all", ...Object.keys(ViewerManager.colors).filter(e => ViewerManager.colors[e].palette == undefined)])
    }
    if (!isMain) {
        close = document.createElement('a')
        close.classList.add('close')
        close.onclick = () => {
            main.remove()
            data.splice(data.indexOf(trace), 1)
        }
        row.appendChild(close)
    }
    main.appendChild(row)

    let advanced = document.createElement('div')
    advanced.classList.add('advanced')
    createDomElem(advanced, 'n contours', 'number', trace, 'ncontours', defaultOptions.ncontours ? defaultOptions.ncontours : 20)
    createDomElem(advanced, 'color scale', 'select', trace, 'colorscale', defaultOptions.colorscale ? defaultOptions.colorscale : 'Viridis', ['Viridis', 'Hot', 'Jet','Blackbody', 'Bluered', 'Blues', 'Cividis', 'Earth', 'Electric', 'Greens', 'Greys',  'Picnic', 'Portland', 'Rainbow', 'RdBu', 'Reds', 'YlGnBu', 'YlOrRd'])
    createDomElem(advanced, 'reverse scale', 'checkbox', trace, 'reversescale', defaultOptions.reversescale !== undefined ? defaultOptions.reversescale : false)
    createDomElem(advanced, 'show scale', 'checkbox', trace, 'showscale', defaultOptions.showscale !== undefined ? defaultOptions.showscale : true)
    main.appendChild(advanced)

    let more = document.createElement('a')
    more.classList.add('more')
    more.onclick = () => {
        advanced.classList.toggle('advanced')
    }
    main.appendChild(more)

    container.appendChild(main)
    data.push(trace)
}

export default class Plots {
    constructor(parent) {
        this.displayed = false;
        this.container = document.createElement("div");
        this.container.style.width = '100%';
        this.container.style.height = '100%';
        this.container.style.overflowY = 'auto';
        this.container.style.paddingTop = '35px'
        this.container.style.backgroundColor = '#fff'
        this.plotsContainer = document.createElement("div");
        this.configContainer = document.createElement("div");
        this.container.appendChild(this.configContainer)
        this.container.appendChild(this.plotsContainer)
        parent.appendChild(this.container)
        this.plots = []
    }
    
    get currentInfosNeed(){
        const validInfo = ViewerManager.continousInfo
        let currentInfosNeed = []
        for(let p of this.plots){
            for(let d of p.data){
                if (d.metadata.x !== 'time'){
                    if (validInfo.includes(d.metadata.x) && !currentInfosNeed.includes(d.metadata.x)) currentInfosNeed.push(d.metadata.x)
                    if (validInfo.includes(d.metadata.y) && !currentInfosNeed.includes(d.metadata.y)) currentInfosNeed.push(d.metadata.y)
                    if (d.metadata.on !== 'all' && d.metadata.on && !currentInfosNeed.includes(d.metadata.on)) currentInfosNeed.push(d.metadata.on)
                }
            }
        }
        return currentInfosNeed
    }

    get currentInfosNeedOnTime() {
        const validInfo = ViewerManager.continousInfo
        let currentInfosNeedOnTime = []
        for (let p of this.plots) {
            for (let d of p.data) {
                if (d.metadata.x === 'time') {
                    if (validInfo.includes(d.metadata.y) && !currentInfosNeedOnTime.includes(d.metadata.y)) currentInfosNeedOnTime.push(d.metadata.y)
                }
            }
        }
        return currentInfosNeedOnTime
    }

    addPlot(data){
        data.data.forEach(t=>{
            if (t.metadata.x === 'time') t.metadata.y = t.metadata.y === '' ? 'count' : t.metadata.y
            if (t.metadata.x) t.x = []
            if (t.metadata.y) t.y = []
        })

        data.domElement = document.createElement('div')
        data.domElement.style.height = '300px'
        this.plotsContainer.appendChild(data.domElement)
        data.layout.margin = { l: 40, r: 10, b: 10, t: 10, pad: 0 }
        data.layout.showlegend = true;
        data.layout.legend = {
            orientation: "h",
            xanchor: "center",
            x: 0.5,
            y: -0.3
        }

        this.plots.push(data)
        Plotly.newPlot(data.domElement, data.data, data.layout, this.getDefaultConfig(data))
        ViewerManager.updateSavedPlots()
        return data
    }

    reset() {
        this.plots = []
        this.plotsDom = []
        this.plotsContainer.innerHTML = ''
        this.configContainer.innerHTML = ''
    }


    generateHeader(){
        generateHeader(this.configContainer)
    }

    update(){
        if(this.displayed){
            const step = ViewerManager.player.currentStep
            const buffer = ViewerManager.buffer
            const colors = ViewerManager.colors
            //const plots = JSON.parse(JSON.stringify(this.plots))
            this.plots.forEach((p) => {
                let data = {x:[],y:[]}
                let alreadyUpdate = false;
                for (const t of p.data) {
                    if (t.metadata.lastUpdate === buffer.bufferOnTime.currentMaxStep){
                        alreadyUpdate = true;
                        break;
                    }
                    if (t.metadata.x==='time') {
                        let datas = buffer.getDataOnTime(t.metadata.y, t.metadata.func, t.metadata.on, t.metadata.name)
                        if(datas){
                            data.x.push(datas.time)
                            data.y.push(datas[t.metadata.y])
                            if (datas.time.length>0)t.metadata.lastUpdate = buffer.bufferOnTime.currentMaxStep
                        } else {
                            data.x.push([])
                            data.y.push([])
                        }
                    } else {
                        let x = []
                        let y = []
                        if (t.metadata.x) x = buffer.getData(step, t.metadata.x)
                        if (t.metadata.y) y = buffer.getData(step, t.metadata.y)
                        if (!x || !y) {
                            x = []
                            y = []
                        }
                        if (t.metadata.on) {
                            const allData = buffer.getData(step, t.metadata.on)
                            if (allData){
                                const indexOn = Object.keys(colors[t.metadata.on]).sort().indexOf(t.metadata.name)
                                x = x.filter((_, i) => allData[i] === indexOn)
                                y = y.filter((_, i) => allData[i] === indexOn)
                            } else {
                                x = []
                                y = []
                            }
                        }
                        if (t.metadata.x) data.x.push(x)
                        if (t.metadata.y) data.y.push(y)
                    }
                }
                if (data.x.length === 0) delete data.x
                if (data.y.length === 0) delete data.y
                if(!alreadyUpdate)Plotly.update(p.domElement, data)
            })
        }
    }

    getDefaultConfig(data) {
        return {
            modeBarButtonsToAdd: [
                {
                    name: 'close',
                    icon: {
                        'width': 20,
                        'height': 20,
                        'path': "M15.898,4.045c-0.271-0.272-0.713-0.272-0.986,0l-4.71,4.711L5.493,4.045c-0.272-0.272-0.714-0.272-0.986,0s-0.272,0.714,0,0.986l4.709,4.711l-4.71,4.711c-0.272,0.271-0.272,0.713,0,0.986c0.136,0.136,0.314,0.203,0.492,0.203c0.179,0,0.357-0.067,0.493-0.203l4.711-4.711l4.71,4.711c0.137,0.136,0.314,0.203,0.494,0.203c0.178,0,0.355-0.067,0.492-0.203c0.273-0.273,0.273-0.715,0-0.986l-4.711-4.711l4.711-4.711C16.172,4.759,16.172,4.317,15.898,4.045z"
                    },
                    click: (gd) => {
                        Plotly.purge(data.domElement);
                        data.domElement.remove();
                        this.plots.splice(this.plots.indexOf(data), 1)
                        ViewerManager.updateSavedPlots()
                    }
                }],
            modeBarButtonsToRemove: ['pan2d', 'select2d', 'lasso2d', 'resetScale2d', "toggleSpikelines", "hoverClosestGl2d", "hoverClosestPie", "toggleHover"],
            responsive: true,
            displaylogo: false,
            toImageButtonOptions: {
                format: 'svg', // one of png, svg, jpeg, webp
                filename: 'custom_image',
                height: 500,
                width: 700,
                scale: 1 // Multiply title/legend/axis/canvas sizes by this factor
            }
        }
    }

}