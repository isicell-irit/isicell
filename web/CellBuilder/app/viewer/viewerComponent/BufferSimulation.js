import { fetchW } from "../../utils/misc";
import ViewerManager from "../ViewerManager"

var groupBy = function (xs, key) {
    return xs.reduce(function (rv, x) {
        (rv[x[key]] = rv[x[key]] || []).push(x);
        return rv;
    }, {});
};


export default class BufferSimulation{

    constructor(token) {
        this.token = token
        this.coordStepReqested = []
        this.infoStepReqested = { }
        this.sizeBit = 0 
        this.buffer = {}
        this.discret =  Object.keys(ViewerManager.colors).filter(e => ViewerManager.colors[e].palette == undefined)
        this.bufferOnTime = { step: [], count: [], currentMaxStep:-1 }
        for (let d of this.discret) this.bufferOnTime[d] = []
        this.maxStep = 10
        this.isRunning = true
        this.eventOnUpdate = {}
        this.coordColumn = {true:['x', 'y'],
                false:['x', 'y', 'z', 'radius']}
    }

    getDataWithCoord(step,info){
        if (this.buffer[step])
            if (this.buffer[step].x && this.buffer[step][info]) {
                let ret = {};
                this.coordColumn[ViewerManager.is2D].forEach(c => ret[c] = this.buffer[step][c])
                ret.info = this.buffer[step][info]
            return ret
        }
    }
    getData(step, info) {
        if (this.buffer[step])
            if (this.buffer[step][info])
                return this.buffer[step][info]
    }

    getDataOnTime(infos, func, filterOn, name) {
        if (func === 'prop') infos = ['count']
        if (!Array.isArray(infos)) infos = [infos]
        infos = infos.filter(i=>Object.keys(this.bufferOnTime).includes(i))
        if(infos.length>0){
            let filter = filterOn ? Object.keys(ViewerManager.colors[filterOn]).sort().indexOf(name):-1
            let data = { time: new Array(this.bufferOnTime.currentMaxStep+1) }
            let infosComp;
            let condition;
            if (filter !== -1) condition = (i) => filter === this.bufferOnTime[filterOn][i]
            else condition = (i) => true
            if (func === 'avg') infosComp = [...infos, 'count']
            else infosComp = infos
            infosComp.forEach(i => data[i] = new Array(this.bufferOnTime.currentMaxStep + 1))
            if (func === 'prop') data.count_tot = new Array(this.bufferOnTime.currentMaxStep + 1)
            let startI = 0
            for(let t=0;t<=this.bufferOnTime.currentMaxStep;t++){
                data.time[t] = t
                infosComp.forEach(i => data[i][t] = 0)
                if (func === 'prop') data.count_tot[t] = 0
                let endI = this.bufferOnTime.step.indexOf(t+1)
                endI = endI >0 ?endI:this.bufferOnTime.step.length
                for (; startI < endI; startI++) {
                    if (condition(startI)) infosComp.forEach(i => data[i][t] += this.bufferOnTime[i][startI])
                    if (func === 'prop') data.count_tot[t] += this.bufferOnTime.count[startI]
                }
            }
            if (func === 'avg') {
                infos.forEach(i => data[i]=data[i].map((v,j)=>v/data.count[j]))
                delete data.count
            }
            if (func === 'prop') {
                infos.forEach(i => data[i] = data[i].map((v, j) => v / data.count_tot[j]))
                delete data.count_tot
            }
            const coefHour = ViewerManager.player.dt / 3600
            data.time = data.time.map(s => s * coefHour)
            return data
        }
    }

    addEvent(name, callBack) {
        this.eventOnUpdate[name] = callBack
    }

    removeEvent(name) {
        delete this.eventOnUpdate[name]
    }

    //rajouter un system d'estimation du temps pouur savoir si ça vaut le coup de ping (taux d'augmentation du maxStep)

    loadStep(currentStep, info, n) {
        if(!this.lock){
            const uniqueInfosNeed = [...this.coordColumn[ViewerManager.is2D],...ViewerManager.plots.currentInfosNeed, info].filter((v, i, a) => a.indexOf(v) === i)
            if (!n) n = ViewerManager.options.targetFPS*2
            if(!this.isRunning && currentStep+n>this.maxStep) n=this.maxStep-currentStep
            let realInfoNeed = {}

            for (let i = currentStep; i < currentStep + n; i++) {
                if (i in this.buffer){
                    const uniqueInfosNeedFiltered = uniqueInfosNeed.filter(j=>!(j in this.buffer[i]))
                    if (uniqueInfosNeedFiltered.length > 0) realInfoNeed[i] = uniqueInfosNeedFiltered
                } else realInfoNeed[i] = uniqueInfosNeed
            }


            const infoNeedOnTime = { exist: [], new: [], currentMaxStep: this.bufferOnTime.currentMaxStep}
            ViewerManager.plots.currentInfosNeedOnTime.filter((v, i, a) => a.indexOf(v) === i).forEach(i=>{
                if (i in this.bufferOnTime || this.bufferOnTime.currentMaxStep<0) infoNeedOnTime.exist.push(i) 
                else infoNeedOnTime.new.push(i)
            })
            infoNeedOnTime.discret = this.discret

            if (Object.keys(realInfoNeed).length > 0 || infoNeedOnTime.new.length>0) {
                this.lock = true
                for (let i = currentStep; i < currentStep + n*2.5; i++) {
                    if (i in this.buffer) {
                        realInfoNeed[i] = uniqueInfosNeed.filter(j => !(j in this.buffer[i]))
                    } else realInfoNeed[i] = uniqueInfosNeed
                }
                //console.log('number Of data need:',Object.keys(realInfoNeed).reduce((p, a) => p + Object.keys(realInfoNeed[a]).length, 0))


                fetchW('/api/simu/multistep/' + this.token, {
                    method: 'POST',
                    headers: {'Accept': 'application/json',
                        'Content-Type': 'application/json'},
                    body: JSON.stringify({ infoNeed: realInfoNeed, infoNeedOnTime})
                }).then(res=>res.json()).then((data) => {
                    if (data.maxStep){
                        Object.keys(data.data).forEach(step => {
                            if (!(step in this.buffer)) this.buffer[step]={}
                            Object.keys(data.data[step]).forEach(info => { 
                                this.buffer[step][info] = data.data[step][info];
                                this.sizeBit += this.buffer[step][info].length
                            })
                        });
                        if (Object.keys(data.dataOnTime).length>0)
                        [...Object.keys(data.dataOnTime).filter(v=>v!=='step'),'step'].forEach(colName => {
                            if (!(colName in this.bufferOnTime)) this.bufferOnTime[colName] = data.dataOnTime[colName]
                            else this.bufferOnTime[colName] = [...this.bufferOnTime[colName], ...data.dataOnTime[colName].filter((v, i) => !this.bufferOnTime.step.includes(data.dataOnTime.step[i]))]
                            this.sizeBit += data.dataOnTime[colName].length
                        })
                        this.bufferOnTime.currentMaxStep = data.maxStep
                        this.maxStep = data.maxStep
                        this.isRunning = data.isRunning
                        console.log("buffer Size :", this.size, 'Mo')
                        //console.log("real buffer Size :", this.size2, 'Mo')
                    }
                    this.lock = false
                    Object.values(this.eventOnUpdate).forEach(cb => cb(this))
                    delete this.eventOnUpdate.temp
                }).catch((err) => console.error(err)).finally(()=>this.lock = false)
            }
        }
    }

    updateMaxStep(cb) {
        if (!this.lock) {
        this.lock = true
        fetchW('/api/simumaxstep/' + this.token, {
            method: 'GET'
        }).then(res => res.json()).then((data) => {
            this.maxStep = data.maxStep
            this.isRunning = data.isRunning
            cb()
        }).finally(() => this.lock = false)
        }
    }

/*
idée créer des attribut virtuel/calculer :
const attrVirt = {name:'mult', func:'$x*$y'}
eval('this.virtualAttr["'+name+'"] = (step)=>this.buffer[step].x.map((_,i)=>' + attrVirt.func.replaceAll('$x','this.buffer[step].x[i]').replaceAll('$y','this.buffer[step].y[i]') + ')')

let x=[1,2,3,4,5]
let y=[10,11,12,13,14]
let func = {}
eval('func.mul = ()=>x.map((a,i)=>x[i]*y[i])')
*/
    flush(){
        this.coordStepReqested = []
        this.infoStepReqested = {}
        this.sizeBit = 0
        this.buffer = { coord: {}, info: {} }
    }

    get size() {
        return this.sizeBit * 53 / 8e+6;
    }
    get size2() {
        return Object.keys(this.buffer).reduce((p, step) => p + Object.keys(this.buffer[step]).reduce((p2, info) => p2+this.buffer[step][info].length,0), 0) * 53 / 8e+6;
    }
}