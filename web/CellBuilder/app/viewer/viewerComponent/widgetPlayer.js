import ViewerManager from "../ViewerManager"

function getInterval(list) {
    list = list.map(i => parseInt(i))
    let j = 1, ret = []
    if (list.length === 1) ret = [list[0], list[0] + 1]
    while (j < list.length) {
        let i = j
        while (list[i] === list[i - 1] + 1) i++
        ret.push([list[j - 1], list[i - 1] + 1])
        j = i + 1
    }
    return ret
}
function timeFormat(step, dt) {
    let t = step * dt;
    let m = Math.round((t % 3600) / 60);
    let h = Math.floor(t / 3600)
    if (m < 10) m = "0" + m;
    if (h < 10) h = "0" + h;
    return (h + "h" + m);
}
/*
function timeFormat(step,dt){
    return Math.floor(step*dt / 3600) + ':' + Math.round(((step*dt) % 3600) / 60)//+'m'
}
*/
export default class WidgetPlayer {

    constructor(parent, dt, theoryMaxStep) {
        this.btn = {}
        this.stopped = false;
        const container = document.createElement('div')
        parent.appendChild(container)
        container.classList.add('viewerHeaderPlayer')

        const btnPlot = document.createElement('a')
        btnPlot.classList.add('viewerBtnPlot')
        container.appendChild(btnPlot);
        btnPlot.onclick = () => ViewerManager.togglePlot()

        this.btn.prevStep = document.createElement('a')
        this.btn.prevStep.classList.add('viewerBtnPrevStep')
        this.btn.prevStep.addEventListener('click', () => {
            if (this.currentStep>0){
                this.currentStep -= 1
                ViewerManager.buffer.loadStep(this.currentStep, ViewerManager.options.info)
            }
        })

        this.btn.nextStep = document.createElement('a')
        this.btn.nextStep.classList.add('viewerBtnNextStep')
        this.btn.nextStep.addEventListener('click', () => {
            if (this.currentStep < this.theoryMaxStep) {
                this.currentStep += 1
                ViewerManager.buffer.loadStep(this.currentStep, ViewerManager.options.info)
            }
        })

        container.appendChild(this.btn.prevStep);

        this.btn.play = document.createElement('a')
        this.btn.play.classList.add('viewerBtnPause')
        container.appendChild(this.btn.play);
        this.btn.play.addEventListener('click', () => {
            this.btn.play.classList.toggle('viewerBtnPlay');
            this.btn.play.classList.toggle('viewerBtnPause');
            if (this.isPlaying && !ViewerManager.buffer.isRunning) this.loop()
            this.btn.prevStep.classList.toggle('disable')
            this.btn.nextStep.classList.toggle('disable')
            this.resizeCanvas()
        })
        container.appendChild(this.btn.nextStep);
        this.btn.prevStep.classList.toggle('disable')
        this.btn.nextStep.classList.toggle('disable')

        this.btn.stop = document.createElement('a')
        this.btn.stop.classList.add('viewerBtnStop')
        container.appendChild(this.btn.stop);
        this.btn.stop.addEventListener('click', () => {
            let info = { event: "stop Button", deleteSimu: false };
            navigator.sendBeacon("/api/kill/" + ViewerManager.token, JSON.stringify(info))
            this.btn.stop.remove()
            this.resizeCanvas()
            this.stopped = true;
        })
        const canvascontainer = document.createElement("div");
        this.canvas = document.createElement("CANVAS");
        this.canvas.height = "35";
        container.style.height = "35px";
        canvascontainer.style.width = "100%";
        canvascontainer.appendChild(this.canvas);
        container.appendChild(canvascontainer);
        this.btn.record = document.createElement('a')
        this.btn.record.classList.add('viewerBtnRecord')
        container.appendChild(this.btn.record);
        this.btn.record.addEventListener('click', () => {
            ViewerManager.toggleRecordVideo()
        })
        const btnDebug = document.createElement('a')
        btnDebug.id = "btnDebug"
        btnDebug.classList.add('viewerBtnDebug')
        container.appendChild(btnDebug);
        btnDebug.onclick = () => ViewerManager.toggleDebug()
        window.addEventListener('resize', (e) => { this.resizeCanvas() });
        this.canvas.oncontextmenu = function () { return false; };
        window.addEventListener("mousemove", (e) => { this.mouseMove(e) }, false);
        window.addEventListener("mouseup", (e) => { this.mouseRelease(e) }, false);
        this.canvas.addEventListener("mousedown", (e) => { this.mouseClick(e) }, false);
        this.currentStepValue = 0
        this.widthTimeText = 130;
        this.dt = dt
        this.theoryMaxStep = theoryMaxStep-1
        this.maxStep = 1
        this.eventClick = {}
        this.bufferCoord = []
        this.bufferCurrentInfo = []
        if (this.canvas && this.canvas.getContext) {
            this.context = this.canvas.getContext('2d');
            if (this.context) this.resizeCanvas();
        }

        this.loop()
    }

    get currentStep() { return this.currentStepValue}

    set currentStep(newStep) {
        if ((this.currentStepValue !== newStep || this.stepClick) && newStep <= this.maxStep && !this.dragMove) {
            const exist = this.changeStepView(newStep)
            if (this.stepClick === newStep){ 
                this.currentStepValue = newStep
                this.plot()
                this.stepClick = undefined
                if (!exist) {
                    if (ViewerManager.buffer.lock){
                        ViewerManager.buffer.addEvent('temp', () => {
                            ViewerManager.buffer.loadStep(this.currentStep, ViewerManager.options.info)
                            ViewerManager.buffer.addEvent('temp', () => {
                                this.changeStepView(newStep)
                            })
                        })
                    } else {
                        ViewerManager.buffer.loadStep(this.currentStep, ViewerManager.options.info)
                        ViewerManager.buffer.addEvent('temp', () => {
                            this.changeStepView(newStep)
                        })
                    }
                }
            }
        }
    }

    changeStepView(newStep) {
        const data = ViewerManager.buffer.getDataWithCoord(newStep, ViewerManager.options.info)
        if (data) {
            ViewerManager.viewer.updateView(data)
            this.maxStep = ViewerManager.buffer.maxStep
            this.currentStepValue = newStep
            this.plot()
            Object.values(this.eventClick).forEach(cb => cb(this))
            return true
        }
        return false
    }

    forceUpdateView(){
        this.stepClick = this.currentStep
        this.currentStep = this.stepClick
        this.updateBufferView()
    }

    loop(lastTime = performance.now()) {
        if (!this.stopped || this.currentStep !== this.maxStep)
        setTimeout(() => {
            lastTime = performance.now()
            if (this.isPlaying) {
                this.currentStep += 1
                ViewerManager.buffer.loadStep(this.currentStep, ViewerManager.options.info)
                this.loop(lastTime)
            } else if (ViewerManager.buffer.isRunning){
                ViewerManager.buffer.updateMaxStep(() => {
                    this.maxStep = ViewerManager.buffer.maxStep
                    if (!ViewerManager.buffer.isRunning && this.btn.stop) {
                        this.btn.stop.remove()
                        this.resizeCanvas()
                        this.stopped = true;
                    }
                    this.plot()
                })
                this.loop(lastTime)
            }
        }, Math.max(this.isPlaying?(1000.0 / ViewerManager.options.targetFPS ) - (performance.now() - lastTime):1000, 0));
        else{
            this.btn.play.classList.toggle('viewerBtnPlay');
            this.btn.play.classList.toggle('viewerBtnPause');
            this.btn.prevStep.classList.toggle('disable')
            this.btn.nextStep.classList.toggle('disable')
        }
    }


    updateBufferView() {
        this.bufferCoord = getInterval(Object.keys(ViewerManager.buffer.buffer).filter(s => 'x' in ViewerManager.buffer.buffer[s]))
        this.bufferCurrentInfo = getInterval(Object.keys(ViewerManager.buffer.buffer).filter(s => ViewerManager.options.info in ViewerManager.buffer.buffer[s]))
        this.maxStep = ViewerManager.buffer.maxStep
        if (!ViewerManager.buffer.isRunning && this.btn.stop) {
            this.btn.stop.remove()
            this.resizeCanvas()
            this.stopped = true;
        }
        this.plot()
    }

    resizeCanvas() {
        this.canvas.width = 1;
        window.requestAnimationFrame(()=>{
            this.canvas.width = this.canvas.parentElement.clientWidth;
            this.plot();
        },10)
    }

    plot(previewStep) {
        const height = this.canvas.height = this.canvas.parentElement.clientHeight;
        const width = this.canvas.width = this.canvas.clientWidth;// - this.offsetBtnX;

        const widthTimeLine = width - this.widthTimeText

        const currentStep = previewStep ? previewStep : this.currentStep
        const time = currentStep / this.theoryMaxStep



        this.context.fillStyle = '#111'; // black background
        this.context.fillRect(0, 0, width, height);

        this.context.fillStyle = '#ccc'; // white background timeline
        this.context.fillRect(0, height * 0.3, widthTimeLine, height * 0.4);

        this.context.fillStyle = '#40b2bf'; // blue time
        this.context.fillRect(0, height * 0.3, widthTimeLine * time, height * 0.4);

        this.context.fillStyle = '#888';
        this.context.fillRect(this.maxStep / this.theoryMaxStep * widthTimeLine, height * 0.3, (this.theoryMaxStep - this.maxStep) / this.theoryMaxStep * widthTimeLine, height * 0.4);

        this.context.globalAlpha = 0.25;
        this.context.fillStyle = '#555'; // grey underline
        this.context.fillRect(0, height * 0.55, widthTimeLine, height * 0.15);


        this.context.globalAlpha = 0.1;
        this.context.fillStyle = '#000'; // green bufferCoord
        for (const [s, e] of this.bufferCoord)
            this.context.fillRect(s / this.theoryMaxStep * widthTimeLine, height * 0.55, (e - s) / this.theoryMaxStep * widthTimeLine, height * 0.15);

        this.context.fillStyle = '#000'; // blue bufferInfo
        for (const [s, e] of this.bufferCurrentInfo)
            this.context.fillRect(s / this.theoryMaxStep * widthTimeLine, height * 0.55, (e - s) / this.theoryMaxStep * widthTimeLine, height * 0.15);


        this.context.globalAlpha = 1.0;

        this.context.fillStyle = '#fff';
        this.context.font = 'bold 15px "DejaVu Sans Mono"';
        this.context.textBaseline = 'middle'
        this.context.fillText(timeFormat(currentStep, this.dt) + ' / ' + timeFormat(this.theoryMaxStep, this.dt), widthTimeLine + 10, height *0.55, this.widthTimeText - 20);
    }

    mouseClick(e) {
        if (!e) e = window.event;
        // e.button: 0 is left, 1 is middle, 2 is right.
        if (e.button == 0) {
            this.dragMove = true;
            this.plot(this.convertPxtoStep(e.clientX))
            e.preventDefault();
            e.stopPropagation();
            return false;
        } else {
            return true;
        }
    }

    mouseRelease(e) {
        if (!e) e = window.event;
        if (this.dragMove && e.button === 0) {
            if (e.button === 0) {
                this.dragMove = false;
                this.stepClick = this.convertPxtoStep(e.clientX)
                this.currentStep = this.stepClick
            }
        }
        this.plot();
        this.slideButtonDown = false;
        this.inMove = false;
    }

    mouseMove(e) {
        if (!e) e = window.event;
        if (this.dragMove) {
            this.plot(this.convertPxtoStep(e.clientX))
            e.preventDefault();
            e.stopPropagation();
            return false;
        } else if (this.clickableZone(e.clientX, e.clientY)){
            this.canvas.style.cursor = 'pointer'
        } else {
            this.canvas.style.cursor = 'default'
            return true;
        }
    }

    clickableZone(x,y) {
        const rect = this.canvas.getBoundingClientRect()
        if (y < rect.top || y > rect.top + rect.height) return false
        if (x < rect.left || x > (rect.left + (rect.width - this.widthTimeText)*(this.maxStep/this.theoryMaxStep))) return false
        return true
    }
    convertPxtoStep(x) {
        const step = Math.round(Math.max(Math.min((x - this.canvas.getBoundingClientRect().left) / (this.canvas.width - this.widthTimeText), 1),0) * this.theoryMaxStep)
        return step>this.maxStep?this.maxStep:step
    }

    get isPlaying() {
        return this.btn.play.classList.contains("viewerBtnPause")
    }

    set isPlaying(is) {
        if(is){
            this.btn.play.classList.add("viewerBtnPause")
            this.btn.play.classList.remove("viewerBtnPlay")
        } else {
            this.btn.play.classList.remove("viewerBtnPause")
            this.btn.play.classList.add("viewerBtnPlay")
        }
    }

    addEvent(name, callBack) {
        this.eventClick[name] = callBack
    }

    removeEvent(name) {
        delete this.eventClick[name]
    }
}