import * as monaco from 'monaco-editor';
let CPP_custom = {
    defaultToken: '',
    tokenPostfix: '.hpp',

    brackets: [
        { token: 'delimiter.curly', open: '{', close: '}' },
        { token: 'delimiter.parenthesis', open: '(', close: ')' },
        { token: 'delimiter.square', open: '[', close: ']' },
        { token: 'delimiter.angle', open: '<', close: '>' }
    ],

    types: [
        'array',
        'bool',
        'char',
        'class',
        'double',
        'float',
        'int',
        'long',
        'namespace',
        'short',
        'signed',
        'string',
        'struct',
        'void',
        'World',
        'cell_t',
        'world_t',
    ],

    constants: [
        'false',
        'nullptr',
        'true',
    ],

    keywords: [
        'abstract',
        'amp',
        'auto',
        'break',
        'case',
        'catch',
        'const',
        'constexpr',
        'const_cast',
        'continue',
        'cpu',
        'decltype',
        'default',
        'delegate',
        'delete',
        'do',
        'dynamic_cast',
        'each',
        'else',
        'enum',
        'event',
        'explicit',
        'export',
        'extern',
        'final',
        'finally',
        'for',
        'friend',
        'gcnew',
        'generic',
        'goto',
        'if',
        'in',
        'initonly',
        'inline',
        'interface',
        'interior_ptr',
        'internal',
        'literal',
        'mutable',
        'new',
        'noexcept',
        '__nullptr',
        'operator',
        'override',
        'partial',
        'pascal',
        'pin_ptr',
        'private',
        'property',
        'protected',
        'public',
        'ref',
        'register',
        'reinterpret_cast',
        'restrict',
        'return',
        'safe_cast',
        'sealed',
        'sizeof',
        'static',
        'static_assert',
        'static_cast',
        'switch',
        'template',
        'thread_local',
        'throw',
        'tile_static',
        'try',
        'typedef',
        'typeid',
        'typename',
        'union',
        'unsigned',
        'using',
        'vector',
        'virtual',
        'volatile',
        'wchar_t',
        'where',
        'while',

        '_asm', // reserved word with one underscores
        '_based',
        '_cdecl',
        '_declspec',
        '_fastcall',
        '_if_exists',
        '_if_not_exists',
        '_inline',
        '_multiple_inheritance',
        '_pascal',
        '_single_inheritance',
        '_stdcall',
        '_virtual_inheritance',
        '_w64',

        '__abstract', // reserved word with two underscores
        '__alignof',
        '__asm',
        '__assume',
        '__based',
        '__box',
        '__builtin_alignof',
        '__cdecl',
        '__clrcall',
        '__declspec',
        '__delegate',
        '__event',
        '__except',
        '__fastcall',
        '__finally',
        '__forceinline',
        '__gc',
        '__hook',
        '__identifier',
        '__if_exists',
        '__if_not_exists',
        '__inline',
        '__int128',
        '__int16',
        '__int32',
        '__int64',
        '__int8',
        '__interface',
        '__leave',
        '__m128',
        '__m128d',
        '__m128i',
        '__m256',
        '__m256d',
        '__m256i',
        '__m64',
        '__multiple_inheritance',
        '__newslot',
        '__nogc',
        '__noop',
        '__nounwind',
        '__novtordisp',
        '__pascal',
        '__pin',
        '__pragma',
        '__property',
        '__ptr32',
        '__ptr64',
        '__raise',
        '__restrict',
        '__resume',
        '__sealed',
        '__single_inheritance',
        '__stdcall',
        '__super',
        '__thiscall',
        '__try',
        '__try_cast',
        '__typeof',
        '__unaligned',
        '__unhook',
        '__uuidof',
        '__value',
        '__virtual_inheritance',
        '__w64',
        '__wchar_t'
    ],

    ponctuation: [
        ';',
        '->',
        '.'
    ],
    operators: [
        '=',
        '>',
        '<',
        '!',
        '~',
        '?',
        ':',
        '==',
        '<=',
        '>=',
        '!=',
        '&&',
        '||',
        '++',
        '--',
        '+',
        '-',
        '*',
        '/',
        '&',
        '|',
        '^',
        '%',
        '<<',
        '>>',
        '>>>',
        '+=',
        '-=',
        '*=',
        '/=',
        '&=',
        '|=',
        '^=',
        '%=',
        '<<=',
        '>>=',
        '>>>='
    ],

    // we include these common regular expressions
    symbols: /[=><!~?:&|+\-*\/\^%]+/,
    escapes: /\\(?:[abfnrtv\\"']|x[0-9A-Fa-f]{1,4}|u[0-9A-Fa-f]{4}|U[0-9A-Fa-f]{8})/,
    integersuffix: /([uU](ll|LL|l|L)|(ll|LL|l|L)?[uU]?)/,
    floatsuffix: /[fFlL]?/,
    encoding: /u|u8|U|L/,

    // The main tokenizer for our languages
    tokenizer: {
        root: [
            // C++ 11 Raw String
            [/@encoding?R\"(?:([^ ()\\\t]*))\(/, { token: 'string.raw.begin', next: '@raw.$1' }],
            [/(class|struct)(\s*)(\w+)(?=\s*\{)/, [{ token: 'keyword.$1' }, '', 'entity.name.type']],
            [/(\.|\-\>|this\-\>)(?=\w+)/, 'ponctuation'],
            [/\w+(?=\:\:)/, 'entity.name'],
            //[/(\.|\-\>)(\w+)(?=\()/, ['ponctuation', 'entity.name.function']],

            [/\w+(?=\s*\([^\)]*\)\s*\n*\{)/, {
                cases: {
                    '@keywords': { token: 'keyword.$0' },
                    '@default': { token: 'entity.name.function', next: '@parametersBlock' }
                }
            }],
            [/\w+(?=\()/, {
                cases: {
                    '@keywords': { token: 'keyword.$0' },
                    '@default': { token: 'entity.name.function' }
                }
            }],
            // identifiers and keywords
            [
                /[a-zA-Z_]\w*/,
                {
                    cases: {
                        '@keywords': { token: 'keyword.$0' },
                        '@constants': { token: 'constant.language' },
                        '@types': { token: 'storage.type' },
                        '@default': 'identifier'
                    }
                }
            ],

            // The preprocessor checks must be before whitespace as they check /^\s*#/ which
            // otherwise fails to match later after other whitespace has been removed.

            // Inclusion
            [/^\s*#\s*include/, { token: 'keyword.directive.include', next: '@include' }],

            // Preprocessor directive
            [/^\s*#\s*\w+/, 'keyword.directive'],

            ['this', 'variable.language'],

            // whitespace
            { include: '@whitespace' },

            // [[ attributes ]].
            [/\[\s*\[/, { token: 'annotation', next: '@annotation' }],
            // delimiters and operators
            [/[{}()\[\]]/, '@brackets'],
            [/< *>/, 'delimiter'],
            [
                /@symbols/,
                {
                    cases: {
                        '@operators': 'operator',
                        '@default': ''
                    }
                }
            ],

            // numbers
            [/\d*\d+[eE]([\-+]?\d+)?(@floatsuffix)/, 'number.float'],
            [/\d*\.\d+([eE][\-+]?\d+)?(@floatsuffix)/, 'number.float'],
            [/0[xX][0-9a-fA-F']*[0-9a-fA-F](@integersuffix)/, 'number.hex'],
            [/0[0-7']*[0-7](@integersuffix)/, 'number.octal'],
            [/0[bB][0-1']*[0-1](@integersuffix)/, 'number.binary'],
            [/\d[\d']*\d(@integersuffix)/, 'number'],
            [/\d(@integersuffix)/, 'number'],

            // delimiter: after number because of .\d floats
            [/[;,.]/, 'delimiter'],

            // strings
            [/"([^"\\]|\\.)*$/, 'string.invalid'], // non-teminated string
            [/"/, 'string', '@string'],

            // characters
            [/'[^\\']'/, 'string'],
            [/(')(@escapes)(')/, ['string', 'string.escape', 'string']],
            [/'/, 'string.invalid']
        ],


        parametersBlock: [
            [/\w+(?=\s*,)/, 'variable.parameter'],
            [/\w+(?=\s*\)\s*\n*\{)/, 'variable.parameter', '@popall'],
            [/\)(?=\s*\{)/, '@brackets','@popall'],
            [/(\.|\-\>)(\w+)(?=\()/, ['ponctuation', 'entity.name.function']],
            [/[a-zA-Z_]\w*/,{
                    cases: {
                        '@keywords': { token: 'keyword.$0' },
                        '@constants': { token: 'constant.language' },
                        '@types': { token: 'storage.type' },
                        '@default': 'identifier'
                    }
                }
            ],
            [/@symbols/, {
                cases: {
                    '@operators': 'operator',
                    '@default': ''
                }
            }],
            [/[;,.]/, 'delimiter']
        ],

        whitespace: [
            [/[ \t\r\n]+/, ''],
            [/\/\*\*(?!\/)/, 'comment.doc', '@doccomment'],
            [/\/\*/, 'comment', '@comment'],
            [/\/\/.*\\$/, 'comment', '@linecomment'],
            [/\/\/.*$/, 'comment']
        ],

        comment: [
            [/[^\/*]+/, 'comment'],
            [/\*\//, 'comment', '@pop'],
            [/[\/*]/, 'comment']
        ],

        //For use with continuous line comments
        linecomment: [
            [/.*[^\\]$/, 'comment', '@pop'],
            [/[^]+/, 'comment']
        ],

        //Identical copy of comment above, except for the addition of .doc
        doccomment: [
            [/[^\/*]+/, 'comment.doc'],
            [/\*\//, 'comment.doc', '@pop'],
            [/[\/*]/, 'comment.doc']
        ],

        string: [
            [/[^\\"]+/, 'string'],
            [/@escapes/, 'string.escape'],
            [/\\./, 'string.escape.invalid'],
            [/"/, 'string', '@pop']
        ],

        raw: [
            [
                /(.*)(\))(?:([^ ()\\\t"]*))(\")/,
                {
                    cases: {
                        '$3==$S2': [
                            'string.raw',
                            'string.raw.end',
                            'string.raw.end',
                            { token: 'string.raw.end', next: '@pop' }
                        ],
                        '@default': ['string.raw', 'string.raw', 'string.raw', 'string.raw']
                    }
                }
            ],
            [/.*/, 'string.raw']
        ],

        annotation: [
            { include: '@whitespace' },
            [/using|alignas/, 'keyword'],
            [/[a-zA-Z0-9_]+/, 'annotation'],
            [/[,:]/, 'delimiter'],
            [/[()]/, '@brackets'],
            [/\]\s*\]/, { token: 'annotation', next: '@pop' }]
        ],

        include: [
            [
                /(\s*)(<)([^<>]*)(>)/,
                [
                    '',
                    'keyword.directive.include.begin',
                    'string.include.identifier',
                    { token: 'keyword.directive.include.end', next: '@pop' }
                ]
            ],
            [
                /(\s*)(")([^"]*)(")/,
                [
                    '',
                    'keyword.directive.include.begin',
                    'string.include.identifier',
                    { token: 'keyword.directive.include.end', next: '@pop' }
                ]
            ]
        ]
    }
}


let PYTHON_custom = {
	defaultToken: "",
	tokenPostfix: ".python",

	storageTypeClassPython: ["class"],
	constantLanguagePython: ["True", "False", "None", "Notimplemented"],
	storageTypeFunctionPython: ["def"],
	variableLanguageSpecial: ["self"],
	keywords: [
        "and",
		"as",
        "assert",
		"async",
		"continue",
		"del",
		"assert",
		"break",
		"finally",
		"for",
        "from",
        "global",
		"elif",
		"else",
		"if",
        "import",
        "in",
        "is",
		"except",
        "not",
        "or",
		"pass",
		"raise",
		"return",
		"try",
		"while",
        "with",
        "yield",
        
	],

	supportFunctionBuiltin: [
		"__import__",
		"abs",
		"all",
		"any",
		"ascii",
        "bin",
        "bool",
		"callable",
		"chr",
		"compile",
		"copyright",
		"credits",
		"delattr",
        "dir",
        "dict",
		"divmod",
		"enumerate",
		"eval",
		"exec",
		"exit",
		"filter",
		"format",
		"getattr",
		"globals",
		"hasattr",
		"hash",
		"help",
		"hex",
		"id",
		"input",
		"isinstance",
		"issubclass",
        "iter",
        "int",
        "float",
        "lambda",
        "long",
        "complex",
        "hex",
		"len",
        "license",
        "list",
		"locals",
		"map",
		"max",
		"memoryview",
		"min",
		"next",
		"oct",
		"open",
		"ord",
		"pow",
		"print",
		"quit",
		"range",
		"reload",
		"repr",
		"reversed",
		"round",
		"setattr",
		"sorted",
        "sum",
        "super",
        "set",
        "slice",
        "str",
        "vars",
        "tuple",
        "type",
        "unichr",
        "unicode",
		"zip",
	],

    ponctuation: [
        ';',
        '->',
        '.'
    ],
    operators: [
        '=',
        ':=',
        '>',
        '<',
        '!',
        '~',
        '?',
        ':',
        '==',
        '<=',
        '>=',
        '!=',
        '&&',
        '||',
        '++',
        '--',
        '+',
        '-',
        '*',
        '**',
        '/',
        '//',
        '&',
        '|',
        '^',
        '%',
        '%%',
        '<<',
        '>>',
        '>>>',
        '+=',
        '-=',
        '*=',
        '/=',
        '&=',
        '|=',
        '^=',
        '%=',
        '<<=',
        '>>=',
        '>>>='
    ],
    // we include these common regular expressions
    symbols: /[=><!~?:&|+\-*\/\^%]+/,
    escapes: /\\(?:[abfnrtv\\"']|x[0-9A-Fa-f]{1,4}|u[0-9A-Fa-f]{4}|U[0-9A-Fa-f]{8})/,
    integersuffix: /([uU](ll|LL|l|L)|(ll|LL|l|L)?[uU]?)/,
    floatsuffix: /[fFlL]?/,
    encoding: /u|u8|U|L/,

	tokenizer: {
		root: [
			{ include: "@whitespace" },
            [/@[a-zA-Z]\w*/, "tag"],
            [/\b(def\s+)([A-Za-z|_][A-Za-z0-9_]+)(?=\s*\(.*\)[^\S\r\n]*\:)/, [
					"storage.type.function",
                    { token: 'entity.name.function', next: '@parametersBlockDef' }
			]],
			[/\b(class\s+)([A-Za-z|_][A-Za-z0-9_]*)/, [
					"storage.type.class",
                "entity.name.type.class"
			]],
            { include: "@codeBody" },
		],

        parametersBlockDef: [
            [/\((?=\s*\w+)/, { token: '@brackets', next: '@parameterDef' }],
            [/,(?=\s*\w+[\=,\)])/, { token: 'delimiter', next: '@parameterDef' }],
            [/\)(?=[^\S\r\n]*\:)/, '@brackets', '@pop'],
            { include: "@codeBody" },
        ],
        parameterDef: [
            ["self", { token: 'ponctuation', next: '@pop' }],
            [/[a-zA-Z_]\w*/, { token: 'variable.parameter', next: '@pop' }],
            { include: "@codeBody" },
        ],
        parametersBlock: [
            [/\(/, '@brackets'],
            [/[a-zA-Z_]\w*(?=\s*\=)/, { token: 'variable.parameter' }],
            [/\)/, '@brackets', '@pop'],
            { include: "@codeBody" },
        ],
        codeBody: [
			{ include: "@whitespace" },
            { include: "@numbers" },
            { include: "@strings" },
            [/(\.)(\w+)(?=\()/, ["delimiter",{ token: 'entity.name.function', next: '@parametersBlock' }]],
            [/\w+(?=\()/, {
                cases:{
                    "self": "ponctuation",
                    "@keywords": "keyword",
                    "@constantLanguagePython": "constant.language",
                    "@supportFunctionBuiltin": { token: "support.function.builtin", next: '@parametersBlock' },
                    "@storageTypeFunctionPython": "storage.type.function",
                    '@default':{ token: 'entity.name.function', next: '@parametersBlock' }
                }
            }],
            //[/\(/, { token: '@brackets', next: '@codeBody' }],
            [/\b[-_A-Z0-9]{2,}\b/, "constant.other.caps"],
            [/[{}[\]()]/, "@brackets"],
            [/[a-zA-Z_]\w*/, {
                cases: {
                    "self": "ponctuation",
                    "@keywords": "keyword",
                    "@constantLanguagePython": "constant.language",
                    "@supportFunctionBuiltin": "support.function.builtin",
                    "@storageTypeFunctionPython": "storage.type.function",
                    "@default": "identifier",
                }
            }],
            [/@symbols/, {
                cases: {
                    '@operators': 'operator',
                    '@default': ''
                }
            }],
            [/[;,.]/, 'delimiter'],

        ],
		// Deal with white space, including single and multi-line comments
		whitespace: [
			[/^\s*""".*(?=""")/, "comment", "@endDblComment"],
			[/^\s*""".*$/, "comment", "@endDblComment"],
			[/^\s*'''.*(?=""")/, "comment", "@endDblComment"],
			[/^\s*'''.*$/, "comment", "@endDblComment"],
			[/\s+/, "white"],
			[/(#.*$)/, "comment"],
        ],
		endDblDocString: [
			[/.*"""$/, "string", "@popall"],
			[/.*'''$/, "string", "@popall"],
			[/.*$/, "string"],
		],
		endDblComment: [
			[/.*"""$/, "comment", "@popall"],
			[/.*'''$/, "comment", "@popall"],
			[/.*$/, "comment"],
		],

		// Recognize hex, negatives, decimals, imaginaries, longs, and scientific notation
		numbers: [
			[/-?0x([abcdef]|[ABCDEF]|\d)+[lL]?/, "number.hex"],
			[/-?(\d*\.)?\d+([eE][+-]?\d+)?[jJ]?[lL]?/, "number"],
		],

		// Recognize strings, including those broken across lines with \ (but not without)
        strings: [
			[/"""/, "string.quote", "@stringBody2Triple"],
			[/'''/, "string.quote", "@stringBodyTriple"],
            [/(f)(''')/, ["support.function.builtin", { token: 'string.quote', next: '@stringWithCodeTriple' }]],
            [/(f)(""")/, ["support.function.builtin", { token: 'string.quote', next: '@stringWithCode2Triple' }]],
            [/(r)(''')/, ["support.function.builtin", { token: 'string.quote', next: '@stringRegexTriple' }]],
            [/(r)(""")/, ["support.function.builtin", { token: 'string.quote', next: '@stringRegex2Triple' }]],
            [/[fr]?"([^"\\]|\\.)*$/, 'string.invalid' ],
            [/[fr]?'([^'\\]|\\.)*$/, 'string.invalid' ],
            [/'/, "string.quote", "@stringBody"],
            [/"/, "string.quote", "@stringBody2"],
            [/(f)(')/, ["support.function.builtin", { token: 'string.quote', next: '@stringWithCode' }]],
            [/(f)(")/, ["support.function.builtin", { token: 'string.quote', next: '@stringWithCode2' }]],
            [/(r)(')/, ["support.function.builtin", { token: 'string.quote', next: '@stringRegex' }]],
            [/(r)(")/, ["support.function.builtin", { token: 'string.quote', next: '@stringRegex2' }]],
        ],
        regex:[
            [/(\\A|\\b|\\B|\\d|\\D|\\s|\\S|\\w|\\W|\\Z)/, 'string.regexp.sequence'], //entity.name.function
            [/\\\d+|\{\d+\}/, 'string.regexp.operator'],
            [/\\.|\\$/, 'string.escape'],
            [/(\(\?\=|\(\?\<\=|\(\?\<\!|\(\?|\(\!)/,'string.regexp.group'],//support.function.builtin'],
            [/\(|\)/,'string.regexp.group'],
            [/[\+\*\?\.\^\$\|]/,'string.regexp.operator'],
            //[/\[|\]/,'string.regexp.set'],//'ponctuation'
            [/(\[)(\^)(.*)(\])/,['string.regexp.set','string.regexp.operator','string.regexp.setcontent','string.regexp.set']],
            [/(\[)(.*)(\])/,['string.regexp.set','string.regexp.setcontent','string.regexp.set']],
            [/\s+/,''],
        ],
        stringRegex: [
            [/[^\\'\{\}\[\]\(\)\^\$\?\!\=\<\>\+\.\*\|]+/,  'string'],
            { include: "@regex" },
            [/'/, { token: 'string.quote', next: '@pop' } ],
            [/'/,'string'],
        ],
        stringRegex2: [
            [/[^\\"\{\}\[\]\(\)\^\$\?\!\=\<\>\+\.\*\|]+/,  'string'],
            { include: "@regex" },
            [/"/, { token: 'string.quote', next: '@pop' } ],
            [/"/,'string'],
        ],
        stringRegex2Triple: [
            [/[^\\"\{\}\[\]\(\)\^\$\?\!\=\<\>\+\.\*\|]+/,  'string'],
            { include: "@regex" },
            [/"""/, { token: 'string.quote', next: '@pop' } ],
            [/"/,'string'],
        ],
        stringRegexTriple: [
            [/[^\\'\{\}\[\]\(\)\^\$\?\!\=\<\>\+\.\*\|]+/,  'string'],
            { include: "@regex" },
            [/'''/, { token: 'string.quote', next: '@pop' } ],
            [/'/,'string'],
        ],
        stringWithCode: [
            [/[^\\'\{]+/,  'string'],
            [/@escapes|\\$/, 'string.escape'],
            [/\{/, "@brackets", "@stringCodeBody"],
            [/\\./, 'string'],
            [/'/, { token: 'string.quote', next: '@pop' } ]
        ],
        stringWithCode2: [
            [/[^\\"\{]+/,  'string'],
            [/@escapes|\\$/, 'string.escape'],
            [/\{/, "@brackets", "@stringCodeBody"],
            [/\\./, 'string'],
            [/"/, { token: 'string.quote', next: '@pop' } ]
        ],
        stringWithCode2Triple: [
            [/[^\\"\{]+/,  'string'],
            [/@escapes|\\$/, 'string.escape'],
            [/\{/, "@brackets", "@stringCodeBody"],
            [/"""/, { token: 'string.quote', next: '@pop' } ],
            [/\\.|"/, 'string'],
        ],
        stringWithCodeTriple: [
            [/[^\\'\{]+/,  'string'],
            [/@escapes|\\$/, 'string.escape'],
            [/\{/, "@brackets", "@stringCodeBody"],
            [/'''/, { token: 'string.quote', next: '@pop' } ],
            [/\\.|'/, 'string'],
        ],
        stringCodeBody: [
            [/\}/, "@brackets", "@pop"],
            { include: "@codeBody" },
        ],
        stringBody: [
            [/[^\\']+/,  'string'],
            [/@escapes|\\$/, 'string.escape'],
            [/\\./, 'string'],
            [/'/, { token: 'string.quote', next: '@pop' } ]
        ],
        stringBody2: [
            [/[^\\"]+/,  'string'],
            [/@escapes|\\$/, 'string.escape'],
            [/\\./, 'string'],
            [/"/, { token: 'string.quote', next: '@pop' } ]
           // [/.*\\$/, "string"],
           // [/.*$/, "string", "@pop"],
        ],
        stringBody2Triple: [
            [/[^\\"]+/,  'string'],
            [/@escapes|\\$/, 'string.escape'],
            [/"""/, { token: 'string.quote', next: '@pop' } ],
            [/\\.|"/, 'string']
        ],
        stringBodyTriple: [
            [/[^\\']+/,  'string'],
            [/@escapes|\\$/, 'string.escape'],
            [/'''/, { token: 'string.quote', next: '@pop' } ],
            [/\\.|'/, 'string']
        ],
	},
};

let themeDark = {
    "base": "vs-dark",
    "inherit": true,
    "rules": [
        {
            "foreground": "666466",
            "token": "comment"
        },
        {
            "foreground": "FFD866",
            "token": "string",
            "fontStyle" : "no-ligature"
        },
        {
            "foreground": "948ae3",
            "token": "string.escape"
        },
        {
            "foreground": "a2a0a2",
            "token": "string.quote"
        },
        {
            "foreground": "5ad4e6",
            "token": "string.regexp.sequence"
        },
        {
            "foreground": "fc618d",
            "token": "string.regexp.operator"
        },
        {
            "foreground": "7bd88f",
            "token": "string.regexp.group"
        },
        {
            "foreground": "a2a0a2",
            "token": "string.regexp.set"
        },
        {
            "foreground": "F7F1FF",
            "token": "string.regexp.setcontent"
        },
        {
            "foreground": "948ae3",
            "token": "number"
        },
        {
            "foreground": "948ae3",
            "token": "constant.numeric"
        },
        {
            "foreground": "948ae3",
            "token": "constant.language"
        },
        {
            "foreground": "948ae3",
            "token": "constant.character"
        },
        {
            "foreground": "948ae3",
            "token": "constant.other"
        },
        {
            "foreground": "fc618d",
            "token": "keyword"
        },
        {
            "foreground": "fc618d",
            "token": "storage"
        },
        {
            "foreground": "5ad4e6",
            "fontStyle": "italic",
            "token": "storage.type"
        },
        {
            "foreground": "7bd88f",
            "token": "entity.name"
        },
        {
            "foreground": "5ad4e6",
            "token": "entity.name.type.class"
        },
        {
            "foreground": "7bd88f",
            "token": "entity.other.inherited-class"
        },
        {
            "foreground": "7bd88f",
            "token": "entity.name.function"
        },
        {
            "foreground": "fd9353",
            "fontStyle": "italic",
            "token": "variable.parameter"
        },
        {
            "foreground": "fc618d",
            "token": "entity.name.tag"
        },
        {
            "foreground": "fc618d",
            "token": "operator"
        },
        {
            "foreground": "a2a0a2",
            "token": "ponctuation"
        },
        {
            "foreground": "a2a0a2",
            "token": "delimiter"
        },
        {
            "foreground": "7bd88f",
            "token": "entity.other.attribute-name"
        },
        {
            "foreground": "5ad4e6",
            "token": "support.function"
        },
        {
            "foreground": "5ad4e6",
            "token": "support.constant"
        },
        {
            "foreground": "5ad4e6",
            "fontStyle": "italic",
            "token": "support.type"
        },
        {
            "foreground": "5ad4e6",
            "fontStyle": "italic",
            "token": "support.class"
        },
        {
            "foreground": "ff0000",
            "token": "entity.other.attribute-name"
        },
        {
            "foreground": "fc618d",
            "token": "markup.deleted"
        },
        {
            "foreground": "7bd88f",
            "token": "markup.inserted"
        },
        {
            "foreground": "FFD866",
            "token": "entity.name.filename"
        }
    ],
    "colors": {
        "editor.foreground": "#F7F1FF",
        "editor.background": "#222222",
        "editor.selectionBackground": "#F7F1FF16",
        "editor.lineHighlightBackground": "#F7F1FF0A",
        "editorCursor.foreground": "#F7F1FF",
        "editorWhitespace.foreground": "#F7F1FF08",
        "editorIndentGuide.background": "#a2a0a260",
        "editorIndentGuide.activeBackground": "#a2a0a290",
        'editor.findMatchHighlightBackground':"#10bfd230",
        "editor.findMatchBorder":"#10bfd2",
        //"editor.findMatchHighlightBorder":"#ffffff50",
        "editor.findMatchBackground": "#10bfd240",
        "editor.selectionHighlightBorder": "#222222",
        "editorLineNumber.foreground": "#a2a0a2",
        "editorLineNumber.activeForeground": "#F7F1FF",
        "editorBracketHighlight.foreground1": "#fc618d",
        "editorBracketHighlight.foreground2": "#fd9353",
        "editorBracketHighlight.foreground3": "#FFD866",
        "editorBracketHighlight.foreground4": "#7bd88f",
        "editorBracketHighlight.foreground5": "#5ad4e6",
        "editorBracketHighlight.foreground6": "#948ae3",
        'diffEditor.insertedTextBackground': '#7bd88f11',
        'diffEditor.removedTextBackground': '#fc618d11',
        'diffEditor.insertedLineBackground':'#7bd88f11',
        'diffEditor.removedLineBackground':'#fc618d11',
        
    }
}
let colorDark = {
    'base':'vs-dark',
    'keyword':'#fc618d',
    'parameter':'#fd9353',
    'string':'#FFD866',
    'function': '#7bd88f',
    'type':'#5ad4e6',
    'number':'#948ae3',
    'background':'#222222',
    'foreground':'#F7F1FF',
    'comment':'#666466',
    'highlight':'#10bfd2',
    'lineNumber':'#a2a0a2'
}
let colorLight = {
    'base': 'vs',
    'keyword':'#CE356B',
    'parameter':'#cb4b16',
    'string':'#b58900',
    'function': '#2aa198',
    'type':'#268bd2',
    'number':'#8c51b4',
    'background':'#fcfcfc',
    'foreground':'#657b83',
    'comment':'#a3b1b1',
    'highlight':'#10bfd2',
    'lineNumber':'#80979c'
}

/* HACK TO DISABLE LIGATURE */
function insert( code ) {
    var style = document.createElement('style');
    if (style.styleSheet) style.styleSheet.cssText = code;
    else style.innerHTML = code;
    document.getElementsByTagName("head")[0].appendChild( style );
}
insert(".mtks {text-decoration: none!important; font-variant-ligatures: none;font-feature-settings: normal}")
themeDark = JSON.parse(JSON.stringify(themeDark).replaceAll('no-ligature','strikethrough'))
/* END HACK */
let generateTheme=(color)=>JSON.parse(Object.keys(colorDark).reduce((a, k, i, arr) => a.replaceAll(colorDark[k].replace('#', ''), color[k].replace('#', '')), JSON.stringify(themeDark)))



function setColorTheme(theme,colors){
    monaco.editor.defineTheme(theme, generateTheme(colors));
}

function init(){
    monaco.languages.setMonarchTokensProvider('cpp', CPP_custom)
    monaco.languages.setMonarchTokensProvider('python', PYTHON_custom)

    document.getElementById('theme-switch').onchange = (e) => {
        if (e.target.checked) {
            localStorage.setItem('theming', 'dark')
            monaco.editor.setTheme("dark")
        } else {
            localStorage.setItem('theming', 'light')
            monaco.editor.setTheme("light")
        }
    }
}

export { init, setColorTheme }