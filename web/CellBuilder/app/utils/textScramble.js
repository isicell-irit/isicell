// ——————————————————————————————————————————————————
// TextScramble
// ——————————————————————————————————————————————————

function spannify(html) {
  var regex = /(<span class="dud">.<\/span>)|([^<>])/gm
  var matches = html.matchAll(regex);
  var newHtml = [...matches].map((match=>match[1]?match[1]:"<span>"+match[2]+"</span>")).join('');
  return newHtml;
};
function despannify(html) {
  var chars = [...html.matchAll(/(?<=<span>).(?=<\/span>)/gm)];
  return chars.join('');
};

export default class TextScramble {
  constructor(el) {
    this.el = el;
    this.chars = '!-_\\/[]{}—=+*^?#________';
    this.update = this.update.bind(this);
  }
  setText(newText) {
    const oldText = despannify(this.el.childNodes[1].innerHTML);
    const length = Math.max(oldText.length, newText.length);
    const promise = new Promise(resolve => this.resolve = resolve);
    this.queue = [];
    for (let i = 0; i < length; i++) {
      const from = oldText[i] || '';
      const to = newText[i] || '';
      const start = Math.floor(Math.random() * 10);
      const end = start + Math.floor(Math.random() * 10);
      this.queue.push({ from, to, start, end });
    }
    cancelAnimationFrame(this.frameRequest);
    this.frame = 0;
    this.update();
    return promise;
  }
  update() {
    let output = '';
    let complete = 0;
    for (let i = 0, n = this.queue.length; i < n; i++) {
      let { from, to, start, end, char } = this.queue[i];
      if (this.frame >= end) {
        complete++;
        output += to;
      } else if (this.frame >= start) {
        if (!char || Math.random() < 0.28) {
          char = this.randomChar();
          this.queue[i].char = char;
        }
        output += `<span class="dud">${char}</span>`;
      } else {
        output += from;
      }
    }
    var spannifiedOutput = spannify(output);
    for(let h1 of this.el.childNodes){
      if(h1.innerHTML !== undefined){
        h1.innerHTML = spannifiedOutput;
      }
    }
    if (complete === this.queue.length) {
      this.resolve();
    } else {
      this.frameRequest = requestAnimationFrame(this.update);
      this.frame++;
    }
  }
  randomChar() {
    return this.chars[Math.floor(Math.random() * this.chars.length)];
  }
}
