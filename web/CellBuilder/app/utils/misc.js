import { longToast } from "../guiComponent/notification";
import LoggingManager from "../manager/LoggingManager";
import ansiToHtml from 'ansi-to-html';
// Save a reference to the original ResizeObserver
const OriginalResizeObserver = window.ResizeObserver;

// Create a new ResizeObserver constructor
window.ResizeObserver = function (callback) {
  const wrappedCallback = (entries, observer) => {
    window.requestAnimationFrame(() => {
      callback(entries, observer);
    });
  };

  // Create an instance of the original ResizeObserver
  // with the wrapped callback
  return new OriginalResizeObserver(wrappedCallback);
};

// Copy over static methods, if any
for (let staticMethod in OriginalResizeObserver) {
  if (OriginalResizeObserver.hasOwnProperty(staticMethod)) {
    window.ResizeObserver[staticMethod] = OriginalResizeObserver[staticMethod];
  }
}

export var regexVariableDeclarationSplit = /(?<type>.*)\s+(?<value>\w+)/

export function utf8_to_b64(str){
  return window.btoa(unescape(encodeURIComponent( str )));
}

export function b64_to_utf8(str){
  return decodeURIComponent(escape(window.atob( str )));
} 

export function firstLetterToLower(str) {
    return str.charAt(0).toLowerCase() + str.slice(1);
}

/**
 * Generates a Universally unique
 */
export function create_UUID(){
    var dt = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (dt + Math.random()*16)%16 | 0;
        dt = Math.floor(dt/16);
        return (c=='x' ? r :(r&0x3|0x8)).toString(16);
    });
    return uuid;
}

export function isOlderVersion(ver, cur_ver){
  if(ver){
	//console.log("version : ",ver)
    let subVers = ver.split('.')
    let currentSubVers = cur_ver.split('.')
    for(let i=0; i<subVers.length; i++)
      if(parseInt(subVers[i]) < parseInt(currentSubVers[i])) return true;
    return false;
  }
  return true;
}

export function addGridPatternToSVG(svg){
	const vp = svg.querySelector('.viewport')
	const id = Math.floor(Math.random() * 100000)
    vp.insertAdjacentHTML("afterBegin",`<g>
        <rect x="-49550" y="-49690" width="100000" height="100000"
            style="fill: url(&quot;#djs-grid-pattern-${id}&quot;);"></rect>
    </g>
    <defs>
        <pattern id="djs-grid-pattern-${id}" width="25" height="25"
            patternUnits="userSpaceOnUse">
            <path d="M 25 0 L 0 0 0 25" fill="none" stroke="var(--diagram-grid)" stroke-width="1.5"/>
        </pattern>
    </defs>`)
}

class Loader{
	constructor(){
		this.content = document.getElementById('loader');
		this.title = document.getElementById('loader-title');
	}
	show(){
		this.content.style.display = 'block';
	}
	hide(){
		this.content.style.display = 'none';
	}
	setText(str){
		this.title.innerHTML = str;
	}
	pointerEvents(pe){
    this.content.style.pointerEvents = pe;
	}
}
const loader = new Loader()
export function getLoader(){return loader};

export function downloadFile(data, filename){
	var link = document.createElement('a');
	link.setAttribute("href", data);
	link.setAttribute("download", filename);
	link.setAttribute("target", "_blank");
	link.click();
}

const ansiToHtmlConverter = new ansiToHtml(
	{ colors: { 0: 'var(--terminal-black)',8: 'var(--terminal-black-bright)',
				1: 'var(--terminal-red)',9: 'var(--terminal-red-bright)',
				2: 'var(--terminal-green)',10: 'var(--terminal-green-birght)',
				3: 'var(--terminal-yellow)',11: 'var(--terminal-yellow-birght)',
				5: 'var(--terminal-blue)',12: 'var(--terminal-blue-birght)',
				4: 'var(--terminal-purple)',13: 'var(--terminal-purple-birght)',
				6: 'var(--terminal-cyan)',14: 'var(--terminal-cyan-birght)',
				7: 'var(--terminal-white)',15: 'var(--terminal-white-bright)'
			} });

export {ansiToHtmlConverter};

export function fetchW(url,opt){
	return new Promise((resolve, reject) => {
		fetch(url,opt).then(h=>{
			if (h.status ===200){
				resolve(h)
			} else {
				if(h.status===401){
					if(LoggingManager.logged){
						longToast.fire({
							icon: 'error',
							title: 'You have been logged out'
						})
						LoggingManager.logged = undefined
					}
					reject(h)
				}
			}
		}).catch(h=>{
			reject(h)
		})
	})
}

export function streamJson(url,opt,callBacks){
	return fetchW(url, opt).then((res) => {
		let reader = res.body.getReader();
		let buffer = "["
		reader.read().then(async function processText({ done, value }) {
			if (done) {
				if(callBacks.callBackDone) callBacks.callBackDone();
				return;
			}
			value = (buffer + (new TextDecoder()).decode(value)).replaceAll(/}(?=\s*{)/gm, '},');
			let jsonValue;
			try {
				jsonValue = JSON.parse(value + "]")
				buffer = '['
			} catch (err) {
				buffer = value
			}
			if(jsonValue) callBacks.callBackouput(jsonValue)
			return reader.read().then(processText);
		});
		if(callBacks.callBackInit) callBacks.callBackInit();
	});
}