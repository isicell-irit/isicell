import JSZip from 'jszip';
import { downloadFile, utf8_to_b64 } from "./misc"
import stateMachine from '../diagrams/stateMachine';
import behaviorDiagram from '../diagrams/behaviorDiagram';
import firaCodeURI from '../../resources/fonts/FiraCode-Regular.woff2'

export async function draw(modeler, format, quality, background, fileName,forZip) {
    const promise = new Promise(async (resolve, reject) => {
        format = format || 'svg'
        quality = quality || 1
        background = background || false
        modeler = modeler || stateMachine.modeler
        let svg = await modeler.saveSVG()
        svg = svg.svg

        let matchSize = svg.match(/height=\"(\d+)/m);
        let height = matchSize && matchSize[1] ? parseInt(matchSize[1], 10) : 200;
        matchSize = svg.match(/width=\"(\d+)/m);
        let width = matchSize && matchSize[1] ? parseInt(matchSize[1], 10) : 200;
        let margin = 10 * quality; // set margin
        let fill = getComputedStyle(document.querySelector('.djs-container')).getPropertyValue('background-color');

        // it needs a namespace
        if (!svg.match(/xmlns=\"/mi)) {
            svg = svg.replace('<svg ', '<svg xmlns="http://www.w3.org/2000/svg" ');
        }

        let div = document.createElement('div')
        document.body.appendChild(div)
        div.innerHTML = svg


        let svgElements = div.querySelectorAll("svg *");
        let availableStyle = ['stroke-opacity', 'fill-opacity', 'stroke-width', 'stroke', 'fill', 'stroke-width', 'font-family', 'font-size', 'marker-end']
        for (let i = 0; i < svgElements.length; i++) {
            let element = svgElements[i];
            let classes = element.getAttribute("class");
            let elementStyles = {};

            let classStyles = getComputedStyle(element);
            for (let j = 0; j < element.style.length; j++) {
                let styleName = element.style[j];
                if (availableStyle.includes(styleName)) {
                    elementStyles[styleName] = classStyles.getPropertyValue(styleName);
                }
            }
            if (classes) {
                for (let j = 0; j < classStyles.length; j++) {
                    let styleName = classStyles[j];
                    if (!elementStyles.hasOwnProperty(styleName) && availableStyle.includes(styleName)) {
                        elementStyles[styleName] = classStyles.getPropertyValue(styleName);
                    }
                }
            }
            element.setAttribute("style", Object.entries(elementStyles).reduce((t, [styleName, styleValue]) => t + `${styleName}: ${styleValue};`, ""));
        }

        for (let i = 0; i < svgElements.length; i++) {
            let element = svgElements[i];
            element.removeAttribute("class");
        }

        let svgElement = div.querySelector('svg')
        let resp = await fetch(firaCodeURI);
        let blob = await resp.blob();
        let f = new FileReader();
        f.addEventListener('load', () => {
            let defs = document.createElement('defs')
            defs.innerHTML = `<style>
            @font-face {
                font-family: 'Fira Code';
                src: url(${f.result}) format('woff2');
            }
            <style>`
            svgElement.insertBefore(defs, svgElement.firstChild)

            let svgString = new XMLSerializer().serializeToString(svgElement);
            div.remove()

            if (format === 'svg') {
                if(forZip){
                    resolve({ fileName: fileName + ".svg", data: "data:image/svg+xml;base64," + utf8_to_b64(svgString)})
                } else {
                    let a = document.createElement("a");
                    a.href = "data:image/svg+xml;charset=utf-8," + encodeURIComponent(svgString);
                    a.download = fileName + ".svg";
                    a.click();
                    resolve()
                }
            } else {
                let canvas = document.createElement("canvas");
                canvas.width = width * quality + margin * 2;
                canvas.height = height * quality + margin * 2;

                try {
                    let domUrl = window.URL || window.webkitURL || window;
                    if (!domUrl) {
                        throw new Error("(browser doesnt support this)")
                    }
                    let ctx = canvas.getContext("2d");
                    let svgCanvas = new Blob([svgString], { type: "image/svg+xml;charset=utf-8" });
                    let url = domUrl.createObjectURL(svgCanvas);
                    var img = new Image();
                    img.onload = function () {
                        ctx.fillStyle = fill;
                        ctx.fillRect(0, 0, canvas.width, canvas.height);
                        ctx.drawImage(this, margin, margin, canvas.width - margin * 2, canvas.height - margin * 2);
                        if (forZip) {
                            resolve({ fileName: fileName + "." + format, data: canvas.toDataURL("image/" + format)})
                        } else {
                            let a = document.createElement("a");
                            a.href = canvas.toDataURL("image/" + format);
                            a.download = fileName + "." + format;
                            a.click();
                        }
                        domUrl.revokeObjectURL(url);
                    };
                    img.src = url;
                } catch (err) {
                    reject('failed to convert svg to png ' + err);
                }
            }
        });
        f.readAsDataURL(blob);
    })
    return promise;
}


export async function drawAll(format, quality, background) {
    var zip = new JSZip();

    let allImg = []
    allImg.push(await draw(undefined, format, quality, background, 'stateDiagram', true))
    behaviorDiagram.currentState = undefined;
    var prec1 = stateMachine.modeler.get('elementRegistry').get('Process_1');
    for (let compl of ['loop', 'initialisation']) {
        await stateMachine.viewElement(prec1, 'scenarioFunction', { complementTitle: compl });
        allImg.push(await draw(behaviorDiagram.modeler, format, quality, background, 'scenario_'+compl,true))
    }

    var activities = stateMachine.modeler.get('elementRegistry').filter((element) => (element.type === 'bpmn:Task' || element.type === 'bpmn:IntermediateCatchEvent')).sort((a, b) => (a.type === 'bpmn:IntermediateCatchEvent') ? 1 : -1);

    behaviorDiagram.currentState = undefined;
    for (let activity of activities) {
        await stateMachine.viewElement(activity, 'cellFunction');
        allImg.push(await draw(behaviorDiagram.modeler, format, quality, background, 'cell_' +activity.businessObject.name, true))
    }

    behaviorDiagram.currentState = undefined;
    behaviorDiagram.currentSup = "";

    allImg.forEach(img => zip.file(img.fileName, img.data.replace(/data:.*?;base64,/, ""), { base64: true }))
    zip.generateAsync({ type: "base64" }).then((base64) => {
        downloadFile("data:application/zip;base64," + base64, "allDiagrams.zip")
    });
}