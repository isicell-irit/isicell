
export function rgb(_r, _g, _b) {
    return { r: _r / 255.0, g: _g / 255.0, b: _b / 255.0 }
}
export const gradientPalette = {
    viridis: [rgb(68, 1, 84), rgb(72, 36, 117), rgb(65, 68, 135), rgb(53, 95, 141), rgb(42, 120, 142), rgb(33, 144, 141), rgb(34, 168, 132), rgb(66, 190, 113), rgb(122, 209, 81), rgb(189, 223, 38), rgb(189, 223, 38)],
    spectral: [rgb(158, 1, 66), rgb(209, 59, 75), rgb(240, 112, 74), rgb(252, 171, 99), rgb(254, 220, 140), rgb(251, 248, 176), rgb(224, 243, 161), rgb(170, 221, 162), rgb(105, 189, 169), rgb(66, 136, 181), rgb(66, 136, 181)],
    YlGnBu: [rgb(255, 255, 217), rgb(239, 249, 189), rgb(213, 239, 179), rgb(169, 221, 183), rgb(116, 201, 189), rgb(70, 180, 194), rgb(40, 151, 191), rgb(32, 115, 178), rgb(35, 78, 160), rgb(29, 49, 133), rgb(29, 49, 133)],
    plasma: [rgb(13, 8, 135), rgb(65, 4, 157), rgb(106, 0, 168), rgb(143, 13, 164), rgb(177, 42, 144), rgb(203, 70, 121), rgb(225, 100, 98), rgb(241, 131, 76), rgb(252, 166, 54), rgb(252, 206, 37), rgb(252, 206, 37)],
    cool: [rgb(110, 64, 170), rgb(97, 84, 200), rgb(76, 110, 219), rgb(54, 140, 225), rgb(36, 170, 216), rgb(26, 199, 194), rgb(29, 222, 163), rgb(48, 238, 131), rgb(82, 246, 103), rgb(126, 246, 88), rgb(126, 246, 88)],
    RdBu: [rgb(103, 0, 31), rgb(171, 32, 46), rgb(213, 95, 80), rgb(240, 162, 133), rgb(250, 214, 195), rgb(242, 239, 238), rgb(205, 227, 238), rgb(144, 194, 221), rgb(75, 148, 196), rgb(34, 101, 163), rgb(34, 101, 163)],
    PiYG: [rgb(142, 1, 82), rgb(192, 38, 126), rgb(221, 114, 173), rgb(240, 178, 214), rgb(250, 221, 237), rgb(245, 243, 239), rgb(225, 242, 202), rgb(183, 222, 136), rgb(129, 187, 72), rgb(79, 145, 37), rgb(79, 145, 37)],
    PuOr: [rgb(127, 59, 8), rgb(177, 90, 9), rgb(221, 132, 31), rgb(248, 182, 100), rgb(253, 221, 178), rgb(243, 238, 234), rgb(215, 215, 233), rgb(176, 170, 208), rgb(129, 112, 173), rgb(85, 45, 132), rgb(85, 45, 132)],
    BrBG: [rgb(84, 48, 5), rgb(139, 83, 15), rgb(188, 132, 52), rgb(221, 189, 123), rgb(242, 228, 191), rgb(238, 241, 234), rgb(195, 231, 226), rgb(128, 201, 191), rgb(57, 152, 144), rgb(10, 103, 95), rgb(10, 103, 95)]
};

export const gradientPaletteCSS = Object.fromEntries(Object.entries(gradientPalette).map(([nameP,listC])=>[nameP,listC.map(c=>rgbToHex(c))]))

export function componentToHex(c) {
    var hex = (c*255).toString(16);
    return hex.length == 1 ? "0" + hex : hex;
}

export function rgbToHex(c) {
    return "#" + componentToHex(c.r) + componentToHex(c.g) + componentToHex(c.b);
}

export function getDefaultColor(value){
    const defaultDiscret = ["#1B9E77", "#ff7f0e", "#B62728", "#1F77B4", "#E6AB02", "#9467BD", "#66A61E", "#E7298A", "#A6761D", "#666666"]
    if (Array.isArray(value)) return Object.fromEntries(value.map((name, i) => [name, defaultDiscret[i % defaultDiscret.length]]))
    else return { min: 0, max: 1, palette: 'viridis', reverse: false }
}