const LOG10 = Math.log(10)
var DELTA_THRESHOLD = 0.1;

var STEP_SIZE = 0.06505149978319905;

export default function addEventPanSVG(svg){
  svg.addEventListener('mousedown', onPointerDown); // Pressing the mouse
  svg.addEventListener('mouseup', onPointerUp); // Releasing the mouse
  svg.addEventListener('mouseleave', onPointerUp); // Mouse gets out of the SVG area
  svg.addEventListener('mousemove', onPointerMove); // Mouse is moving

  // Add all touch events listeners fallback
  svg.addEventListener('touchstart', onPointerDown); // Finger is touching the screen
  svg.addEventListener('touchend', onPointerUp); // Finger is no longer touching the screen
  svg.addEventListener('touchmove', onPointerMove); // Finger is moving
  svg.addEventListener('wheel', wheel)
// This function returns an object with X & Y values from the pointer event

function getPointFromEvent (event) {
    var point = {x:0, y:0};
    // If even is triggered by a touch event, we get the position of the first finger
    if (event.targetTouches) {
      point.x = event.targetTouches[0].clientX;
      point.y = event.targetTouches[0].clientY;
    } else {
      point.x = event.clientX;
      point.y = event.clientY;
    }
    
    return point;
  }
  
  // This variable will be used later for move events to check if pointer is down or not
  var isPointerDown = false;
  
  // This variable will contain the original coordinates when the user start pressing the mouse or touching the screen
  var pointerOrigin = {
    x: 0,
    y: 0
  };
  
  // Function called by the event listeners when user start pressing/touching
  function onPointerDown(event) {
    isPointerDown = true; // We set the pointer as down
    
    // We get the pointer position on click/touchdown so we can get the value once the user starts to drag
    var pointerPosition = getPointFromEvent(event);
    pointerOrigin.x = pointerPosition.x;
    pointerOrigin.y = pointerPosition.y;
  }
  
  // We save the original values from the viewBox
  const info = ['x','y','width','height']
  var pan = {x:0,y:0}
  // The distances calculated from the pointer will be stored here
  var newPan = {
    x: 0,
    y: 0
  };
  var viewport = svg.querySelector('g.viewport')
  // Calculate the ratio based on the viewBox width and the SVG width
  var ratio = 1/*viewBox.width / svg.getBoundingClientRect().width;
  console.log(viewBox.width, svg.getBoundingClientRect().width)
  window.addEventListener('resize', function() {
    ratio = viewBox.width / svg.getBoundingClientRect().width;
  });*/
  
  // Function called by the event listeners when user start moving/dragging
  function onPointerMove (event) {
    // Only run this function if the pointer is down
    if (!isPointerDown) {
      return;
    }
    event.preventDefault();
    var pointerPosition = getPointFromEvent(event);
    newPan.x = pan.x + (pointerPosition.x - pointerOrigin.x);
    newPan.y = pan.y + (pointerPosition.y - pointerOrigin.y);
    updateMatrix()
  }

  function updateMatrix(){
    viewport.setAttribute('transform', `matrix(${ratio},0,0,${ratio},${newPan.x},${newPan.y})`);
  }
  
  function onPointerUp() {
    // The pointer is no longer considered as down
    isPointerDown = false;
  
    // We save the viewBox coordinates based on the last pointer offsets
    pan.x = newPan.x;
    pan.y = newPan.y;
  }
  var _totalDelta = 0


  function wheel(event){
    event.preventDefault();

    // pinch to zoom is mapped to wheel + ctrlKey = true
    // in modern browsers (!)

    var isZoom = event.ctrlKey;

    var isHorizontalScroll = event.shiftKey;

    var factor = -1 * ratio,
        delta;

    if (isZoom) {
      factor *= event.deltaMode === 0 ? 0.020 : 0.32;
    } else {
      factor *= event.deltaMode === 0 ? 1.0 : 16.0;
    }

    if (isZoom) {
      var elementRect = svg.getBoundingClientRect();

      delta = (
        Math.sqrt(
          Math.pow(event.deltaY, 2) +
          Math.pow(event.deltaX, 2)
        ) * Math.sign(event.deltaY) * factor
      );
      _totalDelta += delta;

      if (Math.abs(_totalDelta) > DELTA_THRESHOLD) {
          var direction = delta > 0 ? 1 : -1;
      
          var currentLinearZoomLevel = Math.log(ratio) / LOG10;
      
          // snap to a proximate zoom step
          var newLinearZoomLevel = Math.round(currentLinearZoomLevel / STEP_SIZE) * STEP_SIZE;
      
          // increase or decrease one zoom step in the given direction
          newLinearZoomLevel += STEP_SIZE * direction;
      
          // calculate the absolute logarithmic zoom level based on the linear zoom level
          // (e.g. 2 for an absolute x2 zoom)
          const prevRatio = ratio
          ratio = Math.max(0.2, Math.min(4, Math.pow(10, newLinearZoomLevel)));
          _totalDelta = 0;
          pan.x -= (event.clientX - elementRect.left)*(ratio-prevRatio)
          pan.y -= (event.clientY - elementRect.top)*(ratio-prevRatio)
          newPan.x = pan.x;
          newPan.y = pan.y;
          updateMatrix()
      }
    } else {

      if (isHorizontalScroll) {
        pan.x += factor * event.deltaY
      } else {
          pan.x += factor * event.deltaX
          pan.y += factor * event.deltaY
      }
      newPan.x = pan.x;
      newPan.y = pan.y;
      updateMatrix()
    }
  }
}
