import PaletteProviderC from 'bpmn-js/lib/features/palette/PaletteProvider';
import TouchInteractionEvents from 'diagram-js/lib/features/touch/TouchInteractionEvents.js'
import BpmnModeler from 'bpmn-js/lib/Modeler';
import CustomBundle from '../customBpmn/bundle/customBundleBehaviorDiagram'
import PanelManager from '../guiComponent/panelManager'
import ModalWindows from '../guiComponent/modalWindow'
import CodeEditorProvider from '../guiComponent/codeEditorProvider'
import FunctionManager from '../manager/functionManager'
import Parameters from '../manager/ParametersManager';
import SearchManager from '../manager/searchManager';
import stateMachine from "../diagrams/stateMachine";
import {
    delegate as domDelegate,
    event as domEvent
} from 'min-dom';
import Hammer from 'hammerjs';
import { addGridPatternToSVG } from '../utils/misc';
import cppErrorManager from '../manager/CppErrorManager';


class BehaviorDiagram {
    constructor() {}

    init(idContainer) {
        var self = this;
        this.currentState = undefined;
        this.currentSubDiagKey = undefined;
        this.currentSup = "";
        this.listenBusEvent = false
        //this.AllElementRegisteries = {};

        /**
         * Containers setup
         */
        this.category = 'cellFunction' //scenarioFunction   cellFunction
        this.currentTaskEditing = 0;
        this.onChangeTaskEditing = 0;
        this.currentSelectedElement = undefined;
        this.mainContainer = document.getElementById(idContainer)

        /**
         * Panel management for the bottom editor
         */
        PanelManager.createSplitterContainer(idContainer, 'splitterBehaviorDiagram', false)
        // PanelManager.createSplitterContainer(idContainer, 'splitterBehaviorDiagram', false)
        this.localPannelManager = new PanelManager('splitterBehaviorDiagram', false, 0)
        this.diagramContainer = this.localPannelManager.createPanel('behaviorDiagram')
        var codeContainer = this.localPannelManager.createPanel('panelEditor')
        this.editorCall = document.createElement('div')
        this.editorCall.style = "height: 25px; width:100%"
        this.editorCall.id = 'editorCall'
        this.editorFunction = document.createElement('div')
        this.editorFunction.style = "height: 100%; width:100%"//calc(100% - 30px)
        this.editorFunction.id = 'editorFunction'

        this.editorFunction.addEventListener('keydown', (evt) => {
            if ((evt.key === 'f' || evt.key === 'F') && evt.shiftKey && evt.ctrlKey) {
                const editor = CodeEditorProvider.getEditor('editorFunction')
                const value = editor.getModel().getValueInRange(editor.getSelection())
                if(value) SearchManager.search(value)
                evt.preventDefault();
                evt.stopPropagation();
            }
        }, true)
        codeContainer.appendChild(this.editorCall)
        codeContainer.appendChild(document.createElement('hr'))
        codeContainer.appendChild(this.editorFunction)
        this.localPannelManager.setWidthPanel([70, 30], [false, false])
        this.disposableOnchangeEditorCall = undefined;
        this.disposableOnchangeEditorFunction = undefined;
        CodeEditorProvider.createEditor('editorCall', {
            lineNumbers: 'off',
            monoline: true
        });
        CodeEditorProvider.createEditor('editorFunction', {
            lineNumbers: 'on'
        });

        this.currentEditorCode=undefined
        this.currentEditorPrevCode=undefined
        this.currentEditorElement=undefined
        this.currentFunctionPastInfo=undefined
        editorFunction.addEventListener('focusout', function(){
            self.updateFunction();
            self.currentEditorCode=undefined
            self.currentEditorPrevCode=undefined
            self.currentEditorElement=undefined
        })
        this.updatePromise = undefined
        this.autoUpdateTimer = 5000 //ms

        this.behaviorContainer = document.createElement("div");
        this.behaviorContainer.id = "behaviorContainer";
        //this.behaviorContainer.tabIndex = 0
        this.behaviorContainer.style.height = "100%";
        this.diagramContainer.appendChild(this.behaviorContainer);
        

        this.behaviorContainer.addEventListener('mousedown', () => {
            this.behaviorContainer.tabIndex = 0
            stateMachine.mainContainer.tabIndex = null
          },true)

        /**
         * Title dom element
         */
        this.titleDom = document.createElement('div')
        this.titleDom.className = 'panel-title'
        this.behaviorContainer.appendChild(this.titleDom);
        // this.mainContainer.appendChild(this.titleDom);

        /**
         * Diagram error highlighting
         */
        this.errorDom = document.createElement('div')
        this.errorDom.className = 'panel-error'
        this.errorDom.style.display = 'none'
        this.behaviorContainer.appendChild(this.errorDom);
        this.hiligthElement = false;
        /**
         * Modal window for condition editing
         */
        var editorContainer = document.createElement('div')
        editorContainer.style = 'height: 25px;margin-top: 5px;'
        editorContainer.id = 'conditionBehaviorEditor'
        this.modalWindowCondtion = new ModalWindows('ConditionBehavior', editorContainer, (win)=>{
            CodeEditorProvider.TriggerCallBackMonolineEnter('conditionBehaviorEditor');
        });
        this.modalWindowCondtion.contentAndHeader.style.overflow='visible'
        CodeEditorProvider.createEditor('conditionBehaviorEditor', {monoline: true});



        /**
         * Create the modeler and adds all the event functions
         */
        this.modeler = new BpmnModeler({
            container: this.behaviorContainer,
            additionalModules: [CustomBundle],
            keyboard: { bindTo: this.behaviorContainer },
            textRenderer: {
                defaultStyle: {
                    fontFamily: 'Fira Code',
                },
                externalStyle: {

                    fontFamily: 'Fira Code',
                }
            }
        });


        addGridPatternToSVG(this.modeler.get('canvas')._svg)

        var eventBus = this.modeler.get('eventBus');
        this.modeling = this.modeler.get('modeling');

        this.modeling.setColor = function (elements, di) {
            elements.forEach(element => self.modeler.get('commandStack').execute('element.updateProperties', { element, properties: { di } }));
        }

        /**
         * This event is called when an MPMN object is being dragged from a list or a palette
         */
        eventBus.on('create.start', function (context) {
            if (context.shape.type === 'bpmn:Task') { // if the object is a function
                console.log(context.shape.businessObject.name)
                let width = context.shape.businessObject.name.length * 8 + 30
                let height = 50
                if (self.pastedElements[context.shape.id]) {
                    width = self.pastedElements[context.shape.id].width
                    height = self.pastedElements[context.shape.id].height
                }
                context.shape.x -= (width-context.shape.width)/2
                context.shape.y -= (height-context.shape.height)/2
                context.shape.width = width;
                context.shape.height = height;
            }
        })
        /**
         * This event is called when an MPMN object finished being dragged from a list or a palette
         */
        eventBus.on('create.end', function (context) {
            for(const element of context.elements){
                if (element.type === 'bpmn:Task') { // if the object is a function
                    if (self.pastedElements[element.id]) {
                        let toUpdate = self.pastedElements[element.id].businessObject
                        for (let k in toUpdate) element.businessObject[k] = toUpdate[k];
                        self.modeling.updateProperties(element, toUpdate);
                    } else {
                        let call = FunctionManager.getFunctionCall(self.category, element.businessObject.idFunc, '')
                        self.modeling.updateProperties(element, {
                            call: call,
                            idFunc: element.businessObject.idFunc
                        });
                        self.viewElement(element) // opens the code editor by default
                    }
                    
                }
            }
            return true; // leave the func but continue the event
        })

        eventBus.on('connection.added', function (event) {
            if (self.listenBusEvent) {
                if (event.element.type === 'bpmn:SequenceFlow' && event.element.source?.type === 'bpmn:Gateway'){
                    let flowOutFromSource = event.element.source.businessObject.outgoing?.map(f => behaviorDiagram.modeler.get('elementRegistry').get(f.id))
                    if (!flowOutFromSource || flowOutFromSource.length === 0) event.element.businessObject.name = 'YES'
                    else if (flowOutFromSource.length === 1)
                        if (flowOutFromSource[0].businessObject.name === 'YES') event.element.businessObject.name = 'NO'//self.modeling.updateProperties(event.element, { name: 'NO' });
                        else event.element.businessObject.name = 'YES'//self.modeling.updateProperties(event.element, { name: 'YES' });*/
                    else setTimeout(() => self.modeling.removeConnection(event.element),1);
                } else if (event.element.type === 'bpmn:SequenceFlow' && (event.element.source?.type === 'bpmn:Task'||event.element.source?.type === 'bpmn:StartEvent')){
                    if (event.element.source.outgoing.length > 1) setTimeout(() => self.modeling.removeConnection(event.element.source.outgoing[0]), 1);
                }
            }
        })
        this.pastedElements = {}
        eventBus.on('copyPaste.pasteElement', (context) => {
            if (context.descriptor.type === "bpmn:Task") {
                this.pastedElements[context.descriptor.businessObject.id] = this.copiedElements[context.descriptor.id]
            }
        });

        eventBus.on('copyPaste.elementsCopied', (context) => {
            this.copiedElements = {}
            for(let element of context.elements) {
                if (element.type === "bpmn:Task") {
                    let info = { businessObject:{call: element.businessObject.get('call'), idFunc: element.businessObject.get('idFunc')},width:element.width,height:element.height }
                    this.copiedElements[element.id] = info
                }
            }
        });

        eventBus.on('copyPaste.canCopyElements', context => {
            return context.elements.filter(e => e.type !== 'bpmn:StartEvent')
        })

        /**
         * This event is called when a BPMN object is clicked
         */
        eventBus.on('element.click', function (context) {
            var element = context.element;
            if (element.id === 'Process_1' && element.id !== self.currentSelectedElement) { // The element is the diagram background
                if (self.disposableOnchangeEditorCall) self.disposableOnchangeEditorCall.dispose()
                if (self.disposableOnchangeEditorFunction) self.disposableOnchangeEditorFunction.dispose()
                CodeEditorProvider.getEditor('editorFunction').updateOptions({
                    readOnly: true
                })
                self.editorCall.style.display = 'none'
                self.localPannelManager.unminimize(['panelEditor'])
                self.editorFunction.style.setProperty('height', 'calc(100% - 5px)');
                CodeEditorProvider.setValue('editorFunction', self.generateCode())
                CodeEditorProvider.setError('editorFunction',cppErrorManager.getMarkers('condition',element.businessObject.id))
                self.currentFunctionPastInfo = undefined;
                self.currentSelectedElement = element.id;
            } else if ((element.type === 'bpmn:Task' || (self.category === 'cellFunction' && element.type === 'bpmn:StartEvent' && self.currentState.type !== "bpmn:IntermediateCatchEvent")) && context.gfx.classList.contains('selected')) {
            // The element is a function call
                self.viewElement(element)
                self.currentSelectedElement = element.id;
            } else { // anything else
                if (self.disposableOnchangeEditorCall) self.disposableOnchangeEditorCall.dispose()
                if (self.disposableOnchangeEditorFunction) self.disposableOnchangeEditorFunction.dispose()
                self.disposableOnchangeEditorCall = undefined
                self.disposableOnchangeEditorFunction = undefined
                CodeEditorProvider.setValue('editorCall', '')
                CodeEditorProvider.setValue('editorFunction', '')
                self.localPannelManager.minimize(['panelEditor'])
                self.currentFunctionPastInfo = undefined;
                self.currentSelectedElement = undefined;
            }
            // Deselect stateMachine's element to avoid deletion in the stateMachine
            for(let el of stateMachine.modeler.get('selection').get("selectedElements"))
                stateMachine.modeler.get('selection').deselect(el)
        });
        /**
         * This event is called when an MPMN object is double clicked
         */
        eventBus.on('element.dblclick', 10000, (context) => {
            var element = context.element;
            // The element is a function call, the start event or a label
            if (element.type === 'bpmn:Task' ||
                element.type === 'bpmn:StartEvent' ||
                (element.type === 'label' && element.businessObject.$type !== 'bpmn:Gateway' ||
                element.type === 'bpmn:SequenceFlow')) {
                return true;
            }
            if (element.type === 'bpmn:Gateway' ||
                (element.type === 'label' && element.businessObject.$type === 'bpmn:Gateway')) { // The element is an arrow
                CodeEditorProvider.setValue('conditionBehaviorEditor', element.businessObject.name ? element.businessObject.name : '')
                CodeEditorProvider.getEditor('conditionBehaviorEditor')
                CodeEditorProvider.setError('conditionBehaviorEditor',cppErrorManager.getMarkers('condition',element.id))
                CodeEditorProvider.setCallBackMonolineEnter('conditionBehaviorEditor',(value) => {
                    if(value === '')value=undefined
                    //element.businessObject.name = value;
                    self.modeling.updateProperties(element, {
                        name: value
                    });
                    this.modalWindowCondtion.hide()
                });
                setTimeout(() =>{this.modalWindowCondtion.show();CodeEditorProvider.getEditor('conditionBehaviorEditor').focus()}, 10);
                return true;
            }
        });
        
        /**
         * Serializes the behavior diagram in the state diagram's XML whenever an element changes
         */
        eventBus.on('element.changed', (event) => {
            if (this.listenBusEvent){
                this.saveXML();
                let element = event.element
                if (element.type === 'bpmn:Task' || element.type === 'bpmn:StartEvent') {
                    SearchManager.updateIndexBeaviorCall(this.category, this.currentState ? this.currentState.id : this.titleDom.innerText , element.id, element.businessObject.get('call'))
                } else if (element.type === 'bpmn:Gateway') {
                    SearchManager.updateIndexBeaviorTransition(this.category, this.currentState ? this.currentState.id : this.titleDom.innerText , element.id, element.businessObject.get('name'))
                }
            }
        });


        eventBus.on('shape.removed', (event) => {
            if (this.listenBusEvent) {
                let element = event.element
                if (element.type === 'bpmn:Task') {
                    SearchManager.removeIndexBehavior(this.category, this.currentState ? this.currentState.id : this.titleDom.innerText , element.id)
                    if (this.currentEditorElement && this.currentEditorElement.id === element.id)
                        this.viewFunc(this.currentEditorElement.businessObject.get('idFunc'));
                }
            }
        });/*
        eventBus.on('connection.removed',  (event) => {
            if (this.listenBusEvent) {
                let element = event.element
                 if (element.type === 'bpmn:SequenceFlow') {
                     SearchManager.removeIndexBehavior(this.category, this.currentState ? this.currentState.id : this.titleDom.innerText , element.id)
                }
            }
        });*/

        eventBus.on('import.done', async (context) => {
            this.modeler.get('elementRegistry').forEach((element, gfx) => {
                if (element.type === 'bpmn:Task') {
                    SearchManager.updateIndexBeaviorCall(this.category, this.currentState ? this.currentState.id : this.titleDom.innerText , element.id, element.businessObject.get('call'))
                } else if (element.type === 'bpmn:Gateway') {
                    SearchManager.updateIndexBeaviorTransition(this.category, this.currentState ? this.currentState.id : this.titleDom.innerText , element.id, element.businessObject.get('name'))
                }
            })
            await this.saveXML();
            this.listenBusEvent = true
        });
    }

    /**
     *  Opens the code editor
     * @param {*} element The behavior diagram element to open
     */
    viewElement(element){
        // The editor cannot be in readOnly if this function is called
        CodeEditorProvider.getEditor('editorFunction').updateOptions({
            readOnly: false
        })
        this.editorCall.style.display = 'block'
        this.editorFunction.style.setProperty('height', 'calc(100% - 30px)');
        this.localPannelManager.unminimize(['panelEditor']) // opens the panel
        if (this.disposableOnchangeEditorCall) this.disposableOnchangeEditorCall.dispose()
        if (this.disposableOnchangeEditorFunction) this.disposableOnchangeEditorFunction.dispose()

        // Sets the inside of the code editors with the right code
        try{
        CodeEditorProvider.setValue('editorCall', element.businessObject.get('call') ? element.businessObject.get('call') : '')
        CodeEditorProvider.setError('editorCall',cppErrorManager.getMarkers('call',element.id))
        CodeEditorProvider.setValue('editorFunction', FunctionManager.getCode(this.category, element.businessObject.get('idFunc')))
        CodeEditorProvider.setError('editorFunction',cppErrorManager.getMarkers('function',element.businessObject.get('idFunc')))
        }catch(err){
            console.error(element.businessObject.get('call'), element.businessObject.get('idFunc'))
        }
        this.currentFunctionPastInfo = FunctionManager.getInfo(element.businessObject.get('idFunc'));

        this.currentEditorElement = element
        this.currentEditorFunction = undefined

        // Callback on modification of the function call
        this.disposableOnchangeEditorCall = CodeEditorProvider.getEditor('editorCall').getModel().onDidChangeContent(() => {
            
            var call = CodeEditorProvider.getValue('editorCall')
            var newCall = FunctionManager.getFunctionCall(this.category, element.businessObject.get('idFunc'), call)

            if (call !== element.businessObject.get('call') && newCall !== undefined){
                if (element.businessObject.get('idFunc')!=='commonBehaviorFunction'){
                    let x=element.x + element.width/2
                    let newWidth = newCall.length*8+30
                    this.modeling.resizeShape(element,{x:x - newWidth/2, y:element.y, width:newWidth, height:element.height})
                    this.modeling.updateProperties(element, {
                        name: newCall,
                        call: newCall
                    });
                } else {
                    this.modeling.updateProperties(element, {
                        call: newCall,
                    });
                }
                /*if(call!==newCall)
                CodeEditorProvider.setValue('editorCall', newCall)
                CodeEditorProvider.setError('editorCall',cppErrorManager.getMarkers('call',element.id))*/
                Parameters.changeDetected()
            }
            if(newCall===undefined || call!==newCall){
                CodeEditorProvider.getEditor('editorCall').trigger('whatever...', 'undo')
            }
        })

        // Callback on modification of the function definition
        this.disposableOnchangeEditorFunction = CodeEditorProvider.getEditor('editorFunction').getModel().onDidChangeContent(() => {
            var code = CodeEditorProvider.getValue('editorFunction')

            var prevCode = FunctionManager.getCode(this.category, element.businessObject.get('idFunc'))
            FunctionManager.addOrUpdateFunction(this.category, element.businessObject.get('idFunc'), code)
            Parameters.changeDetected()
            this.currentEditorCode = code
            this.currentEditorPrevCode = prevCode
            this.currentEditorElement = element

            // set timer for update
            if(this.cancellable !== undefined)
                this.cancellable.cancel()
            this.cancellable = this.createCancellableSignal()
            this.updatePromise = this.autoUpdate(this.cancellable.signal, this.autoUpdateTimer, this);
            this.updatePromise.then(res => console.debug("editor autoUpdate")).catch(err => console.debug("cancel editor autoUpdate"));
        })
    }

    // to view the function independently of any element
    viewFunc(function_id){
        if(!function_id)return
        this.currentFunctionPastInfo = FunctionManager.getInfo(function_id);

        CodeEditorProvider.getEditor('editorFunction').updateOptions({
            readOnly: false
        })

        this.editorCall.style.display = 'none'
        this.editorFunction.style.setProperty('height', 'calc(100% - 5px)');
        this.localPannelManager.unminimize(['panelEditor']) // opens the panel
        if (this.disposableOnchangeEditorCall) this.disposableOnchangeEditorCall.dispose()
        if (this.disposableOnchangeEditorFunction) this.disposableOnchangeEditorFunction.dispose()

        CodeEditorProvider.setValue('editorFunction', FunctionManager.getCode(this.category, function_id)||'')
        CodeEditorProvider.setError('editorFunction',cppErrorManager.getMarkers('function',function_id)||'')
        this.currentEditorElement = undefined;
        this.currentEditorFunction = function_id;

        // Callback on modification of the function definition
        this.disposableOnchangeEditorFunction = CodeEditorProvider.getEditor('editorFunction').getModel().onDidChangeContent(() => {
            var code = CodeEditorProvider.getValue('editorFunction')

            var prevCode = FunctionManager.getCode(this.category, function_id)
            FunctionManager.addOrUpdateFunction(this.category, function_id, code)

            this.currentEditorCode = code
            this.currentEditorPrevCode = prevCode
            this.currentEditorElement = undefined
            this.currentEditorFunction = function_id
            Parameters.changeDetected()

            // set timer for update
            if(this.cancellable !== undefined)
                this.cancellable.cancel()
            this.cancellable = this.createCancellableSignal()
            this.updatePromise = this.autoUpdate(this.cancellable.signal, this.autoUpdateTimer, this);
            this.updatePromise.then(res => console.debug("editor autoUpdate")).catch(err => console.debug("cancel editor autoUpdate"));
            Parameters.changeDetected()
        })
    }

    updateFunction() {
        if(this.cancellable !== undefined){
            this.cancellable.cancel()
            this.cancellable = undefined
            this.updatePromise = undefined
        }
        if(this.currentEditorCode!==undefined &&
           this.currentEditorPrevCode!==undefined &&
           (this.currentEditorElement!==undefined || this.currentEditorFunction!==undefined)){
            var self = this;
            var code = this.currentEditorCode;
            var prevCode = this.currentEditorPrevCode;
            var idFunc = this.currentEditorElement!==undefined ? this.currentEditorElement.businessObject.get('idFunc') : this.currentEditorFunction;
            let prevFuncInfo = this.currentFunctionPastInfo;
            let oldFuncName = (prevFuncInfo === undefined) ? "" : prevFuncInfo.functionName;
            let checked = FunctionManager.checkFunctionAvailability(idFunc, this.category, code);
            if(code !== checked.code){
                code = checked.code;
                FunctionManager.addOrUpdateFunction(this.category, idFunc, code);
            }

            let parNest = "\\((?:[^)(]+|\\((?:[^)(]+|\\([^)(]*\\))*\\))*\\)"; // Parenthesis nesting (Depth 2max)
            let callRegex = new RegExp("((?:[\\w|?]+ *= *)?"+oldFuncName+parNest+")", "g");

            var elemCall = undefined;
            if(this.currentEditorElement!==undefined)
                elemCall = this.currentEditorElement.businessObject.get('call');
            else{
                // take one the existing calls
                let elements = this.modeler.get('elementRegistry').filter(function (elementR) {
                    return (elementR.businessObject !== undefined && elementR.businessObject.get('idFunc') === idFunc);
                })
                if(elements.length > 0)
                    elemCall = elements[0].businessObject.get('call')
            }
            if(elemCall===undefined){//try to find a call in all the other elements
                var found = false;
                stateMachine.forEachElement(self.category, (xml,code,element)=>{
                    let res = {xml,code};

                    if(!found){
                        let match = code.match(callRegex);
                        if(match !== null){
                            elemCall = match[0];
                            found = true;
                        }
                    }

                    return res;
                })
            }

            var newCall =  FunctionManager.getFunctionCall(this.category, idFunc, elemCall);

            if(newCall!==undefined && prevCode !== code){
                if (elemCall !== newCall) {
                    var functionName = FunctionManager.getInfo(idFunc).functionName;
                    var returnType = FunctionManager.getInfo(idFunc).returnType;
                    var params = FunctionManager.getInfo(idFunc).params;
                    CodeEditorProvider.setValue('editorCall', newCall);
                    CodeEditorProvider.setError('editorCall',cppErrorManager.getMarkers('call',idFunc))
                    var item = document.querySelector('custom-item[data-idFunc="' + idFunc + '"]');
                    item.valueTextNoIntegrityCheck = functionName + '()';
                    item.setAttribute('data-name',functionName+'()');
                    item.type = returnType;
                    item.setAttribute('title',returnType+' '+functionName+'('+params+')');

                    if(elemCall!==undefined){

                        //* Replaces every occurences of this function in the project
                        //? Pretty trash but I can't think of anything better...
                        stateMachine.forEachElement(this.category, (xml,code,element)=>{
                            let res = {};
                            let somethingChanged = false;
                            res['xml'] = xml.replace(callRegex,(match, offset, string)=>{
                                let oldCallInfo = FunctionManager.parseFunctionCall(elemCall);
                                let call = FunctionManager.getFunctionCall(this.category, idFunc, match);
                                let callInfo = FunctionManager.parseFunctionCall(call);

                                if(oldCallInfo.variable !== callInfo.variable || callInfo.params.length > oldCallInfo.params.length)
                                    somethingChanged = true;

                                return call;
                            });
                            res['code'] = code.replace(callRegex,(match, offset, string)=>{
                                let oldCallInfo = FunctionManager.parseFunctionCall(elemCall);
                                let call = FunctionManager.getFunctionCall(this.category, idFunc, match);
                                let callInfo = FunctionManager.parseFunctionCall(call);

                                if(oldCallInfo.variable !== callInfo.variable || callInfo.params.length > oldCallInfo.params.length)
                                    somethingChanged = true;

                                return call;
                            });

                            if(self.category==='cellFunction' && somethingChanged)
                                stateMachine.setStateChanged(element);

                            if (somethingChanged) Parameters.changeDetected()

                            return res;
                        })
                        var elements = this.modeler.get('elementRegistry').filter((elementR)=>{
                            return (elementR.businessObject !== undefined && elementR.businessObject.get('idFunc') === idFunc);
                        })
                        for (var i = 0; i < elements.length; i++) {
                            let call = FunctionManager.getFunctionCall(self.category, idFunc, elements[i].businessObject.get('call'))
                            if (idFunc !=='commonBehaviorFunction'){
                                let x=elements[i].x + elements[i].width/2
                                this.modeling.resizeShape(elements[i],{x:x - call.length*8/2, y:elements[i].y, width:call.length*8+30, height:elements[i].height})
                                self.modeling.updateProperties(elements[i], {
                                    name: call,
                                    call: call
                                });
                            } else {
                                self.modeling.updateProperties(elements[i], {
                                    call: call,
                                });
                            }
                        }
                    }
                    this.saveXML();
                }
            }
            if(newCall===undefined) {
                FunctionManager.addOrUpdateFunction(this.category, idFunc, prevCode)
                CodeEditorProvider.getEditor('editorFunction').trigger('whatever...', 'undo')
            }
            this.currentFunctionPastInfo = FunctionManager.getInfo(idFunc);
        }
        return true;
    }

    autoUpdate(signal, ms, self) {
        return new Promise((res, rej) => {
            const timeOut = setTimeout(() => {
                res("ok");
                self.updateFunction()
            }, ms);

            signal.catch(err => {
                rej(err);
                clearTimeout(timeOut);
            });
        });
    }
    createCancellableSignal() {
        const ret = {};
        ret.signal = new Promise((resolve, reject) => {
            ret.cancel = () => {
                reject(new Error("Promise was cancelled"));
            };
        });
        return ret;
    }

    /**
     * Serializes the diagram in the state diagram
     */
    async saveXML() {
        try {
            const oldXML = stateMachine.subDiagram[this.currentState?.id?this.currentState.id:this.currentSup]?.behaviorDiagram
            const code = this.generateCode()
            const { xml } = await this.modeler.saveXML({format: true});
            this.onChangeTaskEditing(xml, code, document.getElementById('Local Variables').items.map(i=>{return{type:i.type,value:i.valueText}})) // calls the stateMachine callback
            if(oldXML !== xml) {
                stateMachine.setStateChanged(this.currentState);
                Parameters.changeDetected()
            }
        } catch (err) {
            console.error(err);
        }
    }

    /**
     * Returns all the transitions from the diagram as an object (source,target,condition,id)
     */
    getFlow() {
        return this.modeler.get('elementRegistry').filter(function (element) {
            return (element.type === 'bpmn:SequenceFlow');
        }).map((element) => {
            return {
                source: element.source.businessObject,
                target: element.target.businessObject,
                condition: element.businessObject.name,
                idFlow: element.businessObject.id
            }
        })
    }

    /**
     * Generates the C++ code based on the behavior diagram and handles errors
     */
    generateCode() {
        // All local variables declarations
        var variablesDeclaration = document.getElementById('Local Variables').items.map((v) => {
            return v.type + ' ' + v.valueText + ';\n'
        }).join('');

        // Gets all the transitions
        var flows = this.getFlow()
        var flowWithStart = flows.filter((f) => f.source.$type === 'bpmn:StartEvent')
        let bpmnStart = this.modeler.get('elementRegistry').filter((e) => e.type === 'bpmn:StartEvent')[0]
        var callCommonBehavior = ''
        if (this.currentState && bpmnStart) callCommonBehavior = (this.currentState.type !== "bpmn:IntermediateCatchEvent" && this.category=="cellFunction") ? (bpmnStart.businessObject.get('call')+';\n'):''
        if(!bpmnStart){
            this.modeler.get('elementRegistry').forEach(e=>console.error(e))
            alert("if you see this message, please contact David Bernard")
        }
        //console.trace(callCommonBehavior)
        // Returns empty if the start isn't connected
        if (flowWithStart.length < 1) return variablesDeclaration+callCommonBehavior

        // Calls the function manager generation function and checks for errors
        var idStart = flowWithStart[0].source.id;
        var generatedCode = FunctionManager.generateCode(this.currentSubDiagKey,idStart, flows)

        // If an error is detected displays it in the diagram and opens the error panel and return empty
        if (generatedCode.error) {
            this.errorDom.style.display = 'block'
            this.errorDom.innerText = generatedCode.error.text
            this.hiligthElement = true;
            console.error("generated code error", generatedCode.error)
            this.modeler.get('elementRegistry').forEach(function (element, gfx) {
                if (element.type === 'bpmn:Task'){
                    if (generatedCode.error.idElement.includes(element.businessObject.id)) {
                        gfx.querySelector('g rect').style.stroke = 'red'
                    } else {
                        gfx.querySelector('g rect').style.stroke = 'black'
                    }
                } else if (element.type === 'bpmn:Gateway') {
                        if (generatedCode.error.idElement.includes(element.businessObject.id)) {
                            gfx.querySelector('g polygon').style.fill = 'red'
                        } else {
                            gfx.querySelector('g polygon').style.fill = 'white'
                        }
                } else if (element.type === 'bpmn:SequenceFlow') {
                    if (generatedCode.error.idElement.includes(element.businessObject.id)) {
                        gfx.querySelector('g path').style.setProperty('stroke','red','important')
                    } else {
                        gfx.querySelector('g path').style.stroke = 'black'
                    }
                }
            });
            // return ""
            return variablesDeclaration + callCommonBehavior + generatedCode.code
        } else { // if no error is detected go back to normal and return the generated code
            if (this.hiligthElement) {
                this.errorDom.style.display = 'none'
                this.modeler.get('elementRegistry').forEach(function (element, gfx) {
                    if (element.type === 'bpmn:Task') {
                        gfx.querySelector('g rect').style.stroke = 'black'
                    } else if (element.type === 'bpmn:SequenceFlow') {
                        gfx.querySelector('g path').style.stroke = 'black'
                    } else if (element.type === 'bpmn:Gateway') {
                        gfx.querySelector('g polygon').style.fill = 'white'
                    }
                })
                this.hiligthElement = false;
            }
            return variablesDeclaration + callCommonBehavior + generatedCode.code
        }
    }


    /**
     * Updates the diagram when an element is clicked in the state diagram
     * @param {*} category cellFunction or ScenarioFunction
     * @param {*} diagramXML The diagram of the state diagram element
     * @param {*} variablesList The local variables of the state diagram element
     * @param {*} onChangeCallBack The state diagram function to be called on modification of the behavior diagram
     */
    async updateDiagram(category, diagramXML, variablesList, onChangeCallBack) {
        this.listenBusEvent = false
        this.modeler.clear()
        this.category = category;
        let sec = document.getElementById('Local Variables')
        sec.clear()
        variablesList.forEach(i=>{
            let item = sec.itemBuilder()
            item.valueTextNoIntegrityCheck = i.value
            item.type = i.type
            sec.addItem(item)
        })
        if (this.disposableOnchangeEditorCall) this.disposableOnchangeEditorCall.dispose()
        if (this.disposableOnchangeEditorFunction) this.disposableOnchangeEditorFunction.dispose()
        this.disposableOnchangeEditorCall = undefined
        this.disposableOnchangeEditorFunction = undefined
        // this.currentEditorCode = undefined
        // this.currentEditorPrevCode = undefined
        // this.currentEditorElement = undefined
        this.localPannelManager.minimize(['panelEditor'])
        try {
            const { warnings} = await this.modeler.importXML(diagramXML);
        } catch (err) {
            console.error(err.message, err.warnings);
        }
        
        this.onChangeTaskEditing = onChangeCallBack;
        
        /*
        if(this.currentState)
            this.AllElementRegisteries[this.currentState.id] = this.modeler.get('elementRegistry');*/
    }

    /**
     * Sets the title for the behavior diagram panel
     * @param {*} text The text to set the title to
     */
    setTitle(text) {
        this.titleDom.innerText = text;
    }


    /**
     * Clones all events from the BPMN palette to an other object
     * @param {*} idNewPalette The id of the object to which transfer the events
     */
    transfertEventPalette(idNewPalette) {
        let container = document.getElementById(idNewPalette);
        let palette = this.modeler.get('palette');
        domDelegate.bind(container, '.entry', 'click', function (event) {
            palette.trigger('click', event);
        });

        // prevent drag propagation
        domEvent.bind(container, 'mousedown', function (event) {
            event.stopPropagation();
        });

        // prevent drag propagation
        domDelegate.bind(container, '.entry', 'dragstart', function (event) {
            palette.trigger('dragstart', event);
        });

        function stopEvent(event) {
            event.preventDefault();
            if (typeof event.stopPropagation === 'function') {
                event.stopPropagation();
            } else if (event.srcEvent && typeof event.srcEvent.stopPropagation === 'function') {
                // iPhone & iPad
                event.srcEvent.stopPropagation();
            }

            if (typeof event.stopImmediatePropagation === 'function') {
                event.stopImmediatePropagation();
            }
        }

        function createTouchRecognizer(node) {
            var mouseEvents = [
                'mousedown',
                'mouseup',
                'mouseover',
                'mouseout'
            ];
            function stopMouse(event) {

                mouseEvents.forEach(function (e) {
                    domEvent.bind(node, e, stopEvent, true);
                });
            }

            function allowMouse(event) {
                setTimeout(function () {
                    mouseEvents.forEach(function (e) {
                        domEvent.unbind(node, e, stopEvent, true);
                    });
                }, 500);
            }

            domEvent.bind(node, 'touchstart', stopMouse, true);
            domEvent.bind(node, 'touchend', allowMouse, true);
            domEvent.bind(node, 'touchcancel', allowMouse, true);

            // A touch event recognizer that handles
            // touch events only (we know, we can already handle
            // mouse events out of the box)

            var recognizer = new Hammer.Manager(node, {
                inputClass: Hammer.TouchInput,
                recognizers: [],
                domEvents: true
            });


            var tap = new Hammer.Tap();
            var pan = new Hammer.Pan({ threshold: 10 });
            var press = new Hammer.Press();
            var pinch = new Hammer.Pinch();

            var doubleTap = new Hammer.Tap({ event: 'doubletap', taps: 2 });

            pinch.requireFailure(pan);
            pinch.requireFailure(press);

            recognizer.add([pan, press, pinch, doubleTap, tap]);

            recognizer.reset = function (force) {
                var recognizers = this.recognizers,
                    session = this.session;

                if (session.stopped) {
                    return;
                }

                log('recognizer', 'stop');

                recognizer.stop(force);

                setTimeout(function () {
                    var i, r;

                    log('recognizer', 'reset');
                    for (i = 0; (r = recognizers[i]); i++) {
                        r.reset();
                        r.state = 8; // FAILED STATE
                    }

                    session.curRecognizer = null;
                }, 0);
            };

            recognizer.on('hammer.input', function (event) {
                if (event.srcEvent.defaultPrevented) {
                    recognizer.reset(true);
                }
            });

            return recognizer;
        }

        // touch recognizer
        var padRecognizer = createTouchRecognizer(container);

        padRecognizer.on('panstart', function (event) {
            event.target = event.target.closest('.entry')
            palette.trigger('dragstart', event, true);
        });

        padRecognizer.on('press', function (event) {
            palette.trigger('dragstart', event, true);
        });

        padRecognizer.on('tap', function (event) {
            palette.trigger('click', event);
        });
    }
}
const behaviorDiagram = new BehaviorDiagram()
export default behaviorDiagram;