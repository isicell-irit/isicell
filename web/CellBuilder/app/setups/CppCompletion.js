const FUNCTION = 0
const ENUM = 13
const FIELD = 3
const CONSTRUCTOR= 2
const CLASS = 5
const ENUM_MEMBER = 16
const VARIABLE = 4


function generateDocModule(files,supp={}){

		let configJson = files['config.json']||{}
		if(!(configJson instanceof Object)) configJson = JSON.parse(configJson)
		return {
			'Types':Object.assign(...Object.entries(files).filter(([file,_])=>/\.[hc]pp|\.h/.test(file)).map(([_,content]) =>{
					return getDocFromFile(content).typing
				})),
			'cellFunctionTemplate':configJson['cellFunctionTemplate']||[],
			'scenarioFunctionTemplate':configJson['scenarioFunctionTemplate']||[],
			'cellFunctions':configJson['cellFunction']||[],
			'scenarioFunction':configJson['scenarioFunction']||[],
			...supp
		}
}

const convertType=(type)=>{
	if(type==='struct' || type==='class'){
		return CLASS
	} else if(type==='constructor'){
		return CONSTRUCTOR
	} else if(type==='function'){
		return FUNCTION
	} else if(type==='namespace'){
		return ENUM
	}
}
function getDocFromFile(str,withNamespace=false,cursorPosition=-1,withPrivate=false) {
  const isHook = f=>['beginUpdate','preBehaviorUpdate','onAddCell','preDeleteDeadCellsUpdate','postBehaviorUpdate','endUpdate','onCellPluginLinking'].reduce((a,v)=>a||f.includes(v),false)

  let iterator = new RegExp(/[^;\}]*?(\{|\/\*|private[^\S\r\n]*:|public[^\S\r\n]*:|protected[^\S\r\n]*:|\}|\*\/|\/\/.*|;)/, "g"),
  bracketCount, match, capture, comment;
  let current = { members: [] }
  bracketCount = 0;
  let cursorContentContext = undefined
  comment = {isOpen:false}
  const typing = {}
  let ignoreNext = false;
  let lastIsMember = false;
  let res;
  try{
    while (match = iterator.exec(str)) {
      const token = match[1]
	  //console.log(token)
        if(token.startsWith('//')){
			ignoreNext = /^\/\/\/\s*@ignore/.test(token)
        } else if (token === '*/' && comment.isOpen) {
            comment.isOpen = false
            comment.content = str.slice(comment.matchStartIndex, iterator.lastIndex-2)
			if(lastIsMember && current?.members?.length > 0){
				current.members[current.members.length-1].doc = comment.content.replace(/[^\S\r\n]*[*!][^\S\r\n]*|@brief|\\brief/g, '')
				comment = {isOpen:false}
			}
        } else if(comment.isOpen){
        
        }else if(token === '/*'){
          comment = {isOpen:true, matchStartIndex: iterator.lastIndex, parent:current};
        }  else if (current.type === 'class' && /^private|^protected/.test(token)) {
          current.isPublic = false
        } else if (current.type === 'class' && /^public/.test(token)) {
          current.isPublic = true
        } else if (token==='}') {
          comment = {isOpen:false}
          bracketCount--;
          if (bracketCount === current?.bracketNum) {
            current.matchEndIndex = iterator.lastIndex
			current.kind = convertType(current.type)
				//const header = `${current.returnType||''} ${current.name}(${current.parameters||''}) {`
				//console.log(current.matchStartIndex, '<', cursorPosition, '<', current.matchEndIndex,header)
			if((current.matchStartIndex < cursorPosition && cursorPosition < current.matchEndIndex) && !cursorContentContext && (current.kind===FUNCTION||current.kind===CONSTRUCTOR)){
				//const header = `${current.returnType||''} ${current.name}(${current.parameters||''}) {`
				cursorContentContext = {content: str.slice(current.matchStartIndex, match.index),parent:current?.parent?.name,current, relativePosition:cursorPosition-current.matchStartIndex}
			}
            //current.content = str.slice(current.matchStartIndex, match.index);
            const parent = current.parent
            delete current.parent
			delete current.bracketNum
            if(!withPrivate) delete current.isPublic
			delete current.matchStartIndex
			delete current.matchEndIndex
			//if(!current.doc) delete current.doc
			if(current.type === 'class'|| current.type === 'struct' || current.type === 'namespace'){
                typing[current.name]=current
                if(withNamespace && parent.type === 'namespace') parent.members.push({kind:current.kind, name:current.name, returnType:current.name})
            } else if((parent.type === 'class'|| parent.type === 'struct' || parent.type === 'namespace') && bracketCount===parent.bracketNum+1) parent.members.push(current)
			delete current.type
            current = parent
          }
        } else if (token==='{') {
          if(current.type !== 'function' && !ignoreNext){
            capture = match[0].match(/(?<type>class|struct|namespace)\s+(?<name>\w+)\s*.*?\{/)?.groups
            if (capture) {
              if(capture.type === 'namespace') current = { type: capture.type, name: capture.name, matchStartIndex: match.index, bracketNum: bracketCount, parent: current,isPublic:true, members: [] }
              else current = { type: capture.type, name: capture.name, matchStartIndex: match.index, bracketNum: bracketCount, parent: current,isPublic:(capture.type==='struct'), members: [] }
            } else if ((current.type === 'class' && (current.isPublic||withPrivate) ) || current.type === 'struct' || (withNamespace && current.type === 'namespace')) {
			  const prototype = match[0].match(/\s*(template\<.*\>)?\s*(inline |const |static |explicit |\s)*(?<type>[^=()&]*?\*?)&?\s*(?<functionName>[\w_\d]+?) *\((?<params>[\s\S]*?)\)\s*(\:[\s\S]*)?(const)?\s*\{/)?.groups
			  let doc;
              if(prototype){
				doc = ((comment.parent===current&&comment?.content)?(comment.content.replace(/[^\S\r\n]*[*!][^\S\r\n]*|@brief|\\brief/g, '').trim()||''):'')//|@param.*|@return.*|\s{2}
				comment = {isOpen:false}
				if(prototype?.type) prototype.type = prototype.type.trim()
			  }
			  if(prototype && !prototype?.type && prototype?.functionName === current.name){
                current = { type: 'constructor', name:prototype.functionName.trim(),parameters:prototype.params.replace(/\s+/g,' ').trim(), doc, matchStartIndex: match.index, bracketNum: bracketCount, parent: current }
			  } else if(prototype && !isHook(prototype.functionName) && !/operator\s*.\s*\(/.test(match[0])){
                current = { type: 'function', name:prototype.functionName.trim(),returnType:prototype?.type?.replaceAll(' ',''),parameters:prototype.params.replace(/\s+/g,' ').trim(), doc ,matchStartIndex: match.index, bracketNum: bracketCount, parent: current }
              }
            }
          } else if(ignoreNext) ignoreNext=false
          bracketCount++;
        } else if(token===';' && current.bracketNum===bracketCount-1 && ((current.type === 'class' && (current.isPublic||withPrivate) ) || current.type === 'struct' || (withNamespace && current.type === 'namespace')) && !ignoreNext){
            if(!/\w+\(\)\s*=\s*default\s*;$/.test(match[0].trim()) && !/operator\s*..?\s*\(/.test(match[0])){
				const doc = ((comment.parent===current&&comment?.content)?(comment.content.replace(/[^\S\r\n]*[*!][^\S\r\n]*|@brief|\\brief/g, '').trim()||''):'')//|@param.*|@return.*|\s{2}
				comment = {isOpen:false}
				const prototype = match[0].match(/\s*(inline |const |static |explicit |\s)*(?<type>\w[^=]*[\s\*])\s*(?<variables>\w[^\=<>]*)(\=.*)?;/)
				const returnType = (prototype?.groups?.type||'').trim()
                if(returnType !== 'using')
				prototype?.groups?.variables?.split(/ *, */)?.forEach(v=>{
                    const attr = {kind:FIELD, name:v.trim(), returnType:returnType.replaceAll(' ',''), doc}
                    if(withPrivate) attr.isPublic = current.isPublic
					current.members.push(attr)//, matchStartIndex: iterator.lastIndex })
					lastIsMember = true
				})
            }
        } else if(ignoreNext) ignoreNext=false;
		else lastIsMember = false
        /* else {
			console.log("token uncatch",current,current.bracketNum,bracketCount, ((current.type === 'class' && (current.isPublic||withPrivate) ) || current.type === 'struct'),match[0])
		}*/
    }
	res = {typing,cursorContentContext}
  }catch(e){
	console.error(e);
	res = {typing,cursorContentContext}
  }
  /*
  if(bracketCount>0){
    res = {'error': 'error: too many "{" in file '+fileName }
  } else if(bracketCount<0){
    res = {'error': 'error: too many "}" in file '+fileName }
  }*/
  return res
}

function purExpr(code) {
    let simplified = "";
    let dP = 0;
    let dB = 0;
    let dA = 0;
	let prev = ""
    for (let i = code.length - 1; i >= 0; i--) {
      const char = code[i];
      if (char === '(' && dP==0) break;
      else if (char === '[' && dB==0) break;
      else if (char === '{' && dA==0) break;
      else if (dB===0 && dP===0 && dA===0 && (/[=;+/*!<&|]/.test(char)||(char==='-'&&prev!=='>'))) break;
      else if (char === '(') dP--;
      else if (char === '[') dB--;
      else if (char === '}') dA--;
      if (dP === 0 && dB === 0 && dA === 0) simplified = char + simplified;
      if (char === ')') dP++;
      else if (char === ']') dB++;
      else if (char === '}') dA++;
	  prev = char
    }
    return simplified;
  }

function resolveLocalExpressionType(expr, contextVariables, contextTypes,prev=false) {
    // On gère ::, ->, . et [ ] dans le tokenizer
    let tokens = expr.match(/->|\.|::|\[|\]|[\w]+/g) || [];
    if (!tokens.length) return null;

    // Premier token => variable locale ou type enum/namespace
    let currentType = contextVariables[tokens[0]] || null;
    if (!currentType && contextTypes[tokens[0]]) {
        currentType = tokens[0]; // ex: "State"
    }
    let i = 1, j = 1;
	if(prev){
		 while(/->|\.|::/.test(tokens[tokens.length-j-1])) j++;
		 tokens = tokens.slice(0,tokens.length-j)
	}
    while (i < tokens.length && currentType) {
        const token = tokens[i++];
        let memberName = tokens[i++];
        const baseType = currentType.replace(/\*|const|\<.*\>|\s*/g, "").trim();
        const typeDef  = contextTypes[baseType];
		const isPointer = currentType.endsWith("*")
		//console.log(token,memberName,currentType,baseType,typeDef,isPointer)
		if(!typeDef || !typeDef.members) return null
		else if((token==='::' && typeDef.kind !== ENUM)) return null
		else if(token === "->" && !isPointer) return null
		else if(token === "." && isPointer) return null
		else if(token==='['){
            currentType = extractElementTypeOnce(currentType);
            i++;
            if (i < tokens.length && tokens[i] === "]") i++;
        } else {
		if(!memberName) return currentType;
		//const isFunction = memberName.endsWith('(')
		//if(isFunction) memberName = memberName.slice(0,memberName.length-1)
        const member = typeDef.members.find(m => m.name === memberName);
		if (!member) return null;
		//if((member.kind === FUNCTION || member.kind === CONSTRUCTOR) && !isFunction) return null
        currentType = member.returnType;	
		}	
    }
    return currentType;
}


function parseCppVariablesWithContext(code, contextVariables, contextTypes) {
    // Supprime les commentaires
    code = code
        .replace(/\/\/.*$/gm, "")
        .replace(/\/\*[\s\S]*?\*\//g, "");

    // Regex pour déclarations, capturant une éventuelle initialisation
    const regexDeclarations = /(\b[\w:]+(?:<.*?>)?(?:\*|\s)?)\s+([\w,\s]+)(?:\s*=\s*([^;]+))?;/g;
    let match;
    while ((match = regexDeclarations.exec(code)) !== null) {
        const [_, typeDeclared, names, initExpr] = match;
        const varNames = names.split(",").map(n => n.trim());

        for (let varName of varNames) {
            let finalType = typeDeclared.trim();
            // Si c'est 'auto' et qu'on a une initExpr => on tente de résoudre le type
            if ((finalType === "auto" || finalType === "auto*") && initExpr) {
                const resolvedType = resolveLocalExpressionType(initExpr.trim(), contextVariables, contextTypes);
                if (resolvedType) {
                    finalType = (finalType === "auto*") ? (resolvedType + "*") : resolvedType;
                }
            }
            contextVariables[varName] = finalType;
        }
    }

    // Regex pour for-range "for(auto x : expr)"
    const regexForLoops = /for\s*\((auto(?:\*|\s)?)\s+([\w]+)\s*:\s*([\w\.\->\(\)\[\]]+)\)/g;
    while ((match = regexForLoops.exec(code)) !== null) {
        const [_, autoType, name, expression] = match;
        const resolvedType = resolveLocalExpressionType(expression, contextVariables, contextTypes);
        const elementType = extractElementTypeOnce(resolvedType);
        const finalType = elementType || autoType.trim();
        contextVariables[name.trim()] = finalType;
    }

    return contextVariables;
}
function extractElementTypeOnce(type) {
    if (!type) return null;
    const match = type.match(/^std::vector<(.*)>$/);
    if (match) {
        return match[1].trim();
    }
    return type;
}
function splitLeveled(str,split=',',opener=['{','[','('],closer=['}',']',')']) {
	const closer2 = closer.map(c=>'\\'+c)
	const opener2 = opener.map(c=>'\\'+c)
	let iterator = new RegExp(`([^${closer2.join('')}]*?)(${closer2.join('|')}|${opener2.join('|')}|${split})`, "g")
	let match, current = '', lastIndex=0;
	let count = new Array(opener.length).fill(0);
	const counter = {}
	opener.forEach((c,i)=>counter[c]=(()=>count[i]++))
	closer.forEach((c,i)=>counter[c]=(()=>count[i]--))
	const res = []
	while (match = iterator.exec(str)) {
		const [_,txt,token] = match
		if(token===split && count.reduce((a,c)=>a+c,0)===0){
			res.push(current+txt); current = ""
		} else if(token!==split) {
			counter[token](); current += txt
		} else current += txt+split
		lastIndex = iterator.lastIndex
	}
	res.push(current+str.substring(lastIndex))
	return res
}

function getCompletion(model,position,isSuggestions, contextTypes, contextVariables = {}, template='',thisHasRoot=true){

		const iword = model.getWordAtPosition(position)
		const endColumn = isSuggestions?position.column: iword?.endColumn || position.column
		const word = iword?.word || ''
		const range = { startLineNumber: 0, endLineNumber: position.lineNumber, startColumn: 0, endColumn: endColumn };
		let cursorPosition = model.getValueInRange(range).length
		let codeContext = model.getValue()
		if(template){
			codeContext = template.replace('$code',codeContext)
			cursorPosition+=template.indexOf('$code')
		}
        let res = getDocFromFile(codeContext,false,cursorPosition,true)

		if(template) contextTypes = Object.assign(res.typing,contextTypes)
		else contextTypes = Object.assign(contextTypes,res.typing)
		res = res.cursorContentContext
		if(!res) return
		const localContext = res?.content?.substring(0,res?.relativePosition) || ''
		if(res?.parent && contextTypes[res.parent]){
			if(thisHasRoot) contextVariables = Object.assign(contextVariables,Object.fromEntries(contextTypes[res.parent].members.filter(m=>m.kind===FIELD).map(m=>[m.name,m.returnType])))
			contextVariables['this']=res.parent+'*'
		}
		//console.log(res)
		if(res.current?.parameters){
			try{
				let params = splitLeveled(res.current.parameters)
				params = Object.fromEntries(params.map(p=>splitLeveled(p,'=')[0].trim().match(/^(.*[\s\*])(\w+)$/)).map(p=>[p[2],p[1].trim()]))
				contextVariables = Object.assign(params,contextVariables)
			}catch(e){}
		}
		//console.log(params)
		contextVariables = parseCppVariablesWithContext(localContext,contextVariables,contextTypes)
		//console.log(localContext,contextVariables)
		let baseExpr = model.getLineContent(position.lineNumber).substring(0, endColumn - 1);;
		//baseExpr = baseExpr.match(/(?:.*[=;:](?!:))?\s*([^]*)$/)?.[1].trim() || baseExpr
		//console.log(baseExpr)
		baseExpr = purExpr(baseExpr).trim();
		if(!baseExpr || !/::|\.|->/.test(baseExpr)){
			//console.log('ok',word,isSuggestions)
			if(!isSuggestions) {
				const contents = Object.entries(contextVariables).filter(([v,t]) => v===word).map(([v,t]) => ({
					value:"```cpp\n"+`${t} ${v}`+"\n```"
				}))
				//console.log(contents)
				return { contents };
			} else {
				const suggestions = Object.entries(contextVariables).map(([v,t]) => ({
					label: v,
					kind: VARIABLE,
					detail:t,
					insertText: v
				}));
				return { suggestions };
			}
		}
		//console.log(baseExpr, contextVariables, contextTypes)
		const baseType = resolveLocalExpressionType(baseExpr, contextVariables, contextTypes,true);
		//console.log(baseType)
		if (!baseType) return;
		let typeDefName = baseType.replace(/\*|const|\<.*\>|\s*/g, "").trim();
		let typeDef;
		if (typeDefName.includes('::')){
			typeDefName = resolveLocalExpressionType(baseType+'.', contextVariables, contextTypes,true);
			typeDef = contextTypes[typeDefName];
		} else {
			typeDef = contextTypes[typeDefName];
		}
		//console.log(typeDef,isSuggestions)
		if (!typeDef?.members) return;
		if(isSuggestions){
			const suggestions = typeDef.members.filter(m=>m.kind!==CONSTRUCTOR).map(m => ({
				label: m.name+(m.parameters!==undefined?'('+m.parameters+')':''),
				kind: m.kind,
				insertText: m.name+(m.parameters!==undefined?'()':''),
				documentation: {value:"```cpp\n"+`${m.returnType||''} ${m.name+(m.parameters!==undefined?'('+m.parameters+')':'')}`+"\n```\n"+m.doc},
				detail:m.returnType
			}));
			return { suggestions };
		} else {
			//console.log(word,typeDef.members)
			const contents = typeDef.members.filter(m=>m.name===word).map(m => ({
				value:"```cpp\n"+`${m.returnType||''} ${m.name+(m.parameters!==undefined?'('+m.parameters+')':'')}`+"\n```\n"+m.doc
			}));
			//console.log(contents)
			return { contents };
		}
}

module.exports = {
	generateDocModule,
	getDocFromFile,
	getCompletion,

}
/*
// Hover on each property to see its docs!
const container1 = document.createElement('div')
container1.style.height = "50%"
const container2 = document.createElement('div')
container2.style.height = "50%"
document.getElementById("container").append(container1)
document.getElementById("container").append(container2)

const myEditor = monaco.editor.create(container1, {
	value,
	language: "cpp",
	automaticLayout: true,
	theme: "vs-dark",
});
const myEditor2 = monaco.editor.create(container2, {
	value,
	language: "json",
	automaticLayout: true,
	theme: "vs-dark",
});


const template = ''//"class Cell {public:\n$code\n}"


monaco.languages.registerCompletionItemProvider("cpp", {
    triggerCharacters: ['.', '>', ':'],
	provideCompletionItems: (model, position,context) => {
		if(['>', ':'].includes(context.triggerCharacter)) {
			const token = model.getValueInRange({startLineNumber: position.lineNumber, endLineNumber: position.lineNumber, startColumn: position.column-2, endColumn: position.column})
			if(token!=='::'  && token!=='->') return
		}
		return getCompletion(model,position,true,{},{},template)
	}
})
monaco.languages.registerHoverProvider('cpp', {
	provideHover: (model, position) => {
		return getCompletion(model,position,false,{},{},template)
	}
})*/