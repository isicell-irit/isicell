import PanelManager from "../guiComponent/panelManager";

function setupPanelManager(){
	//* Creates the parent panel "mainSplitter" in the "mainContent" id html element
	PanelManager.createSplitterContainer('mainContent','mainSplitter',true);
	const panelManager = new PanelManager("mainSplitter",true);

	//* Creates the four main panels
	

	let managerContainer = panelManager.createPanel("managerContainer");
	managerContainer.classList.add("manager-container")
	let globalListDom = document.createElement('custom-list')
	globalListDom.id = 'globalList'
	managerContainer.appendChild(globalListDom)

	panelManager.createPanel("contentView");
	panelManager.setWidthPanel([250, 100], [true, false])


	PanelManager.createSplitterContainer('contentView', 'visualProgrammingView', true);
	const panelManagerContent = new PanelManager("visualProgrammingView", true);

	panelManagerContent.createPanel("containerState");
	panelManagerContent.createPanel("containerBehavior");
	panelManagerContent.createPanel("panelLocalList");

	panelManagerContent.setWidthPanel([50,50,250],[false,false,true])

	panelManagerContent.minimize(['panelLocalList','containerBehavior'],-1)

	return { panelManager, panelManagerContent};
}
const { panelManager, panelManagerContent} = setupPanelManager();

export { panelManager, panelManagerContent}