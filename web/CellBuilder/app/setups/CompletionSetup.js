import stateMachine from "../diagrams/stateMachine"
import CodeEditorProvider from "../guiComponent/codeEditorProvider"
import CppTypeManager from "../manager/CppTypeManager"
import customModulesManager from "../manager/customModulesManager"
import FunctionManager from "../manager/functionManager"
import moduleManager from "../manager/moduleManager"
import { firstLetterToLower } from "../utils/misc"

const FUNCTION = 0
const ENUM = 13
const FIELD = 3
const CONSTRUCTOR= 2
const CLASS = 5
const ENUM_MEMBER = 16

const PluginsTypes = {'standard': [], 'custom': []}
const BodiesMethods = {'standard': [], 'custom': []}
const CellMethodsFromModule = {'standard': [], 'custom': []}
const ScenarioMethodsFromModule = {'standard': [], 'custom': []}

export default function setupCompletion(defaultData) {

	function wrapperFunctionInfo(members){
		return members.map((i) => { 
		  const info = FunctionManager.extractHeaderCppFunction(i["prototype"])
		  if(info)
		  return {
			kind: FUNCTION,
			label: info.functionName + i['prototype'].split(info.functionName)[1],
			insertText: info.functionName + '()',
			detail: info.returnType,
			documentation: i.description?.trim()?.replaceAll("\n", "\n\n")
		}
	  }).filter((f) => f)
	}

	
	const cellsMethods = defaultData.Default.Type.PrimoCell.members
	const scenarioMethods = defaultData.Default.Type.Scenario.members
	const defaultTypes = {...defaultData.Default.Type}
	defaultTypes['MecaCell'].members.push({ kind: CLASS, name: 'Vec', returnType: 'Vector3D' })
	delete defaultTypes.PrimoCell
	delete defaultTypes.Scenario
	

	moduleManager.addUpdater((modules)=>{
		PluginsTypes['standard'] = []
		BodiesMethods['standard'] = []

		CellMethodsFromModule['standard'] = []
		ScenarioMethodsFromModule['standard'] = []

		modules.forEach((mod)=>{
			if(mod.hasOwnProperty('cellFunctions')) CellMethodsFromModule['standard'].push(...wrapperFunctionInfo(mod["cellFunctions"]))
			if(mod.hasOwnProperty('scenarioFunction')) CellMethodsFromModule['standard'].push(...wrapperFunctionInfo(mod["scenarioFunction"]))
			Object.entries(mod.Types).forEach(([type, info]) => {
				if(type.startsWith('Body')) BodiesMethods['standard'].push(...info.members)
				else PluginsTypes['standard'][type] = info
			})
		})
	})

	customModulesManager.addUpdater((modules)=>{
		PluginsTypes['custom'] = []
		BodiesMethods['custom'] = []
		CellMethodsFromModule['custom'] = []
		ScenarioMethodsFromModule['custom'] = []

		Object.values(modules).forEach((mod)=>{
			mod = Object.fromEntries(Object.entries(mod).map(([name,file])=>[name,file.code]))
			if(mod.hasOwnProperty('config.json')){
				const config = mod['config.json']
				if(config.hasOwnProperty('cellFunctions')) CellMethodsFromModule['custom'].push(...wrapperFunctionInfo(config["cellFunctions"]))
				if(config.hasOwnProperty('scenarioFunction')) CellMethodsFromModule['custom'].push(...wrapperFunctionInfo(config["scenarioFunction"]))
			} else {
				Object.values(mod).forEach((file)=>{
					Object.entries(file.Types).forEach(([type, info]) => {
						if(type.startsWith('Body')) BodiesMethods['custom'].push(...info.members)
						else PluginsTypes['custom'][type] = info
					})
				})
			}
		})
	})

	const GenType = () => {
		const mergedPuginsTypes = Object.assign(PluginsTypes['standard'], PluginsTypes['custom'])
		const mergedBodiesMethods = BodiesMethods['standard'].concat(BodiesMethods['custom'])
		let contextTypes = Object.assign(defaultTypes, mergedPuginsTypes,{
			State: {
				kind: ENUM,
				members: stateMachine.getStates().map((state) => {
					return {
						name: state.name,
						returnType: 'State::' + state.name,
						kind: ENUM_MEMBER,
						doc: "created by the user, in ISiCell interface"
					}
				})
			},
			Type: {
				kind: ENUM,
				members: document.getElementById('Cell Types').items.map((item) => {
					return {
						name: item.valueText,
						returnType: 'Type::' + item.valueText,
						kind: ENUM_MEMBER,
						doc: "created by the user, in ISiCell interface"
					}
				})
			},
			Body: {
				kind: CLASS,
				members: mergedBodiesMethods
			},
			Cell: {
				kind: CLASS,
				members: [{
					kind: FUNCTION,
					name: 'getBody',
					parameters: '',
					returnType: 'Body'
				}, ...CellMethodsFromModule['standard'], ...CellMethodsFromModule['custom'],...cellsMethods,
				 ...document.getElementById('Cell Attributes').items.filter(i=>!i.hasTag('CORE')).map((item) => {
					return {
						kind: FIELD,
						name: item.valueText,
						returnType: item.type,
						doc: "created by the user, in ISiCell interface"
					}
				}),
				]
			},
			Scenario: {
				kind: CLASS,
				members: [...ScenarioMethodsFromModule['standard'], ...ScenarioMethodsFromModule['custom'],...scenarioMethods,
				...document.getElementById('Scenario Attributes').items.filter(i=>!i.hasTag('CORE')).map((item) => {
					return {
						kind: FIELD,
						name: item.valueText,
						returnType: item.type,
						doc: "created by the user, in ISiCell interface"
					}
				})
				]
			},
			cellPlugin_t: {
				kind: CLASS,
				members: Object.keys(mergedPuginsTypes).filter(t=>t.startsWith('Plugin')).map((plugin) => {
					const accessPlugin = firstLetterToLower(plugin)
					return {
						kind: CLASS,
						name: accessPlugin,
						returnType: plugin,
					}})
			}
		})
		Object.entries(CppTypeManager.getEnums()).forEach(([enumName, values]) => {
			contextTypes[enumName] = {
				kind: ENUM,
				members: values.map((value) => {
					return {
						name: value,
						returnType: enumName + '::' + value,
						kind: ENUM_MEMBER,
						doc: "created by the user, in ISiCell interface"
					}
				})
			}
		})
		return contextTypes
	}

	const getLocalVariables = () => {
		return Object.fromEntries(document.getElementById('Local Variables').items.map((item) => ([item.valueText, item.type])))
	}

	const commonVar = { std:"std", MecaCell:  "MecaCell" }
	const commonVarISiCell = {State: "State", Type: "Type", Rand: "Rand", TimeConvert: "TimeConvert"}
	CodeEditorProvider.addCompletionProvider('Cell', () => {
		return { variables: {...commonVarISiCell, ...commonVar, world:"World*" },
		variablesType: GenType(),
		template:"class Cell {public:\n$code\n}" }
	})
	CodeEditorProvider.addCompletionProvider('Cell_condition_state', () => {
		return { variables: { ...commonVarISiCell, ...commonVar, world:"World*" },
		variablesType: GenType(),
		template:"class Cell {public:\nvoid tmp(){\n$code\n}\n}" }
	})
	CodeEditorProvider.addCompletionProvider('Cell_condition', () => {
		return { variables: { ...commonVarISiCell, ...commonVar, world:"World*", ...getLocalVariables() },
		variablesType: GenType(),
		template:"class Cell {public:\nvoid tmp(){\n$code\n}\n}" }
	})
	CodeEditorProvider.addCompletionProvider('Scenario', () => {
		return { variables: {  ...commonVarISiCell, ...commonVar, world:"World" },
		variablesType: GenType(),
		template:"class Scenario {public:\n$code\n}" }
	})
	CodeEditorProvider.addCompletionProvider('Scenario_condition', () => {
		return { variables: { ...commonVarISiCell, ...commonVar, world:"World", ...getLocalVariables()  },
		variablesType: GenType(),
		template:"class Scenario {public:\nvoid tmp(){\n$code\n}\n}" }
	})
	CodeEditorProvider.addCompletionProvider('module', () => {
		const types = GenType()
		types['world_t'] = types['World']
		types['cell_t'] = types['Cell']
		return { variables: commonVar,
		variablesType: types,
		template:"",
		thisHasRoot: true }
	})
}