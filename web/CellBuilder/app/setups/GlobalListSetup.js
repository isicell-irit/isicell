import CppTypeManager from '../manager/CppTypeManager';
import FunctionManager from '../manager/functionManager'
import { regexVariableDeclarationSplit } from '../utils/misc';
import { getTitleCoreFonction, callBackCheckIntegrity } from '../utils/utilsVaraibleName';
import Parameters from '../manager/ParametersManager';

export default function setupGlobalList(defaultData){


  var globalList = document.getElementById('globalList')
  
  function createSection(id, title, sectionOptions = {}, ItemOptions = {}, CoreItems) {
    ItemOptions.taggable = true
    if (ItemOptions.type && (sectionOptions.addable || ItemOptions.editable)) ItemOptions['typeProvider'] = () =>CppTypeManager.getAllTypes()
    const itemBuilder = () => document.createElement('custom-item').applyOptions(ItemOptions)
    if (sectionOptions.addable) sectionOptions.addable = itemBuilder
    const section = document.createElement('custom-section').applyOptions(sectionOptions);
    section.itemBuilder = itemBuilder
    section._disabledSorting = true
    let ItemOptionsCore = { ...ItemOptions}
    delete ItemOptionsCore.deletable
    delete ItemOptionsCore.editable
    CoreItems.forEach(item => {
      const i = document.createElement('custom-item')
      i.valueText = item.kind===3?item.name:item.name+'('+(item.parameters||'')+')'
      i.applyOptions(ItemOptionsCore)
      i.deletable = false
      if (item.returnType) i.type = item.returnType
      if (item.doc) i.title = item.doc
      i.addTag('CORE')
      section.addItem(i)
    });
    delete section._disabledSorting
    section.sort()
    section.titleText = title
    section.classList.add('title')
    section.id = id
    globalList.appendChild(section)
    section.updateSize()
    return section
  }


  var cbCell = (item, prevVal) => { const newVal = callBackCheckIntegrity(item, prevVal); FunctionManager.refactorCode(item.valueText, newVal, 'cellFunction'); return newVal }
  var cbScenario = (item, prevVal) => { const newVal = callBackCheckIntegrity(item, prevVal); FunctionManager.refactorCode(item.valueText, newVal, 'scenarioFunction'); return newVal }

  let sec;
  createSection("Cell Types", 'Cell Types', { addable: true, toggleable: true }, { editable: () => { Parameters.changeDetected() }, deletable: true, intergrityCheck:cbCell }, [])
 
  createSection("Cell Attributes", 'Cell Attributes', { addable: true, toggleable: true }, { ioable: true, editable: () => { Parameters.changeDetected()}, deletable: true, type: 'int', intergrityCheck: cbCell, completion_prefix:'this->' },
    defaultData.Default.Type.PrimoCell.members.filter(m=>m.kind===3))
  sec = createSection("Scenario Attributes", 'Protocol Attributes', { addable: true, toggleable: true }, { noviewTag:true,ioable: true, editable: () => { Parameters.changeDetected() }, deletable: true, type: 'int', intergrityCheck: cbScenario },
    defaultData.Default.Type.Scenario.members.filter(m=>m.kind===3))
  sec.style.display = 'none'
  
  createSection("Bodies Methods",'Module Cell Methods', { toggleable: true }, { }, [])
  sec = createSection("Plugins Methods",'Module Protocol Methods', { toggleable: true }, { }, [])
  sec.style.display = 'none'

  createSection("Cell Methods",'Cell Methods', { toggleable: true }, { }, defaultData.Default.Type.PrimoCell.members.filter(m=>m.kind===0))
  sec = createSection("Scenario Methods",'Protocol Methods', { toggleable: true }, { }, defaultData.Default.Type.Scenario.members.filter(m=>m.kind===0))
  sec.style.display = 'none'

} 

