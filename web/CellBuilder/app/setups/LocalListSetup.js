import CppTypeManager from '../manager/CppTypeManager';
import { getTitleCoreFonction, callBackCheckIntegrity } from '../utils/utilsVaraibleName';
import FunctionManager from '../manager/functionManager'
import { utf8_to_b64 } from '../utils/misc';
import behaviorDiagram from '../diagrams/behaviorDiagram';



export default function setupLocalList(defaultData){
  let container = document.getElementById('panelLocalList')
  let localList = document.createElement('custom-list')
  localList.id = 'localList'
  container.appendChild(localList)

  function addDomSpecDraggable(item,cat) {
    item.dataset.action = 'create.plugin-function'
    item.classList.add('entry')
    item.dataset.cat = cat
    item.draggable = true
    return item
  }

  function createSection(id, title, sectionOptions = {}, ItemOptions = {}, CoreItems) {
    ItemOptions.taggable = true
    if (ItemOptions.type && (sectionOptions.addable || ItemOptions.editable) && ItemOptions.type !== 'void') ItemOptions['typeProvider'] = () => ['int', 'double']//CppTypeManager.getAllTypes() TODO
    else if (ItemOptions.type === 'void') ItemOptions['typeProvider'] = () => ['void', ...CppTypeManager.getAllTypes()]
    let itemBuilder;
    let cat;
    if (id == 'Cell functions' || id == 'Default functions cell') cat = 'cellFunction'
    if (id == 'Scenario functions' || id == 'Default functions scenario') cat = 'scenarioFunction'
    if (ItemOptions.draggable) itemBuilder = () => addDomSpecDraggable(document.createElement('custom-item').applyOptions(ItemOptions),cat)
    else itemBuilder = () => document.createElement('custom-item').applyOptions(ItemOptions)
    
    if (typeof ItemOptions.addable === 'function'){ sectionOptions.addable = () => { const item = itemBuilder(); item.callBackEdit = ItemOptions.addable; return item}
    } else if (sectionOptions.addable) sectionOptions.addable = itemBuilder
    const section = document.createElement('custom-section').applyOptions(sectionOptions);
    if (!sectionOptions.addable) section.itemBuilder = itemBuilder
    section._disabledSorting = true
    let ItemOptionsCore = { ...ItemOptions }
    delete ItemOptionsCore.deletable
    delete ItemOptionsCore.editable
    CoreItems.forEach(item => {
      const i = itemBuilder()
      i.deletable = false
      if (item.type) i.type = item.type
      if (item.title) i.title = item.title
      if (item.completion_prefix) i.completion_prefix = item.completion_prefix
      if (item.idFunc) i.setAttribute('data-idFunc', FunctionManager.addOrUpdateFunction(cat, item.idFunc, item.type + ' ' + item.value + ' {\n\n}'))
      if(item.template) i.dataset.template = item.template
      i.valueTextNoIntegrityCheck = item.value
      i.applyOptions(ItemOptionsCore)
      i.addTag('CORE')
      section.addItem(i)
    });
    delete section._disabledSorting
    section.sort()
    section.titleText = title
    section.classList.add('title')
    section.id = id
    localList.appendChild(section)
    section.updateSize()
    return section
  }


  var intergrityCheckCell = (item, prevVal) => { const newVal = callBackCheckIntegrity(item, prevVal); FunctionManager.refactorCode(item.valueText, newVal, 'cellFunction'); return newVal }
  var intergrityCheckScenario = (item, prevVal) => { const newVal = callBackCheckIntegrity(item, prevVal); FunctionManager.refactorCode(item.valueText, newVal, 'scenarioFunction'); return newVal }

var  generateCallBack = (cat) => {
    return {
      addable: (item) => {
        console.trace(item.type, item.valueText,item._type,item._typeSelector)
        let newVal = item.valueText + '()'
        item.valueTextNoIntegrityCheck = newVal
        item.dataset.idfunc = FunctionManager.addOrUpdateFunction(cat, -1, item.type + ' ' + newVal + ' {\n\n}')
        item.dataset.name = newVal
        },
      deletable: (item) => {
        let idFunc = item.getAttribute('data-idFunc')
        let bpmnElem = behaviorDiagram.modeler.get('elementRegistry').filter((e) => (e.businessObject.idFunc === idFunc))
        if(bpmnElem)behaviorDiagram.modeling.removeElements(bpmnElem)
        FunctionManager.deleteFunction(cat, idFunc) }}
  }

  createSection('Local Variables','Local Variables',
  { toggleable: true, addable: true},
  { editable: true, deletable: true, type: 'int', completion_prefix: '' }, [])
  
  createSection('Cell functions', 'Cell functions', 
  { toggleable: true, addable: true  },
    Object.assign({ type: 'void', draggable: true, completion_prefix: 'this->', intergrityCheck: intergrityCheckCell }, generateCallBack('cellFunction')), [{ type: 'void', value: 'commonBehavior()',idFunc:'commonBehaviorFunction'}])

  createSection("Scenario functions",'Protocol functions', 
  { toggleable: true, addable: true },
    Object.assign({ type: 'void', draggable: true, completion_prefix: 'this->', intergrityCheck: intergrityCheckScenario  },generateCallBack('scenarioFunction')), [])

  function wrapperFunctionInfo(idx, completion_prefix) {
    return defaultData.Default[idx].map((i) => {
      let info = FunctionManager.extractHeaderCppFunction(i.code)
      return { type: info.returnType, value: info.functionName + '()', template: utf8_to_b64(i.code), title: getTitleCoreFonction(Object.assign({ completion_prefix: completion_prefix, description: i.description }, info)) }
    })
  }
  createSection('Default functions cell', "Template functions", { toggleable: true }, { type: 'int', completion_prefix: '', draggable: true }, wrapperFunctionInfo('cellFunctionTemplate', ''))
  createSection('Default functions scenario', "Template functions", { toggleable: true }, { type: 'int', completion_prefix: '', draggable: true }, wrapperFunctionInfo('scenarioFunctionTemplate', ''))

}