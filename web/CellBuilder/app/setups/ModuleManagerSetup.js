import { longToast } from '../guiComponent/notification';
import CustomModulesManager from '../manager/customModulesManager';
import FunctionManager from '../manager/functionManager'
import ModuleManager from '../manager/moduleManager'
import { regexVariableDeclarationSplit, utf8_to_b64, firstLetterToLower } from '../utils/misc';
import { getTitleCoreFonction } from '../utils/utilsVaraibleName';
import { generateDocModule } from './CppCompletion';


function wrapperFunctionInfo(json, completion_prefix) {
  return json.map((i) => {
    let info = FunctionManager.extractHeaderCppFunction(i.code || i["prototype"])
    if (info) return {header:{ type: info.returnType, valueTextNoIntegrityCheck: info.functionName + '()', title: getTitleCoreFonction(Object.assign({ completion_prefix: completion_prefix, description: i.description }, info))},code:i.code }
  })
}
function updateUIfromConfigJson(mod, from){
  let sec = document.getElementById("Default functions cell")
      
  if(mod.hasOwnProperty('cellFunctionTemplate'))
  wrapperFunctionInfo(mod['cellFunctionTemplate']).forEach(info => {if (info) {
    info.header[from] = true
    const item = sec.itemBuilder()
    item.dataset.template = utf8_to_b64(info.code)
    sec.addItem(item.applyOptions(info.header))
  }})

  sec = document.getElementById("Default functions scenario")
  wrapperFunctionInfo(mod['scenarioFunction']).forEach(info => {if (info) {
    info.header[from] = true
    const item = sec.itemBuilder()
    item.dataset.template = utf8_to_b64(info.code)
    sec.addItem(item.applyOptions(info.header))
  }})

  const secBodies = document.getElementById("Bodies Methods")
  const secPlugins = document.getElementById("Plugins Methods")
  Object.entries(mod.Types).forEach(([type, info]) => {
    if(type.startsWith('Body')){
      info.members.forEach((m) => {
        if(m.kind === 0 || m.kind === 3) {
          const item = secBodies.itemBuilder()
          const info = { 
            type: m.returnType,
            valueTextNoIntegrityCheck: m.name + (m.parameters!==undefined?'('+m.parameters+')':''),
            createdByModule: true,
            title: m.doc,
          }
          info[from] = true
          secBodies.addItem(item.applyOptions(info))
        }
      })
    } else if(type.startsWith('Plugin')){
      info.members.forEach((m) => {
        if(m.kind === 0 || m.kind === 3) {
          const item = secPlugins.itemBuilder()
          const info = { 
            type: m.returnType,
            valueTextNoIntegrityCheck: m.name + (m.parameters!==undefined?'('+m.parameters+')':''),
            createdByModule: true,
            title: m.doc,
          }
          info[from] = true
          secPlugins.addItem(item.applyOptions(info))
        }
      })
    }
  })

  sec  = document.getElementById("Cell Methods")
  if(mod.hasOwnProperty('cellFunctions')) {
    wrapperFunctionInfo(mod['cellFunctions'],'this->').forEach(info => { if(info){
      const item = sec.itemBuilder()
      info.header['completion_prefix'] = 'this->'
      sec.addItem(item.applyOptions(info.header))
    }})
  }

}

export default function setupModuleManager(){
  ModuleManager.addUpdater((modules)=>{
    let doDelete = item => {if(item.createdByModule) item.delete()};
    let catsModule = ["Default functions cell", "Default functions scenario", "Bodies Methods", "Plugins Methods", "Scenario Attributes"]
    
    catsModule.forEach(id => document.getElementById(id).items.forEach(doDelete))
    catsModule.forEach(id => document.getElementById(id)._disabledSorting = true)



    for(let mod of modules){
      updateUIfromConfigJson(mod, 'createdByModule')
    }
  catsModule.forEach(id => {let sec = document.getElementById(id); sec._disabledSorting = false; sec.sort()})
  })
  ModuleManager.updateSelectedModules() // Now update to setup everything




  CustomModulesManager.addUpdater((modules)=>{
    let doDelete = item => {if(item.createdByCustomModule) item.delete()};
    let catsModule = ["Default functions cell", "Default functions scenario", "Bodies Methods", "Plugins Methods", "Scenario Attributes"]
    
    catsModule.forEach(id => document.getElementById(id).items.forEach(doDelete))
    catsModule.forEach(id => document.getElementById(id)._disabledSorting = true)

    Object.entries(modules).forEach(([nameModule,mod])=>{
      mod = Object.fromEntries(Object.entries(mod).map(([name,file])=>[name,file.code]))
      updateUIfromConfigJson(generateDocModule(mod), 'createdByCustomModule')
    })
  catsModule.forEach(id => {let sec = document.getElementById(id); sec._disabledSorting = false; sec.sort()})
  })
}