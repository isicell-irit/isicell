import stateMachine from "../diagrams/stateMachine"
import ActivityManager from "../manager/ActivityManager"
import CppTypeManager from "../manager/CppTypeManager"
import CustomModulesManager from "../manager/customModulesManager"
import ExploreManager from "../manager/exploreManager"
import FunctionManager from "../manager/functionManager"
import ModuleManager from "../manager/moduleManager"
import Parameters from "../manager/ParametersManager"
import ViewerManager from "../viewer/ViewerManager"

export default  function clear(withoutBuild=false){
    stateMachine.loadIntialDiagram()
    let doDelete = item => { if (!item.hasTag('CORE')) item.delete() };
    [...document.getElementById('globalList').children].forEach(sec => sec.items.forEach(doDelete));
    [...document.getElementById('localList').children].forEach(sec => sec.items.forEach(doDelete));

  
    FunctionManager.clear()
    FunctionManager.addOrUpdateFunction("cellFunction", 'commonBehaviorFunction', 'void commonBehavior() {\n\n}')
    ModuleManager.unselectedAll()
    ModuleManager.updateSelectedModules()
    CppTypeManager.resetTypes()
    if(!withoutBuild)Parameters.clear()
    ExploreManager.clear()
    CustomModulesManager.clear()
    ViewerManager.reset()
    ActivityManager.toggleActivity('cells_behavior')

  document.getElementById('viewer').parentElement.style.display = 'none'
  document.getElementById('explore').parentElement.style.display = 'none'
  }

let catsModule = ["Default functions cell", "Default functions scenario", "Bodies Methods", "Plugins Methods", "Scenario Attributes"]

