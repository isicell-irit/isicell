import TextScramble from '../utils/textScramble'

export default class IdleScreen {
	constructor(idleTriggerTime=60,scramblePeriod=5) {
		var idleTime = 0;
		var idle = false;
		var idleInterval = setInterval(() => {
			if (idleTime++ >= idleTriggerTime && !idle)
				showIdleScreen();
		}, 60000); // every minute

		var idleTitle = document.getElementById('idle-title');
		idleTitle.innerHTML = spannify(idleTitle.innerHTML);
		function spannify(html) {
			var chars = html.split("");
			return '<span>' + chars.join('</span><span>') + '</span>';
		};

		//Wave
		var idleHeader = document.getElementById('idle-header');
		for (var i = 0; i < 2; i++)
			idleHeader.append(idleTitle.cloneNode(true));

		//Scramble
		const scramblePhrases = [
			'ISiCell',
			'The best platform!',
			'Such modeling',
			'Much agent-based',
			'Very biologic',
			'Incredible idle screen',
			'Wow',
			'......',
			'Why are you still here?',
			'You should start working now',
			'I know it\'s great here',
			'But go to work',
			'You can do it!',
			'I\'m sure your model will turn out great'
		];
		var textScramble = new TextScramble(idleHeader);
		var idleWordCounter = 0;
		var startedScrambling = false;
		const nextScramble = () => {
			if (idle) {
				startedScrambling = true;
				var nextWord = scramblePhrases[idleWordCounter];
				textScramble.setText(nextWord).then(() => {
					setTimeout(nextScramble, scramblePeriod*1000); // 5 sec
				});
				idleWordCounter = (idleWordCounter + 1) % scramblePhrases.length;
			}
		};

		// Waking up
		function wakeUp(e) {
			idleTime = 0;
			hideIdleScreen();
			return true;
		}
		window.onmousemove = wakeUp;
		window.onkeypress = wakeUp;
		window.onmousedown = wakeUp;
		function showIdleScreen() {
			document.getElementById('idle-screen').style.display = "block";
			idle = true;
			if (!startedScrambling) {
				idleWordCounter = 0;
				nextScramble();
			}
		}
		function hideIdleScreen() {
			document.getElementById('idle-screen').style.display = "none";
			idle = false;
			startedScrambling = false;
		}
		var kc = ['ArrowUp', 'ArrowUp', 'ArrowDown', 'ArrowDown', 'ArrowLeft', 'ArrowRight', 'ArrowLeft', 'ArrowRight', 'b', 'a'];
		var kcPos = 0;
		document.addEventListener('keydown', function (e) {
			var requiredKey = kc[kcPos];
			if (e.key == requiredKey) {
				kcPos++;
				if (kcPos == kc.length) {
					kcPos = 0;
					showIdleScreen();
				}
			}

			else
				kcPos = 0;
		});
	}
}