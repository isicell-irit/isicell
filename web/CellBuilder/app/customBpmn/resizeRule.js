import RuleProvider from 'diagram-js/lib/features/rules/RuleProvider';

import inherits from 'inherits';


export default function ResizeAllRules(eventBus) {
  RuleProvider.call(this, eventBus);
}

inherits(ResizeAllRules, RuleProvider);

ResizeAllRules.$inject = [ 'eventBus' ];


ResizeAllRules.prototype.init = function() {

    this.addRule('shape.resize', 1500, function(context) {
      //if(context.shape.type === "label" || context.shape.type === 'bpmn:Task'){
      if (context.shape.type !== 'bpmn:SequenceFlow' && context.shape.type !== 'bpmn:EndEvent' && context.shape.type !== 'bpmn:IntermediateCatchEvent' && context.shape.type !== 'bpmn:StartEvent' && context.shape.type !== 'bpmn:Gateway'){
          return true;
      }
});

};