
import KeyboardBindings from "diagram-js/lib/features/keyboard/KeyboardBindings";

export default class DisableKeyboardBinding extends KeyboardBindings {
    constructor(eventBus, injector, keyboard,selection,modeling) {
        super(eventBus, keyboard);
        injector.invoke(KeyboardBindings, this);
        this.selection = selection;
        this.modeling = modeling;
    }
    registerBindings(keyboard, editorActions) {
        keyboard.addListener(10000, context => {
            var elements = this.selection.get()
            //if (elements.length === 0) return true;
            const event = context.keyEvent;
            //if (keyboard.hasModifier(event)) return;
            if (keyboard.isKey(['Backspace', 'Delete', 'Del'], event))
                for (var e of elements) {
                    if (e.type === 'bpmn:IntermediateCatchEvent' || e.type === 'bpmn:StartEvent') return true;
                    else if (e.type === 'label' && (e.labelTarget.type === 'bpmn:IntermediateCatchEvent' || e.labelTarget.type === 'bpmn:StartEvent')) return true;
                    else if (e.type === 'label' && e.labelTarget.type === 'bpmn:EndEvent' && !elements.includes(e.labelTarget)) return true;
                }
        });
    }
}
DisableKeyboardBinding.$inject = ["eventBus", "injector", "keyboard","selection","modeling"];