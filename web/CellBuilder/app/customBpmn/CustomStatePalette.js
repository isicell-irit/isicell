export default class CustomPalette {
  constructor(bpmnFactory, create, elementFactory, palette,eventBus) {
    this.bpmnFactory = bpmnFactory;
    this.create = create;
    this.elementFactory = elementFactory;
    this.eventBus = eventBus

    palette.registerProvider(500,this);
  }


  getPaletteEntries(element) {
    const {
      bpmnFactory,
      create,
      elementFactory,
      eventBus
    } = this;

    function createComment(){
      return function(event) {
        const businessObject = bpmnFactory.create('bpmn:TextAnnotation');
  
        const shape = elementFactory.createShape({
          type: 'bpmn:TextAnnotation',
          businessObject: businessObject
        });
  
        create.start(event, shape); 
      }
    }

    return function(entries){

      delete entries["create.data-store"];
      delete entries["create.participant-expanded"];
      delete entries["create.data-object"];
      delete entries["create.subprocess-expanded"];
      delete entries["create.exclusive-gateway"];
      delete entries["create.intermediate-event"];
      delete entries["create.start-event"];
      delete entries["create.group"];
      //delete entries["create.end-event"];
      entries["create.end-event"].title = "Create Dead Cell State"
      entries["create.task"].title = "Create State"

      entries['create.comment'] = {
        group: 'activity',
        className: 'bpmn-icon-text-annotation',
        title: 'Create comment',
        action: {
          dragstart: createComment(),
          click: createComment()
        }
      }
      entries['lock.stateMachine'] = {
        group: 'tools',
        className: 'icon-lock-open',
        title: 'Prevent opening of the activity diagram',
        action: {
          click: (event)=>{
            event.target.classList.toggle('icon-lock-open')
            eventBus.fire('lock.stateMachine',{isLock:event.target.classList.toggle('icon-lock')})
          }
        }
      }
      return entries
    }
  }
}

CustomPalette.$inject = [
  'bpmnFactory',
  'create',
  'elementFactory',
  'palette',
  'eventBus'
];