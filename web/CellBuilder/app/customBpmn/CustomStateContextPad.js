/*export default class CustomContextPadProvider {
    constructor(contextPad) {
        contextPad.registerProvider(this);
    }
    getContextPadEntries(element) {
        var entriesToDelete = [];
        if (element.type === 'bpmn:EndEvent') {
            entriesToDelete = ["append.end-event", "append.intermediate-event", "append.gateway", "append.append-task", "append.text-annotation", "replace", "delete", "connect"];
        } else if (element.type === 'bpmn:IntermediateCatchEvent') {
            entriesToDelete = ["append.end-event", "append.intermediate-event", "append.gateway", "append.append-task", "append.text-annotation", "replace", "delete"]
        } else if (element.type === 'bpmn:StartEvent') {
            entriesToDelete = ["append.end-event", "append.intermediate-event", "append.gateway", "append.append-task", "append.text-annotation", "replace", "delete"]
        } else {
            entriesToDelete = ["append.end-event", "append.intermediate-event", "append.gateway", "append.append-task", "append.text-annotation", "replace"];
        }
        return function (entries) {
            for (var e of entriesToDelete) {
                delete entries[e];
            }
            return entries;
        };
    }
}
CustomContextPadProvider.$inject = ["contextPad"];*/

import stateMachine from "../diagrams/stateMachine";

export default class CustomStateContextPad {
    constructor(bpmnFactory, create, elementFactory, translate, contextPad) {
        this.bpmnFactory = bpmnFactory;
        this.create = create;
        this.elementFactory = elementFactory;
        this.translate = translate;
  
        contextPad.registerProvider(500,this);
    }
  
    getContextPadEntries(element) {
      const {
        bpmnFactory,
        create,
        elementFactory,
        translate,
        modeling
      } = this;

      function check(event, element) {
        if(event.pointerType==='mouse' || event.isFinal)
          if(stateMachine.getSerializedElement("state", element) == "completed"){
            console.debug(element, " : uncheck")
            stateMachine.serializeElement("state", "on going", element);
            stateMachine.colorState(element);
          }
          else{
            console.debug(element, " : check")
            stateMachine.serializeElement("state", "completed", element);
            stateMachine.colorState(element);
          }
      }

      var entriesToDelete = [];
        if (element.type === 'bpmn:EndEvent') {
            entriesToDelete = ["append.end-event", "append.intermediate-event", "append.gateway", "append.append-task", "append.text-annotation", "replace", "connect"];
        } else if (element.type === 'bpmn:IntermediateCatchEvent') {
            entriesToDelete = [ "append.intermediate-event", "append.gateway", "append.text-annotation", "replace", "delete"]
        } else if (element.type === 'bpmn:StartEvent') {
            entriesToDelete = [ "append.intermediate-event", "append.gateway", "append.append-task", "append.text-annotation", "replace", "delete"]
        } else {
            entriesToDelete = [ "append.intermediate-event", "append.gateway", "replace"];
        }
        return function (entries) {
            for (var e of entriesToDelete) {
                delete entries[e];
            }
            if("append.end-event" in entries) entries["append.end-event"].title = "Create Dead Cell State"
            if("append.append-task" in entries) entries["append.append-task"].title = "Create State"
            if(element.type ==='bpmn:Task'){
                entries["rename"] = {
                        group: 'model',
                        className: 'icon-rename',
                        title: translate('rename'),
                        action: {
                            click: ()=>setTimeout(()=>{
                                const editing = stateMachine.modeler.get('directEditing')
                                if(editing.isActive(element)) editing.complete(element)
                                else editing.activate(element)
                            },10)
                        }
                }
            }
            if(element.type === 'bpmn:IntermediateCatchEvent' || element.type === 'bpmn:Task'){
                if(stateMachine.getSerializedElement("state", element) == "completed"){
                    entries["check"] = {
                        group: 'model',
                        className: 'icon-cancel',
                        title: translate('change color of state (modified)'),
                        action: {
                            click: check
                        }
                    }
                }
                else {
                    entries["check"] = {
                        group: 'model',
                        className: 'icon-ok',
                        title: translate('change color of state (valided)'),
                        action: {
                            click: check
                        }
                    }
                }
            }
            return entries;
        };
    }
  }
  
  CustomStateContextPad.$inject = [
    'bpmnFactory', 
    'create', 
    'elementFactory', 
    'translate', 
    'contextPad'
  ];