/*export default class CustomContextPadProvider {
    constructor(contextPad) {
        contextPad.registerProvider(this);
    }
    getContextPadEntries(element) {
        var entriesToDelete = [];
        if (element.type === 'bpmn:EndEvent') {
            entriesToDelete = ["append.end-event", "append.intermediate-event", "append.gateway", "append.append-task", "append.text-annotation", "replace", "delete", "connect"];
        } else if (element.type === 'bpmn:IntermediateCatchEvent') {
            entriesToDelete = ["append.end-event", "append.intermediate-event", "append.gateway", "append.append-task", "append.text-annotation", "replace", "delete"]
        } else if (element.type === 'bpmn:StartEvent') {
            entriesToDelete = ["append.end-event", "append.intermediate-event", "append.gateway", "append.append-task", "append.text-annotation", "replace", "delete"]
        } else {
            entriesToDelete = ["append.end-event", "append.intermediate-event", "append.gateway", "append.append-task", "append.text-annotation", "replace"];
        }
        return function (entries) {
            for (var e of entriesToDelete) {
                delete entries[e];
            }
            return entries;
        };
    }
}
CustomContextPadProvider.$inject = ["contextPad"];*/

import behaviorDiagram from "../diagrams/behaviorDiagram";

export default class CustomBehaviorContextPad {
    constructor(bpmnFactory, create, elementFactory, contextPad) {
        this.bpmnFactory = bpmnFactory;
        this.create = create;
        this.elementFactory = elementFactory;
  
        contextPad.registerProvider(500,this);
    }
  
    getContextPadEntries(element) {
      const {
        bpmnFactory,
        create,
        elementFactory,
        modeling
      } = this;

      var entriesToDelete = [];
        if (element.type === 'bpmn:EndEvent') {
            entriesToDelete = ["append.end-event", "append.intermediate-event", "append.gateway", "append.append-task", "replace", "delete", "connect"];
        } else if (element.type === 'bpmn:IntermediateCatchEvent') {
            entriesToDelete = ["append.end-event", "append.intermediate-event", "append.gateway", "append.append-task", "replace", "delete"]
        } else if (element.type === 'bpmn:StartEvent') {
            entriesToDelete = ["append.end-event", "append.intermediate-event", "append.gateway", "append.append-task", "replace", "delete"]
        } else {
            entriesToDelete = ["append.end-event", "append.intermediate-event", "append.gateway", "append.append-task", "replace"];
        }
        return function (entries) {

            delete entries.append;
            for (var e of entriesToDelete) {
                delete entries[e];
            }
            if (element.type === 'bpmn:SequenceFlow' && element.source?.type === 'bpmn:Gateway') {
                if (element.businessObject.name == "YES") {
                    entries["check"] = {
                        group: 'model',
                        className: 'icon-cancel',
                        title: 'NO',
                        action: {
                            click: (event) => {
                                if (event.pointerType === 'mouse' || event.isFinal){
                                    let flows = element.source.businessObject.outgoing.map(f => behaviorDiagram.modeler.get('elementRegistry').get(f.id))
                                    for (let f of flows) {
                                        if (f.businessObject.name === 'NO') {
                                            f.businessObject.name = 'YES';
                                            behaviorDiagram.modeling.updateProperties(f, { name: 'YES' });
                                        }
                                    }
                                    element.businessObject.name = 'NO';
                                    behaviorDiagram.modeling.updateProperties(element, { name: 'NO' });
                                }
                            }
                        }
                    }
                }
                else {
                    entries["check"] = {
                        group: 'model',
                        className: 'icon-ok',
                        title: 'YES',
                        action: {
                            click: (event) => {
                                if (event.pointerType === 'mouse' || event.isFinal){
                                    let flows = element.source.businessObject.outgoing.map(f => behaviorDiagram.modeler.get('elementRegistry').get(f.id))
                                    for (let f of flows) {
                                        if (f.businessObject.name === 'YES') {
                                            f.businessObject.name = 'NO';
                                            behaviorDiagram.modeling.updateProperties(f, { name: 'NO' });
                                        }
                                    }
                                    element.businessObject.name = 'YES';
                                    behaviorDiagram.modeling.updateProperties(element, { name: 'YES' });
                                }
                            }
                        }
                    }
                }
            }
            return entries;
        };
    }
  }
  
  CustomBehaviorContextPad.$inject = [
    'bpmnFactory', 
    'create', 
    'elementFactory', 
    'contextPad'
  ];