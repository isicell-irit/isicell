import ModuleManager from '../manager/moduleManager'
import FunctionManager from '../manager/functionManager'
import { b64_to_utf8 } from '../utils/misc';
import behaviorDiagram from '../diagrams/behaviorDiagram';

var tapCount = 0

export default class CustomPalette {
  constructor(bpmnFactory, create, elementFactory, palette) {
    this.bpmnFactory = bpmnFactory;
    this.create = create;
    this.elementFactory = elementFactory;

    palette.registerProvider(500,this);
  }

  getPaletteEntries(element) {
    const {
      bpmnFactory,
      create,
      elementFactory,
      modeling
    } = this;



    function createTask() {
      return function(event) {
        tapCount++;
        setTimeout(() => {
          tapCount--;
        }, 250)

        if (event.type === 'dragstart' || event.type === 'panstart'){
          let target = event.target.closest('.entry')
          const businessObject = bpmnFactory.create('bpmn:Task');
          var idFunc = target.getAttribute('data-idFunc');
          if (idFunc === null && target.getAttribute('data-template')){
            var template = b64_to_utf8(target.getAttribute('data-template'));
            var cat = target.getAttribute('data-cat');
            businessObject.idFunc = FunctionManager.addOrUpdateFunction(cat,-1,template);
            
            let checked = FunctionManager.checkFunctionAvailability(businessObject.idFunc, cat, template);
            FunctionManager.addOrUpdateFunction(cat,businessObject.idFunc,checked.code);


            const info = FunctionManager.getInfo(businessObject.idFunc);
            businessObject.name = info.functionName+'('+info.params.join(',')+')';

            var section;
            if (cat === 'cellFunction') section = document.getElementById('Cell functions')
            else if (cat === 'scenarioFunction') section = document.getElementById('Scenario functions')
            var item = section.itemBuilder()
            item.valueTextNoIntegrityCheck = info.functionName+'()'
            item.type = info.returnType
            item.setAttribute('data-idFunc', businessObject.idFunc);
            item.setAttribute('data-cat', cat);
            section.addItem(item)
          } else if(idFunc !==null){
            businessObject.idFunc = idFunc;
            const info = FunctionManager.getInfo(idFunc);
            businessObject.name = info.functionName+'('+info.params.join(',')+')';
          } else {
            console.error("ERROR !!");
            return false;
          }
    
          const shape = elementFactory.createShape({
            type: 'bpmn:Task',
            businessObject: businessObject
          });
          create.start(event, shape);
        }
        else if(tapCount===2){ // double click open function
          let target = event.target;
          if (target.tagName === 'SPAN') target = target.parentElement
          if (target.tagName === 'DIV') target = target.parentElement
          let idFunc = target.getAttribute('data-idfunc')
          if(idFunc) behaviorDiagram.viewFunc(idFunc);
        }
      }
    }

    function createComment() {
      return function (event) {
        const businessObject = bpmnFactory.create('bpmn:TextAnnotation');

        const shape = elementFactory.createShape({
          type: 'bpmn:TextAnnotation',
          businessObject: businessObject
        });

        create.start(event, shape);
      }
    }
    function createCondition() {
      return function (event) {
        const businessObject = bpmnFactory.create('bpmn:Gateway');

        const shape = elementFactory.createShape({
          type: 'bpmn:Gateway',
          businessObject: businessObject
        });

        create.start(event, shape);
      }
    }

    return function(entries){
      delete entries.create;
      delete entries["create.data-store"];
      delete entries["create.participant-expanded"];
      delete entries["create.data-object"];
      delete entries["create.subprocess-expanded"];
      delete entries["create.exclusive-gateway"];
      delete entries["create.intermediate-event"];
      delete entries["create.start-event"];
      delete entries["create.end-event"];
      delete entries["create.task"];
      delete entries["create.group"];
      entries['create.plugin-function'] = {
          group: 'activity',
          className: 'bpmn-icon-task hidden',
          title: '',
          action: {
            dragstart: createTask(),
            click: createTask(),
            panstart: createTask(),
            touchstart: createTask()
          }
        }
      entries['create.comment'] = {
          group: 'activity',
          className: 'bpmn-icon-text-annotation',
          title: 'Create comment',
          action: {
            dragstart: createComment(),
            click: createComment()
          }
        },
        entries['create.condition'] = {
          group: 'activity',
          className: 'bpmn-icon-gateway-none',
          title: 'Create condition',
          action: {
            dragstart: createCondition(),
            click: createCondition()
          }
        }
      

      return entries;
    }
  }
}

CustomPalette.$inject = [
  'bpmnFactory',
  'create',
  'elementFactory',
  'palette'
];