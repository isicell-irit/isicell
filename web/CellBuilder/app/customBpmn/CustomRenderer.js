import BaseRenderer from 'diagram-js/lib/draw/BaseRenderer';
import {
  getLabel
} from 'bpmn-js/lib/features/label-editing/LabelUtil';

import {
  append as svgAppend,
  create as svgCreate,
  classes as svgClasses,
} from 'tiny-svg';

import {
  getBounds,
  getSemantic,
} from 'bpmn-js/lib/draw/BpmnRenderUtil';

import { is } from 'bpmn-js/lib/util/ModelUtil';
import { isAny } from 'bpmn-js/lib/features/modeling/util/ModelingUtil';
import {
  isObject,
  assign,
  forEach,
  reduce
} from 'min-dash';
import Text from './Text';
const HIGH_PRIORITY = 15000,
  TASK_BORDER_RADIUS = 10;


var DEFAULT_FONT_SIZE = 12;
var LINE_HEIGHT_RATIO = 1.2;

var MIN_TEXT_ANNOTATION_HEIGHT = 30;

function colorerSyntaxe(svgLabel) {
  const tokens = [
    { regex: /"([^"]*)"/g, className: "LSH_p3" },
    { regex: /(this->|->|\.|,)/g, className: "LSH_a" },
    { regex: /(&&|\|\||<=|>=|>|<|==|!=|=|\+\+|--|\+|-|!)/g, className: "LSH_t" },
    { regex: /([A-Za-z_]+\w*)(?=\()/g, className: "LSH_f" },
    { regex: /([A-Za-z_]+\w*)(?=::)/g, className: "LSH_f" },
    { regex: /(\()/g, className: () => `LSH_p${++parCount % 7}` },
    { regex: /(\))/g, className: () => `LSH_p${parCount-- % 7}` },
    { regex: /(\[)/g, className: () => `LSH_p${++braCount % 7}` },
    { regex: /(\])/g, className: () => `LSH_p${braCount-- % 7}` },
    { regex: /(true|false)/g, className: "LSH_n" },
    { regex: /([A-Za-z_]+\w*)/g, className: "editor-foreground" },
    { regex: /(\d+\.?\d*)/g, className: "LSH_n" },
  ];

  let parCount = 0;
  let braCount = 0;
  let allText = [...svgLabel.children].map(dom => dom.textContent.replace(/[&<>"']/g, c => `&#${c.charCodeAt(0)}`))
  let currentIndex = 0;
  let splitIndexesOrigin = allText.map(text => { currentIndex += text.length; return currentIndex; });
  allText = allText.join("");

  const globalRegex = new RegExp(tokens.map(t => t.regex.source).join('|').replace(/[&<>"']/g, c => `&#${c.charCodeAt(0)}`), 'g');

  let lastIndexOrigin = 0;
  let buffer = [''];
  const updateBuffer = (endIndex, transform) => {
    while (splitIndexesOrigin.length && splitIndexesOrigin[0] < endIndex) {
      const nextSplit = splitIndexesOrigin.shift();
      buffer[buffer.length - 1] += transform(allText.slice(lastIndexOrigin, nextSplit))
      buffer.push('')
      lastIndexOrigin = nextSplit
    }
    buffer[buffer.length - 1] += transform(allText.slice(lastIndexOrigin, endIndex))
    lastIndexOrigin = endIndex;
  }
  [...allText.matchAll(globalRegex)].forEach(args => {
    let offset = args.index
    const match = args.shift()
    const token = tokens[args.findIndex(m => m !== undefined)]
    updateBuffer(offset, (text) => text)
    const cls = typeof token.className === "function" ? token.className(match) : token.className;
    updateBuffer(offset + match.length, (text) => `<tspan class="${cls}">${text}</tspan>`)
  })
  updateBuffer(Infinity, (text) => text);
  [...svgLabel.children].forEach((dom, i) => {
    dom.innerHTML = buffer[i] || "";
  });
}
export default class CustomRenderer extends BaseRenderer {
  constructor(eventBus, bpmnRenderer, textRenderer, config, elementRegistry,styles) {
    super(eventBus, HIGH_PRIORITY);

    this.bpmnRenderer = bpmnRenderer;
    this.elementRegistry = elementRegistry;
    this.styles = styles;

    const defaultStyle = this.defaultStyle = assign({
      fontFamily: 'Fira Code',
      //fontFamily: 'Arial, sans-serif',
      fontSize: DEFAULT_FONT_SIZE,
      fontWeight: 'normal',
      lineHeight: LINE_HEIGHT_RATIO
    }, config && config.defaultStyle || {});

    const fontSize = parseInt(defaultStyle.fontSize, 10) - 1;

    const externalStyle = this.externalStyle = assign({}, defaultStyle, {
      fontSize: fontSize
    }, config && config.externalStyle || {});

    const textUtil = this.textUtil = new Text({
      style: defaultStyle
    });
    textRenderer.getExternalLabelBounds = function (bounds, text) {
      if (bounds.di) {
        var boxFlow = elementRegistry.getGraphics(bounds.di.bpmnElement.id).getBBox()
        var width = bounds.width === 90 ? text.length * 8 + 30 : (bounds.width > boxFlow.width ? bounds.width : boxFlow.width)
      } else {
        var width = bounds.width < 250 ? 250 : bounds.width
      }
      var layoutedDimensions = textUtil.getDimensions(text, {
        box: {
          width: width,
          //width: bounds.width<150 ? 150 : bounds.width, //was hardcoded to 90
          height: bounds.height < 60 ? 60 : bounds.height, //was hardcoded to 30
          x: width / 2 + bounds.x,
          y: bounds.height / 2 + bounds.y
        },
        style: externalStyle
      });

      // resize label shape to fit label text
      return {
        x: Math.round(bounds.x + bounds.width / 2 - layoutedDimensions.width / 2),
        y: Math.round(bounds.y),
        width: Math.ceil(layoutedDimensions.width),
        height: Math.ceil(layoutedDimensions.height)
      };

    };
  }

  canRender(element) {
    return isAny(element, ['bpmn:SequenceFlow', 'bpmn:Gateway', 'bpmn:IntermediateCatchEvent', 'bpmn:EndEvent', 'bpmn:StartEvent']) && element.labelTarget || is(element, 'bpmn:Task');
  }

  renderExternalLabel(parentGfx, element) {
    //var boxFlow = this.elementRegistry.getGraphics(element.di.bpmnElement.id).getBBox()
    var box = {
      //width: element.width <150 ? 150  : (element.width>boxFlow.width?element.width:boxFlow.width), //was hardcoded to 90
      width: element.width < 150 ? 150 : element.width, //was hardcoded to 90
      height: element.height < 60 ? 60 : element.height, //was hardcoded to 30
      x: element.width / 2 + element.x,
      y: element.height / 2 + element.y
    };
    var svgLabel = this._renderLabel(parentGfx, getLabel(element), {
      box: box,
      fitBox: true,
      style: assign(
        {},
        this.externalStyle,
        {
          fill: 'var(--editor-foreground)'
        }
      )
    });
    colorerSyntaxe(svgLabel);
    return svgLabel;
  }

  renderEmbeddedLabel(parentGfx, element, align, attrs = {}) {
  var semantic = getSemantic(element);

      var box = getBounds({
        x: element.x,
        y: element.y,
        width: element.width,
        height: element.height
      }, attrs);

      var svgLabel = this._renderLabel(parentGfx, semantic.name, {
        align,
        box,
        padding: 7,
        style: {
          //fill: 'var(--editor-foreground)'
          fontWeight: 'bold',
          fontSize: DEFAULT_FONT_SIZE-1,
          fill: 'var(--light-editor-foreground) !important'
        }
      });
      colorerSyntaxe(svgLabel);
      svgLabel.classList.add('lightC');
      return svgLabel;
  }

  shapeStyle(attrs) {
    return this.styles.computeStyle(attrs, {
      strokeLinecap: 'round',
      strokeLinejoin: 'round',
      stroke: 'var(--light-ui-text-2)',
      strokeWidth: 2,
      fill: 'var(--light-editor-background)',
    });
  }
  drawRect(parentGfx, width, height, r, offset, attrs) {

    if (isObject(offset)) {
      attrs = offset;
      offset = 0;
    }

    offset = offset || 0;

    attrs = this.shapeStyle(attrs);

    var rect = svgCreate('rect', {
      x: offset,
      y: offset,
      width: width - offset * 2,
      height: height - offset * 2,
      rx: r,
      ry: r,
      ...attrs
    });

    svgAppend(parentGfx, rect);

    return rect;
  }

  renderActivity(parentGfx, element, attrs = {}) {
    var {
      width,
      height
    } = getBounds(element, attrs);

    return this.drawRect(parentGfx, width, height, TASK_BORDER_RADIUS, {
      ...attrs,
      fill: 'var(--light-editor-background)',
      fillOpacity: 1,
      stroke: 'var(--light-ui-text-2)',
    });
  }

  renderTask(parentGfx, element, attrs = {}) {
    var activity = this.renderActivity(parentGfx, element, attrs);

    this.renderEmbeddedLabel(parentGfx, element, 'center-middle', attrs);


    return activity;
  }
  drawShape(parentNode, element) {
    const isTask = is(element, 'bpmn:Task')
    if (isTask && !element.businessObject.state) {
      const shape = this.renderTask(parentNode, element);
      return shape;
    } else if(!isTask){
      return this.renderExternalLabel(parentNode, element)
    }
  }
  _renderLabel(parentGfx, label, options) {

    options = assign({
      size: {
        width: 1000
      }
    }, options);

    var text = this.textUtil.layoutText(label || '', options).element;

    svgClasses(text).add('djs-label');

    svgAppend(parentGfx, text);

    return text;
  }
}

CustomRenderer.$inject = ['eventBus', 'bpmnRenderer', 'textRenderer', 'config.textRenderer', 'elementRegistry','styles'];

// copied from https://github.com/bpmn-io/diagram-js/blob/master/lib/core/GraphicsFactory.js
function prependTo(newNode, parentNode, siblingNode) {
  parentNode.insertBefore(newNode, siblingNode || parentNode.firstChild);
}