

import CustomContextPad from '../CustomBehaviorContextPad';
import ResizeRule from '../resizeRule';
import DisableKeyboardBinding from '../DisableKeyboardBinding';
import CustomPalette from '../CustomBehaviorPalette';
import CustomRenderer from '../CustomRenderer';
import TouchInteractionEvents from '../CustomTouchInteractionEvents';

export default {
  __init__: [ 'customContextPad', 'resizeRule', 'disableKeyboardBinding', 'customPalette', 'customRenderer', 'touchInteractionEvents'],
  customContextPad: [ 'type', CustomContextPad ],
  resizeRule: [ 'type', ResizeRule ],
  disableKeyboardBinding: [ 'type', DisableKeyboardBinding ],
  customPalette: [ 'type', CustomPalette ],
  customRenderer: [ 'type', CustomRenderer ],
  touchInteractionEvents: [ 'type', TouchInteractionEvents ]
};