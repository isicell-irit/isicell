const CopyWebpackPlugin = require('copy-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const MonacoWebpackPlugin = require('monaco-editor-webpack-plugin');
const webpack = require('webpack');
const path = require('path')

var config = {
  entry:  './app/app.js',

  output: {
    path: path.join(__dirname, '/public'),
    filename: 'app.bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      },
      {
        test: /\.(jpe?g|svg|mp4|png|gif|ico|eot|ttf|woff2?)(\?v=\d+\.\d+\.\d+)?$/i,
        type: 'asset/resource',
      },
      {
        test: /\.(bpmn|html)$/,
        use: 'raw-loader'
      }
    ]
  },
  optimization: {
    splitChunks: {
            cacheGroups: {
                monacoCommon: {
                    test: /[\\/]node_modules[\\/]monaco\-editor/,
                    name: 'monaco-editor-common',
                    chunks: 'async'
                }
            }
        },
  },
  resolve: {
    fallback: {
      stream: false,
      buffer: false,
    }
  },
  plugins: [
    new CopyWebpackPlugin({
      patterns: [
        { from: '**/*.html', context: 'app/' },
        { from: '*.{png,jpg}', to: 'textures', context: 'resources/textures/' },
        { from: '*.{png,jpg,gif,ico,mp4}', to: 'img', context: 'resources/img/' },
    ]}),
    new MonacoWebpackPlugin({
      languages: ['cpp','python','json'],
      features:['bracketMatching','suggest']
    })
  ],
}

module.exports = (env,argv) => {

  if (argv.mode === 'production') {
    config.mode = 'production'
    config.performance = {
      hints: false,
      maxEntrypointSize: 512000,
      maxAssetSize: 512000
    }
    config.optimization.minimize =  true
    config.optimization.minimizer = [new TerserPlugin()]
    config.plugins.push(new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production')
      }
    }))
  }
  else{
    config.devServer = {
      static: {
        directory: path.join(__dirname, 'public'),
      },
      port: 3030,
        compress: true,
      proxy: {
        '/**': {target:'http://localhost:8080',secure:false},
      },
    }
    config.devtool = 'source-map'
    config.plugins.push(new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('development')
      }
    }))
  }

  return config;
};