#!/usr/bin/env node
//https://stackoverflow.com/questions/4018154/how-do-i-run-a-node-js-app-as-a-background-service
require('./service/consoleTimestamp')();
const compression = require('compression');
const express = require('express');
const helmet = require('helmet');
const session = require('express-session');
const bodyParser = require('body-parser');
const crypto = require('crypto');
const path = require('path');
const fs = require('fs');
const { createServer } = require('node:http');
const { Server } = require('socket.io');
const Users = require('./users');
const LogRecorder = require('./service/logRecorder');
const lr = new LogRecorder();

const notebookAPI = require('./routes/notebook')
const compileAPI = require('./routes/compile')
const viewerAPI = require('./routes/viewer')
const pythonAPI = require('./routes/python')
const defaultDataAPI = require('./routes/defaultData')
const usersAPI = require('./routes/users')
const terminalAPI = require('./service/terminal')
const { v4: uuidv4 } = require('uuid');

const app = express();
const server = createServer(app);
const io = new Server(server);
const port = 8080;

const {CellbuilderPath, isicellPath, frontendPath, docPath} = require('./service/isiCellPath')


const { connectToDb } = require('./service/db');

var dbInfo = JSON.parse(fs.readFileSync(path.join(__dirname, '../dbInfo.json')));

var users = new Users(path.join(CellbuilderPath,'users', 'users.json'))
users.autoSave();




async function main() {


  try {
    await connectToDb(dbInfo["sql"]);

    const helmetI = helmet({
      frameguard: {
        action: 'SAMEORIGIN',
      }})
    app.use(helmetI)
    app.use(helmet.contentSecurityPolicy({
        useDefaults: true,
        directives: {
          "img-src": ["'self'", "https: data: blob:"],
        },
    }));
    io.engine.use(helmetI);

    const MemoryStore = require('memorystore')(session)

    const sessionMiddleware = session({
      secret: crypto.randomBytes(64).toString('hex'),
      store: new MemoryStore({
        checkPeriod: 86400000 // prune expired entries every 24h
      }),
      resave: false,
      saveUninitialized: false,
      cookie: { sameSite: true, maxAge: (7 * 86400 * 1000) },
    })

    app.set('views', path.join(__dirname, 'views'));
    app.engine('html', require('ejs').renderFile);
    app.set('view engine', 'html');
    app.use(sessionMiddleware);
    io.engine.use(sessionMiddleware);
    
    app.use(compression());
    app.use(bodyParser.json({ limit: '500mb', type: 'application/json' }));
    app.use(bodyParser.text());
    app.use(bodyParser.urlencoded({ extended: false }));


    app.use('/**', (req, res, next) => {
      const ip = (req.headers['x-real-ip'] ? req.headers['x-real-ip'] : req.ip)?.replace('::ffff:', '')
      if(req.session.loggedin && req.session.realip !== ip){
        req.session.loggedin = false
        req.session.username = undefined
        req.session.realip=ip
      } else if (!req.session.realip || req.session.realip !== ip){
        req.session.realip=ip
      } 
      next();
    })

    usersAPI(app, lr, users)
    //! END LOGIN SYSTEM


    // Définit la CSP spécifique pour la route "/doc"
    app.use('/doc', (req, res, next) => {
      // Autorise les scripts inline pour la route "/doc" uniquement
      res.setHeader("Content-Security-Policy", "script-src 'self' 'unsafe-inline' 'unsafe-eval';");
      next();
    });
    app.use('/doc', express.static(docPath));
    app.use('/', express.static(frontendPath));

    app.get("/", function (req, res) {
      const nonce = uuidv4(); // Génère un nonce aléatoire
      // Ajoutez le nonce à l'en-tête CSP
      res.setHeader('Content-Security-Policy', `script-src 'self' 'nonce-${nonce}'`);
      res.render(path.join(__dirname, 'views', 'index.html'), { nonce });
    })

    app.use('/api/**', (req, res, next) => {

      console.log(req.session.username, req.originalUrl)
      //if (req.originalUrl.startsWith('/api/notebook/evaluate/'))
      //console.log(req.session.username, req.protocol + '://' + req.get('host') + req.originalUrl)
      res.append('Content-Type', 'application/json');
      next();
    });

    app.get('/api/version', (req, res) => {
      res.send(JSON.stringify({
        name: process.env.npm_package_name,
        version: process.env.npm_package_version,
      }));
    });




    defaultDataAPI(app, lr, users)
    compileAPI(app, lr, users, dbInfo)
    viewerAPI(app, lr, users, dbInfo)
    pythonAPI(app, lr, users)
    notebookAPI(app, lr, users)
    terminalAPI(io, lr, users)


    app.get('/api/exit',async (req,res)=>{
      if (req.session.loggedin) {
        if (req.session.username === 'admin') {
          res.end("exit")
          process.exit()
          return;
        }
      }
      res.end("Unauthorized action")
    })


    server.listen(port, () => console.log(`ISiCell listening at http://localhost:${port}`));
  } catch (e) {
    console.error(e);
  }
}


main();
