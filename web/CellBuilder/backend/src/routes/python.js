const path = require('path');
const fs = require('fs');
const multer = require('multer');
const si = require('systeminformation');
const util = require('util');
const { spawn, exec } = require('child_process');
const { userFolder, simulationsPath, scriptsPath } = require("../service/isiCellPath");
const zipDir = require('zip-dir');
const { isSubPathSafe, subPathSafe, joinSubDirSafe, validateToken } = require('../service/safePathCheck');
const execPromise = util.promisify(require('child_process').exec);

function generateToken() {
    let dt = new Date();
    return 'sim_'.concat(dt.getFullYear(), (dt.getMonth() + 1), dt.getDate(), "_", dt.getHours(), dt.getMinutes(), '_', Math.floor(Math.random() * 1000));
}

function splitPath(path) {
    let i = path.lastIndexOf('/') + 1
    return [path.slice(0, i), path.slice(i)]
}

module.exports = function(app,lr,users){
    var storage = multer.diskStorage({
        destination: function (req, file, cb) {
            let ownFolder = path.join(userFolder, req.session.username)
            let destPath = path.join(ownFolder,req.body.path)
            destPath = isSubPathSafe(ownFolder,destPath)?destPath:ownFolder
            cb(null, destPath)
        },
        filename: function (req, file, cb) {
            console.log(req.session.username, 'upload',file.originalname)
            cb(null, file.originalname)
        }
    })
    var upload = multer({ storage: storage,limits:{fieldSize: 2_000_000_000}})/*,fileFilter: function (req, file, callback) {
        var ext = path.extname(file.originalname);
        if(ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg') {
            return callback(new Error('Only images are allowed'))
        }
        callback(null, true)
    }, })*/

    app.use('/api/uploadfiles', users.protected)
    app.post('/api/uploadfiles', upload.array('files'), (req, res, next) => {
        const files = req.files
        if (!files) {
            res.status(500).send({err:'error'})
        }
        files.forEach(f=>fs.chownSync(f.path, users.getUID(req.session.username), users.isiCellGID))
        res.send(files)
    })

    app.post('/api/runPython/:builderToken',users.protected, async (req, res) => {
        lr.record(req, "explorer")
        try {
            let builderToken = req.params.builderToken;
            if (builderToken.includes('/')) {
                res.status(500).send({ message: err });
                return;
            }
            let params = req.body;
            let pythonRun = spawn('python', [path.join(scriptsPath, 'paramatersExploration.py'), path.join(simulationsPath, builderToken), JSON.stringify(params)],{uid:users.getUID(req.session.username),gid:users.isiCellGID,env:users.getEnv(req.session.username)})

            res.writeHead(200, { 'Content-Type': 'application/json' });

            pythonRun.stdout.on('data', function (data) {
                dataStr = data.toString();
                res.write(dataStr); res.flush();
            });

            pythonRun.stderr.on('data', function (data) {
                dataStr = data.toString();
                res.write(JSON.stringify({ err: dataStr })); res.flush();
            });

            pythonRun.on('close', (code) => {
                console.log('exploreClose', code);
                res.end();
            });
            pythonRun.on('error', (err) => {
                console.log('exploreErr', err);
                res.end();
            });
            console.log("api : [ip:" + req.session.realip + "] new simulation launch with token :" + builderToken + "\t\t pid=" + pythonRun.pid);
            // return token
        } catch (err) {
            console.error(err);
            res.status(500).send({});
        }
    });

    app.get('/api/monitor',users.protected, async (req, res) => {
        Promise.all([si.currentLoad(),si.mem()]).then((values)=>{
            res.send({ cpu: values[0].currentLoad, totalMem: values[1].total / 1073741824, activeMem: values[1].active / 1073741824 });
        }).catch(()=>res.send({err:true}))/*
        exec("top - bn1 | awk '/^%Cpu/{print $2, $4, $6, $8, $10, $12, $14}/^MiB Mem/{print $4}/^MiB Swap/{print $9}'", (err, out) => {
            let [totalCPU,totalMem,availableMem] = out.split('\n')
            totalCPU = totalCPU.split(' ').map(str => Number(str))
            let idleCPU = totalCPU[3]
            totalCPU = totalCPU.reduce((a, b) => a + b, 0)
            res.send({ cpu: err ? -1 : totalCPU - idleCPU, totalMem: Number(totalMem) / 1024, availableMem: Number(availableMem) / 1024 });
        })*/

    })


    app.get('/api/statesnohup',users.protected, async (req, res) => {
        let hidenDir = path.join(userFolder, req.session.username, '.backgroundRun')
        if (!fs.existsSync(hidenDir)) {
            fs.mkdirSync(hidenDir, { recursive: true });
        }
        let backgroundRunPath = path.join(hidenDir, "backgroundRun.json")
        fs.readFile(backgroundRunPath, function (err, data) {
            if (err) { res.status(200).send({data:{ finish: [], interrupt: [], running: [] }}); return }
            let json = JSON.parse(data)
            let runningProcess = Object.values(json).filter((v)=>v.status==='running')
            let pidList = runningProcess.map((v) => v.pid)
            if(pidList.length>0) {
                exec("ps --no-heading -o pid "+pidList.join(' '),{uid:users.getUID(req.session.username),gid:users.isiCellGID,env:users.getEnv(req.session.username)},(err,out)=>{
                    if(!err){
                        let activeProcess = out.split('\n').map(v=>Number(v))
                        for (let entry of runningProcess){
                            if(!activeProcess.includes(entry.pid)) entry.status = 'finish'
                        }
                    } else {
                        for (let entry of runningProcess) {
                            entry.status = 'finish'
                        }
                    }
                    fs.writeFile(backgroundRunPath, JSON.stringify(json), () => { })
                    let ret = { finish: [], interrupt: [], running: [] }
                    Object.entries(json).forEach(([token, entry]) => ret[entry.status].push({ date: entry.date, fileRun: entry.fileRun, token: token }))
                    res.send({ data: ret })
                })
            } else {
                let ret = { finish: [], interrupt:[],running:[]}
                Object.entries(json).forEach(([token, entry]) => ret[entry.status].push({ date: entry.date, fileRun: entry.fileRun, token: token }))
                res.send({ data: ret })
            }

        })

    })


    app.post('/api/nohup/:builderToken',users.protected, async (req, res) => {
        let builderToken, pathBuild, filePath, ownFolder
        ownFolder = path.join(userFolder, req.session.username)
        try{
            builderToken = validateToken(req.params.builderToken);
            pathBuild = joinSubDirSafe(simulationsPath, builderToken)
            filePath = path.join(ownFolder, req.body.path)
            if(isSubPathSafe(ownFolder, filePath))
                filePath = path.relative(ownFolder, filePath)
            else res.status(500).send({ message: '' });
        } catch(err){
            console.error(err)
            res.status(500).send({ message: err });
            return;
        }
        let hidenDir = path.join(userFolder, req.session.username, '.backgroundRun')
        if (!fs.existsSync(hidenDir)) {
            fs.mkdirSync(hidenDir, { recursive: true });
        }
        let token = generateToken()
        const launcher = path.join(scriptsPath, 'scriptLauncher.py')
        exec(`cd ${ownFolder};nohup nice python3 -u ${launcher} ${pathBuild} ${filePath} > ${path.join(hidenDir,token + ".log")} 2>&1 & echo $!`,{uid:users.getUID(req.session.username),gid:users.isiCellGID,env:users.getEnv(req.session.username)}, (err, out) => {
            //create hiden folder with information about running (pid, timestamp start)
            const entry = { date: Date.now(), pid: parseInt(out), logFile: token + '.log', fileRun: filePath.slice(filePath.lastIndexOf('/')+1), status: 'running' }
            let backgroundRunPath = path.join(hidenDir, "backgroundRun.json")
            fs.readFile(backgroundRunPath, function (err, data) {
                let json;
                if (err) json = {}
                else json = JSON.parse(data)
                json[token] = entry;
                fs.writeFile(backgroundRunPath, JSON.stringify(json),()=>{})
            })/*
            let killcmd = "{echo " + pid + "; ps -o pid --ppid " + pid + " --no-heading} | xargs kill -9"
            let killcmd = "{echo 182787; ps -o pid --ppid 182787 --no-heading} | xargs kill -9"
            let timeRuningInSecond = "ps -p " + pid + " -o etimes="*/
            res.send({ err, token });
        })
    })

    app.get('/api/killnohup/:token',users.protected, async (req, res) => {
        let token = req.params.token
        let backgroundRunPath = path.join(userFolder, req.session.username, '.backgroundRun', "backgroundRun.json")
        fs.readFile(backgroundRunPath, function (err, data) {
            if (err) {
                res.send({ err })
                return
            }
            let json = JSON.parse(data)
            if (!json[token]) {
                res.send({ err: true })
                return
            }

            exec("{ echo " + json[token].pid + "; ps -o pid --ppid " + json[token].pid + " --no-heading; } | xargs kill -9",{uid:users.getUID(req.session.username),gid:users.isiCellGID,env:users.getEnv(req.session.username)}, (err) => {
                console.log(err)
                if (!err) {
                    json[token].status = 'interrupt'
                    fs.writeFile(backgroundRunPath, JSON.stringify(json),()=>{})
                    res.send({})
                } else {
                    res.send({ err })
                }
            })
        })
    })
    app.get('/api/removenohup/:token',users.protected, async (req, res) => {
        let token = req.params.token
        let hidenDir = path.join(userFolder, req.session.username, '.backgroundRun')
        let backgroundRunPath = path.join(hidenDir, "backgroundRun.json")
        fs.readFile(backgroundRunPath, function (err, data) {
            if (err) { res.send({ err });return}
            let json = JSON.parse(data)
            if (!json[token]) {
                res.send({ err: true })
                return
            }
            if(json[token].status !== 'running')
                fs.rm(path.join(hidenDir, token+'.log'), function (err) {
                    delete json[token]
                    fs.writeFile(backgroundRunPath, JSON.stringify(json), () => { })
                    res.send({})
                });
            else res.send({ err: true })
        })
    })

    app.get('/api/userFolder/download/:path/:name',users.protected, async (req, res) => {
        try{
            const path_utf8 = Buffer.from(req.params.path,'base64').toString('utf8')
            console.log('download', path_utf8 + req.params.name)
            let ownFolder = path.join(userFolder, req.session.username)
            const filePath = path.join(ownFolder, path_utf8, req.params.name)
            if(!isSubPathSafe(ownFolder,filePath)){
                res.status(401).send({ message: "unauthorized action" });
                return;
            }
            if (!fs.existsSync(filePath)) { res.status(500).send(''); return; }
            const st = fs.statSync(filePath)
            if (st.isFile()) {
                res.download(filePath)
            } else if(st.isDirectory()){
                zipDir(filePath, function (err, buffer) {
                    console.log(buffer, err)
                    if (err !== null)
                        res.status(500).send({ message: err });
                    else {
                        res.setHeader('Content-disposition', 'attachment; filename=' + req.params.name + '.zip');
                        // res.setHeader('Content-type', buffer.mime_type);
                        res.type('bin');
                        res.send(buffer);
                    }
                });
            }
        } catch(err){
            console.error(err)
            res.status(500).send({ message: err });
            return;
        }
    })

    app.post('/api/userFolder/:action',users.protected, async (req, res) => {
        console.log(req.params.action)
        console.log(userFolder, req.session.username)
        let ownFolder = path.join(userFolder, req.session.username)
        let reqPath;
        try{
            switch (req.params.action) {
                case 'delete':
                    reqPath = path.join(ownFolder, req.body.path);
                    if(!isSubPathSafe(ownFolder,reqPath)){
                        res.send({ err: true })
                        return;
                    }
                    fs.rm(path.join(ownFolder, req.body.path), { recursive: true }, function (err) {
                        res.send({ err })
                    });
                    break;
                case 'newDir':
                    reqPath = path.join(ownFolder, req.body.path);
                    if(!isSubPathSafe(ownFolder,reqPath)){
                        res.send({ err: true })
                        return;
                    }
                    exec(`mkdir '${reqPath}'`,{uid:users.getUID(req.session.username),gid:users.isiCellGID}, (err) => {
                        if(err){
                            console.log("error")
                            res.send({ err })
                        } else {

                        }
                    });
                    break;
                case 'refresh':
                    if (!fs.existsSync(ownFolder)){
                        fs.mkdirSync(ownFolder);
                        fs.chownSync(ownFolder, users.getUID(req.session.username), users.isiCellGID)
                    }
                    const folderToRefresh = path.relative(ownFolder,subPathSafe(ownFolder, path.join(ownFolder, req.body.path))) || '.'
                    const listOpen = req.body.listOpen || ['.']
                    async function getFiles(cdir) {
                        const dirents = await fs.promises.readdir(path.join(ownFolder, cdir), { withFileTypes: true });
                        let files = await Promise.all(dirents.map((dirent) => {
                            if (dirent.name.startsWith('.')) return undefined
                            if (dirent.name === '__pycache__') return undefined
                            if (dirent.isDirectory()){
                                if(listOpen.includes(cdir+'/'+dirent.name)) 
                                return getFiles(path.join(cdir, dirent.name))
                                else return {path:path.join(cdir, dirent.name),files:[]}
                            } else return dirent.name
                        }));
                        files = files.filter((f) => f !== undefined)
                        return { path: cdir, files };
                    }
                    getFiles(folderToRefresh).then((files) => {
                        res.send({ files })
                    })
                    break;
                case 'write':
                    reqPath = path.join(ownFolder, req.body.path);
                    if(!isSubPathSafe(ownFolder,reqPath)){
                        res.send({ err: true })
                        return;
                    }
                    fs.writeFile(path.join(ownFolder, req.body.path), req.body.data, (err) => {
                        res.send({ err })
                        fs.chownSync(path.join(ownFolder, req.body.path), users.getUID(req.session.username), users.isiCellGID)
                    });
                    break;
                case 'rename':
                    const dest = subPathSafe(ownFolder, path.join(ownFolder, req.body.newPath))
                    const from = subPathSafe(ownFolder, path.join(ownFolder, req.body.path))
                    if (fs.existsSync(dest) && !from) {
                        res.send({ err:'destination already exist' })
                    } else {
                        exec("mv -n '" + from + "' '" + dest +"'",{uid:users.getUID(req.session.username),gid:users.isiCellGID}, (err) => {
                            if (fs.existsSync(dest)) {
                                console.log("no error")
                                res.send({})
                            } else {
                                console.log("error")
                                res.send({ err })
                            }
                        });
                    }
                    break;
                case 'copy':
                    let destcp = subPathSafe(ownFolder, path.join(ownFolder, req.body.newPath))
                    if(fs.lstatSync(destcp).isFile()) destcp = path.dirname(destcp)
                    destcp += '/'
                    let allErrCp = ''
                    const newNameCp = {}
                    for(let pathFrom of req.body.path){
                        const fromcp = subPathSafe(ownFolder, path.join(ownFolder, pathFrom))
                        let name = path.basename(fromcp)
                        let destPath = destcp+name
                        while (fs.existsSync(destPath)) {
                            if (name.includes('.')) {
                                const lastDotPos = name.lastIndexOf('.');
                                name = name.slice(0, lastDotPos) + ' copy' + name.slice(lastDotPos)
                            } else {
                                name += ' copy'
                            }
                            destPath = destcp + name
                        }
                        newNameCp[pathFrom] = './'+path.relative(ownFolder,destPath)
                        try {
                            const { stdout, stderr } = await execPromise("cp -r '" + fromcp + "' '" + destcp + "'",{uid:users.getUID(req.session.username),gid:users.isiCellGID})
                            if (!fs.existsSync(destcp) ||stderr) {
                                console.log("error")
                                allErrCp += stderr+'\n'
                            }
                        } catch (e) {
                            console.error(e)
                            allErrCp += e+'\n'
                        }
                    }
                    if(allErrCp === '') res.send({newName:newNameCp})
                    else res.send({ err: allErrCp })
                    break;
                case 'move':
                    let destmv = subPathSafe(ownFolder, path.join(ownFolder, req.body.newPath))
                    //if(fs.lstatSync(destmv).isFile()) destmv = path.dirname(destmv)
                    destmv += '/'
                    let allErr = ''
                    const newNamemv = {}
                    for(let pathFrom of req.body.path){
                        const frommv = subPathSafe(ownFolder, path.join(ownFolder, pathFrom))
                        let name = path.basename(frommv)
                        let destPath = destmv+name
                        while (fs.existsSync(destPath)) {
                            if (name.includes('.')) {
                                const lastDotPos = name.lastIndexOf('.');
                                name = name.slice(0, lastDotPos) + ' copy' + name.slice(lastDotPos)
                            } else {
                                name += ' copy'
                            }
                            destPath = destmv + name
                        }
                        newNamemv[pathFrom] = './'+path.relative(ownFolder,destPath)
                        try {
                            const { stdout, stderr } = await execPromise(`mv -n '${frommv}' '${destPath}'`,{uid:users.getUID(req.session.username),gid:users.isiCellGID})
                            if (!fs.existsSync(destmv) ||stderr) {
                                console.log("error")
                                allErr += stderr+'\n'
                            }
                        } catch (e) {
                            console.error(e)
                            allErr += e+'\n'
                        }
                    }
                    if(allErr === '') res.send({newName:newNamemv})
                    else res.send({ err: allErr })
                    break;
                case 'read':
                    reqPath = subPathSafe(ownFolder, path.join(ownFolder, req.body.path))
                    if ((/\.(gif|jpe?g|tiff?|png|webp|bmp)$/i).test(req.body.path)) {
                        const ext = req.body.path.match(/\.(gif|jpe?g|tiff?|png|webp|bmp)$/i)[1].toLowerCase() 
                        fs.readFile(reqPath, 'base64', (err, data) => res.send({ err, isImg:true, data: 'data:image/' + ext +';base64,'+data }))
                    } else {
                        fileSize = fs.statSync(reqPath).size/(1024*1024)
                        console.log(req.body.path,fileSize,'Mo')
                        if(!(/\.(ipynb)$/i).test(req.body.path) && fileSize>25){
                            res.send({ err:`${req.body.path} [${fileSize.toFixed(2)}Mo] is too big (>25Mo)` })
                            break;
                        }
                        fs.readFile(reqPath, 'utf8', (err, data) => {
                            res.send({ err, data })
                        });
                    }
                    break;
                default:
                    console.log('error', '/api/userFolder/' + req.params.action)
                    res.send({ err: true })
            }
        } catch(err){
            console.error(err)
            res.status(500).send({ message: err });
            return;
        }
    });
}