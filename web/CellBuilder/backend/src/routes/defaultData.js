const { demosPath, defaultJsonPath, officialModulesJsonPath, cellModulesPath, mecaCellPath, coreSrcPath, configPath, isicellPath } = require("../service/isiCellPath");
const path = require('path');
const fs = require('fs');
const { joinSubDirSafe, validateToken } = require("../service/safePathCheck");
const { getDocFromFile, generateDocModule } = require("../../../app/setups/CppCompletion");

module.exports = function(app,lr,users){
    app.get("/api/getDemo/:name", async (req, res) => {
        lr.record(req, "getDemo")
        let name = req.params.name
        try{
            name = validateToken(name);
            pathDemo = joinSubDirSafe(demosPath, name)
        } catch(err){
            console.error(err)
            res.status(500).send({ message: err });
            return;
        }
        pathDemo += '.json'
        try {
            let xml = JSON.parse(fs.readFileSync(pathDemo)).xml;
            res.send({ xml })
        } catch (e) {
            console.error(e)
        }
    })

    app.get('/api/defaultJson', async (req, res) => {
        lr.record(req, "defaultJson")
        let frontData;
        let error;

        try {
            frontData = {}
            frontData['User'] = { username: req.session.username, logged: req.session.loggedin }
            frontData['Demos'] = fs.readdirSync(demosPath).map(f => {
                let json = JSON.parse(fs.readFileSync(path.join(demosPath, f)))
                return { name: f.split('.')[0], svg: json.svg, D3: json.D3, description: json.description }
            })
            
            frontData['Default'] = {
                'Type': Object.assign(...['cells/PrimoCell.hpp','scenarios/Scenario.hpp'].map(file =>getDocFromFile(fs.readFileSync(path.join(coreSrcPath, file),{ encoding: 'utf8', flag: 'r' }),false,-1,true).typing),
                    getDocFromFile(fs.readFileSync(path.join(mecaCellPath,'world.hpp'),{ encoding: 'utf8', flag: 'r' }),false,-1,false).typing,
                    getDocFromFile(fs.readFileSync(path.join(mecaCellPath,'geometry/vector3D.h'),{ encoding: 'utf8', flag: 'r' }),true,-1,false).typing,
                    ...fs.readdirSync(path.join(coreSrcPath,'tools')).filter(file=>/\.[hc]pp|\.h/.test(file)).map(file =>getDocFromFile(fs.readFileSync(path.join(coreSrcPath,'tools', file),{ encoding: 'utf8', flag: 'r' }),true).typing),
                    getDocFromFile(fs.readFileSync(path.join(isicellPath, 'core/external/docHelper','std.hpp'),{ encoding: 'utf8', flag: 'r' }),true,-1,false).typing,
                ),
                ...JSON.parse(fs.readFileSync(defaultJsonPath,{ encoding: 'utf8', flag: 'r' }))
            }
            
            
            const moduleKeysToKeep = ['Name', 'Description', 'Type', 'Dependencies', 'DirName'];
            const officialModulesJson = JSON.parse(fs.readFileSync(officialModulesJsonPath));
            frontData['Modules'] = officialModulesJson['Modules'].map(m => {
                let files = fs.readdirSync(path.join(cellModulesPath, m['DirName'])).filter(file=>/(\.([hc]pp|h)|^config.json)$/.test(file))
                files = Object.fromEntries(files.map(file => ([file,fs.readFileSync(path.join(cellModulesPath, m['DirName'], file),{ encoding: 'utf8', flag: 'r' })])))
                return generateDocModule(files, moduleKeysToKeep.reduce((acc, curr) => { if (curr in m) acc[curr] = m[curr]; return acc }, {}))
            })
        } catch (err) {
            console.error(err)
            error = err
        }
        res.send(JSON.stringify({
            info: "Getting the front.json (return it directly as script output?)",
            error: error,
            defaultJson: frontData
        }));
    });

    app.get('/api/module/:dirName', async (req, res) => {
        lr.record(req, "getModule")
        let dirName = req.params.dirName
        try{
            pathModule = joinSubDirSafe(cellModulesPath, dirName)
            let files = {}
            fs.readdirSync(pathModule).forEach(file => {
                if (['h', 'cpp', 'hpp','json'].indexOf(file.split('.').pop()) !== -1) {
                    files[file] = fs.readFileSync(path.join(pathModule, file)).toString()
                }
            })
            res.send(JSON.stringify(files));
        } catch (err) {
            console.error(err)
            res.status(500).send({ message: err });
        }
    });
}