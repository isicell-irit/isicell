
const path = require('path');
const fs = require('fs');
const { simulationsPath } = require("../service/isiCellPath");
const { spawn } = require('child_process');
const {  getMultiStep, getMaxStep, deleteSimu, getListTokenTable } = require('../service/db');
const { validateToken, joinSubDirSafe } = require('../service/safePathCheck');


const runsParamsPath = 'config/runsParams'

var processRunning = [];

module.exports = function(app,lr,users, dbInfo){

    app.post('/api/launch/:builderToken', async (req, res) => {
        try {

            //res.set({'Content-Type':'text/event-stream','Cache-Control':'no-cache','Connection':'keep-alive'})
            //res.flushHeaders();
            let builderToken,pathBuild;
            
            try{
                builderToken = validateToken(req.params.builderToken);
                pathBuild = joinSubDirSafe(simulationsPath, builderToken)
            } catch(err){
                console.error(err)
                res.status(500).send({ message: err });
                return;
            }
        
            if (!fs.existsSync(path.join(pathBuild, 'build/console'))) {
                res.status(500).send({ 'buildNotFound': true });
                return null;
            }
            lr.record(req, "lauchSimu")
            if (req.session.loggedin) users.using(req.session.username, 'viewer')
            let dt = new Date();
            let token = 'web'.concat(dt.getFullYear(), (dt.getMonth() + 1), dt.getDate(), "_", dt.getHours(), dt.getMinutes(), '_', Math.floor(Math.random() * 1000));
            // Enregistre le {token}.json <- pour l'app C++
            let params = { main: { ...dbInfo } };
            params.input = req.body;
            params.main.id = token;
            let currentParamsPath = path.join(pathBuild, runsParamsPath, 'params_' + token + '.json')
            fs.writeFileSync(currentParamsPath, JSON.stringify(params, 'utf8', 2));

            // child.process qui lance la simulation avec en paramètre mon fichier {token}.json
            let processSimu = spawn(path.join(pathBuild, 'build/console'), [currentParamsPath],{uid:users.getUID(req.session.username),gid:users.isiCellGID,env:users.getEnv(req.session.username)})

            res.writeHead(200, { 'Content-Type': 'text/event-stream', 'Cache-Control': 'no-cache', 'Connection': 'keep-alive' });

            let errStream = "", lastErrStream = "";
            processSimu.stderr.on('data', function (data) {
                errStream = data.toString();
                if (errStream !== lastErrStream) {
                    res.write(JSON.stringify({ errLog: errStream })); res.flush();
                    errStream = lastErrStream;
                }
            });
            let outStream = "", lastOutStream = "";
            processSimu.stdout.on('data', function (data) {
                outStream = data.toString();
                if (outStream !== lastOutStream) {
                    res.write(JSON.stringify({ outLog: outStream })); res.flush();
                    outStream = lastOutStream;
                }
            });
            processSimu.on('close', (code) => {
                console.log(code);
                res.end();
                delete processRunning[token];
                console.log("close [ip:" + req.session.realip + "] " + token);
            });
            processSimu.on('error', (err) => {
                console.log(err);
                res.end();
                delete processRunning[token];
                console.log("error [ip:" + req.session.realip + "] " + token);
            });
            processRunning[token] = processSimu;
            console.log("api : [ip:" + req.session.realip + "] new simulation launch with token :" + token + "\t\t pid=" + processSimu.pid);
            // return token
            res.write(JSON.stringify({ token: token })); res.flush();
        } catch (err) {
            console.error(err);
            res.status(500).send({});
        }
    });


    app.post('/api/simu/multistep/:token', async (req, res) => {

        try{
            token = validateToken(req.params.token);
        } catch(err){
            console.error(err)
            res.status(500).send({ message: err });
            return;
        }
        let isRunning = processRunning.hasOwnProperty(token)
        let info = req.body.infoNeed;
        let infoOnTime = req.body.infoNeedOnTime;
        console.log("api : [ip:" + req.session.realip + "] resqest MULTI data :: " + token);
        getMultiStep(token, info, infoOnTime).then(data => {
            data.isRunning = isRunning;
            res.send(data);
        }).catch(error=>{
            res.send({});
        })
    });

    app.post('/api/kill/:token', async (req, res) => {
        lr.record(req, "kill")
        let token;
        try{
            token = validateToken(req.params.token);
        } catch(err){
            console.error(err)
            res.status(500).send({ message: err });
            return;
        }
        let info = JSON.parse(req.body);
        if (processRunning.hasOwnProperty(token)) {
            console.log("api : [ip:" + req.session.realip + "] kill process " + token + "\tmethod :" + info.event);
            processRunning[token].kill();//spawn("kill", ["-9", ]);
        } else {
            console.log("api : [ip:" + req.session.realip + "] echec try to kill process" + token + "\tmethod :" + info.event);
        }
        if (info.deleteSimu) {
            deleteSimu(token);
            console.log("\tdata deleted from data base");
        }
        res.send({ ok: true })
    });


    app.get('/api/simumaxstep/:token', async (req, res) => {
        //info  :   type,state,pressure,infla,reso,eatme
        let token;
        try{
            token = validateToken(req.params.token);
        } catch(err){
            console.error(err)
            res.status(500).send({ message: err });
            return;
        }
        //console.log("api : [ip:" + req.session.realip + "] resqest max step " + token);
        try {
            const maxStep = await getMaxStep(token);
            const data = {
                maxStep: maxStep,
                isRunning: processRunning.hasOwnProperty(token)
            };
            res.send(data);
        } catch (error) {
            res.send({});
        }
    });

    app.get('/api/listtokensimu',users.protectedAdmin,async (req,res)=>{
        res.send({data:(await getListTokenTable()).map(t=>({token:t,isRunning:processRunning.hasOwnProperty(t)}))})
        res.end()
    })

}