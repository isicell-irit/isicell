const { simulationsPath, isicellPath, demosPath, scriptsPath, userFolder } = require("../service/isiCellPath");
const { exec } = require('child_process');
const path = require('path');
const fs = require('fs');
const zipDir = require('zip-dir');
const { validateToken, joinSubDirSafe } = require("../service/safePathCheck");
function generateToken() {
    let dt = new Date();
    return 'sim_'.concat(dt.getFullYear(), (dt.getMonth() + 1), dt.getDate(), "_", dt.getHours(), dt.getMinutes(), '_', Math.floor(Math.random() * 1000));
}

demosToken = []
module.exports = function(app,lr,users,dbInfo){

    if (process.env.NODE_ENV !== 'dev')
        Promise.all(fs.readdirSync(demosPath).map(file => new Promise((resolve, reject) => {
            let finalJson = JSON.parse(fs.readFileSync(path.join(demosPath, file))).finalJson
            let cellJson = Buffer.from(JSON.stringify(finalJson, 'utf8')).toString('base64')
            const token = file.split('.')[0] 
            demosToken.push(token)
            exec(`cd ${simulationsPath} && python3 ${path.join(scriptsPath,'mainGenerator.py')} ${token} sql "${cellJson}"`,{uid:users.guestUID,gid:users.isiCellGID,env:users.getEnv('guest')} ,(error, stdout, stderr) => {
                if (error) {
                    console.warn(error);
                }
                resolve(stdout ? stdout : stderr);
            });
        }))).then(() => {
            console.log('demos compilation finish',demosToken)
        })


    app.post('/api/compile',users.protected, async (req, res) => {
        lr.record(req, "compileSimu")
        users.using(req.session.username, 'builder')
        console.log("Compiling")
        let config = "";
        let token = req.body.token || generateToken()
        let type = undefined
        let pathBuild = undefined 
        try {
            config = req.body.data;
            try{
                token = validateToken(token);
                pathBuild = joinSubDirSafe(simulationsPath, token)
            } catch(err){
                console.error(err)
                res.status(500).send({ message: err });
                return;
            }
            if(req.body.type==='debug') type='debug'
            else type = req.body.type==='sql'?'sql':(req.body.type==='python'?'python':'all');
            
            console.log('currentToken', token)
            if (!token || !fs.existsSync(pathBuild) || demosToken.includes(token)) {
                token = generateToken();
                pathBuild = path.join(simulationsPath, token)
                console.log('new token', token)
            }
            cellJson = Buffer.from(JSON.stringify(config, 'utf8')).toString('base64')
            exec(`cd ${path.join(userFolder,req.session.username)} && python3 ${path.join(scriptsPath,'mainGenerator.py')} ${token} ${type} "${cellJson}"`,{uid:users.getUID(req.session.username),gid:users.isiCellGID,env:users.getEnv(req.session.username,true)}, (err, stdout, stderr) => {
                if(type !== 'debug'){
                    console.log(err ? "Error" : "Done", token)
                    res.send(JSON.stringify({
                        error: err ? stderr.replaceAll(pathBuild,'') : undefined,
                        token: token
                    }, 'utf8'));
                } else {
                    console.log(err ? "Error" : "Done", token)
                    if(err){
                        res.send(JSON.stringify({
                            error: stderr,
                            token: token
                        }, 'utf8'));
                    } else {
                        let params = { main: { ...dbInfo } };
                        params.input = req.body.params;
                        params.main.id = 'debug';
                        let currentParamsPath = path.join(pathBuild, 'config/runsParams/debug.json')
                        fs.writeFileSync(currentParamsPath, JSON.stringify(params, 'utf8', 2));

                        exec(`cd ${path.join(pathBuild,'build')}; script -eqc 'gdb -ex "set pagination off" -ex "set confirm off" -ex "run ../config/runsParams/debug.json" -ex "bt" -ex "quit" --args ./console' .typescript; rm .typescript`,{uid:users.getUID(req.session.username),gid:users.isiCellGID,env:users.getEnv(req.session.username,true)}, (errGDB, stdoutGDB, stderrGDB) => {
                            res.send(JSON.stringify({
                                error: stdout.replaceAll(pathBuild,''),
                                gdb: (stdoutGDB+'\n\n'+stderrGDB).replaceAll(pathBuild,''),
                                token: token
                            }, 'utf8'));
                        })
                    }
                }
            })
        }
        catch (err) {
            console.log("Error", token)
            console.log(err)
            res.send(JSON.stringify({
                error: err,
                token: token
            }, 'utf8'));
        }
    });


    app.post('/api/generateDemo', users.protectedAdmin, async (req, res) => {
        console.log("api : [ip:" + req.session.realip + "] generate demo " + req.body.name);
        let name = req.body.name.replace(' ', '_').replace('.', '_')
        demosToken.push(name)
        let jsonDemoPath = path.join(demosPath, req.body.name + '.json')
        fs.writeFileSync(jsonDemoPath, JSON.stringify({ description: req.body.description, xml: req.body.xml, finalJson: req.body.finalJson, svg: req.body.svg, D3: req.body.D3 }, 'utf8'));
        fs.chownSync(jsonDemoPath, users.guestUID, users.isiCellGID)
        let cellJson = Buffer.from(JSON.stringify(req.body.finalJson, 'utf8')).toString('base64')
        
        exec(`cd ${simulationsPath} && python3 ${path.join(scriptsPath,'mainGenerator.py')} ${name} sql "${cellJson}"`,{uid:users.guestUID,gid:users.isiCellGID,env:users.getEnv('guest')} ,(error, stdout, stderr) => {
            if (error) {
                console.error(error)
            }
            res.send({error});
        });
    });

    app.get('/api/checksum/:token', async (req, res) => {
        let folderSim;
        try{
            folderSim = joinSubDirSafe(simulationsPath, req.params.token || '_')
        } catch(err){
            console.error(err)
            res.status(500).send({ message: err });
            return;
        }
        if (!fs.existsSync(folderSim)) { res.send({err:'token not exist'}); return; }
        fs.readFile(path.join(folderSim, 'checksum.txt'), 'utf8', function (err, data) {
            if (err) { res.send({ err:'checksum file not exist' }); return;}
            res.send({checksum:data});
        })
    })


    app.get('/api/download/:token', async (req, res) => {
      let token, pathBuild;
      try{
          token = validateToken(req.params.token);
          pathBuild = joinSubDirSafe(simulationsPath, token)
      } catch(err){
          console.error(err)
          res.status(500).send({ message: err });
          return;
      }
      if(!demosToken.includes(token) && !req.session.loggedin) {
        res.status(401).send({ message: "unauthorized action, please login" });
        return;
      }
      lr.record(req,"downloadProject")
      console.log("Downloading project " + token)
      // var error = undefined
      
      if(fs.existsSync(pathBuild)){
          zipDir(pathBuild, { filter: (path, stat) => !/(.*?\/build\/.*)/.test(path) },  function (err, buffer) {
            console.log(buffer, err)
            if(err !== null)
              res.status(500).send({message: err});
            else {
              res.setHeader('Content-disposition', 'attachment; filename='+token+'.zip');
              // res.setHeader('Content-type', buffer.mime_type);
              res.type('bin');
              res.send(buffer);
            }
          });
      }
      else{
        res.send(JSON.stringify({error: 'token undefined'},'utf8'))
      }
      console.log("DONE");
    });
}