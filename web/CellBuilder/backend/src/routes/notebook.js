const Notebook = require("../service/notebook");
const si = require('systeminformation');
const { validateToken } = require("../service/safePathCheck");

var notebookRunning = {};
module.exports = function(app,lr,users){



    app.post('/api/notebook/evaluate/:idnb/:builderToken', async (req, res) => {
        if (!req.session.loggedin) {
            res.send(Notebook.msgUnauthorized());
            return;
        }
        try {
            let idnb = req.params.idnb;
            let builderToken = req.params.builderToken;
            try{
                builderToken = validateToken(builderToken);
                idnb = validateToken(idnb);
            } catch(err){
                console.error(err)
                res.status(500).send({ message: err });
                return;
            }
            let params = req.body;
            if (!notebookRunning.hasOwnProperty(idnb)) {
                lr.record(req, "notebook")
                users.using(req.session.username, 'notebook')
                console.log("api : [ip:" + req.session.realip + '] running new Notebook, token=', idnb);
                notebookRunning[idnb] = new Notebook(req.session.username, "#_#COMMAND#_#", "#_#BLOCK#_#", "#_#STOP#_#", () => { delete notebookRunning[idnb] })
                notebookRunning[idnb].start(builderToken,users.getUID(req.session.username),users.isiCellGID,users.getEnv(req.session.username))
            } else if (notebookRunning[idnb].builderToken !== builderToken) {
                console.log("api : [ip:" + req.session.realip + '] restart Notebook, token=', idnb);
                notebookRunning[idnb].kill()
            }
            notebookRunning[idnb].execute(res, params.command)

        } catch (err) {
            console.error(err);
            res.end()
        }
    });

    app.get('/api/notebook/getNotebooksRunning',users.protectedAdmin, async (req,res)=>{
        res.writeHead(200, { 'Content-Type': 'application/json' });
        let dataNotebook = Object.keys(notebookRunning).map(n => { return { 'id': n, 'user': notebookRunning[n].user, 'lastUse': notebookRunning[n].lastDateUsed, 'pid': [notebookRunning[n].process?.pid], 'cpu': 0, 'mem': 0 } })
        
        si.processes((data)=>{
            let newChildPid = true
            while (newChildPid) {
                newChildPid = false
                for (let i = 0; i < dataNotebook.length;i++){
                    let childPid = Array.from(new Set([...dataNotebook[i].pid, ...data.list.filter(p => dataNotebook[i].pid.includes(p.parentPid)).map(p=>p.pid)]))
                    if (childPid.length > dataNotebook[i].length){
                        newChildPid=true
                    }
                    console.log(i, newChildPid)
                    dataNotebook[i].pid = childPid
                }
            }
            let pidInfo = Object.fromEntries(data.list.map(p => [p.pid, { mem: p.mem, cpu: p.cpu }]))
            for (let i = 0; i < dataNotebook.length; i++) {
                dataNotebook[i].cpu = dataNotebook[i].pid.reduce((total, item) => total + pidInfo[item]?.cpu|0,0)
                dataNotebook[i].mem = dataNotebook[i].pid.reduce((total, item) => total + pidInfo[item]?.mem|0,0)
            }

                
            res.end(JSON.stringify({ 'data': dataNotebook}));
        })
    })

    app.get('/api/notebook/getMyNotebooksRunning',users.protected, async (req,res)=>{
        res.writeHead(200, { 'Content-Type': 'application/json' });
        let dataNotebook = Object.keys(notebookRunning).filter(n=>notebookRunning[n].user === req.session.username).map(n => { return { 'id': n, 'lastUse': notebookRunning[n].lastDateUsed, 'pid': [notebookRunning[n].process?.pid], 'cpu': 0, 'mem': 0 } })
        
        si.processes((data)=>{
            let newChildPid = true
            while (newChildPid) {
                newChildPid = false
                for (let i = 0; i < dataNotebook.length;i++){
                    let childPid = Array.from(new Set([...dataNotebook[i].pid, ...data.list.filter(p => dataNotebook[i].pid.includes(p.parentPid)).map(p=>p.pid)]))
                    if (childPid.length > dataNotebook[i].length){
                        newChildPid=true
                    }
                    dataNotebook[i].pid = childPid
                }
            }
            let pidInfo = Object.fromEntries(data.list.map(p => [p.pid, { mem: p.mem, cpu: p.cpu }]))
            for (let i = 0; i < dataNotebook.length; i++) {
                dataNotebook[i].cpu = dataNotebook[i].pid.reduce((total, item) => total + pidInfo[item]?.cpu|0,0)
                dataNotebook[i].mem = dataNotebook[i].pid.reduce((total, item) => total + pidInfo[item]?.mem|0,0)
            }

                
            res.end(JSON.stringify({ 'data': dataNotebook}));
        })
    })


    app.get('/api/notebook/Interupt/:idnb',users.protected, async (req, res) => {
        let idnb = req.params.idnb
        if (notebookRunning.hasOwnProperty(idnb)) {
            notebookRunning[idnb].interrupt()
            console.log("api : [ip:" + req.session.realip + "] interupt notebook " + idnb + "\tmethod :");
        }
        res.send({ ok: true })
    });

    app.post('/api/notebook/kill/:idnb', async (req, res) => {
        let info = req.body;
        let idnb = req.params.idnb
        if (notebookRunning.hasOwnProperty(idnb)) {
            console.log("api : [ip:" + req.session.realip + "] kill notebook " + idnb + "\tmethod :" + info.event);
            notebookRunning[idnb].kill(() => { res.send({ ok: true }) });
        } else {
            console.log("api : [ip:" + req.session.realip + "] echec try to kill notebook " + idnb + "\tmethod :" + info.event);
            res.send({ ok: true })
        }

    });
}