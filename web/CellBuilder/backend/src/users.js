const crypto = require('crypto');
const fs = require('fs');
const { exec, execSync } = require("child_process");
const path = require('path');
const { userFolder, isicellPath, simulationsPath, scriptsPath, demosPath, configPath } = require('./service/isiCellPath');

const getHashedPassword = (password,salt) => {
    const sha256 = crypto.createHash('sha256');
    const hash = sha256.update(password+salt).digest('hex');
    return hash;
}
const generateAuthToken = () => {
    return crypto.randomBytes(64).toString('hex');
}

module.exports = class Users {
    constructor(filePath){
        this.filePath = filePath;
        this.isiCellGID = 3042;
        this.guestUID = 2041;
        this.setupFilesAccessBase()
        if (process.env.NODE_ENV !== 'dev') this.setupFilesAccess()
        if (fs.existsSync(this.filePath)) {
            this.data = JSON.parse(fs.readFileSync(this.filePath))
            Object.keys(this.data).forEach(name => ['login', 'viewer', 'builder', 'notebook'].forEach(k => { if (!this.data[name].stats.hasOwnProperty(k)) this.data[name].stats[k]=0 }))
            this.data['admin'].uid = 0
            let i = this.guestUID
            Object.keys(this.data).forEach(user => {
                if(this.data[user].uid === undefined) this.data[user].uid = ++i
            })
            this.currentUID = Object.keys(this.data).reduce((m, k)=>{ return this.data[k].uid > m ? this.data[k].uid : m }, 2042);
            Object.keys(this.data).forEach((user)=>this.createUserAndHomeDir(user))
        } else {
            const salt = generateAuthToken()
            this.data = { 'admin': {'uid':0, 'pwd': getHashedPassword("admin",salt), salt, 'lastUse': -1, 'status': 'active', 'stats': { 'login': 0, 'viewer': 0, 'builder': 0, 'notebook':0 }}}
        }
        this.updated = false;
        this.protected = (req, res, next) => {
            if (!req.session.loggedin) {
                res.status(401).send({ err: "unauthorized action, please login" });
            } else {
                req.user = this.data[req.session.username]
                next();
            }
        }
        this.protectedAdmin = (req, res, next) => {
            if (!req.session.loggedin) {
                res.status(401).send({ err: "unauthorized action, please login" });
            } else if(req.session.username === 'admin'){
                req.user = this.data[req.session.username]
                next();
            } else {
                res.status(401).send({ err: "unauthorized action" });
            }
        }
    }

    autoSave(){
        var self = this;
        setTimeout( () => {
            if(self.updated) self.save();
            self.updated = false;
            self.autoSave();
        }, 5 * 60000);
    }

    save(){
        console.log("database saved")
        fs.writeFileSync(this.filePath, JSON.stringify(this.data,null,2));
    }

    newUser(user) {
        this.data[user] = {'uid': ++this.currentUID,'pwd': generateAuthToken(), 'lastUse': Date.now(), 'status': 'pending', 'stats':{'login':0,'viewer':0,'builder':0, 'notebook': 0} };
        this.save();
        this.createUserAndHomeDir(user)
        return this.data[user].pwd
    }

    setPwd(user, pwd, status = undefined) {
        if(!this.data[user].salt) this.data[user].salt = generateAuthToken()
        this.data[user].pwd = getHashedPassword(pwd,this.data[user].salt);
        if (status) this.data[user].status = status;
        this.save();
    }

    getPwd(user) {
        return this.data[user]['pwd']
    }

    getUID(user) {
        if(user === undefined){
            return this.guestUID
        }
        else{
            return this.data[user]['uid']
        }
    }

    getEnv(user,base=false) {
        if(user==='guest' || user===undefined){
            return {PATH:`/opt/venv/.base/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`,
                    VIRTUAL_ENV: `/opt/venv/.base/bin`,
                    HOME:''}

        }
        return {PATH:`/opt/venv/${base?'.base':user}/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin`,
                VIRTUAL_ENV: `/opt/venv/${base?'.base':user}/bin`,
                HOME:path.join(userFolder,user),
                MPLCONFIGDIR: `/opt/venv/${user}/tmp`,
                TERM: "xterm-256color",
                USER:user}
    }

    isValidLoginNewUser(user, pwd) {
        if (user in this.data)
            if (this.data[user].pwd === pwd) {
                return true;
            }
        return false;
    }
    isValidLogin(user,pwd){
        if(user in this.data)
            if (this.data[user].pwd === getHashedPassword(pwd,this.data[user].salt||'')){
                this.using(user,'login')
                return true;
            }
        return false;
    }

    using(name,type){
        this.data[name].stats[type]+=1;
        this.data[name].lastUse = Date.now();
        this.updated = true;
    }

    getInfo(){
        let data = []
        for(let name in this.data){

            data.push({ 'name':name,
                'lastUse': this.data[name].lastUse,
                'status': this.data[name].status,
                'url': this.data[name].status === 'pending' ? 'setPwd/' + this.data[name].pwd + '/' + name:'',
                'stats': this.data[name].stats
            })
        }
        return data
    }

    resetPassword(username){
        this.data[username].status = 'pending'
        this.data[username].pwd = generateAuthToken()
        this.data[username].salt = undefined
        this.save();
        return this.data[username].pwd;
    }

    deleteUser(username){
        delete this.data[username];
        this.save();
    }

    setupFilesAccess(){
        execSync(`chmod o-rwx -R ${isicellPath}/* &&
                find ${isicellPath}/* -maxdepth 0 \\( -name simulations -o -name  homes -o -name .devcontainer \\) -prune -o -exec chown root:root -R {} \\;  &&
                mkdir -p ${simulationsPath} &&
                chown :isicellUser -R ${simulationsPath} &&
                chmod o+rx,g+rwx -R ${simulationsPath} &&
                chown :isicellUser -R ${scriptsPath} &&
                chmod g+r -R ${scriptsPath} &&
                chown :isicellUser -R ${configPath} &&
                chmod g+r -R ${configPath} &&
                chmod g+r -R ${path.join(isicellPath,'core')} &&
                chown :isicellUser -R ${path.join(isicellPath,'core')} &&
                chmod g+r -R ${userFolder} &&
                chown :isicellUser ${userFolder}
            `)
    }

    setupFilesAccessBase(){
        try{
            let stdout = execSync(`grep '^guest\\:' /etc/passwd`).toString()
            if (stdout.includes('guest')) {
                return;
            }
        }catch(e){}
        execSync(`groupadd -f -g ${this.isiCellGID} isicellUser ;
            chown :isicellUser -R /opt/venv/.base/bin &&
            chmod g-w+x /opt/venv/.base/bin &&
            useradd -N -M -u ${this.guestUID} -g ${this.isiCellGID} guest
            `)
    }

    createUserAndHomeDir(user){
        exec(`grep '^${user}\\:' /etc/passwd`, (error, stdout, stderr) => {
            let homeDir = path.join(userFolder,user)
            if(user === 'admin' && fs.existsSync('/opt/venv/admin') ) return;
            let userRealName = user
            if(user==='admin') userRealName = 'root'
            let uid = this.data[user].uid;
            if(!stdout.includes(user)){
                exec(`useradd -N -M -u ${uid} -g ${this.isiCellGID} -p ${crypto.randomBytes(64).toString('hex')} ${userRealName} ;
                      mkdir -p ${homeDir} &&
                      chown -R ${userRealName}:isicellUser ${homeDir} &&
                      chmod 700 -R ${homeDir} &&
                      virtualenv-clone /opt/venv/.base /opt/venv/${user} &&
                      mkdir -p /opt/venv/${user}/tmp &&
                      chown -R ${userRealName}:isicellUser /opt/venv/${user} &&
                      chmod 700 -R /opt/venv/${user}`, (error, stdout, stderr) => {
                    if (error) {
                        console.log(`error: ${error.message}`);
                        return;
                    }
                    if (stderr) {
                        console.log(`stderr: ${stderr}`);
                        return;
                    }
                });
            } else {
                exec(`chown -R ${userRealName}:isicellUser ${homeDir} &&
                      chmod 700 -R ${homeDir}`)
            }
        });
    }

    
}