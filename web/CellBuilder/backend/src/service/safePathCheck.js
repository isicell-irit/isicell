const path = require('path');

function validateToken(token) {
    if (token.indexOf('\0') !== -1) {
        throw new Error('Poison Null Bytes Attack');
    }
    if (!/^[a-zA-Z0-9\_\-]+$/.test(token)) {
        throw new Error('Unvalid user input');
    }
    return token;
}
function isSubPathSafe(parent,user_path){
    if (user_path.indexOf('\0') !== -1) {
        throw new Error('Poison Null Bytes Attack');
    }
    const relative = path.relative(parent, user_path);
    return relative && !relative.startsWith('..') && !path.isAbsolute(relative);
}
function subPathSafe(parent,user_path){
    if (user_path.indexOf('\0') !== -1) {
        throw new Error('Poison Null Bytes Attack');
    }
    const relative = path.relative(parent, user_path);
    if(!relative.startsWith('..') && !path.isAbsolute(relative)){
        return path.join(parent,relative)
    } else {
        console.log(parent,user_path,relative)
        throw new Error('Path traversal attack');
    }
}
function joinSubDirSafe(root, dirName) {
    validateToken(dirName)
    const normalized_dirName = path.normalize(dirName)
    const safe_dirName = normalized_dirName.replace(/^(\.\.(\/|\\|$))+/, '');
    if(normalized_dirName !== safe_dirName){
        throw new Error('Unvalid user input');
    }
    const path_string = path.join(root, safe_dirName);
    if (path_string.indexOf(root) !== 0) {
        throw new Error('Path traversal attack');
    }
    return path_string;
}

module.exports = {
    joinSubDirSafe,
    validateToken,
    isSubPathSafe,
    subPathSafe
};
