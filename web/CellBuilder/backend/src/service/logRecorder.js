
module.exports = class logRecorder {
    
    constructor() {
        this.data = []
    }

    record(req,key){
        this.data.push({ date: Date.now(), ip: req.session.realip, username: req.session.username?req.session.username :'anonymous', key,url:req.originalUrl})
    }

    getData(){
        return this.data
    }
}