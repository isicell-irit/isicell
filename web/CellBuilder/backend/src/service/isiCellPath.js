
const path = require('path'); 
const CellbuilderPath = path.join(__dirname, '../../..');
const isicellPath = path.join(CellbuilderPath, '../..');
const demosPath = path.join(CellbuilderPath, 'demos');
const frontendPath = path.join(CellbuilderPath, 'public');
const configPath = path.join(isicellPath, 'config');
const simulationsPath = path.join(isicellPath, 'simulations');
const defaultJsonPath = path.join(configPath, 'default.json');
const officialModulesJsonPath = path.join(configPath, 'officialModules.json');
const cellModulesPath = path.join(isicellPath, 'core/modules/cellModules');
const mecaCellPath = path.join(isicellPath, 'core/external/MecaCell/mecacell');
const coreSrcPath = path.join(isicellPath, 'core/src');
const scriptsPath = path.join(isicellPath, 'scripts');
const docPath = path.join(isicellPath, 'core/html');
const userFolder = path.join(isicellPath, 'homes');

module.exports = {
    CellbuilderPath, isicellPath, demosPath, frontendPath, configPath, simulationsPath, defaultJsonPath, officialModulesJsonPath, cellModulesPath, mecaCellPath, coreSrcPath, scriptsPath, docPath, userFolder
}