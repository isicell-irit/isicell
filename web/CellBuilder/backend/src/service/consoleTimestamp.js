module.exports = function () {
    originalLog = console.log;
    // Overwriting
    console.log = function () {
      var args = [].slice.call(arguments);
      originalLog.apply(console.log,[getCurrentDateString()].concat(args));
    };
    // Returns current timestamp
    function getCurrentDateString() {
      return (new Date()).toLocaleDateString('fr-FR',{ year: "numeric", month: "2-digit", day: "2-digit",hour:"2-digit",minute:"2-digit",second:"2-digit"}) + ' ::';
    };
  }