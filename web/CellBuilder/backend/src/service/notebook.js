
const path = require('path');
const { simulationsPath, scriptsPath, userFolder } = require('./isiCellPath');
const { spawn, exec } = require('child_process');

const msgKeyInterupt = JSON.stringify({
    "ename": "KeyboardInterrupt",
    "evalue": "",
    "output_type": "error",
    "traceback": ["\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
        "\u001b[0;31mKeyboardInterrupt\u001b[0m: "]
})

const msgUnauthorized = JSON.stringify({
    "ename": "KeyboardInterrupt",
    "evalue": "",
    "output_type": "error",
    "traceback": ["\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
        "\u001b[0;31mUnauthorized action\u001b[0m: please login"]
})


module.exports = class Notebook {

    constructor(user, tokenCommand, tokenBlock, tokenInterrupt,actionToKill){
        this.tokenCommand = tokenCommand;
        this.tokenBlock = tokenBlock;
        this.actionToKill = actionToKill;
        this.tokenInterrupt = tokenInterrupt;
        this._buffer = '';
        let safeTokenBlock = this.tokenBlock.replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1")
        let reBlock = new RegExp('(?<beforeBlock>.*?)' + safeTokenBlock + "(?<block>.*?)" + safeTokenBlock + '|(?<beforeBuffer>.*?)(?<buffer>' + safeTokenBlock + '.*)', 'sg');
        this.extractBlock = (txt) => { 
            if (txt.includes(this.tokenInterrupt)) {
                this._buffer = ''
                return { output: '', blocks: [msgKeyInterupt], end: true }
            }
            let res = {output:'',blocks:[],end:false}
            txt = this._buffer + txt
            res.end = txt.includes(this.tokenCommand)
            if(res.end) txt=txt.replace(this.tokenCommand,'')
            this._buffer = ''
            for(const match of txt.matchAll(reBlock)){
                if (match.groups.block){
                    res.output += match.groups.beforeBlock
                    res.blocks.push(match.groups.block)
                } else {
                    res.output += match.groups.beforeBuffer
                    this._buffer = match.groups.buffer
                }
            }
            res.output += txt.replaceAll(reBlock, '')
            return res
        }
        this.process = null;
        this.user = user;
        this._callback = {
            default: (txt,name) => { },
            send: null
        }
    }

    start(builderToken, uid, gid, env){
        this.builderToken = builderToken;
        this.ownerInfo={uid,gid,env}
        this.process = spawn("python", ["-u", path.join(scriptsPath, 'kernelIsiCell.py'), this.tokenCommand, this.tokenBlock, this.tokenInterrupt ,path.join(simulationsPath, builderToken), path.join(userFolder,this.user)],{uid,gid,env:env})
        this.process.stdout.on('data', (data) => {
            if (this._callback.send) this._callback.send(data.toString(),'stdout')
            else this._callback.default(data.toString())
        });
        this.process.stderr.on('data', (data) => {
            if (this._callback.send) this._callback.send(data.toString(),'stderr')
            else this._callback.default(data.toString())
        });
        this.process.on('close', (code) => {
            this.actionToKill();
            if (this._callback.send) this._callback.send(this.tokenInterrupt)
        })
    }

    interrupt() {
        this.process.stdin.write(this.tokenInterrupt + '\n');
    }
    execute(res,command){
        this.lastDateUsed = Date.now()
        this._restTimeout();
        let sendMsgToMaintainConnection = () => {
            clearTimeout(this.keepActiveConnection)
            res.write(JSON.stringify({ 'ping': 0 }));
            res.flush();
            this.keepActiveConnection = setTimeout(sendMsgToMaintainConnection, 8 * 60 * 1000)
        }

        this.keepActiveConnection = setTimeout(sendMsgToMaintainConnection, 8 * 60 * 1000)
        this._callback.send = (txt,name)=>{
            let data = this.extractBlock(txt)
            if (data.output!==''){
                res.write(JSON.stringify({ stream: data.output, name }));
                res.flush();
            }
            data.blocks.forEach(b => {
                res.write(b);
                res.flush();
            })
            clearTimeout(this.keepActiveConnection)
            if (data.end) {
                this._callback.send = this._callback.default
                res.end()
            } else {
                this.keepActiveConnection = setTimeout(sendMsgToMaintainConnection, 8 * 60 * 1000)
            }
        }
        this.process.stdin.write(command+'\n'+ this.tokenCommand + '\n');
    }

    kill(cb) {
        if (this.process?.stdin) {
            if (this._callback.send !== this._callback.default && this._callback.send !== null ){
                this.interrupt();
                let waitingStop = setInterval(() => {
                    if (this._callback.send === this._callback.default){
                        clearInterval(waitingStop);
                        this.kill(cb)
                    }
                },500)
                return
            }
            this.process.stdin.destroy();
        }
        if (this.process?.stdout) this.process.stdout.destroy();
        if (this.process?.stderr) this.process.stderr.destroy() ;
        if(this.process?.pid)
        exec("{ echo " + this.process.pid + "; ps -o pid --ppid " + this.process.pid + " --no-heading; } | xargs kill -9",this.ownerInfo);
        this.process?.kill();
        exec('pkill -9 -P 1 -f kernelIsiCell.py',this.ownerInfo)
        this.process = null;
        this.buffer = ''
        this._callback.send = null;
        if(cb) cb()
        this.actionToKill();
    }

    _restTimeout(){
        if(this.timeoutKill){
            clearTimeout(this.timeoutKill);
        }
        this.timeoutKill = setTimeout(() =>this.kill(),24*3600*1000);
    }

    static msgUnauthorized(){
        return msgUnauthorized;
    }
}