# PrimoCell

## Overview

`PrimoCell` is a template class that represents a cell in the ISiCell framework. It allowing users to define their own cell states behaviors, and transitions. The class uses a combination of C++ and Python code injection (via special "isigen" comments) to dynamically generate and manage cell-specific code.

## Key Features

- **Customizable Cell Behaviors**: Define custom behaviors for different cell states.
- **State Transitions**: Manage state transitions based on user-defined conditions.
- **Integration with ISiCell Modules**: Seamlessly integrate with various ISiCell modules for extended functionality.

## Special Comments ("isigen")

The `PrimoCell` class contains special comments marked with "isigen". These comments contain Python code that is used to dynamically generate and inject C++ code based on user input from the ISiCell web interface. This allows for flexible and dynamic customization of cell behaviors and properties.

### isigen:insert@includes

This section dynamically includes necessary header files based on the modules configured in the ISiCell web interface. The Python code iterates through the module configurations and generates `#include` directives for the required header files.

### isigen:insert@behave

This section generates the C++ code for the `behave` method, which defines the behavior of the cell in different states. The Python code iterates through the states defined in the ISiCell web interface and generates switch-case statements for each state.

### isigen:insert@nextState

This section generates the C++ code for the `nextState` method, which defines the state transitions for the cell. The Python code iterates through the transitions defined in the ISiCell web interface and generates switch-case statements for each transition.

### isigen:insert@attributes

This section generates the C++ code for the cell attributes. The Python code iterates through the attributes defined in the ISiCell web interface and generates member variable declarations for each attribute.

### isigen:insert@functions

This section generates the C++ code for the cell functions. The Python code iterates through the functions defined in the ISiCell web interface and generates function definitions for each function.

### isigen:insert@cellModuleFunctions

This section generates the C++ code for the cell module functions. The Python code iterates through the module configurations and generates function definitions for each module function.

### isigen:insert@init

This section generates the C++ code for the `init` method, which initializes the cell. The Python code generates the initialization code and state transition conditions defined in the ISiCell web interface.

[More details in source code of PrimoCell.hpp](_primo_cell_8hpp_source.html)
