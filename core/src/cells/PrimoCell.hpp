/**
 * @file PrimoCell.hpp
 * @brief Defines the PrimoCell class template for representing a cell in the simulation.
 */

#ifndef BASEPROJECT_Cell_HPP
#define BASEPROJECT_Cell_HPP

#include <mecacell/mecacell.h>
#include "../core/Model.hpp"
#include "../tools/TimeConvert.hpp"
#include "../tools/RandomManager.hpp"

/*isigen:eval@utilityDeclarations'''
	from string import Template
	from functools import cmp_to_key
	caseTemplate = Template("""
	case State::$name : {
	\t$code
	\tbreak;
	}""")
	def buildConditionsCode(destinations):
		destinations = sorted(destinations, key=cmp_to_key(lambda a, b: (a["condition"] == '')-(b["condition"] == '')))
		allCode = ''
		for i,d in enumerate(destinations):
			action = 'this->state = State::{state}'.format(state=d["to"]) if d['to'] != 'DeadCell' else 'this->die()'
			if (i == 0) & (d["condition"] != ''): code = 'if({condition}) {action};\n'
			elif (i == 0) & (d["condition"] == ''): code = '{action};\n'
			elif d["condition"] == '': code = 'else {action};\n'
			else: code = 'else if({condition}) {action};\n'
			allCode += code.format(condition=d["condition"],action=action);
		return allCode;
	'''*/

/*isigen:insert@includes'''
	includes = ''
	for m,configJson in modulesConfigs.items():
		if "Body" in configJson:
			includes += '\n'.join(["#include \"../../{cellModulesPath}/{moduleName}/{cellInclude}\""\
				.format(cellModulesPath=cellModulesPath,moduleName=m,cellInclude=ci) for ci in configJson["Body"]["cellIncludes"] ])+'\n'
	print(includes)
	'''*/

using Vec = MecaCell::Vec;

/**
 * @class PrimoCell
 * @brief Represents a cell in the simulation.
 * 
 * @tparam B The base class template.
 */
template<template<typename> class B>
class PrimoCell : public MecaCell::ConnectableCell<PrimoCell<B>, B> {
	using Base = MecaCell::ConnectableCell<PrimoCell<B>, B>;
public:
	using Cell = PrimoCell;
	using World = MecaCell::World<PrimoCell<B>>;
private:
	/// @ignore
	static inline int id_count = 0;
	/// @ignore
	Model* model;
	Type type;
	State state;
	World* world;
	/// @ignore
	bool _alreadyNextState = false;


	/// @ignore
	void behave() {
		switch(this->state) {
			/*isigen:insert@behave'''
				behaviour = ''
				for s in builderData["cell"]["States"]:
					behaviour += caseTemplate.substitute({'name':s['name'], 'code': s['code'].replace('\n','\n\t')})
				print(behaviour)
				'''*/
		default:
		{
			cout << "UNSPECIFIED CELL STATE : " << static_cast<unsigned int>(this->state) << endl;
			break;
			}
		}
	}

	/// @ignore
	void nextState() {
		switch(this->state){
			/*isigen:insert@nextState'''
				transition = ''
				for t in builderData["cell"]["Transitions"]:
					transition += caseTemplate.substitute({'name': t['from'], 'code': buildConditionsCode(t['destinations']).replace('\n','\n\t')})
				print(transition)
				'''*/
		default:
		{
			//cout << "UNSPECIFIED CELL STATE : " << static_cast<unsigned int>(this->state) << endl;
			break;
			}
		}
	}

	/**
	 * @brief Creates a new cell.
	 * 
	 * The new cell is initialized with the same Type as the mother cell and added to the world.
	 * 
	 * @return Pointer to the newly created cell.
	 */
	Cell * createCell(){
		int idMother = this->cell_id;
		this->cell_id = PrimoCell::id_count++;
		auto *daughter = new PrimoCell(this);
		daughter->mother_id = idMother;
		this->mother_id = idMother;
		world->addCell(daughter);
		daughter->setType(this->type);
		daughter->init();
		return daughter;
	}
	
	/**
	 * @brief Creates a new cell of a specific type.
	 *
	 * The new cell is initialized with the specified type and added to the world.
	 * 
	 * @param t The type of the new cell.
	 * @return Pointer to the newly created cell.
	 */
	Cell * createCell(Type t){
		int idMother = this->cell_id;
		this->cell_id = PrimoCell::id_count++;
		auto *daughter = new PrimoCell(this);
		daughter->mother_id = idMother;
		this->mother_id = idMother;
		world->addCell(daughter);
		daughter->setType(t);
		daughter->init();
		return daughter;
	}

public:
	/// @ignore
	int cell_id = 0;
	/// @ignore
	int mother_id = -1;

    /**
     * @brief Sets the world for the cell.
     * 
     * @param w Pointer to the world.
     */
    void setWorld(World* w) {
        this->world = w;
    }
	/*isigen:insert@attributes'''
		print('\n'.join(["\t{type} {value};".format(type=a['type'], value=a['value']) for a in builderData["cell"]["attributes"] if a['source'] != 'core']))
		'''*/


	/*isigen:insert@functions'''
		print('\n'.join([code.replace('\n','//isigen:metaDataDebug['+re.search(r'(\w+[\d_\w]+)\s*\(',code).group(1)+'] '+'{"idFunc":"'+idFunc+'"}\n',1) for idFunc,code in builderData["cell"]["functions"].items()]))
		'''*/

	
	/*isigen:insert@cellModuleFunctions'''
		print('\n'.join([ '\n'.join([c['code'] for c in v['cellFunctions']]) for v in modulesConfigs.values() if 'cellFunctions' in v]))
		'''*/

	/**
	 * @brief Constructs a PrimoCell with a position and model.
	 * 
	 * @param v The position vector.
	 * @param m Pointer to the model.
	 */
	PrimoCell(const MecaCell::Vec &v, Model * m)
	:Base(v), model(m), type((Type)0)
	{
		this->getBody().setPosition(v);
		this->cell_id = PrimoCell::id_count++;
	}

	/**
	 * @brief Constructs a PrimoCell with a model.
	 * 
	 * @param m Pointer to the model.
	 */
	PrimoCell(Model * m)
	:Base(MecaCell::Vec(0.,0.,0.)), model(m), type((Type)0)
	{
		this->getBody().setPosition(MecaCell::Vec(0.,0.,0.));
		this->cell_id = PrimoCell::id_count++;
	}

	/**
	 * @brief Constructs a PrimoCell from a mother cell.
	 * 
	 * @param mother Pointer to the mother cell.
	 */
	PrimoCell(PrimoCell<B> *mother)
	:Base(mother->getBody().getPosition()), 
	model(mother->getModel()), type((Type)0)
	{
		this->getBody().setPosition(mother->getBody().getPosition());
		this->cell_id = PrimoCell::id_count++;
	}

	/**
	 * @brief Initializes the cell.
	 * 
	 * This function resets the cell's parameters and calls the code inside the "Cell Instantiation".
	 */
	void init() {
		model->initCell(this);
		/*isigen:insert@init'''
			initCode = builderData["cell"]["initializationCode"]
			if len(builderData["cell"]["initializationTransitions"])>0:
				destinations = builderData["cell"]["initializationTransitions"][0]["destinations"].copy()
				print(initCode+'\n'+buildConditionsCode(destinations))
			'''*/
		this->_alreadyNextState = true;
	}

	/// @ignore
	Model * getModel() const { return model; }

	/**
	 * @brief Gets the type of the cell.
	 * 
	 * @return The type of the cell.
	 */
	Type getType() const { return type; }

	/**
	 * @brief Sets the type of the cell and modifies parameters accordingly.
	 * 
	 * @param t The type to set.
	 */
	void setType(Type t) { type = t; model->initCell(this); }

	/**
	 * @brief Gets the state of the cell.
	 * 
	 * @return The state of the cell.
	 */
	State getState() { return state; } 

	/**
	 * @brief Sets the state of the cell.
	 * 
	 * @param s The state to set.
	 */
	void setState(State s) { state = s; this->_alreadyNextState = true; } 
	
	/**
	 * @brief Gets the radius of the cell.
	 * 
	 * @return The radius of the cell.
	 */
	double getRadius(){ return this->getBody().getBoundingBoxRadius(); }

	/**
	 * @brief Sets the radius of the cell.
	 * 
	 * @param _radius The radius to set.
	 */
	void setRadius(const double &_radius){ this->getBody().setRadius(_radius); }

	/**
	 * @brief Sets the position of the cell.
	 * 
	 * @param _pos The position vector.
	 */
	void setPosition(const MecaCell::Vec &_pos) { this->getBody().setPosition(_pos); }

	/**
	 * @brief Checks if the cell is of a specific type.
	 * 
	 * @param t The type to check.
	 * @return True if the cell is of the specified type, false otherwise.
	 */
	bool isType(Type t) {return this->type == t;}

	/**
	 * @brief Checks if the cell is in contact with a cell of a specific type.
	 * 
	 * @param t The type to check.
	 * @return True if the cell is in contact with a cell of the specified type, false otherwise.
	 */
	bool isInContactWith(Type t){ 
		for(auto *c : this->getConnectedCells())
			if(c->isType(t)) return true; 
		return false;
	}

	/**
	 * @brief Counts the number of neighboring cells of a specific type.
	 * 
	 * @param t The type to count.
	 * @return The number of neighboring cells of the specified type.
	 */
	int countNeighborType(Type t){
		int count = 0;
		for(auto *c : this->getConnectedCells())
			if(c->isType(t)) count++;
		return count;
	}

	/**
	 * @brief Checks if the cell is in contact with any other cell.
	 * 
	 * @return True if the cell is in contact with any other cell, false otherwise.
	 */
	bool isInContact(){ return (this->getConnectedCells().size() > 0); }

	/// @ignore
	void updateBehavior(){
		this->_alreadyNextState = false;
		behave();
		if(!this->_alreadyNextState) nextState();
	}

	/**
	 * @brief Gets the ID of the cell.
	 * 
	 * @return The ID of the cell.
	 */
	int getId() const { return this->cell_id; }

	/**
	 * @brief Gets the mother ID of the cell.
	 * 
	 * @return The mother ID of the cell.
	 */
	int getMotherId() const { return this->mother_id; }
};


#endif //BASEPROJECT_Cell_HPP
//'''
