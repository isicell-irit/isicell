/**
 * @file TimeConvert.hpp
 * @brief Provides utilities for converting between different time units.
 */

#ifndef TIMECONVERT
#define TIMECONVERT

namespace TimeConvert {

    /**
    * @brief Number of seconds in a minute.
    */
    static const double mnInS = 60.;

    /**
    * @brief Number of seconds in an hour.
    */
    static const double hInS = 3600.;

    /**
    * @brief Number of minutes in an hour.
    */
    static const double hInMn = 60.;

    /**
    * @brief Converts seconds to minutes.
    * 
    * @param s The number of seconds.
    * @return The equivalent number of minutes.
    */
    static inline double sToMn(double s) { return s / mnInS; }

    /**
    * @brief Converts seconds to hours.
    * 
    * @param s The number of seconds.
    * @return The equivalent number of hours.
    */
    static inline double sToH(double s) { return s / hInS; }

    /**
    * @brief Converts minutes to seconds.
    * 
    * @param mn The number of minutes.
    * @return The equivalent number of seconds.
    */
    static inline double mnToS(double mn) { return mn * mnInS; }

    /**
    * @brief Converts minutes to hours.
    * 
    * @param mn The number of minutes.
    * @return The equivalent number of hours.
    */
    static inline double mnToH(double mn) { return mn / hInMn; }

    /**
    * @brief Converts hours to seconds.
    * 
    * @param h The number of hours.
    * @return The equivalent number of seconds.
    */
    static inline double hToS(double h) { return h * hInS; }

    /**
    * @brief Converts hours to minutes.
    * 
    * @param h The number of hours.
    * @return The equivalent number of minutes.
    */
    static inline double hToMn(double h) { return h * hInMn; }

}  // namespace TimeConvert

#endif