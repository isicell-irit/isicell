/**
 * @file RandomManager.hpp
 * @brief Provides random number generation utilities.
 */

#ifndef RANDOMMANAGER
#define RANDOMMANAGER

#include <mecacell/mecacell.h>
#include <time.h>

namespace Rand{
    /// @ignore
    static std::uniform_real_distribution<double> probaGen(0.,1.);

    /**
     * @brief Initializes the random seed.
     * 
     * @param seed The seed value. Defaults to the current clock value.
     */
    static inline void initRandomSeed(int seed = clock()) {MecaCell::Config::globalRand().seed(seed);}

    /**
     * @brief Generates a random probability between 0 and 1.
     * 
     * @return A random probability.
     */
    static inline double getProba(){return probaGen(MecaCell::Config::globalRand());}

    /**
     * @brief Generates a random double in a uniform distribution.
     * 
     * @param min The minimum value.
     * @param max The maximum value.
     * @return A random double between min and max.
     */
    static inline double getUniformDouble(double min, double max){
        std::uniform_real_distribution<double> dist(min, max);
        return dist(MecaCell::Config::globalRand());
    }

    /**
     * @brief Generates a random integer in a uniform distribution.
     * 
     * @param min The minimum value.
     * @param max The maximum value.
     * @return A random integer between min and max.
     */
    static inline int getUniformInt(int min, int max){
        std::uniform_int_distribution<int> dist(min, max);
        return dist(MecaCell::Config::globalRand());
    }

    /**
     * @brief Generates a random double in a normal distribution.
     * 
     * @param mean The mean of the distribution.
     * @param var The variance of the distribution.
     * @return A random double with the specified mean and variance.
     */
    static inline double getNormalDouble(double mean, double var){
        std::normal_distribution<double> dist(mean, var);
        return dist(MecaCell::Config::globalRand());
    }

    /**
     * @brief Generates a random double in an exponential distribution.
     * 
     * @param lambda The rate parameter of the distribution.
     * @return A random double with the specified rate parameter.
     */
    static inline double getExponentialDouble(double lambda){
        std::exponential_distribution<double> dist(lambda);
        return dist(MecaCell::Config::globalRand());
    }

    /**
     * @brief Generates a random double in a bounded normal distribution.
     * 
     * @param mean The mean of the distribution.
     * @param bound The bound of the distribution.
     * @return A random double with the specified mean and bound.
     */
    static inline double getBoundedNormalDouble(double mean, double bound){
        std::normal_distribution<double> dist(mean, bound/3.);
        return std::min(dist(MecaCell::Config::globalRand()), bound);
    }

    /**
     * @brief Generates a random double in a piecewise linear distribution.
     * 
     * @param values A vector of pairs representing the intervals and weights.
     * @return A random double in the specified piecewise linear distribution.
     */
    static inline double getLinearDouble(std::vector<std::pair<double,double>> values){
        std::vector<double> i,w;
        for (auto it = std::make_move_iterator(values.begin()),
            end = std::make_move_iterator(values.end()); it != end; ++it){
            i.push_back(std::move(it->first));
            w.push_back(std::move(it->second));
        }
        std::piecewise_linear_distribution<double> dist(i.begin(), i.end(), w.begin());
        return dist(MecaCell::Config::globalRand());
    }

    /**
     * @brief Generates a random double in a log-normal distribution.
     * 
     * @param mu The mean of the underlying normal distribution.
     * @param sigma2 The variance of the underlying normal distribution.
     * @return A random double in the specified log-normal distribution.
     */
    static inline double getLogNormalDouble(double mu, double sigma2){
        std::lognormal_distribution<double> dist(mu, sigma2);
        return dist(MecaCell::Config::globalRand());
    }

    /**
     * @brief Generates a random double in a log-normal distribution using mean and standard deviation.
     * 
     * @param mean The mean of the distribution.
     * @param std The standard deviation of the distribution.
     * @return A random double in the specified log-normal distribution.
     */
    static inline double getLogNormalDoubleMeanStd(double mean, double std){
        double sigma2 = log(1+(std * std)/(mean * mean));
        double mu = log(mean) - 0.5*sigma2;
        return getLogNormalDouble(mu, sigma2);
    }
}

#endif