#include <mecacell/mecacell.h>
#include "scenarios/Scenario.hpp"
#include <fstream>
#include <numeric>
 
using namespace std;
using scenario_t = Scenario;


/**
Simu - Main class of isiCell python wrapper
===========================================

Parameters
----------
: **params** : `dict | str` The simulation parameters in JSON format.

: **seed** : `Optional[int]` The seed for the random generators. If not provided, the seed is random.

Attributes
----------
: **cells** : `CellVector` provides high-performance access to all cell properties.

    it's also an iterator to access each cells.

: **plugins** : `CellPlugin` allows access to the plugin part of each module.

*/
class Simu {

    scenario_t scenario;

public:

    /**
     *  @brief Cell plugins.
     *
     *  allows access to the plugin part of each module.
     */
    CellPlugin<PrimoCell<CellBody>>* plugins;

    Simu(string params) : scenario() {
        nlohmann::json configuration;
        configuration = nlohmann::json::parse(params);
        scenario.init(configuration);
        plugins = &(scenario.getWorld().cellPlugin);
    }
    Simu(string params, int seed) : scenario() {
        nlohmann::json configuration;
        configuration = nlohmann::json::parse(params);
        MecaCell::Config::globalRand().seed(seed);
        scenario.init(configuration);
        plugins = &(scenario.getWorld().cellPlugin);
    }

    /**
     *  @brief Updates the simulation by one time step.
     *
     *  This method advances the simulation by a single time step.
     */
    void update() {
        scenario.loop();
    }

    /**
     *  @brief Updates the simulation by multiple time steps.
     *
     *  This method advances the simulation by a given number of time steps.
     *
     *  @param nbSteps The number of time steps to advance.
     */
    void update(int nbSteps) {
        for (int i = 0; i < nbSteps; i++)
            scenario.loop();
    }

    /**
     *  @brief Gets the current number of time steps.
     *
     *  @return The current number of time steps in the simulation.
     */
    int getCurrentStep() {
        return scenario.getWorld().getNbUpdates();
    }

    /**
     *  @brief Gets the elapsed time in a given format.
     *
     *  @param format The time format ('d' for days, 'h' for hours,
     *  'm' for minutes, 's' for seconds).
     *
     *  @return The elapsed time in the given format.
     *
     *  @throws std::invalid_argument if the format is invalid.
     */
    double getTime(char format) {
        if (format == 'd') {
            return (double)scenario.getWorld().getNbUpdates() * scenario.getWorld().getDtInH() / 24.0;
        } else if (format == 'h') {
            return (double)scenario.getWorld().getNbUpdates() * scenario.getWorld().getDtInH();
        } else if (format == 'm') {
            return (double)scenario.getWorld().getNbUpdates() * scenario.getWorld().getDtInMn();
        } else if (format == 's') {
            return (double)scenario.getWorld().getNbUpdates() * scenario.getWorld().getDtInS();
        } else {
            throw std::invalid_argument("received invalid format ['d': day, 'h': hours, 'm': minutes, 's': seconds]");
        }
    }

    /**
     *  @brief Gets the cells in the simulation.
     *
     *  This method advances the simulation by a single time step.
     */
    vector<PrimoCell<CellBody>*>* _getCells() {
        return &(scenario.getWorld().cells);
    }


    /*isigen:eval@SimuUtilityDeclarations'''
        from string import Template
        cellAttributeGettersTemplate = Template("""
        vector<$type> get$nameUpper(){
        \tvector<$type> ret;
        \tret.resize(scenario.getWorld().cells.size());
        \ttransform(scenario.getWorld().cells.begin(), scenario.getWorld().cells.end(), ret.begin(), [](auto* c){return $name;});
        \treturn ret;
        }
        """)
        attributesToIgnore = ['type', 'state']
        validType = ['bool','double','int','void']
        validType = [*validType,*[ 'vector<'+vt+'>' for vt in validType]]
        validType = [*validType,*[ 'vector<'+vt+'>' for vt in validType]]
        def checkValidType(type):
            return type.replace(' ','') in validType
        cellAttributeGetters = [a for a in builderData["cell"]["attributes"] if checkValidType(a['type']) and a['value'] not in attributesToIgnore]
        if "enums" in builderData:
            cellAttributeGettersEnum = [a for a in builderData["cell"]["attributes"] if a['type'] in builderData["enums"] and a['value'] not in attributesToIgnore]
        '''*/

    /*isigen:insert@functionsScenarioWrapper'''
        import re
        scenarioFunWrapper = ''
        for fun in builderData["scenario"]["functions"].values():
            res = re.search(r"(?P<ret>.+) +(?P<fun>.+) *\((?P<params>.*)\)",fun)
            if checkValidType(res.group('ret')):
                scenarioFunWrapper+="{ret} {funName}({params}){{\n\t{content}\n}}\n".format(ret=res.group('ret'),funName=res.group('fun'),params=res.group('params'),content=('return 'if res.group('ret')!='void' else '')+'scenario.'+res.group('fun')+'('+ ','.join([ s.split(' ')[-1] for s in res.group('params').split(',')])+');')
        print(scenarioFunWrapper)
        '''*/
};