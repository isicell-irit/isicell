/**
 * @file Scenario.hpp
 * @brief Defines the Scenario class for managing the simulation scenario.
 */

#ifndef BASEPROJECT_BASESCENARIO_HPP
#define BASEPROJECT_BASESCENARIO_HPP

#include <mecacell/mecacell.h>
#include "../cells/PrimoCell.hpp"
#include "../core/CellBody.hpp"
#include "../tools/TimeConvert.hpp"
#include "../tools/RandomManager.hpp"


using Cell = PrimoCell<CellBody>;
using World = MecaCell::World<Cell>;
using Vec = MecaCell::Vec;

/**
 * @class Scenario
 * @brief Manages the simulation scenario.
 */
class Scenario {
private:

	World world;
	Model m;


public:

	double dt = 360.;
	int maxStep = 999999;

	/*isigen:insert@functions'''
		print('\n'.join([code.replace('\n','//isigen:metaDataDebug['+re.search(r'(\w+[\d_\w]+)\s*\(',code).group(1)+'] '+'{"idFunc":"'+idFunc+'"}\n',1) for idFunc,code in builderData["scenario"]["functions"].items()]))

		'''*/


	/*isigen:insert@attributes'''
		print('\n'.join(["{type} {value};".format(type=a['type'],value=a['value']) for a in builderData["scenario"]["attributes"] if a['source'] != 'core']))
		'''*/

	/**
	 * @brief Gets the world of the scenario.
	 * 
	 * @return Reference to the world.
	 */
	World &getWorld(){
		return world;
	}

	/**
	 * @brief Adds a cell to the world.
	 * 
	 * @param c Pointer to the cell to add.
	 */
	void addCell(Cell *c) { world.addCell(c); }

	/**
	 * @brief Gets the time step of the world.
	 * 
	 * @return The time step.
	 */
	double getDt() const { return world.getDt(); }

	/**
	 * @brief Sets the time step of the world.
	 * 
	 * @param dt The time step to set.
	 */
	void setDt(double dt) { world.setDt(dt); }

	/**
	 * @brief Sets the update behavior period of the world.
	 * 
	 * @param n The update behavior period.
	 */
	void setUpdateBehaviorPeriod(int n) { world.setUpdateBehaviorPeriod(n); }

	/**
	 * @brief Creates a cell at a specific position and type, adds it to the world, and initializes it.
	 * 
	 * This function dynamically allocates a new Cell object, adds it to the world,
	 * and then calls the init() method on the newly created Cell.
	 * 
	 * @param pos The position vector.
	 * @param t The type of the cell.
	 * @return A pointer to the newly created and initialized Cell.
	 */
	Cell * createCell(MecaCell::Vec pos, Type t){
		Cell * c = new Cell(pos, &m);
		c->setType(t);
		world.addCell(c);
		c->init();
		return c;
	}

	/**
	 * @brief Creates a cell at a specific position, adds it to the world, and initializes it.
	 * 
	 * This function dynamically allocates a new Cell object, adds it to the world,
	 * and then calls the init() method on the newly created Cell.
	 * 
	 * @param pos The position vector.
	 * @return A pointer to the newly created and initialized Cell.
	 */
	Cell * createCell(MecaCell::Vec pos){
		Cell * c = new Cell(pos, &m);
		world.addCell(c);
		c->init();
		return c;
	}

	/**
	 * @brief Creates a cell of a specific type, adds it to the world, and initializes it.
	 * 
	 * This function dynamically allocates a new Cell object, adds it to the world,
	 * and then calls the init() method on the newly created Cell.
	 * 
	 * @param t The type of the cell.
	 * @return A pointer to the newly created and initialized Cell.
	 */
	Cell * createCell(Type t){
		Cell * c = new Cell(&m);
		c->setType(t);
		world.addCell(c);
		c->init();
		return c;
	}

	/**
	 * @brief Creates a cell.
	 * 
	 * @return Pointer to the newly created cell.
	 */
	/**
	 * @brief Creates a new Cell, adds it to the world, and initializes it.
	 * 
	 * This function dynamically allocates a new Cell object, adds it to the world,
	 * and then calls the init() method on the newly created Cell.
	 * 
	 * @return A pointer to the newly created and initialized Cell.
	 */
	Cell * createCell(){
		Cell * c = new Cell(&m);
		world.addCell(c);
		c->init();
		return c;
	}

	/**
	 * @brief Initializes the scenario with a configuration.
	 * 
	 * @param config The configuration in JSON format.
	 */
	void init(nlohmann::json config){
		m.loadModel(config["input"]);
		m.initScenario(this);
		world.setDt(dt);

		/*isigen:insert@init'''
			print(builderData["scenario"]["codeInit"].replace('\n','//isigen:metaDataDebug[init] {}\n',1))
			'''*/
		world.addNewCells();
	}

	/**
	 * @brief Runs the main loop of the scenario.
	 */
	void loop(){
		world.update();
		/*isigen:insert@loop'''
			print(builderData["scenario"]["codeLoop"].replace('\n','//isigen:metaDataDebug[loop] {}\n',1))
			'''*/
	}

	/**
	 * @brief Checks if the scenario should stop.
	 * 
	 * @return True if the scenario should stop, false otherwise.
	 */
	bool stop() { return world.getNbUpdates() >= maxStep; }
};

#endif //BASEPROJECT_BASESCENARIO_HPP
//'''