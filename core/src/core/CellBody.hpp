//'''
#ifndef BASEPROJECT_CELLBODY_HPP
#define BASEPROJECT_CELLBODY_HPP

#include <mecacell/mecacell.h>
#include "CellPlugin.hpp"
#include "BaseBody.hpp"
#ifndef MOVABLE
#include <mecacell/movable.h>
#endif

/*isigen:insert@includes'''
	print('\n'.join(["#include \"../../{modulesFolder}/{folderName}/{bodyFileName}.hpp\"".\
        format(modulesFolder=cellModulesPath,folderName=m,bodyFileName=configJson["Body"]["name"]) for m,configJson in modulesConfigs.items() if "Body" in configJson]))
	print('\n')
	'''*/

 
/*isigen:insert@definitions'''
	print("""\n#ifdef MOVABLE
	template<typename cell_t>
	class CellBody :  virtual BaseBody<CellPlugin<cell_t> > {bodyNames} {{
	#else
	template<typename cell_t>
	class CellBody : public MecaCell::Movable, virtual BaseBody<CellPlugin<cell_t> > {bodyNames} {{
	#endif
	""".format(bodyNames=''.join([", public "+configJson["Namespace"]+"::"+configJson["Body"]["name"]+"<cell_t,CellPlugin<cell_t> >" for configJson in modulesConfigs.values() if "Body" in configJson])))
	'''*/

private:

#ifndef MOVABLE
	double radius = MecaCell::Config::DEFAULT_CELL_RADIUS;
	MecaCell::Vec pos = MecaCell::Vec::zero();
#endif


public:
    using embedded_plugin_t = CellPlugin<cell_t>;
    CellBody() = default;

	/*isigen:insert@constructor'''
		bodyNameConstrut = [configJson["Namespace"]+"::"+configJson["Body"]["name"]+"<cell_t, embedded_plugin_t>()" for configJson in modulesConfigs.values() if "Body" in configJson]
		print("""#ifdef MOVABLE
			CellBody(cell_t *, MecaCell::Vec pos = MecaCell::Vec::zero())
			: {bodyNames}
			{{}}
		#else""".format(bodyNames=', '.join(bodyNameConstrut)))
		print("""
			CellBody(cell_t *, MecaCell::Vec pos = MecaCell::Vec::zero())
			: MecaCell::Movable(pos) {bodyNames}
			{{}}
		#endif""".format(bodyNames=''.join([", "+n for n in bodyNameConstrut])))
		'''*/


    void setCellPlugin(embedded_plugin_t *_cellPlugin){
        BaseBody<embedded_plugin_t>::setCellPlugin(_cellPlugin);
		/*isigen:insert@setPluginPropagation'''
			print('\n'.join([configJson["Namespace"]+"::"+configJson["Body"]["name"]+"<cell_t,embedded_plugin_t>::onCellPluginLinking();" for configJson in modulesConfigs.values() if "Body" in configJson]))
			'''*/

	 }
#ifndef MOVABLE
	double getBoundingBoxRadius() const { return radius; }
	void setRadius(double r) { radius = r; }
#endif

};


#endif //BASEPROJECT_CELLBODY_HPP
//'''