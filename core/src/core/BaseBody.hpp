//'''
#ifndef BASEPROJECT_BASEBODY_HPP
#define BASEPROJECT_BASEBODY_HPP

template<typename embedded_plugin_t>
class BaseBody {

protected:
    embedded_plugin_t *cellPlugin = nullptr;

public:
    BaseBody() = default;

    inline void setCellPlugin(embedded_plugin_t *_cellPlugin){ this->cellPlugin = _cellPlugin; }
    inline void onCellPluginLinking(){}


};


#endif //BASEPROJECT_CELLBODY_HPP
//'''