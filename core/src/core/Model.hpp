//'''
#ifndef _MODEL_HPP
#define _MODEL_HPP

#include <mecacell/mecacell.h>
#include <time.h>

/*isigen:insert@enumDeclarations'''
    print("enum State { " + ','.join(sorted([s['name'] for s in builderData["cell"]["States"]])) + "};")
    print("enum Type { " + (','.join(sorted(builderData["cellTypes"])) if len(builderData["cellTypes"])>0 else ' Common ') + "};\n")
    if "enums" in builderData:
        for name, listV in builderData["enums"].items():
            print("enum "+name+" { " + ','.join(sorted(listV)) + "};")
    '''*/


struct Protocol {
            double defaultValue = 0;
            double currentValue;
            int currentStep = -1;
			int i = 0;
			std::vector<std::vector<double>> data;
            Protocol(){}
			Protocol(std::vector<std::vector<double>> _data):
			data(_data){
			}
			template <class World> double getValue(World  &w){
                if(currentStep==w.getNbUpdates()) return currentValue;
				double value = defaultValue;
                double currentTime =  w.getDt() * (double)w.getNbUpdates() / 3600.;
				if(i<data.size() && currentTime>=data[i][0]-MecaCell::Config::DOUBLE_EPSILON){
					value = data[i][1];
					i++;
				}
                currentStep = w.getNbUpdates();
                currentValue = value;
				return value;
			}
		};

class Model {
    
    private :
    struct PreSetCell {
        /*isigen:insert@preSetCell'''
            print('\n'.join(['{type} {value};'.format(type=a['type'],value=a['value']) for a in builderData["paramInputs"]["Cell"]]))
            '''*/

        PreSetCell(){}
        PreSetCell(nlohmann::json cellConfig, std::string type){
            /*isigen:insert@preSetCellInit'''
                print('\n'.join(['try {{\n\tif(cellConfig[type].find("{value}") != cellConfig[type].end()) {value} = cellConfig[type]["{value}"];\n\telse {value} = cellConfig["Common"]["{value}"];\n}} catch (...) {{\n\tthrow std::invalid_argument( "problem with cell paramter \\"{value}\\", for cell type \\""+type+"\\"");\n}}'.format(value=a['value']) for a in builderData["paramInputs"]["Cell"]]))
                '''*/
        }

        template <class Cell> void apply(Cell* c){
            /*isigen:insert@preSetCellApply'''
                print('\n'.join(['c->{attribute} = {attribute};'.format(attribute=a['value']) for a in builderData["paramInputs"]["Cell"]]))
                '''*/
        }
    };
    nlohmann::json scenarConfig;
    std::uniform_real_distribution<double> probaGen;
    std::unordered_map<Type, PreSetCell> typeToPreSetCell;


    public :

    void loadModel(nlohmann::json config){
        /*isigen:insert@loadPresets'''
            print('\n'.join(['typeToPreSetCell[Type::{type}] = PreSetCell(config["Cell"],"{type}");'.format(type=ct) for ct in (builderData["cellTypes"] if len(builderData["cellTypes"])>0 else ['Common'])]))
            '''*/
        scenarConfig = config["Scenario"];
    }

    template <class Cell> void initCell(Cell* c){
        typeToPreSetCell[c->getType()].apply(c);
    }

	template <class Scenario> void initScenario(Scenario* s){
        /*isigen:insert@scenarioSettings'''
            print('\n'.join(['s->{attribute} = {funcStart}scenarConfig[\"{attribute}\"]{funcEnd};'.format(attribute=a['value'],funcStart='Protocol(' if a['type']=='Protocol' else '',funcEnd='.get<std::vector<std::vector<double>>>())' if a['type']=='Protocol' else '') for a in builderData["paramInputs"]["Scenario"]]))
            '''*/
    }

};

#endif
//'''
