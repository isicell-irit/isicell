
/*isigen:eval@SimuUtilityDeclarations'''
    from string import Template
    cellAttributeGettersTemplate = Template("""
    vector<$type> get$nameUpper(){
    \tvector<$type> ret;
    \tret.resize($self->size());
    \ttransform($self->begin(), $self->end(), ret.begin(), [](auto* c){return $name;});
    \treturn ret;
    }
    """)
    attributesToIgnore = ['type', 'state']
    validType = ['bool','double','int','void']
    validType = [*validType,*[ 'vector<'+vt+'>' for vt in validType]]
    validType = [*validType,*[ 'vector<'+vt+'>' for vt in validType]]
    def checkValidType(type):
        return type.replace(' ','') in validType
    cellAttributeGetters = [a for a in builderData["cell"]["attributes"] if checkValidType(a['type']) and a['value'] not in attributesToIgnore]
    if "enums" in builderData:
        cellAttributeGettersEnum = [a for a in builderData["cell"]["attributes"] if a['type'] in builderData["enums"] and a['value'] not in attributesToIgnore]
    else: cellAttributeGettersEnum = []
    def upperFirst(txt):
        return txt[0].upper()+txt[1:]
    '''*/
%pythonbegin %{
  import json
  import random
  import pandas as pd
  from tqdm import tqdm
%}

%feature("autodoc", 3);
%feature("python:annotations", "c");


%include "std_string.i"
%include "std_vector.i"
%include "cpointer.i"
%include "exception.i"      
%allowexception; 
%exception {
  try {
    $action
  } catch(const std::exception& e) {
    SWIG_exception(SWIG_RuntimeError, e.what());
  } catch(...) {
    SWIG_exception(SWIG_UnknownError, "Unknown exception");
  }
}

%module isiCellPy
// Add necessary symbols to generated header
%{
#include "src/simu.cpp"
%}




%include "external/MecaCell/mecacell/geometry/basis.hpp"
%include "external/MecaCell/mecacell/geometry/rotation.h"
%include "external/MecaCell/mecacell/geometry/vector3D.h"
%inline %{
namespace MecaCell {
using Vec = Vector3D;
}
%}

%include "src/cells/PrimoCell.hpp"
%template(Cell) PrimoCell<CellBody>;

namespace std {
   %template(Vector3DVector) vector<MecaCell::Vec>;
   %template(intVector) vector<int>;
   %template(intVectorVector) vector<vector<int>>;
   %template(floatVector) vector<double>;
   %template(floatVectorVector) vector<vector<double>>;
   %template(boolVector) vector<bool>;
   %template(boolVectorVector) vector<vector<bool>>;
   %template(strVector) vector<string>;
   %template(strVectorVector) vector<vector<string>>;
   %template(CellVector) vector<PrimoCell<CellBody>*>;
   %template(CellVectorVector) vector<vector<PrimoCell<CellBody>*>>;
   %template(CellVectorVectorVector) vector<vector<vector<PrimoCell<CellBody>*>>>;
   %template(CellBodyVector) vector<CellBody<Cell>*>;
}
/*isigen:insert@includesBody'''
for m,configJson in modulesConfigs.items():
  if "SWIG" in configJson:
    print(configJson["SWIG"])
  if "Plugin" in configJson:
    print(f'%include "{cellModulesPath}/{m}/{configJson["Plugin"]["className"]}.hpp"')
    print(f'%template({configJson["Plugin"]["className"]}) {configJson["Namespace"]}::{configJson["Plugin"]["className"]}<PrimoCell<CellBody>>;')
  if "Body" in configJson:
    print(f'%include "{cellModulesPath}/{m}/{configJson["Body"]["name"]}.hpp"')
    print(f'%template({configJson["Body"]["name"]}) {configJson["Namespace"]}::{configJson["Body"]["name"]}<Cell,CellPlugin<Cell>>;')
  print('\n')
	'''*/

%include "src/core/CellPlugin.hpp"
%template(CellPlugin) CellPlugin<PrimoCell<CellBody>>;
%include "src/core/CellBody.hpp"
%template(CellBody) CellBody<Cell>;

%extend PrimoCell<CellBody>
{
    CellBody<Cell>* getBody() {
        return &($self->getBody());
    }
};
%extend std::vector<PrimoCell<CellBody>*>
{

    /**
     *  @brief Gets the state of each cell.
     *
     *  This method retrieves the state of all cells in the scenario.
     *
     *  @return A vector of integers representing the state of each cell.
     */
    vector<int> getState() {
        vector<int> ret;
        ret.resize($self->size());
        transform($self->begin(), $self->end(), ret.begin(), [](auto* c) { return static_cast<int>(c->getState()); });
        return ret;
    }

    /**
     *  @brief Gets the type of each cell.
     *
     *  This method retrieves the type of all cells in the scenario.
     *
     *  @return A vector of integers representing the type of each cell.
     */
    vector<int> getType() {
        vector<int> ret;
        ret.resize($self->size());
        transform($self->begin(), $self->end(), ret.begin(), [](auto* c) { return static_cast<int>(c->getType()); });
        return ret;
    }

    /**
     *  @brief Gets the id of each cell.
     *
     *  This method retrieves the id of all cells in the scenario.
     *
     *  @return A vector of integers representing the id of each cell.
     */
    vector<int> getId() {
        vector<int> ret;
        ret.resize($self->size());
        transform($self->begin(), $self->end(), ret.begin(), [](auto* c) { return static_cast<int>(c->getId()); });
        return ret;
    }
    vector<int> getMotherId() {
        vector<int> ret;
        ret.resize($self->size());
        transform($self->begin(), $self->end(), ret.begin(), [](auto* c) { return static_cast<int>(c->getMotherId()); });
        return ret;
    }

    /**
     *  @brief Gets the position of each cell.
     *
     *  This method retrieves the position of all cells in the scenario.
     *
     *  @return A vector of MecaCell::Vec representing the position of each cell.
     */
    vector<MecaCell::Vec> getPosition() {
        vector<MecaCell::Vec> ret;
        ret.resize($self->size());
        transform($self->begin(), $self->end(), ret.begin(), [](auto* c) { return c->getPosition(); });
        return ret;
    }


    /*isigen:insert@simuGetters'''
        simuGetters = ''
        for cag in cellAttributeGetters:
            simuGetters += cellAttributeGettersTemplate.substitute({'self':'$self','name':'c->'+cag['value'], 'nameUpper': upperFirst(cag['value']), 'type': cag['type']})
        print(simuGetters)
        '''*/

    
    /*isigen:insert@simuGettersEnum'''
        simuGetters = ''
        if "enums" in builderData:
            for cag in cellAttributeGettersEnum:
                simuGetters += cellAttributeGettersTemplate.substitute({'self':'$self','name':'static_cast<int>(c->'+cag['value']+')', 'nameUpper': upperFirst(cag['value']), 'type': 'int'})
        print(simuGetters)
        '''*/

  %pythoncode %{
    /*isigen:insert@countEnum'''
        print(f'#{cellAttributeGettersEnum}')
        tmpAllEnum = {'State':dict(enumerate(sorted([s["name"] for s in builderData["cell"]["States"]])))}
        tmpAllEnum.update({'Type':dict(enumerate(sorted(builderData["cellTypes"])))})
        if "enums" in builderData:
            tmpAllEnum.update({upperFirst(name): dict(enumerate(sorted(listV))) for name, listV in builderData["enums"].items()})
        countEnum=''
        countEnum += f'def countType(self):\n\tvalues=self.getType()\n\treturn '+'{' + ', '.join([f'"{k}":values.count({i})' for i,k in tmpAllEnum['Type'].items()])+'}\n\n'
        countEnum += f'def countState(self):\n\tvalues=self.getState()\n\treturn '+'{' + ', '.join([f'"{k}":values.count({i})' for i,k in tmpAllEnum['State'].items()])+'}\n\n'
        for attrEnum in cellAttributeGettersEnum:
            countEnum += f'def count{attrEnum["value"]}(self):\n\tvalues=self.get{upperFirst(attrEnum["value"])}()\n\treturn '+'{' + ', '.join([f'"{k}":values.count({i})' for i,k in tmpAllEnum[attrEnum['type']].items()])+'}\n\n'
        print(countEnum)
        '''*/

    _mapCellAttr = {
        "id":_isiCellPy.CellVector_getId,
        "motherid":_isiCellPy.CellVector_getMotherId,
        "state":_isiCellPy.CellVector_getState,
        "type":_isiCellPy.CellVector_getType,
        "id":_isiCellPy.CellVector_getId,
        /*isigen:insert@mapGetter'''
            print('\n'.join([f'"{cag["value"].lower()}": _isiCellPy.CellVector_get{upperFirst(cag["value"])},' for cag in cellAttributeGetters]))
            print('\n'.join([f'"{cag["value"].lower()}": _isiCellPy.CellVector_get{upperFirst(cag["value"])},' for cag in cellAttributeGettersEnum ]))
            '''*/
    }
    /*isigen:insert@mapGetter'''
        print('_attrEnum = {"type":"Type","state":"State",'+',\n'.join([f'"{cag["value"].lower()}": "{cag["type"]}"' for cag in cellAttributeGettersEnum])+'}')
        '''*/


    def get(self, name: Union[str, List[str]]) -> Union[Dict[str, Any], Tuple]:
        """
        Retrieves cells attributes.

        Parameters
        ----------
        - **name** : str or list of str
            The name(s) of the simulation attribute(s) to retrieve.

        Returns
        -------
        - **dict** or **tuple**
            A dictionary with attribute names as keys and their values as values if a list of names is provided.
            A tuple of the specified attribute values if a single name is provided.

        Raises
        ------
        - **KeyError**
            If the specified attribute name does not exist.
        """
        if isinstance(name, list):
            return {n: self._mapCellAttr[n.lower()](self) for n in name}
        else:
            return self._mapCellAttr[name.lower()](self)

    def getPandas(self,name: Union[str, List[str]]) -> 'DataFrame':
        r"""
        Retrieves cells attributes as a pandas DataFrame or Series.

        Parameters
        ----------
         - **name** : str or list of str
            The name(s) of the simulation attribute(s) to retrieve. If a single attribute name is provided, a pandas Series is returned. If a list of attribute names is provided, a pandas DataFrame is returned with each attribute as a column.

        Returns
        -------
         - **pd.DataFrame** or **pd.Series**
            A pandas DataFrame or Series containing the requested simulation data. If multiple attribute names are provided, a DataFrame is returned with each attribute as a separate column. If a single attribute name is provided, a Series is returned.

        Raises
        ------
         - **AttributeError**
            If the specified attribute name(s) do not correspond to any method in the `Simu` class.

        Example
        -------
        ```python
        simu = Simu(params)

        # Get a single attribute as a pandas Series
        state_series = simu.getPandas('state')

        # Get multiple attributes as a pandas DataFrame
        data_frame = simu.getPandas(['state', 'type'])
        ```

        Notes
        -----
        - If the attribute name is an Enum, the corresponding values are replaced and the data type is set to 'category'.
        - For single attribute retrieval, the result is a pandas Series.
        - For multiple attribute retrieval, the result is a pandas DataFrame.
        """
        if isinstance(name, list):
            data = {n: self._mapCellAttr[n.lower()](self) for n in name}
            df = pd.DataFrame(data)
            for col in df.columns:
                if col in self._attrEnum:
                    df[col] = df[col].replace(Simu.enums[self._attrEnum[col]]).astype('category', copy=False)
            return df
        else:
            name = name.lower()
            series = pd.Series(self._mapCellAttr[name](self), name=name)
            if name in self._attrEnum: series = series.replace(Simu.enums[self._attrEnum[name]]).astype('category', copy=False)
            return series
  %}
};

//[params,seed] provide from the edition of Simu.__init__ in the mainGenerator > convertCppToPythonAnnotation
%pythonprepend Simu::Simu(string,int) %{
  args = [params,seed]
  if isinstance(params, dict):
    args[0] = json.dumps(params)
  if seed is None:
    args[1] = random.randrange(2147483647)
%}
%include "src/simu.cpp"


%extend Simu{
  %pythoncode %{
    cells: CellVector = property(_getCells,doc=r"""
    cells properties
    ================

    provides high-performance access to all cell properties.

    it's also an iterator to access each cells.
    """)


    /*isigen:insert@SWIGenumDeclarations2'''
        tmpAllEnum = {'State':dict(enumerate(sorted([s["name"] for s in builderData["cell"]["States"]])))}
        tmpAllEnum.update({'Type':dict(enumerate(sorted(builderData["cellTypes"])))})
        if "enums" in builderData:
            tmpAllEnum.update({name: dict(enumerate(sorted(listV))) for name, listV in builderData["enums"].items()})
        print('enums =', tmpAllEnum)
        '''*/

    
    def iterSteps(self,steps: list) -> 'Iterable[Simu]':
        r"""
        Generates an iterable of the simulation at specified time steps.

        Parameters
        ----------
         - **steps** : `list[int]`
            A list of time steps at which to yield the simulation state. The first step in the list must be greater than or equal to the current simulation step.

        Yields
        ------
         - **Simu**
            The simulation instance at each specified time step.

        Raises
        ------
         - **ValueError**
            If the first step in the list is lower than the current simulation step.

        Example
        -------
        ```python
        steps = [10, 20, 30]
        for simu in Simu(params).iterSteps(steps):
            print(simu.getCurrentStep())
        ```
        """
        if steps[0]<self.getCurrentStep(): raise ValueError('first step of list is lower than current step') 
        for s in [steps[0]]+[s2-s1 for s1,s2 in zip(steps[:-1],steps[1:])]:
            if s>0:self.update(s)
            yield self


    def compute(self,dataCatcher: 'Callable[[Simu], ...]',steps: list,autoDataFrame:'bool | list[bool]'=True, autoConcat:'bool | list[bool]'=True,withTqdm:bool =False) -> list:
        r"""
        Parameters
        ----------
         - **dataCatcher** : `Callable[[Simu], ...]`
            A callable function that processes simulation data. The function should take an instance of `Simu` as input and return the desired data.

         - **steps** : `list[int]`
            A list of time steps at which to capture data from the simulation.

         - **autoDataFrame** : `bool | list[bool]`, optional, default=True
            If `True`, the function's output will be automatically converted to a pandas DataFrame. If a list of booleans is provided, it must match the length of the dataCatcher output, specifying for each output whether to convert it to a DataFrame.

         - **autoConcat** : `bool | list[bool]`, optional, default=True
            If `True`, the function will concatenate the results into a single pandas DataFrame. If a list of booleans is provided, it must match the length of the dataCatcher output, specifying for each output whether to concatenate it.

         - **withTqdm** : `bool`, optional, default=False
            If `True`, displays a progress bar using `tqdm` while iterating through the steps.

        Returns
        -------
         - **list**
            A list containing the results from the dataCatcher function at each specified step. The structure of the list depends on the settings for `autoDataFrame` and `autoConcat`.

        Example
        -------
        ```python
        def dataCatcherOnStep(simu):
            return simu.cells.countState()
        
        steps = list(range(0, 2501, 10))
        data = Simu(params).compute(dataCatcherOnStep, steps, autoDataFrame=True)
        ```
        """
        datas=[]
        if withTqdm:
            for s in tqdm(self.iterSteps(steps),total=len(steps)):
                datas.append(dataCatcher(self))
        else:
            for s in self.iterSteps(steps):
                datas.append(dataCatcher(self))
        nbOutput = len(datas[0]) if (isinstance(datas[0],list) or isinstance(datas[0],tuple)) else 0
        isMultiConcat = isinstance(autoConcat,list)
        isMultiDataFrame = isinstance(autoDataFrame,list)
        if nbOutput>0: datas = list(zip(*datas))
        if nbOutput==1: datas=datas[0]
        if nbOutput>1:
            if not isMultiConcat: autoConcat = [autoConcat]*nbOutput
            if not isMultiDataFrame: autoDataFrame = [autoDataFrame]*nbOutput
            if len(autoConcat)==len(autoDataFrame)==nbOutput:
                for i,(c,d) in enumerate(zip(autoConcat,autoDataFrame)):
                    if d and (not isinstance(datas[i][0],pd.DataFrame)):  datas[i] = pd.DataFrame(datas[i],index=pd.Index(steps,name='step'))
                    elif c: 
                        datas[i] = pd.concat(datas[i],copy=False).set_index(pd.Index([s for j,s in enumerate(steps) for _ in range(len(datas[i][j]))],name='step'))
                        enumC = [e for e in self._attrEnum if e in datas[i].columns]
                        if len(enumC)>0: datas[i] = datas[i].astype({k:'category' for k in enumC},copy=False)
            else: raise Exception('autoConcat or autoDataFrame is not same lenght than dataCatcher output')
        elif isMultiConcat or isMultiDataFrame: raise Exception('autoConcat or autoDataFrame is not same lenght than dataCatcher output')
        elif autoDataFrame: datas = pd.DataFrame(datas,index=pd.Index(steps,name='step'))
        elif autoConcat:
            datas = pd.concat(datas,copy=False).set_index(pd.Index([s for j,s in enumerate(steps) for _ in range(len(datas[j]))],name='step'))
            enumC = [e for e in self._attrEnum if e in datas[i].columns]
            if len(enumC)>0: datas = datas.astype({k:'category' for k in enumC},copy=False)
        return datas


    def recordLineage(self, max_initial_cells: int = 10, max_step: int = 500) -> Dict[str, Any]:
        """
        Records the lineage of cells in the simulation up to a specified maximum number of steps or cells.

        Parameters
        ----------
        - **max_initial_cells** (int): The maximum number of cells to track in the simulation. Default is 10.
        - **max_step** (int): The maximum number of steps to run the simulation. Default is 500.

        Returns
        -------
        - Dict[str, Any]: A dictionary containing the cell lineage data and enumeration map.
        """
        cells = {}
        step = self.getCurrentStep()
        nb_initial_cells = 0
        # Main simulation loop
        while step < max_step:
            # Retrieve cell IDs and their mother IDs
            cell_ids = self.cells.getId()
            cell_mother_ids = self.cells.getMotherId()
            
            # Retrieve cell characteristics and update with IDs and mother IDs
            cells_attributes = self.cells.get(list(self.cells._mapCellAttr.keys()))
            
            # Convert characteristics to a list of dictionaries
            cells_attributes = pd.DataFrame(cells_attributes).to_dict('records')
            
            # Update death_step for cells that are no longer present
            for cell_id, cell_data in cells.items():
                if cell_id not in cell_ids and cell_data['death_step'] is None:
                    cell_data['death_step'] = step
            
            # Process each cell's characteristics
            for cell_attr in cells_attributes:
                cell_id = cell_attr.pop('id')
                mother_id = cell_attr.pop('motherid')
                
                # If cell is new and its mother is in the cells dictionary, add it
                if cell_id not in cells and mother_id in cells:
                    cells[cell_id] = {
                        "id": cell_id,
                        "mother_id": mother_id,
                        "birth_step": step,
                        "death_step": None,
                        **{attr_key: [attr_value] for attr_key, attr_value in cell_attr.items()}
                    }
                # If cell is new and the limit has not been reached, add it
                elif cell_id not in cells and nb_initial_cells < max_initial_cells:
                    nb_initial_cells+=1
                    cells[cell_id] = {
                        "id": cell_id,
                        "mother_id": mother_id,
                        "birth_step": step,
                        "death_step": None,
                        **{attr_key: [attr_value] for attr_key, attr_value in cell_attr.items()}
                    }
                # If cell already exists, update its characteristics
                elif cell_id in cells:
                    for attr_key, attr_value in cell_attr.items():
                        cells[cell_id][attr_key].append(attr_value)
            
            # Update the simulation
            self.update()
            step = self.getCurrentStep()

        # Ensure all cells have a death_step set
        for cell_data in cells.values():
            if cell_data['death_step'] is None:
                cell_data['death_step'] = max_step

        return {
            'data': list(cells.values()),
            'enumMap': {attr: self.enums[enum] for attr, enum in self.cells._attrEnum.items()}
        }

  %}
};