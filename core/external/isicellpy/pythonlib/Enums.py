
def IsicellEnum(name,values):
  class EnumItem(int):
    def __new__(cls, enum, name, value, *args, **kwargs):
      test =  super(cls, cls).__new__(cls,value)
      test.__doc__ = name+' : int'
      test.__enum = enum
      test.__name = name
      return test
    def __repr__(self):
      return f'<{self.__enum}.{self.__name}: {int(self)}>'

  def managed_attribute(name):
    @property
    def prop(self):
        return self._data[name]
    return prop

  class Enum(): 
    def __init__(self,name,values):
      self.__doc__ = name+' : Enum'+name
      self.__name__ = name
      self._data = {k:EnumItem(name,k,v) for v,k in values.items()}
      for v,k in values.items():
        setattr(self.__class__, k, managed_attribute(k))
    def __repr__(self):
        return f"<enum {self.__name__} : [{', '.join([f'{k}={int(v)}' for k,v in sorted(self._data.items(),key=lambda i:i[1])])}]>" 
    def to_dict(self):
      r"""to_dict(Enum self) -> dict"""
      return {k: int(v) for k, v in self._data.items()}
    def to_invert_dict(self):
      r"""to_invert_dict(Enum self) -> dict"""
      return {int(v): k for k, v in self._data.items()}

  return Enum(name,values)

/*isigen:insert@SWIGenumDeclarations'''
    tmpAllEnum = {'State':dict(enumerate(sorted([s["name"] for s in builderData["cell"]["States"]])))}
    tmpAllEnum.update({'Type':dict(enumerate(sorted(builderData["cellTypes"])))})
    if "enums" in builderData:
        tmpAllEnum.update({upperFirst(name): dict(enumerate(sorted(listV))) for name, listV in builderData["enums"].items()})
    print(f'enumList = {tmpAllEnum}')
    '''*/
for enumName,enumValues in enumList.items():
  globals()[enumName] = IsicellEnum(enumName,enumValues)
del enumName,IsicellEnum,enumValues,enumList
