import sqlite3
import pandas as pd
import json
import re

class DatabaseManager:
    """
    Manager for interacting with a SQLite database. This class provides methods to add data, query data, and manage tables in the database.

    Parameters
    ----------
    - **db_filename** : str
        The filename of the SQLite database.

    Methods
    -------
    - **add_conditions** : Adds a DataFrame of conditions to the 'condition' table in the database.
    - **add_parameters** : Adds a list of parameters to the 'parameters' table in the database.
    - **add_data** : Adds a DataFrame of data to a specified table in the database.
    - **add_error** : Adds a DataFrame of error data to the 'error' table in the database.
    - **get** : Retrieves records from a table based on filter conditions.
    - **getByID** : Retrieves records from a table based on ID_PARAMETER and/or ID_CONDITION.
    - **getPareto** : Retrieves the Pareto front for given errors.
    - **getBestScore** : Retrieves the best scores for a given error.
    - **getBest** : Retrieves the best records from a table based on a given error.
    - **summarize** : Returns a summary of the database.

    Example
    -------
    ```python
    conditions = pd.DataFrame([
        [0.5, 1],
        [0.2, 2],
        [0.2, 5]
    ], columns=['info1', 'info2'])
    parameters_list = LHSIterator(param,10)

    with DatabaseManager("example.db") as db:
        db.add_parameters(parameters_list)
        db.add_conditions(conditions)
        
        for states,other in MultiSimu(run_simu,parameters_list,conditions=conditions.values.tolist(),replicat=5,withTqdm=True,batch_size_level='param'):
            db.add_data('states', states.reset_index())
            db.add_data('otherData', other.reset_index())
            error = your_error_function(states)
            db.add_error(error)
    ```
    """

    def __init__(self, db_filename: str):
        self.db_filename = db_filename

    def open(self):
        """
        Opens a connection to the database.
        """
        self.conn = sqlite3.connect(self.db_filename)
        self._check_tables()
        return self

    def close(self):
        """
        Closes the connection to the database.
        """
        self.conn.close()

    def __enter__(self):
        self.open()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()

    def add_conditions(self, conditions_df: pd.DataFrame, id_column: str = None):
        """
        Adds a DataFrame of conditions to the 'condition' table in the database.

        Parameters
        ----------
        - **conditions_df** : pd.DataFrame
            The DataFrame containing the conditions to add.
        - **id_column** : str, optional
            The column to use as the ID_CONDITION. If not provided, the index will be used.
        """
        for i in range(conditions_df.shape[1]):
            if hasattr(conditions_df.iloc[0, i], '__iter__'):
                conditions_df.iloc[:, i] = conditions_df.iloc[:, i].apply(lambda x: json.dumps(x))
        if id_column is None:
            conditions_df = conditions_df.reset_index().rename(columns={'index': 'ID_CONDITION'})
        elif id_column != 'ID_CONDITION':
            conditions_df = conditions_df.rename(columns={id_column: 'ID_CONDITION'})
        
        conditions_df.to_sql('condition', self.conn, if_exists='fail', index=False)
        self._check_tables()
        self._create_index('condition', 'ID_CONDITION')

    def add_error(self, data_df: pd.DataFrame):
        """
        Adds a DataFrame of error data to the 'error' table in the database.

        Parameters
        ----------
        - **data_df** : pd.DataFrame
            The DataFrame containing the error data to add.
        """
        data_df.to_sql('error', self.conn, if_exists='append', index=False)
        self._check_tables()
        if 'ID_PARAMETER' in data_df.columns:
            self._create_index('error', 'ID_PARAMETER')
        if 'ID_CONDITION' in data_df.columns:
            self._create_index('error', 'ID_CONDITION')

    def add_parameters(self, parameters_list: list):
        """
        Adds a list of parameters to the 'parameters' table in the database.

        Parameters
        ----------
        - **parameters_list** : list
            The list of parameters to add.
        """
        flattened_parameters = [self._flatten_dict(p) for p in parameters_list]
        parameters_df = pd.DataFrame(flattened_parameters)
        parameters_df = parameters_df.reset_index().rename(columns={'index': 'ID_PARAMETER'})
        parameters_df.to_sql('parameters', self.conn, if_exists='fail', index=False)
        self._check_tables()
        self._create_index('parameters', 'ID_PARAMETER')

    def add_data(self, table_name: str, data_df: pd.DataFrame):
        """
        Adds a DataFrame of data to a specified table in the database.

        Parameters
        ----------
        - **table_name** : str
            The name of the table to add data to.
        - **data_df** : pd.DataFrame
            The DataFrame containing the data to add.
        """
        data_df.to_sql(table_name, self.conn, if_exists='append', index=False)
        self._check_tables()

        if 'ID_PARAMETER' in data_df.columns:
            self._create_index(table_name, 'ID_PARAMETER')
        if 'ID_CONDITION' in data_df.columns:
            self._create_index(table_name, 'ID_CONDITION')

    def get(self, table: str, filter_conditions: str = None) -> pd.DataFrame:
        """
        Retrieves records from a table based on filter conditions.

        Parameters
        ----------
        - **table** : str
            The name of the table to retrieve records from.
        - **filter_conditions** : str, optional
            The SQL filter conditions to apply.

        Returns
        -------
        - **pd.DataFrame** : The DataFrame containing the retrieved records.
        """
        if table not in self.available_tables:
            raise ValueError(f"Table '{table}' does not exist in the database.")

        table_columns = self._get_table_columns(table)
        join_clauses = []
        referenced_tables = set()
        alias_dict = {table: 'main_table'}
        alias_count = 0

        if filter_conditions:
            filter_conditions = self._parse_json_conditions(filter_conditions)
            conditions_tables = re.findall(r'(\w+)\.', filter_conditions)
            referenced_tables.update(conditions_tables)

        query_columns = "main_table.*"
        query = f"SELECT DISTINCT {query_columns} FROM {table} AS main_table"

        joined_tables = set()

        for ref_table in referenced_tables:
            if ref_table in self.available_tables and ref_table != table:
                ref_alias = f"t_{ref_table}_{alias_count}"
                alias_count += 1
                alias_dict[ref_table] = ref_alias

                ref_columns = self._get_table_columns(ref_table)

                if ref_table not in joined_tables:
                    if 'ID_PARAMETER' in table_columns and 'ID_PARAMETER' in ref_columns:
                        join_clauses.append(f"LEFT JOIN {ref_table} AS {ref_alias} ON main_table.ID_PARAMETER = {ref_alias}.ID_PARAMETER")
                        joined_tables.add(ref_table)
                    elif 'ID_CONDITION' in table_columns and 'ID_CONDITION' in ref_columns:
                        join_clauses.append(f"LEFT JOIN {ref_table} AS {ref_alias} ON main_table.ID_CONDITION = {ref_alias}.ID_CONDITION")
                        joined_tables.add(ref_table)
                    elif 'ID_PARAMETER' in ref_columns and 'ID_PARAMETER' not in table_columns:
                        join_clauses.append(f"LEFT JOIN {ref_table} AS {ref_alias} ON {ref_alias}.ID_PARAMETER = main_table.ID_PARAMETER")
                        joined_tables.add(ref_table)
                    elif 'ID_CONDITION' in ref_columns and 'ID_CONDITION' not in table_columns:
                        join_clauses.append(f"LEFT JOIN {ref_table} AS {ref_alias} ON {ref_alias}.ID_CONDITION = main_table.ID_CONDITION")
                        joined_tables.add(ref_table)

        if filter_conditions:
            for ref_table, ref_alias in alias_dict.items():
                filter_conditions = filter_conditions.replace(f"{ref_table}.", f"{ref_alias}.")

        join_clause = ' '.join(join_clauses)
        where_clause = f"WHERE {filter_conditions}" if filter_conditions else ""
        query = f"{query} {join_clause} {where_clause}"

        df = pd.read_sql(query, self.conn)

        if table == 'conditions':
            for i in range(df.shape[1]):
                value = df.iloc[0, i]
                try:
                    if isinstance(value, str) and (value.startswith('[') or value.startswith('{')):
                        df.iloc[:, i] = df.iloc[:, i].apply(lambda x: json.loads(x))
                except:
                    pass

        if table == 'parameters':
            unflattened_data = []
            for _, row in df.iterrows():
                unflattened_row = self._unflat(row.to_dict())
                unflattened_data.append(unflattened_row)

            return pd.DataFrame(unflattened_data)
        else:
            return df

    def getBestScore(self, errorName: str, n: int = 1, forEachCondition: bool = False) -> pd.DataFrame:
        """
        Retrieves the best scores for a given error.

        Parameters
        ----------
        - **errorName** : str
            The name of the error column to base the scores on.
        - **n** : int, optional
            The number of top scores to retrieve.
        - **forEachCondition** : bool, optional
            Whether to retrieve the best scores for each condition separately.

        Returns
        -------
        - **pd.DataFrame** : The DataFrame containing the best scores.
        """
        if forEachCondition:
            query = f'''WITH ranked_errors AS (
                            SELECT
                                ID_PARAMETER,
                                ID_CONDITION,
                                {errorName},
                                ROW_NUMBER() OVER (PARTITION BY ID_CONDITION ORDER BY {errorName} ASC, ID_PARAMETER ASC) AS rn
                            FROM error)
                        SELECT
                            ID_PARAMETER,
                            ID_CONDITION,
                            {errorName}
                        FROM
                            ranked_errors
                        WHERE
                            rn <= {n};'''
        else:
            query = f'''SELECT ID_PARAMETER, AVG({errorName}) AS avg_error
                        FROM error
                        GROUP BY ID_PARAMETER
                        ORDER BY avg_error ASC
                        LIMIT {n};'''
        return pd.read_sql(query, self.conn)

    def getBest(self, table: str, errorName: str, n: int = 1, forEachCondition: bool = False) -> pd.DataFrame:
        """
        Retrieves the best records from a table based on a given error.

        Parameters
        ----------
        - **table** : str
            The name of the table to retrieve records from.
        - **errorName** : str
            The name of the error column to base the records on.
        - **n** : int, optional
            The number of top records to retrieve.
        - **forEachCondition** : bool, optional
            Whether to retrieve the best records for each condition separately.

        Returns
        -------
        - **pd.DataFrame** : The DataFrame containing the best records.
        """
        if forEachCondition:
            table_columns = self._get_table_columns(table)
            query = f'''WITH ranked_errors AS (
                            SELECT
                                ID_PARAMETER,
                                ID_CONDITION,
                                {errorName},
                                ROW_NUMBER() OVER (PARTITION BY ID_CONDITION ORDER BY {errorName} ASC) AS rn
                            FROM
                                error
                        )
                        SELECT *
                        FROM {table} t
                        WHERE {'(t.ID_PARAMETER, t.ID_CONDITION)' if 'ID_CONDITION' in table_columns else 't.ID_PARAMETER' } IN (
                            SELECT {'ID_PARAMETER, ID_CONDITION' if 'ID_CONDITION' in table_columns else 'ID_PARAMETER' }
                            FROM ranked_errors
                            WHERE rn <= {n}
                        );'''
        else:
            query = f'''SELECT *
                        FROM {table}
                        WHERE ID_PARAMETER IN (
                            SELECT ID_PARAMETER
                            FROM error
                            GROUP BY ID_PARAMETER
                            ORDER BY SUM({errorName}) ASC
                            LIMIT {n}
                        );'''
        return pd.read_sql(query, self.conn)

    def getByID(self, table: str, ID_PARAMETER: int = None, ID_CONDITION: int = None) -> pd.DataFrame:
        """
        Retrieves records from a table based on ID_PARAMETER and/or ID_CONDITION.

        Parameters
        ----------
        - **table** : str
            The name of the table to retrieve records from.
        - **ID_PARAMETER** : int, optional
            The ID_PARAMETER to filter by.
        - **ID_CONDITION** : int, optional
            The ID_CONDITION to filter by.

        Returns
        -------
        - **pd.DataFrame** : The DataFrame containing the retrieved records.
        """
        whereP = ''
        whereC = ''
        if isinstance(ID_PARAMETER, list):
            whereP = f'ID_PARAMETER IN ({",".join(map(str, ID_PARAMETER))})'
        elif ID_PARAMETER is not None:
            whereP = f'ID_PARAMETER = {ID_PARAMETER}'
        if isinstance(ID_CONDITION, list):
            whereC = f'ID_CONDITION IN ({",".join(map(str, ID_CONDITION))})'
        elif ID_CONDITION is not None:
            whereC = f'ID_CONDITION = {ID_CONDITION}'

        where_clauses = [clause for clause in [whereP, whereC] if clause]
        where = ' AND '.join(where_clauses)

        query = f'''SELECT *
                    FROM {table}
                    WHERE {where};'''
        return pd.read_sql(query, self.conn)

    def getPareto(self, errorNameList: list, forEachCondition: bool = False) -> pd.DataFrame:
        """
        Retrieves the Pareto front for given errors.

        Parameters
        ----------
        - **errorNameList** : list
            The list of error column names to consider.
        - **forEachCondition** : bool, optional
            Whether to retrieve the Pareto front for each condition separately.

        Returns
        -------
        - **pd.DataFrame** : The DataFrame containing the Pareto front.
        """
        if forEachCondition:
            query = f'''WITH avg_errors AS (
                            SELECT
                                e.ID_PARAMETER,
                                e.ID_CONDITION,
                                {', '.join(['AVG(e.'+en+') AS '+en for en in errorNameList])}
                            FROM
                                error e
                            GROUP BY
                                e.ID_PARAMETER, e.ID_CONDITION
                        )
                        SELECT t1.*
                        FROM avg_errors t1
                        LEFT JOIN avg_errors t2
                        ON t1.ID_CONDITION = t2.ID_CONDITION
                        AND t1.ID_PARAMETER != t2.ID_PARAMETER
                        AND {' AND '.join(['t1.'+en+' >= t2.'+en for en in errorNameList])}
                        AND ({' OR '.join(['t1.'+en+' > t2.'+en for en in errorNameList])})
                        WHERE t2.ID_PARAMETER IS NULL;'''
        else:
            query = f'''WITH avg_errors AS (
                            SELECT
                                e.ID_PARAMETER,
                                {', '.join(['AVG(e.'+en+') AS '+en for en in errorNameList])}
                            FROM
                                error e
                            GROUP BY
                                e.ID_PARAMETER
                        )
                        SELECT t1.*
                        FROM avg_errors t1
                        LEFT JOIN avg_errors t2
                        ON t1.ID_PARAMETER != t2.ID_PARAMETER
                        AND {' AND '.join(['t1.'+en+' >= t2.'+en for en in errorNameList])}
                        AND ({' OR '.join(['t1.'+en+' > t2.'+en for en in errorNameList])})
                        WHERE t2.ID_PARAMETER IS NULL;'''
        
        return pd.read_sql(query, self.conn)

    def __getitem__(self, table: str) -> pd.DataFrame:
        df = pd.read_sql(f"SELECT * FROM {table}", self.conn)
        df.columns = [c.replace('$', '.') for c in df.columns.values]
        return df

    def summarize(self) -> str:
        """
        Returns a summary of the database.

        Returns
        -------
        - **str** : The summary of the database.
        """
        summary = []
        summary.append("Database Summary:")
        for table in self.available_tables:
            df = pd.read_sql(f"SELECT * FROM {table} LIMIT 5", self.conn)
            count_query = f"SELECT COUNT(*) FROM {table}"
            count = self.conn.execute(count_query).fetchone()[0]
            summary.append(f"\nTable: {table}")
            summary.append(f"Number of rows: {count}")
            summary.append("Preview:")
            summary.append(df.head().to_string())
        return "\n".join(summary)
        
    def __str__(self) -> str:
        return self.summarize()
