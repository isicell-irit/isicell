#pragma once

#include <iostream>
#include <sstream>
#include <stdexcept>
#if __has_include(<libpq-fe.h>)
#  include <libpq-fe.h>
#else
#  include <postgresql/libpq-fe.h>
#endif

namespace PluginSQL {
class Connection {
  PGconn* conn = nullptr;

  public:
  Connection(std::string user, std::string pass, std::string host, int port, std::string dbname) {
    int libpq_ver = PQlibVersion();
    //std::cout << "Version of libpq: " <<  libpq_ver << std::endl;

    std::stringstream ss;
    ss << "user=" << user << " password=" << pass << " host=" << host << " dbname=" << dbname << " port=" << port;

    conn = PQconnectdb(ss.str().c_str());

    if(PQstatus(conn) != CONNECTION_OK)
      throw std::runtime_error(PQerrorMessage(conn));

    //std::cout << "Server version: " <<  PQserverVersion(conn)  << std::endl;
    //std::cout << "User: " <<  PQuser(conn)<< std::endl;
    //std::cout << "Database name: " << PQdb(conn) << std::endl;
  }

  ~Connection(){
    if(conn != nullptr)
      PQfinish(conn);
    conn = nullptr;
  }

  void Execute(std::string query){
    // same for insert, update, delete, begin, commit ...
    PGresult*  res = PQexec(conn, query.c_str()); 

    if(PQresultStatus(res) != PGRES_COMMAND_OK)
      throw std::runtime_error(PQerrorMessage(conn));
  }


};
}
