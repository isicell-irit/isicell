#pragma once

#include <iostream>
#include <chrono>
#include "Connection.hpp"
#include "SchemeSQL.hpp"

#define LOG std::cout << "PluginSQL: "
#define ERR std::cerr << "PluginSQL: "

namespace PluginSQL {
    template<typename cell_t>
    class PluginSQL {
        std::string host;
        int port;
        std::string user;
        std::string password;
        std::string database;
        
        Scheme scheme;
        Connection conn;

    public:

        PluginSQL(nlohmann::json const &config, bool dropTables = false) :
                host(config["sql"]["host"]),
                port(config["sql"]["port"]),
                user(config["sql"]["user"]),
                password(config["sql"]["password"]),
                database(config["sql"]["database"]),
                scheme(config["id"].get<std::string>()),
                conn(user, password, host, port, database){
            CreateTables(dropTables);
        }
 
        template<typename world_t>
        void endUpdate(world_t *w) {
                std::stringstream cellInsertStatement;
                std::stringstream worldInsertStatement;
            try {
                //auto t1 = std::chrono::high_resolution_clock::now();


                // Cell Data Update
                //TODO: use COPY 
                cellInsertStatement << "INSERT INTO " << scheme.CellTable << " " << scheme.CellTableDataLayout
                                    << " VALUES ";

                auto &cells = w->cells;
                for (size_t i = 0; i < std::max(int(cells.size() - 1),0); i++) {
                     scheme.CellValueStatement(cellInsertStatement,cells[i], w);
                     cellInsertStatement << ", ";
                }

                if (cells.size() > 0)
                    scheme.CellValueStatement(cellInsertStatement,cells[cells.size() - 1], w);

                cellInsertStatement << ";";

                // Execute Statement
                if(cells.size() > 0) conn.Execute(cellInsertStatement.str());


                // World Data Update
                worldInsertStatement << "INSERT INTO " << scheme.WorldTable << " " << scheme.WorldTableDataLayout
                                     << " VALUES " << scheme.WorldValueStatement(w) << ";";

                // Execute Statement
                conn.Execute(worldInsertStatement.str());

                /*auto t2 = std::chrono::high_resolution_clock::now();
                ERR << "Database update took "
                    << std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count()
                    << " milliseconds\n";*/
            } catch (const std::exception &e) {
                LOG << cellInsertStatement.str() << std::endl;
                LOG << worldInsertStatement.str() << std::endl;
                ERR << "Error: " << e.what() << std::endl;
                throw;
            }
        }

    protected:
        void CreateTables(bool dropTablesIfExist) {
            if (dropTablesIfExist) {
                LOG << "Dropping table '" << scheme.CellTable << "'" << std::endl;
                std::string dropCellTable = "DROP TABLE IF EXISTS " + scheme.CellTable + ";";
                conn.Execute(dropCellTable);
            }

            LOG << "Creating table '" << scheme.CellTable << "'" << std::endl;
            conn.Execute(scheme.CellTableCreationStatement);

            if (dropTablesIfExist) {
                LOG << "Dropping table '" << scheme.WorldTable << "'" << std::endl;
                std::string dropWorldTable = "DROP TABLE IF EXISTS " + scheme.WorldTable + ";";
                conn.Execute(dropWorldTable);
            }

            LOG << "Creating table '" << scheme.WorldTable << "'" << std::endl;
            conn.Execute(scheme.WorldTableCreationStatement);
        }
    };
}

