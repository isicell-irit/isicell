//'''
#pragma once

#include <string>
#include <sstream>

/*isigen:eval@utilityDeclarations'''
	attributesToIgnore = ['type', 'state']
	correctAttribute = [a for a in builderData['CellAttributesRecorded'] if a['value'] not in attributesToIgnore]
	def convert(type) :
        conversion = "DOUBLE PRECISION"
        if (type == "int") | ((type in builderData['enums']) if 'enums' in builderData else False):
            conversion = "INTEGER"
        return conversion

	def cast_enum(a):
		if a['type'] not in builderData['enums']:
			return 'c->'+a['value']
		else:
			return 'static_cast<unsigned int>(c->'+a['value']+')'
	'''*/

namespace PluginSQL {

	struct Scheme {

	public:
		const std::string WorldTable;
		const std::string CellTable;

		std::string WorldTableCreationStatement;
		std::string CellTableCreationStatement;

		const std::string WorldTableDataLayout;
		const std::string CellTableDataLayout;


		Scheme(const std::string &simulation_id) :
			WorldTable(simulation_id + "_world"),
			CellTable(simulation_id + "_cells"),
			WorldTableCreationStatement("CREATE TABLE " + simulation_id + "_world (" +
																	"step           integer           NOT NULL," +
																	"num_cells      integer           NOT NULL," +
																	"hours          real              NOT NULL"  +
																	"); "),
			CellTableCreationStatement("CREATE TABLE " + simulation_id + "_cells (" +
																	/*isigen:insert@CreateTableCells'''
																		print('\n'.join(['"{value}\t{type}\t\tNOT NULL," +'.format(value=a['value'], type=convert(a['type'])) for a in correctAttribute]))
																		'''*/
																	"step        integer             NOT NULL," +
																	"cell_id     integer             NOT NULL," +
																	"type        integer             NOT NULL," +
																	"state       integer             NOT NULL," +
																	"x           real                NOT NULL," +
																	"y           real                NOT NULL," +
																	"z           real                NOT NULL," +
																	"radius      real                NOT NULL);"),
			CellTableDataLayout(std::string("(")+
													/*isigen:insert@CellTableDataLayout'''
														print('"'+', '.join([a['value'] for a in correctAttribute])+'," +')
														'''*/
													"step, cell_id, type, state, x, y, z, radius)"),
			WorldTableDataLayout("(step, num_cells, hours)")
		{}

		template<typename cell_t, typename world_t>
		void CellValueStatement(std::stringstream& ss, cell_t *c, world_t *w) {
			auto pos = c->getBody().getPosition();
			ss << "(";
			/*isigen:insert@CellValueStatement'''
				print('\n'.join(['ss << "CAST(" << {value} << " AS {type})" << ", ";'.format(value=cast_enum(a), type=convert(a['type'])) for a in correctAttribute]))
				'''*/
			ss << w->getNbUpdates() << ", ";
			ss << c->getId() << ", ";
			ss << static_cast<unsigned int>(c->getType()) << ",";
			ss << static_cast<unsigned int>(c->getState()) << ",";
			ss << pos.x() << ", ";
			ss << pos.y() << ", ";
			ss << pos.z() << ", ";
			ss << c->getBody().getBoundingBoxRadius();
			ss << " )";
		}

		template<typename world_t>
		std::string WorldValueStatement(world_t *w) {
			std::stringstream ss;
			ss << "(";
			ss << w->getNbUpdates() << ", ";
			ss << w->cells.size() << ", ";
			ss << w->getNbUpdates() * w->dt / 3600;
			ss << " )";

			return ss.str();
		}

	};
}
//'''
