# ISiCell

## Overview

ISiCell is a simulation framework for modeling and simulating cellular behaviors and interactions. It provides various modules and plugins to handle different aspects of cellular simulations, such as physics, diffusion, and spheroid management.

## Modules

### HertzianPhysics
- **PhysicsConstraints**: Manages physical constraints in a simulation.
- **PluginHertzianPhysics**: Manages Delaunay triangulation and Hertzian physics.
- **BodyHertzianPhysics**: Represents a cell body with Hertzian physics.
- **Tensor**: Represents a 3x3 tensor for stress calculations.

### MassSpringDamper
- **MassSpringDamper**: Manages mass-spring-damper physics.
- **PluginDelaunayMassSpringDamper**: Manages Delaunay triangulation and mass-spring-damper physics.
- **BodyDelaunayMassSpringDamper**: Represents a cell body with mass-spring-damper physics.

### SpheroidManager
- **PluginSpheroidManager**: Manages spheroid bodies.
- **BodySpheroidManager**: Represents a cell body managed by the SpheroidManager plugin.

### GrimesOxygen
- **PluginGrimesOxygen**: Manages spherical oxygen diffusion.
- **BodyOxygen**: Represents a cell body with spherical oxygen diffusion.

### Diffusion2D
- **PluginDiffusion**: Manages grid-based diffusion.
- **DiffusionGrid**: Manages a 2D grid for molecule diffusion.
- **BodyDiffusion**: Represents a cell body with molecule diffusion.

## Getting Started

To get started with ISiCell, you can explore the various modules and plugins provided in the framework. Each module contains classes and functions that handle specific aspects of cellular simulations.

## Documentation

The detailed documentation for each class and function is available in the respective header files. You can refer to the Doxygen comments in the code for more information on the usage and functionality of each component.