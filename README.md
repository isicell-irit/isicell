<!-- Improved compatibility of back to top link: See: https://gitlab.com/othneildrew/Best-README-Template/pull/73 -->
<a name="readme-top"></a>
<!--
*** Thanks for checking out the Best-README-Template. If you have a suggestion
*** that would make this better, please fork the repo and create a pull request
*** or simply open an issue with the tag "enhancement".
*** Don't forget to give the project a star!
*** Thanks again! Now go create something AMAZING! :D
-->



<!-- PROJECT SHIELDS --->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->
[![Contributors][contributors-shield]][contributors-url]
[![Forks][forks-shield]][forks-url]
[![Stargazers][stars-shield]][stars-url]
[![LGPL-3.0 license][license-shield]][license-url]
<!--[![Issues][issues-shield]][issues-url] -->


<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://gitlab.com/isicell-irit/isicell">
    <img src="https://gitlab.com/uploads/-/system/project/avatar/48083643/logoISiCell.png" alt="Logo">
  </a>

<!--<h3 align="center">ISiCell</h3> -->

  <p align="center">
    ISiCell is a web platform allowing for codesigning cell biology agent based models.
    <br />
    <a href="https://isicell.irit.fr/doc/"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://isicell.irit.fr/">View Demo</a>
    ·
    <a href="https://gitlab.com/isicell-irit/isicell/issues">Report Bug</a>
    ·
    <a href="https://gitlab.com/isicell-irit/isicell/issues">Request Feature</a>
  </p>
</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
    </li>
    <li>
      <a href="#contributing">Contributing</a>
    </li>
    <li>
      <a href="#license">License</a>
    </li>
    <li>
      <a href="#contact">Contact</a>
    </li>
    <li>  
      <a href="#acknowledgments">Acknowledgments</a>
    </li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

<!--[![Product Name Screen Shot][product-screenshot]](https://isicell.irit.fr) -->

The ISiCell platform aims at facilitating interactions between biologists and modelers while developing cell biology agent based models. For this purpose, the platform relies on different tools to maximize the intercomprehension and involvement  of the biologists in the modelling project and minimize frustration and development time.

An accessible instance of ISiCell is testable on our server at <a href="https://isicell.irit.fr"> this address</a>. 

<p align="right">(<a href="#readme-top">back to top</a>)</p>


<!-- GETTING STARTED -->
## Getting Started

Our platform was developed on Debian 12 and we suggest to install your personal instance with an appropriate docker image.
You can also use the Dockerfile provided in this project to build your local instance, using the following commands:
```sh
mkdir -p homes demos users
docker build --build-arg ISICELL_VER=$(date +%s) -t isicell . && docker run -d -v "$(pwd)"/users:/usr/src/app/ISiCell/web/CellBuilder/users  -v "$(pwd)"/demos:/usr/src/app/ISiCell/web/CellBuilder/demos -v "$(pwd)"/homes:/usr/src/app/ISiCell/homes -p 8080:8080 --name isicell -t isicell && docker logs --follow isicell
```

After a few minutes, you will be able to access your local instance at <a href="http://localhost:8080"> localhost:8080</a>. The default username and password are admin and admin.

To get started, we recommend you consult the documentation, which contains a few examples of use, and download one of the demos available on our <a href="https://isicell.irit.fr"> instance</a>.

<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".
Don't forget to give the project a star! Thanks again!

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- LICENSE -->
## License

Distributed under the GNU Lesser General Public License v3.0. See `LICENSE.txt` for more information.

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- CONTACT -->
## Contact

Cogoni Florian - flcogoni@gmail.com

Project Link: [https://gitlab.com/isicell-irit/isicell](https://gitlab.com/isicell-irit/isicell)

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- ACKNOWLEDGMENTS -->
## Acknowledgments

This work is supported by grants from the Occitanie Region and University Toulouse 521
Capitole (OnkoOptim project), from Bristol-Myers-Squibb (no. CA184-575) and from 522
the AI Interdisciplinary Institute ANITI (funded by the French program “Investing for 523
the Future – PIA3” under Grant agreement no. ANR-19-PI3A-0004). The research has 524
received funding from the European Research Council (ERC) under the European 525
Union’s Horizon 2020 Research and Innovation Programme (Grant agreement No. Syn- 526
951329).

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[contributors-shield]: https://img.shields.io/gitlab/contributors/isicell-irit/isicell.svg?style=for-the-badge
[contributors-url]: https://gitlab.com/isicell-irit/isicell/-/graphs/main
[forks-shield]: https://img.shields.io/gitlab/forks/isicell-irit/isicell.svg?style=for-the-badge
[forks-url]: https://gitlab.com/isicell-irit/isicell/-/forks
[stars-shield]: https://img.shields.io/gitlab/stars/isicell-irit/isicell.svg?style=for-the-badge
[stars-url]: https://gitlab.com/isicell-irit/isicell/-/starrers
[issues-shield]: https://img.shields.io/gitlab/issues/isicell-irit/isicell.svg?style=for-the-badge
[issues-url]: https://gitlab.com/isicell-irit/isicell/issues
[license-shield]: https://img.shields.io/gitlab/license/isicell-irit/isicell.svg?style=for-the-badge
[license-url]: https://gitlab.com/isicell-irit/isicell/blob/main/LICENSE
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://linkedin.com/in/linkedin_username
[product-screenshot]: https://isicell.irit.fr/img/builder.png
[Next.js]: https://img.shields.io/badge/next.js-000000?style=for-the-badge&logo=nextdotjs&logoColor=white
[Next-url]: https://nextjs.org/
[React.js]: https://img.shields.io/badge/React-20232A?style=for-the-badge&logo=react&logoColor=61DAFB
[React-url]: https://reactjs.org/
[Vue.js]: https://img.shields.io/badge/Vue.js-35495E?style=for-the-badge&logo=vuedotjs&logoColor=4FC08D
[Vue-url]: https://vuejs.org/
[Angular.io]: https://img.shields.io/badge/Angular-DD0031?style=for-the-badge&logo=angular&logoColor=white
[Angular-url]: https://angular.io/
[Svelte.dev]: https://img.shields.io/badge/Svelte-4A4A55?style=for-the-badge&logo=svelte&logoColor=FF3E00
[Svelte-url]: https://svelte.dev/
[Laravel.com]: https://img.shields.io/badge/Laravel-FF2D20?style=for-the-badge&logo=laravel&logoColor=white
[Laravel-url]: https://laravel.com
[Bootstrap.com]: https://img.shields.io/badge/Bootstrap-563D7C?style=for-the-badge&logo=bootstrap&logoColor=white
[Bootstrap-url]: https://getbootstrap.com
[JQuery.com]: https://img.shields.io/badge/jQuery-0769AD?style=for-the-badge&logo=jquery&logoColor=white
[JQuery-url]: https://jquery.com 
