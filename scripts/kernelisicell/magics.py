import sys
import math
import shlex
from getopt import getopt
import timeit
import os
import re
class MagicFunctionNotFound(Exception):
    def __init__(self, fn):
        super().__init__(f'magic function `{fn}` not found.')

def _format_time(timespan, precision=3):
    """Formats the timespan in a human readable form"""
    if timespan >= 60.0:
        # we have more than a minute, format that in a human readable form
        # Idea from http://snipplr.com/view/5713/
        parts = [("d", 60*60*24),("h", 60*60),("min", 60), ("s", 1)]
        time = []
        leftover = timespan
        for suffix, length in parts:
            value = int(leftover / length)
            if value > 0:
                leftover = leftover % length
                time.append(u'%s%s' % (str(value), suffix))
            if leftover < 1:
                break
        return " ".join(time)
    # Unfortunately the unicode 'micro' symbol can cause problems in
    # certain terminals.  
    # See bug: https://bugs.launchpad.net/ipython/+bug/348466
    # Try to prevent crashes by being more secure than it needs to
    # E.g. eclipse is able to print a µ, but has no sys.stdout.encoding set.
    units = [u"s", u"ms",u'us',"ns"] # the save value   
    if hasattr(sys.stdout, 'encoding') and sys.stdout.encoding:
        try:
            u'\xb5'.encode(sys.stdout.encoding)
            units = [u"s", u"ms",u'\xb5s',"ns"]
        except:
            pass
    scaling = [1, 1e3, 1e6, 1e9]
        
    if timespan > 0.0:
        order = min(-int(math.floor(math.log10(timespan)) // 3), 3)
    else:
        order = 3
    return u"%.*g %s" % (precision, timespan * scaling[order], units[order])

def magic_timeit(interpreter,opts,cell):
    opts, stmt = getopt(shlex.split(opts,posix=True), "n:r:tcp:qo")
    opts = {k.replace('-',''):v for k,v in opts}
    stmt = ' '.join(stmt)

    number = int(opts.get("n", 0))
    repeat = int(opts.get("r",  7 if timeit.default_repeat < 7 else timeit.default_repeat))
    precision = int(opts.get("p", 3))
    quiet = 'q' in opts
    return_result = 'o' in opts
    timefunc = timeit.default_timer
    if hasattr(opts, "t"): timefunc = time.time
    if hasattr(opts, "c"): timefunc = clock
    if cell is None:
        setup = "pass"
        stmt = stmt
    else:
        setup = stmt
        stmt = cell

    if number == 0:
        for index in range(0, 10):
            number = 10 ** index
            time_number = timeit.timeit(stmt,setup,timer=timefunc,number=number,globals=interpreter.scope)
            if time_number >= 0.2:
                break
    all_runs = timeit.repeat(stmt,setup,timer=timefunc,repeat=repeat,number=number,globals=interpreter.scope)
    best = min(all_runs) / number
    worst = max(all_runs) / number
    timings = [ dt / number for dt in all_runs]

    mean = math.fsum(timings) / len(timings)
    std = (math.fsum([(x - mean) ** 2 for x in timings]) / len(timings)) ** 0.5
    res = "{mean} {pm} {std} per loop (mean {pm} std. dev. of {runs} run{run_plural}, {loops:,} loop{loop_plural} each)".format(
                pm='+-',
                runs=repeat,
                loops=number,
                loop_plural="" if number == 1 else "s",
                run_plural="" if repeat == 1 else "s",
                mean=_format_time(mean, precision),
                std=_format_time(std, precision),
            )
    print(res)

def magic_lsmagic(interpreter,opts,cell):
    print(list(interpreter.magics.magic_cmd.keys()))


def magic_history(interpreter,opts,cell):
    flat_list = []
    for row in interpreter.cache.values():
        flat_list += row
    print('\n'.join(flat_list))


def magic_resetHistory(interpreter,opts,cell):
    interpreter.cache = {}
    print("history clear")


class Magics(object):
    def __init__(self,interpreter):
        self.interpreter = interpreter
        self.re_magic = re.compile(r'^\s*%(\w+)\s*(.*)',re.M)
        self.re_magic_cell = re.compile(r'^\s*%%(\w+)\s*(.*)\n([\w\W]*)')
        self.re_cmd = re.compile(r'^\s*!\s*(.*)',re.M)
        self.magic_cmd = {}
        self.magic_cmd['timeit'] = magic_timeit
        self.magic_cmd['lsmagic'] = magic_lsmagic
        self.magic_cmd['history'] = magic_history
        self.magic_cmd['reset_history'] = magic_resetHistory

    def transform(self,code):
        for cmd,header,cell in self.re_magic_cell.findall(code):
            if(cmd in self.magic_cmd): self.magic_cmd[cmd](self.interpreter,header,cell)
            else: raise MagicFunctionNotFound('%%'+cmd)
        code = self.re_magic_cell.sub('',code)

        for cmd,header in self.re_magic.findall(code):
            if(cmd in self.magic_cmd): self.magic_cmd[cmd](self.interpreter,header,None)
            else: raise MagicFunctionNotFound('%'+cmd)
        code = self.re_magic.sub('',code)

        for cmd in self.re_cmd.findall(code):
            os.system(cmd)
        code = self.re_cmd.sub('',code)

        return code


