import inspect
import re


def getType(v):
    if inspect.isclass(v) or inspect.ismodule(v): return v
    t = type(v)
    n = t.__name__
    return t if inspect.isclass(t) and n != 'type' and n != 'method' and n != 'function' and n != 'module' and n != 'EnumType' and n != 'Enum' else v

def checkAnnotationReturn(variable):
    try: 
        t = inspect.signature(variable).return_annotation
        if t.__name__ == '_empty': return None
        else: return str(t)
    except: return None

def getOutputType(variable):
  try:
      varType = getType(variable)
      if inspect.isclass(varType) and (varType.__name__ != 'property'): return varType.__name__
      elif inspect.isclass(varType): varType=variable
      if retType:=checkAnnotationReturn(varType): return retType
      if m:= re.match(r'\s*(.*?)(\(.*\))* (\-\>|\:) (p\.)?([\w_]+[\w\d_]*::)*(?P<type>[\w_]+[\w\d_]*)',varType.__doc__):return m.group('type')
      if m:= re.search(r'\:return\: (p\.)?(?P<type>[\w_]+[\w\d_]*)',varType.__doc__): return m.group('type')
  except Exception as ex:
      return None

def getKind(v):
    if isinstance(v, str): return 18
    if inspect.ismodule(v): return 8
    if inspect.isclass(v): return 5
    if inspect.ismethod(v): return 0
    if inspect.isfunction(v): return 1
    if callable(v): return 1
    return 4 #variable

def getDoc(name, variable, variableType=None,filterPrefix='_'):
    if name.startswith(filterPrefix): return False
    if (name == 'thisown') and (variable.__doc__ == 'The membership flag'): return False
    signatureShort = ''
    signatureLong = ''
    doc = ''
    retType = 'None'
    try:
        if variableType: retType = variableType
        else: retType = getOutputType(variable)
        if callable(variable):
            signatureShort = '()'
            try:
                signatureLong = str(inspect.signature(getType(variable)))
            except: 
                signatureLong = '()'
    except: pass
    try: 
        doc = re.sub(r'(>>>[\s\S]*?(?=\n\n|\Z))',r'```python\n\1\n```',inspect.cleandoc(inspect.getdoc(variable)))
    except: pass
    return {'label': name,
            'insertText': name+signatureShort,
            'return_type':retType,
            'documentation': {'value':'```python\n'+name+signatureLong+'\n```\n\n___\n\n'+doc,'isTrusted': True},
            'kind':getKind(variable) }
    
def getDocClass(variable,deep=1):
    if deep <= 0: return {}
    try:
        vType = getType(variable).__name__
        if vType in alreadySendType: return {}
        alreadySendType.append(vType)
        compClass = getDoc(vType, variable)
        try: propertiesType = inspect.get_annotations(variable)
        except: propertiesType = {}
        typeInfo = {}
        if(compClass):
            classInfo = {'current':compClass, 'members': []}
            for memberName,memberVariable in inspect.getmembers(variable):
                compMember = getDoc(memberName, memberVariable, str(propertiesType[memberName]) if memberName in propertiesType else None)
                if compMember:
                    classInfo['members'].append(compMember)
                    if inspect.isclass(memberVariable):
                        typeInfo.update(getDocClass(memberVariable))
                    if inspect.ismodule(memberVariable):
                        typeInfo.update(getDocClass(memberVariable,deep-1))
                        #if deep>1: print(deep, vType,' > ',memberVariable)
            typeInfo.update({vType:classInfo})
            return typeInfo
    except: pass
    return {}

alreadySendType = []
alreadySendBuiltins = False
builtinsVar = {}

def getCompletion(scopes,filterPrefix='_'):
    global alreadySendBuiltins
    newType = {}
    variables = {**builtinsVar}
    if not alreadySendBuiltins:
        alreadySendBuiltins = True
        comp = getCompletion(dict(inspect.getmembers(scopes['__builtins__'])))['completion']
        newType = comp['newType']
        variables = comp['variables']
        builtinsVar.update(variables)
    for name,variable in scopes.items():
        if name.startswith(filterPrefix): continue
        if inspect.isfunction(variable) or inspect.isbuiltin(variable):
            variables[name] = getDoc(name,variable)
            continue
        vType = getType(variable).__name__
        variables[name] = vType
        if (vType in alreadySendType) : continue
        comp = getDoc(vType, variable)
        if(comp): 
            newType.update(getDocClass(variable))
        alreadySendType.append(vType)
    # scopes.reset_tracker()
    return {'completion':{'variables':variables,'newType':newType}}