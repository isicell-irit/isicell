import base64
from io import BytesIO
from io import StringIO
import matplotlib
import sys
import os
from matplotlib._pylab_helpers import Gcf
from matplotlib.backends.backend_agg import new_figure_manager, FigureCanvasAgg
from matplotlib.backend_bases import ShowBase, FigureManagerBase
from matplotlib.figure import Figure
from .communication import sendOutput

########################################################################
#
# The following functions and classes are for pylab and implement
# window/figure managers, etc...
#
########################################################################

class Show(ShowBase):
    """
    A callable object that displays the figures to the screen. Valid kwargs
    include figure width and height (in units supported by the div tag), block
    (allows users to override blocking behavior regardless of whether or not
    interactive mode is enabled, currently unused) and close (Implicitly call
    matplotlib.pyplot.close('all') with each call to show()).
    """
    def __call__(self, close=None, block=None, **kwargs):
        if close is None:
            close = True
        try:
            managers = Gcf.get_all_fig_managers()
            if not managers:
                return
            for manager in managers:
                manager.show(**kwargs)
        finally:
            # This closes all the figures.
            if close and Gcf.get_all_fig_managers():
                Gcf.destroy_all()


class FigureCanvasISiCell(FigureCanvasAgg):
    """
    The canvas the figure renders into. Calls the draw and print fig
    methods, creates the renderers, etc...
    """
    def get_bytes(self, **kwargs):
        """
        Get the byte representation of the figure.
        Should only be used with jpg/png formats.
        """
        # Make sure format is correct
        fmt = kwargs.get('format', 'png')
        if fmt == 'svg':
            raise ValueError("get_bytes() does not support svg, use png or jpg")
        
        # Express the image as bytes
        buf = BytesIO()
        self.print_figure(buf, **kwargs)
        byte_str = base64.b64encode(buf.getvalue()).decode('ascii')
        buf.close()
        return byte_str

    def get_svg(self, **kwargs):
        """
        Get the svg representation of the figure.
        Should only be used with svg format.
        """
        # For SVG the data string has to be unicode, not bytes
        buf = StringIO()
        self.print_figure(buf, **kwargs)
        svg_str = buf.getvalue()
        buf.close()
        return svg_str
    
    def draw_idle(self, *args, **kwargs):
        """
        Called when the figure gets updated (eg through a plotting command).
        This is overriden to allow open figures to be reshown after they
        are updated when mpl_config.get('close') is False.
        """
        if not self._is_idle_drawing:
            with self._idle_draw_cntx():
                self.draw(*args, **kwargs)
                

class FigureManagerISiCell(FigureManagerBase):
    """
    Wrap everything up into a window for the pylab interface
    """
    def __init__(self, canvas, num):
        FigureManagerBase.__init__(self, canvas, num)
        self._shown = False
                

    def show(self, **kwargs):
        if not self._shown:
            fig = self.canvas.figure
            fmt = kwargs.get('format', 'png')

            # Check if format is supported
            supported_formats = ['png','jpg','svg']
            if fmt not in supported_formats:
                raise ValueError("Unsupported format %s" %fmt)
            
            # For SVG the data string has to be unicode, not bytes
            if fmt == 'svg':
                img = [fig.canvas.get_svg(**kwargs)]
                fmt = 'text/html'
            else:
                # Express the image as bytes
                img = fig.canvas.get_bytes(**kwargs)
                fmt = 'image/'+fmt
            
            # Print the image to the notebook paragraph via the %html magic
            sendOutput("display_data", fmt, img)
        else:
            self.canvas.draw_idle()
            
        self._shown = True


def new_figure_manager(num, *args, **kwargs):
    """
    Create a new figure manager instance
    """
    # if a main-level app must be created, this (and
    # new_figure_manager_given_figure) is the usual place to
    # do it -- see backend_wx, backend_wxagg and backend_tkagg for
    # examples.  Not all GUIs require explicit instantiation of a
    # main-level app (egg backend_gtk, backend_gtkagg) for pylab
    FigureClass = kwargs.pop('FigureClass', Figure)
    thisFig = FigureClass(*args, **kwargs)
    return new_figure_manager_given_figure(num, thisFig)


def new_figure_manager_given_figure(num, figure):
    """
    Create a new figure manager instance for the given figure.
    """
    canvas = FigureCanvasISiCell(figure)
    manager = FigureManagerISiCell(canvas, num)
    return manager

def load_backend():
    sys.path.append(__file__)
    matplotlib.use('module://kernelisicell.'+os.path.basename(__file__).split('.')[0])
    
########################################################################
#
# Now just provide the standard names that backend.__init__ is expecting
#
########################################################################

# Create a reference to the show function we are using. This is what actually
# gets called by matplotlib.pyplot.show().
show = Show()

# Default FigureCanvas and FigureManager classes to use from the backend
FigureCanvas = FigureCanvasISiCell
FigureManager = FigureManagerISiCell