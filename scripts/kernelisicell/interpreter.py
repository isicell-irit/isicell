import threading
import os
import ast
import sys
import re
import traceback
from pygments import highlight
from pygments.lexers import PythonLexer
from pygments.lexers import Python3TracebackLexer
from pygments.formatters import TerminalFormatter
from .communication import sendError,sendEndCellExcution,sendCompletion,sendCurrentLine
from .utils import display
from .magics import Magics
import time

class _KillableThread(threading.Thread):
    def __init__(self, *args, **keywords):
        threading.Thread.__init__(self, *args, **keywords)
        self.killed = False
        self.__run_backup = self.run
        self.run = self.__run

    def __run(self):
        sys.settrace(self.globaltrace)
        self.__run_backup()
        self.run = self.__run_backup

    def globaltrace(self, frame, event, arg):
        if event == 'call':
            return self.localtrace
        else:
            return None

    def localtrace(self, frame, event, arg):
        if self.killed:
            if event == 'line':
                raise SystemExit()
        return self.localtrace
    
    def kill(self):
        from multiprocessing import active_children
        active = [a for a in active_children() if a.pid != self.pid]
        for child in active:
            child.terminate()
        for child in active:
            child.join()
        for child in active:
            child.close()
        self.killed = True


def _format_exception_addCode(exec_count,lineNumber,interpreter):
    if(exec_count not in interpreter.cache): return '       \u001b[39m<history not found>'
    lines = interpreter.cache[exec_count]
    codeError = ''
    hc = [highlighterCode(lines[lineNumber+i]).replace('\n','') for i in range(-2,1) if lineNumber+i>=0 and lineNumber+i<len(lines)]
    if(lineNumber-2>=0): errorLine = 1
    else: errorLine = 0
    for i in range(len(hc)):
        if i==errorLine:
            hc[i]=f'\u001b[32m---> {lineNumber} \u001b[39;49;00m'+hc[i]
        else:
            hc[i]=f'     \u001b[32m{lineNumber+i-errorLine} \u001b[39;49;00m'+hc[i]
    codeError = '\n'.join(hc)
    return codeError

highlighterTraceback = lambda txt: highlight(txt, Python3TracebackLexer(), TerminalFormatter())
highlighterCode = lambda txt: highlight(txt, PythonLexer(), TerminalFormatter())

def _format_exception(e,interpreter):
    exec_count = interpreter.exec_count
    tb = traceback.format_tb(e.__traceback__)
    interTb = tb.pop(0)
    ret = f'\u001b[91m---------------------------------------------------------------------------\n{e.__class__.__name__}                                 \u001b[39;49;00mTraceback (most recent call last)\n'
    if e.__class__.__name__ == "SyntaxError" and bool(re.search(r'File ".*interpreter\.py"', interTb)):
        lineNumber = int(re.findall(' line (\d+)', str(e))[0])
        ret += _format_exception_addCode(exec_count,lineNumber,interpreter)
    else:
        for t in tb:
            tmp = re.findall(r'File "Cell (\d+)", line (\d+)',t)
            tcolored = highlighterTraceback(re.split(r'\n.*(_sys|_kisc)',t)[0])
            if len(tmp)>0:
                exec_count = int(tmp[0][0])
                lineNumber = int(tmp[0][1])
                tcolored = tcolored.replace(', in <module>','')
                tcolored = re.sub(r'File \u001b\[36m"Cell (\d+)"',f'\u001b[35mCell \u001b[32mIn[{exec_count}]',tcolored)
                tcolored += _format_exception_addCode(exec_count,lineNumber,interpreter)+'\n'
            ret += tcolored+'\n'
    tb = (ret+ f'\n\u001b[91m{e.__class__.__name__}\u001b[39m: {str(e).replace("(<unknown>, ","(")}').split('\n')
    return tb


def _evalate_code(code,interpreter):
    try:
        interpreter.currentCommand.pid = threading.get_native_id()
        code = interpreter.magics.transform(code)
        parsed = ast.parse(code)
        lastEval = len(parsed.body)-1
        for i,n in enumerate(parsed.body):
            sendCurrentLine(n.lineno)
            if type(n) != ast.Expr:
                exec(compile(ast.Module(body=[n], type_ignores=[]), f"Cell {interpreter.exec_count}", "exec"), interpreter.scope)
            else:
                _tmp_ = eval(compile(ast.Expression(n.value), f"Cell {interpreter.exec_count}", "eval"), interpreter.scope)
                if (_tmp_ is not None) and (i==lastEval):
                    display(_tmp_)
    except Exception as e:
            sendError(e,_format_exception(e,interpreter))
    sendCompletion(interpreter.scope)
    sendEndCellExcution()

class Interpreter():
    def __init__(self):
        self.currentCommand = None
        self.scope = None
        self.cache = {}
        self.magics = Magics(self)
        self.exec_count = 0
    
    def setScope(self,scope):
        self.scope = scope

    def run(self,code):
        self.exec_count += 1
        self.cache[self.exec_count] = code.split('\n')
        self.currentCommand = _KillableThread(target=_evalate_code, args=(code,self ))
        self.currentCommand.start()
        self.currentCommand.join(2.0)

    def isRnning(self):
        return self.currentCommand.is_alive() if self.currentCommand is not None else False

    def kill(self):
        self.currentCommand.kill()



