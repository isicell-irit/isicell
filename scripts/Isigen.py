from io import StringIO
from contextlib import redirect_stdout
import re,sys,json

class Isigen:

	def __init__(self, _dataTemplate):
		self.__dataTemplate = _dataTemplate
		self.__keepIsigen = True
		self.debugData = {}

	def templateFile(self,filePath,keepIsigen=True):
		self.__keepIsigen=keepIsigen
		with open(filePath, "r+") as f :
			fileContent = f.read()
			f.seek(0)

			try:
				regex = r"(?P<insertTabs>[\t ]*?)(?P<pre>/\*isigen:)(?P<mode>\w*)(?:@(?P<name>\w+))?(?:'{3}\s*)(?P<isicodeTabs>\n[\t ]*)(?P<isicode>[\w\W]*?)(?P<post>\s+?'{3}\*/)"
				fileContent = re.sub(regex, self.__replaceBlockMatch, fileContent).replace('\t',' '*4)

				fileName = filePath.split('/')[-1]
				regex = r"(?P<insertTabs>[\t ]*)(?P<line>.*?)//isigen:metaDataDebug\[(?P<metaDataKey>.*?)\](?P<metaData>.*)\n"
				info = {m.group('metaDataKey'):{'line':fileContent[0:m.start()].count('\n')+1,'column':len(m.group('insertTabs')),**json.loads(m.group('metaData'))} for m in re.finditer(regex,fileContent)}
				
				if len(info)>0: self.debugData[fileName]=info
				fileContent = re.sub(regex, lambda m: m.group('insertTabs')+m.group('line')+'\n', fileContent)
			except Exception as ex:
				ex.message = 'in file "'+filePath+'" '+ex.message
				print('Error', ex.message, file=sys.stderr)
				raise

			f.write(fileContent)
			f.truncate()

	def __replaceBlockMatch(self,blockMatch):
		mode = blockMatch.group('mode')
		name = blockMatch.group('name')
		name = name if name else 'Undefined'
		isicodeTabs = blockMatch.group('isicodeTabs')
		isicode = blockMatch.group('isicode')
		insertTabs = '\n'+blockMatch.group('insertTabs')
		endStatement = '//isigen:end' if self.__keepIsigen else ''

		ret = blockMatch.group(0) if self.__keepIsigen else ''
		try:
			if mode == 'eval':
				self.__getBlockOutput(isicode.replace(isicodeTabs, '\n'))
			else:
				ret += (self.__getBlockOutput(isicode.replace(isicodeTabs, '\n'))+endStatement).replace('\n', insertTabs)
		except Exception as ex:
			ex.message = 'on block "'+name+'"'
			raise

		return ret

	def __getBlockOutput(self,generationCode):
		f = StringIO()
		with redirect_stdout(f):
			exec(generationCode, self.__dataTemplate)
		ret = f.getvalue()
		return '\n' if ret.replace(r'\s','') == '' else '\n'+ret

	def sendDebugData(self):
		print('metaInfo:',json.dumps(self.debugData), file=sys.stderr)


	def convertCppToPythonAnnotation(self,filePath):
		typeConv={
			'bool':'bool',
			'int':'int',
			'double':'float',
			'float':'float',
			'string':'str',
			'void':'None',
			'PrimoCell< CellBody >':'Cell',
			'CellBody< PrimoCell< CellBody > >':'CellPlugin',
			'CellBody< Cell >':'CellBody',
			'Vec':'Vector3D',
			'Vector3D':'Vector3D',
			'Type':'EnumType',
			'State':'EnumState',
		}
		typeConv.update({k:'Enum'+k for k in self.__dataTemplate["builderData"]["enums"].keys()})
		def typeConvertion(typeCpp):
			if typeCpp is not None:
				typeCpp = re.sub(r'([\w_][\w\d_]*\:\:| \*| \&| const |,std\:\:allocator\< .*? \* >)','',typeCpp)
				if typeCpp in typeConv:
					return typeConv[typeCpp]
				else:
					typeCpp = re.sub(r'(\<.*\>)','',typeCpp)
					if typeCpp in typeConv:
						return typeConv[typeCpp]
			return None

		def vectorConvertion(py,cpp):
			return {('vector< '*i)+cpp+' >'*i:py+('Vector'*i) for i in range(1,5) if (py+('Vector'*i)) in classList }

		def replaceFn(match):
			declarationFunc = match.group('declarationFunc')
			params = match.group('params')
			typeRet = typeConvertion(match.group('typeRet'))
			params = re.sub(r'\: \"(.*?)\"',lambda m: '' if (m2:=typeConvertion(m.group(1))) is None else ': '+m2,params)
			if typeRet is None: typeRet = ''
			else: typeRet = ' -> '+typeRet
			return f'{declarationFunc}({params}){typeRet}:'
		def defineSimuInit(m):
  			return f'''class Simu(object):{m.group('startClass')}

    def __init__(self, params: dict, seed: Optional[int] = None):
        
        {m.group('startInit')}
        
        _isiCellPy.Simu_swiginit'''

		with open(filePath, "r+") as f :
			fileContent = f.read()
			f.seek(0)
			regex = r"class (?P<className>.*)\(.*?\)\:"
			classList = re.findall(regex, fileContent)+['EnumState','EnumType']
			typeConv.update({k:v for cpp,py in typeConv.items() for k,v in vectorConvertion(py,cpp).items()})
			typeConv.update({k:k for k in classList})

			regex = r"(?P<declarationFunc>def .*?)\((?P<params>.*?)\)\s*(\-\>\s*\"(?P<typeRet>.*?)\")?\:"
			fileContent = re.sub(regex, replaceFn, fileContent)
			fileContent = re.sub(r': \"(.*?)\" = property\(', lambda m: ' = property(' if (m2:=typeConvertion(m.group(1))) is None else f': {m2} = property(', fileContent)
			fileContent = 'from __future__ import annotations\nfrom typing import Optional\n'+fileContent
			fileContent=re.sub(r'class Simu\(object\)\:(?P<startClass>.*?)\n\s*def __init__\(self, \*args\)\:\s*r""".*?"""\s*(?P<startInit>.*?)\n\s*_isiCellPy\.Simu_swiginit',defineSimuInit,fileContent,1,re.S)

			f.write(fileContent)
			f.truncate()