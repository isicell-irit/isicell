import json
import sys
import os
import subprocess
import traceback

def loadJson(_path):
  jsonFile = open(_path,"r")
  ret = json.load(jsonFile)
  jsonFile.close()
  return ret

def writeJson(_path, _content, _indent=2):
  jsonFile = open(_path,"w")
  ret = jsonFile.write(json.dumps(_content, indent = _indent))
  jsonFile.close()
  return ret

def exec(_cmd, _errorCode):
  ret = subprocess.run(f"script -eqc '{_cmd}' .typescript", shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, encoding='utf-8')
  os.system("rm -f .typescript") # clean script command stuff
  if(ret.returncode != 0):
    print(ret.stdout, file=sys.stderr)
    sys.exit(_errorCode)
  elif ret.stdout != "" :
    print(ret.stdout)
