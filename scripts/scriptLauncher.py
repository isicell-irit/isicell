import sys
import runpy
import os

# add isicell module to sys.path
sys.path.append(sys.argv[1])
# add ownfolder to sys.path
sys.path.append(os.getcwd())

if __name__ == "__main__":
    runpy.run_path(sys.argv[2], run_name='__main__')
