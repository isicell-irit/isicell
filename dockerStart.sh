#!/bin/bash

service postgresql restart

origin="origin"
branch="main"

new_commit(){
        # last commit hash
        commit=$(git log -n 1 --pretty=format:%H "$origin/$branch")

        # url of the remote repo
        url=$(git remote get-url "$origin")

        for line in "$(git ls-remote -h $url)"; do
                fields=($(echo $line | tr -s ' ' ))
                test "${fields[1]}" == "refs/heads/$branch" || continue
                test "${fields[0]}" == "$commit" && return 1 \
                || return 0
        done
}
update(){
        cd /usr/src/app/ISiCell/
        git pull
        git submodule update --remote
        cd /usr/src/app/ISiCell/core/ && doxygen
        cd /usr/src/app/ISiCell/web/CellBuilder && npm install && npm run build
}
while true; do
        cd /usr/src/app/ISiCell/web/CellBuilder/
        echo 'start Cell builder'
        NODE_ENV=prod node backend/src/app.js
        if new_commit; then 
                update
        fi
done