FROM node:current-bookworm

ENV LANG C.UTF-8

RUN apt-get update && apt-get upgrade -y && apt-get install -y util-linux postgresql postgresql-client cmake libcgal-dev libboost-all-dev python3 python3-venv python3-pip swig libblas-dev liblapack-dev libatlas-base-dev gfortran doxygen zsh gdb
RUN update-rc.d postgresql enable && service postgresql restart && su postgres -c 'psql -c "CREATE USER mecacell WITH PASSWORD '"'brdscb9204'"' CREATEDB"' && su postgres -c 'createdb -O mecacell mecacell'

WORKDIR /usr/src/app

ENV VIRTUAL_ENV=/opt/venv/.base
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"
RUN pip3 install --no-cache-dir --upgrade pip && \
    pip3 install --no-cache-dir matplotlib seaborn pandas jsonmerge json_flatten numpy SALib Pygments tqdm deap Jinja2 virtualenv-clone plotly networkx optuna

ARG ISICELL_VER=unknown

RUN git clone https://gitlab.com/isicell-irit/isicell.git  /usr/src/app/ISiCell/ &&\
    cd ISiCell && git submodule init && git submodule update && cd core && doxygen

RUN cd ISiCell/web/CellBuilder &&\
    npm install &&\
    npm run build

EXPOSE 8080

RUN chmod +x ISiCell/dockerStart.sh

CMD /usr/src/app/ISiCell/dockerStart.sh


#docker stop isicell && docker rm isicell; docker build --build-arg ISICELL_VER=$(date +%s) -t isicell . && docker run -d -v "$(pwd)"/users:/usr/src/app/ISiCell/web/CellBuilder/users  -v "$(pwd)"/demos:/usr/src/app/ISiCell/web/CellBuilder/demos -v "$(pwd)"/homes:/usr/src/app/ISiCell/homes -p 8080:8080 --name isicell -t isicell && docker logs --follow isicell
